<?php
?>


<script type="text/javascript">
//jQuery('#portfolio-list-<?php echo $this->itemId;?>').owlCarousel({"items": 4});
jQuery(document).ready(function(){
	jQuery('#jb-smartportfolio-<?php echo $this->itemId;?> .owl-carousel').each(function(){
		var data_plugins = jQuery(this).data('plugin-options');
		console.log(data_plugins);
		jQuery(this).owlCarousel(data_plugins);
		
	});
	jQuery('.owl-carousel-img').owlCarousel({"items": 1, "animateOut": "fadeOut", "autoplay": true, "autoplayTimeout": 3000});

	jQuery('#jb-smartportfolio-<?php echo $this->itemId;?> .lightbox').magnificPopup({ delegate: 'a',
    type: 'image',
    tLoading: 'Loading image #%curr%...',
    mainClass: 'mfp-img-mobile',
    gallery: {
      enabled: true,
      navigateByImgClick: true,
      preload: [0,1] // Will preload 0 - before current, and 1 after the current image
    },
   /* image: {
     tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
      titleSrc: function(item) {
        return item.el.attr('title') + '<small>by Marsel Van Oosten</small>';
      }
    }*/
	});

	

	jQuery('#sidebar').stickySidebar({"minWidth": 991, "containerSelector": ".container", "padding": {"top": 110} ,"sidebarTopMargin": 20,
		"footerThreshold": 100});			
});
jQuery('.masonry').isotope({"itemSelector": ".masonry-item","layoutMode": "masonry"});

</script>

