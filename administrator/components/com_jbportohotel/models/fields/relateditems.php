<?php
/**
 * @package J2Store
 * @copyright Copyright (c)2014-17 Ramesh Elamathi / J2Store.org
 * @license GNU GPL v3 or later
 */
// No direct access to this file
defined('_JEXEC') or die;
require_once (JPATH_ADMINISTRATOR.'/components/com_jbportohotel/helpers/jbportohotel.php');
class JFormFieldRelateditems extends  JFormField
{
	/**
	 * The field type.
	 *
	 * @var		string
	 */
	protected $type = 'Relateditems';

	protected function getInput(){

		//print_r($this);
		$html ='';
		$fieldId =  '	';

		$items = F0FModel::getTmpInstance('Items' ,'jbportohotelModel')->enabled(1)->getList();
		$itemarray =array();
		$value =array();
		$link = 'index.php?option=com_jbportohotel&amp;view=items&amp;task=setItems&amp;layout=items&amp;tmpl=component&amp;object='.$this->name;
		$selected_value =array();
		if(isset($this->value) && !empty($this->value)){
			$selected_value = (isset($this->value['ids']) && !empty($this->value['ids'])) ? $this->value['ids'] : array();
		}

		if(is_array($selected_value) && !empty($selected_value)){
			foreach($items as $item){
				 
				 if(in_array($item->jbportohotel_item_id ,$selected_value)){
					$itemarray[$item->jbportohotel_item_id] =$item->title;
				 }
			}
		}

		$js = "
		function jSelectItem(id, title, object) {
		var exists = jQuery('#jbportohotel-item-li-'+id).html();
			if(!exists){
				var container = jQuery('<li/>' ,{'id' :'jbportohotel-item-li-'+id,'class':'jbportohotel-item-list-menu'}).appendTo(jQuery('#jbportohotel-item-list'));
				var span = jQuery('<label/>',{'class':'label label-info'}).html(title).appendTo(container);
				var input =jQuery('<input/>',{value:id, type:'hidden', name:'jform[request][related_items][ids][]'}).appendTo(container);
				var remove = jQuery('<a/>',{'class':'btn btn-danger btn-mini' ,'onclick':'jQuery(this).closest(\"li\").remove();' }).html('<i class=\"icon icon-remove\"></i>').appendTo(container);
				}else{
					alert('". JText::_('jbportohotel_PRODUCT_ALREADY_EXISTS')."');
				}
		if(typeof(window.parent.SqueezeBox.close=='function')){
			window.parent.SqueezeBox.close();
		}
		else {
			document.getElementById('sbox-window').close();
			}
		}
		function removeProductList(id){
			jQuery('#jbportohotel-item-li-'+id).remove();
		}
		";
		$css ='#jbportohotel-item-list{
					list-style:none;
					margin:5px;
				}'
				;

		//JFactory::getDocument()->addScriptDeclaration($js);
		JFactory::getDocument()->addStyleDeclaration($css);
		$html .=JHTML::_('behavior.modal', 'a.modal');
		$html .='<div id="'.$fieldId.'">';
		$html .='<label class="control-label"></label>';
		$html .= '<span class="input-append">';
		$html .='<input type="text" id="'.$this->name.'" name=""  disabled="disabled"/>';
		$html .='<a class="modal btn btn-primary" title="'.JText::_('JBPORTOHOTEL_AN_ITEM').'"  href="'.$link.'" rel="{handler: \'iframe\', size: {x: 700, y: 450}}"><i class="icon-list"></i>  '.JText::_('JBPORTOHOTEL_SELECT_ITEM').'</a>';
		$html .=jbportohotelHelper::popup("index.php?option=com_jbportohotel&view=items&task=setProducts&layout=ritems&tmpl=component&function=jSelectItem&field=".$fieldId, JText::_( "jbportohotel_SET_PRODUCTS" ), array('width'=>800 ,'height'=>400));
		$html .'</span>';
		$html .='<ul id="jbportohotel-item-list" >';
		foreach($itemarray as $key => $value){
			$html .='<li class="jbportohotel-item-list-menu" id="jbportohotel-item-li-'.$key.'">';
			$html .='<label class="label label-info">';
			$html .=$value;
			$html .='<input type="hidden" value="'.$key.'" name="jform[request][related_items][ids][]">';
			$html .='</label>';
			$html .='<a class="btn btn-danger btn-mini" onclick="removeItemList('.$key.');">';
			$html .='<i class="icon icon-remove"></i>';
			$html .='</a>';
			$html .='</li>';
		}
		$html .='</ul>';

		$html .='</div>';
		return $html ;
	}

}
