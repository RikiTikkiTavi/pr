<?php
/**
 * @package     JB Smart Portfolio
 *
 * @copyright   Copyright (C) 2016 Joomlabuff. All rights reserved.
 * @license     GNU General Public License version 2 or later.
 */

defined('_JEXEC') or die();?>
<?php echo $this->loadTemplate('header'); ?> 
<?php $svlayout = $this->params->get('sview_layout','smallslider');  ?>
<?php if($svlayout != 'fullwidthslider'):?>
<div class="container">
<?php endif;?>
	<div id="jb-smartportfolio-<?php echo $this->itemId;?>" class="jb-smartportfolio jb-smartportfolio-view-item"> 
		
		<?php echo $this->loadTemplate('title');?>		
		<hr class="tall">
		
 		<?php echo $this->loadTemplate($svlayout);?>  
	</div>
<?php if($svlayout != 'fullwidthslider'):?>
</div>
<?php endif;?>
 
<?php echo $this->loadTemplate('footer');?>  
 
