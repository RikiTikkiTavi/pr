<?php

 echo 'sls';


?>
<div class="row">		 	
		 	<div class="col-md-4"> 
			 <?php if($this->item->item_type =='slideshow'):?>
			 <?php $images = JbsmartportfolioHelper::getItemImages($this->item);?>
				<span class="owl-carousel-img owl-carousel owl-theme nav-inside m-none" data-plugin-options='{"items": 1, "margin": 10, "animateOut": "fadeOut", "autoplay": true, "autoplayTimeout": 3000}'>
				 <?php foreach($images as $img):?>
					 <span>
					 	<img class="jb-smartportfolio-img img-responsive" src="<?php echo $img;?>" alt="<?php echo $item->title; ?>" />															
					</span>
					 <?php endforeach;?>
				 </span>
				 <?php elseif($this->item->video): ?>
					 <div class="jb-smartportfolio-embed">
						 <iframe src="<?php echo $this->video_src; ?>" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>
					 </div>	
				<?php else:?>
				<span class="img-thumbnail">
					<img class="jb-smartportfolio-img img-responsive" src="<?php echo $this->item->image; ?>" alt="<?php echo $this->item->title; ?>">
				</span>			
				<?php endif;?> 
			</div>
			
 			<div class="col-md-8">
 				<div class="portfolio-info">
					<div class="row">
						<div class="col-md-12 center">
							<ul class="item-fav-icons-list">
								<?php if($this->params->get('show_like_count',0)):?>
								<li>
									<?php echo $this->item->like_count_html;?>
									<!-- <a href="#" data-tooltip data-original-title="Like"><i class="fa fa-heart"></i></a> -->
								 </li>
								<?php endif;?>
								 <?php if($this->params->get('show_created_date',0)):?>
								 <li>
								 	<i class="fa fa-calendar"></i>
								 	<?php echo JHtml::_('date', $this->item->created_on, JText::_('DATE_FORMAT_LC3')); ?>
								</li>
								<?php endif;?>
								<?php if($this->params->get('show_tags',0)):?>
								<li>
									<i class="fa fa-tags"></i>
									<?php echo implode(', ', $this->newtags); ?>
								</li>
								<?php endif;?>
							</ul>
						</div>
						<h5 class="mt-lg mb-sm"><?php echo JText::_('JBSMARTPORTFOLO_PROJECT_DESCRIPTION');?></h5>
						
				 		<p class="mt-none mb-xlg">
				 			<?php echo $this->item->description; ?>
				 		</p>
		 				<a href="<?php echo $this->item->url;?>" class="btn btn-primary btn-icon">
		 					<i class="fa fa-external-link"></i>
		 					<?php echo JText::_('JBSMARTPORTFOLIO_LIVE_PREVIEW');?>
		 				</a>
 				
 				<span class="arrow hlb appear-animation" data-appear-animation="rotateInUpLeft" data-appear-animation-delay="800"></span>
					<?php if(!empty($this->item->jbsmartportfolio_skill_id)):?>
					<?php $skills  = JbsmartportfolioHelper::getSkills($this->item->jbsmartportfolio_skill_id);?>
					<ul class="portfolio-details">
						<li>
							<h5 class="mt-sm mb-xs"><?php echo JText::_('JBSMARTPORTFOLIO_SKILLS');?></h5>
							<ul class="list list-inline list-icons">								
								<?php foreach($skills as $skill):?>
									<li><i class="fa fa-check-circle"></i><?php echo $skill->title;?></li>
								<?php endforeach;?>														 
							</ul>
						</li>
						<li>
							<h5 class="mt-sm mb-xs"><?php echo JText::_('JBSMARTPORTFOLIO_CLIENT');?></h5>
							<p><?php echo $this->item->client;?>
							<a href="http://www.okler.net" target="_blank"><i class="fa fa-external-link"></i>
							<?php echo $this->item->client_url;?></a></p>
						</li>
					</ul>
					<?php endif;?>	
					
					
					<div class="testimonial testimonial-style-4">
						<blockquote>
							 <p><?php echo $this->item->author_quote;?></p>
						 </blockquote>
								<div class="testimonial-arrow-down"></div>
								<div class="testimonial-author">
									<div class="testimonial-author-thumbnail">
										<img alt="" class="img-responsive img-circle" src="<?php echo $this->item->author_image;?>" />
									</div>
									<p><strong><?php echo $this->item->author_name;?></strong>
									<span><?php echo $this->item->author_role;?> - <?php echo $this->item->author_company;?> </span></p>
								</div>
							</div>
							
																	
					</div>
				</div> 
			</div>
		</div>