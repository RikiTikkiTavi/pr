
<?php
/**
 * @package     JB Smart Portfolio
 *
 * @copyright   Copyright (C) 2016 Joomlabuff. All rights reserved.
 * @license     GNU General Public License version 2 or later.
 */

defined('_JEXEC') or die(); 

?> 

<?php if(!empty($this->items)):?>
<!-- Load the header section -->
<?php echo $this->loadTemplate('header');?> 

 <div class="jbsmartportfolio-list-items jbsplist-<?php echo $this->params->get('layout_type','masonry');?>-<?php echo $this->params->get('columns',2);?>"> 
 <!-- 
 	<div id="jb-smartportfolio" class="jbsmartportfolio-list-items jb-smartportfolio jb-smartportfolio-view-items layout-<?php echo str_replace('_', '-', $this->layout_type); ?>">
 -->
 <?php if( $this->params->get('show_page_heading') && $this->params->get( 'page_heading' ) ):?>
 <h2><?php echo $this->params->get( 'page_heading');?></h2>
 <?php endif;?>
 
 
 
 <?php if($this->params->get('enablesidebar',0)):?>
 	<?php if($this->params->get('sidebar','leftsidebar') =='rightsidebar'):?>
 		<?php echo $this->loadTemplate('rightsidebar');?>
 	<?php elseif($this->params->get('sidebar','rightsidebar') =='leftsidebar'):?>
 		<?php echo $this->loadTemplate('leftsidebar');?>
 	<?php endif;?>
 <?php else:?>
 
 		<?php echo $this->loadTemplate('filters');?>
	 	<hr>
		 	<?php echo $this->loadTemplate('body');?>
	 <?php endif?>
 <?php echo $this->loadTemplate('footer');?>
</div>
<?php else:?>
 <div class="container jbsmartportfolio-list-emptyitems">
 	<?php echo JText::_('COM_JBMSARTPORTFOLIO_NO_RECORDS');?>
 </div>	
<?php endif;?>
