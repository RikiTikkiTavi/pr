<?php
/**
 * @copyright	Copyright (c) 2016 Formbuilder. All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 */
// no direct access
defined('_JEXEC') or die; 

?>
<div class="formbuilder">
	 <?php if($form->show_heading):?>
		<div class="section-title text-left">
			  <<?php echo $form->heading;?>><?php echo $form->title;?></<?php echo $form->heading;?>>
		</div>
	 <?php endif;?>
	 
	 <?php if($form->show_description):?>
	  <div class="desc">
	    <p class="general-form-description">                             
	       <?php echo $form->description;?>
	  	</p>
	  </div>
	 <?php endif;?>
	 <form method="post" class="jbeasyformmaker-anyform-forms" name="formbuilderform" action="index.php" id="formbuilder-anyForm-<?php echo $module->id;?>">
              <?php echo JHtml::_( 'form.token' ); ?>
              <input type="hidden" name="option" value="com_ajax" />
              <input type="hidden" name="module" value="jbeasyformmaker" />
              <input type="hidden" name="method" value="getJBEasyFormMakerData" />
              <input type="hidden" name="format" value="raw" /> 
              <input type="hidden" name="Itemid" value="<?php echo $menu_id;?>" />
              <input type="hidden" name="module_id" value="<?php echo $module->id;?>" />              	
              <input type="hidden" name="jbeasyformmaker_cform_id" value="<?php echo $form->jbeasyformmaker_cform_id;?>" />				
              <div class="formbuilder-form-error" style="display: none;">
                <p class="text text-error">
                  <?php echo JText::_($form->notify_error,'JB_ANYFORM_NOTIFT_ERROR_MESSAGE_DEFAULT');?>
                </p>
              </div>
              <div class="formbuilder-form-success" style="display: none;"></div> 
            	 <?php echo $form->form_body ; ?>  
            </form>
</div>
