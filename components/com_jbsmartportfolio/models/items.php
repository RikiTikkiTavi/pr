<?php
/**
 * @package     JB Smart Portfolio
 *
 * @copyright   Copyright (C) 2016 Joomlabuff. All rights reserved.
 * @license     GNU General Public License version 2 or later.
 */

defined('_JEXEC') or die();


class JbsmartportfolioModelItems extends F0FModel {
	

	protected $_sppagination = null;
	protected $_sfpagetotal = null;

	public function __construct($config = array())
	{	
		parent::__construct($config);
	}


	public function buildQuery($overrideLimits = false)
	{
		
		$table = $this->getTable();
		$tableName = $table->getTableName();
		$tableKey = $table->getKeyName();
		$db = $this->getDbo();

		$query = $db->getQuery(true);

		// Call the behaviors
		$this->modelDispatcher->trigger('onBeforeBuildQuery', array(&$this, &$query));

		$alias = $this->getTableAlias();

		if ($alias)
		{
			$alias = ' AS ' . $db->qn($alias);
		}
		else
		{
			$alias = '';
		}

		$select = $this->getTableAlias() ? $db->qn($this->getTableAlias()) . '.*' : $db->qn($tableName) . '.*';

		$query->select($select)->from($db->qn($tableName) . $alias);

		//Frontend
		if(F0FPlatform::getInstance()->isFrontend()) {
			
			// Get Params
			$app = JFactory::getApplication();
			$params   = $app->getMenu()->getActive()->params; // get the active item

			//has category
			if ($params->get('category_id') != '') {
				$query->where($db->qn('category_id')." = ".$db->quote( $params->get('category_id') ));
			}

			//Enabled
			$query->where($db->qn('enabled')." = ".$db->quote('1'));
			//Access
			$query->where($db->quoteName('access')." IN (" . implode( ',', JFactory::getUser()->getAuthorisedViewLevels() ) . ")");
		}

		if (!$overrideLimits)
		{

			if(F0FPlatform::getInstance()->isFrontend()) {
				$order = 'ordering';
			} else {
				$order = $this->getState('filter_order', null, 'cmd');				
			}

			if (!in_array($order, array_keys($table->getData())))
			{
				$order = $tableKey;
			}

			$order = $db->qn($order);

			if ($alias)
			{
				$order = $db->qn($this->getTableAlias()) . '.' . $order;
			}

			if(F0FPlatform::getInstance()->isFrontend()) {
				$dir = 'ASC';
			} else {
				$dir = $this->getState('filter_order_Dir', 'ASC', 'cmd');			
			}
			
			$query->order($order . ' ' . $dir);
		}

		// Call the behaviors
		$this->modelDispatcher->trigger('onAfterBuildQuery', array(&$this, &$query));

		return $query;

	}
	
	
	/**
	 * Save user vote on article
	 *
	 * @param   integer  $pk    Joomla Article Id
	 * @param   integer  $rate  Voting rate
	 *
	 * @return  boolean          Return true on success
	 */
	public function storeVote($pk = 0, $rate = 0)
	{ 	$app = JFactory::getApplication();
		 //$pk = $app->input->getInt('id', 0);
		
		if ($rate >= 1 && $rate <= 5 && $pk > 0)
		{
			$userIP = $_SERVER['REMOTE_ADDR'];
	
			// Initialize variables.
			$db    = $this->getDbo();
			$query = $db->getQuery(true);
	
			// Create the base select statement.
			$query->select('*')
			->from($db->quoteName('#__jbsmartportfolio_rating'))
			->where($db->quoteName('item_id') . ' = ' . (int) $pk);
	
			// Set the query and load the result.
			$db->setQuery($query);
	
			// Check for a database error.
			try
			{
				$rating = $db->loadObject();
			}
			catch (RuntimeException $e)
			{
				JError::raiseWarning(500, $e->getMessage());
	
				return false;
			}
	
			// There are no ratings yet, so lets insert our rating
			if (!$rating)
			{
				$query = $db->getQuery(true);
	
				// Create the base insert statement.
				$query->insert($db->quoteName('#__jbsmartportfolio_rating'))
				->columns(array($db->quoteName('item_id'), $db->quoteName('lastip'), $db->quoteName('rating_sum'), $db->quoteName('rating_count')))
				->values((int) $pk . ', ' . $db->quote($userIP) . ',' . (int) $rate . ', 1');
	
				// Set the query and execute the insert.
				$db->setQuery($query);
	
				try
				{
					$db->execute();
				}
				catch (RuntimeException $e)
				{
					JError::raiseWarning(500, $e->getMessage());
	
					return false;
				}
			}
			else
			{
				if ($userIP != ($rating->lastip))
				{
					$query = $db->getQuery(true);
	
					// Create the base update statement.
					$query->update($db->quoteName('#__jbsmartportfolio_rating'))
					->set($db->quoteName('rating_count') . ' = rating_count + 1')
					->set($db->quoteName('rating_sum') . ' = rating_sum + ' . (int) $rate)
					->set($db->quoteName('lastip') . ' = ' . $db->quote($userIP))
					->where($db->quoteName('item_id') . ' = ' . (int) $pk);
	
					// Set the query and execute the update.
					$db->setQuery($query);
	
					try
					{
						$db->execute();
					}
					catch (RuntimeException $e)
					{
						JError::raiseWarning(500, $e->getMessage());
	
						return false;
					}
				}
				else
				{
					return false;
				}
			}
	
			return true;
		}
	
		//JError::raiseWarning('SOME_ERROR_CODE', JText::sprintf('COM_CONTENT_INVALID_RATING', $rate), "JModelArticle::storeVote($rate)");
	
		return false;
	}
	
	


 
 
}