<?php
/**
 * @package     JB Smart Portfolio
 *
 * @copyright   Copyright (C) 2016 Joomlabuff. All rights reserved.
 * @license     GNU General Public License version 2 or later.
 */

defined('_JEXEC') or die();

class JbsmartportfolioTableItem extends F0FTable
{

	public function check() {

		
		$result = true;

		//Alias
		if(empty($this->alias)) {
			// Auto-fetch a alias
			$this->alias = JFilterOutput::stringURLSafe($this->title);
		} else {
			// Make sure nobody adds crap characters to the alias
			$this->alias =JFilterOutput::stringURLSafe($this->alias);
		}

		$existingAlias = F0FModel::getTmpInstance('Items','JbsmartportfolioModel')
			->alias($this->alias)
			->getList(true);

		if(!empty($existingAlias)) {
			$count = 0;
			$k = $this->getKeyName();
			foreach($existingAlias as $item) {
				if($item->$k != $this->$k) $count++;
			}
			if($count) {
				$this->setError(JText::_('COM_JBSMARTPORTFOLIO_ALIAS_ERR_SLUGUNIQUE'));
				$result = false;
			}
		}

		
		
		//Tags
		if (is_array($this->jbsmartportfolio_tag_id))
		{
			if (!empty($this->jbsmartportfolio_tag_id))
			{
				$this->jbsmartportfolio_tag_id = json_encode($this->jbsmartportfolio_tag_id);
			}
		}
		if (is_null($this->jbsmartportfolio_tag_id) || empty($this->jbsmartportfolio_tag_id))
		{
			$this->jbsmartportfolio_tag_id = '';
		}
		
		
		
		
		//Skills
		if (is_array($this->jbsmartportfolio_skill_id))
		{
			if (!empty($this->jbsmartportfolio_skill_id))
			{
				$this->jbsmartportfolio_skill_id = json_encode($this->jbsmartportfolio_skill_id);
			}
			
			
		}
		if (is_null($this->jbsmartportfolio_skill_id) || empty($this->jbsmartportfolio_skill_id))
		{
			$this->jbsmartportfolio_skill_id = '';
		}
		
		

		//Skills
		if (is_array($this->related_items))
		{
			if (!empty($this->related_items))
			{
				$this->related_items = json_encode($this->related_items);
			}
				
				
		}
		if (is_null($this->related_items) || empty($this->related_items))
		{
			$this->related_items = '';
		}
		
		//print_r($this ); exit;

		//Generate Thumbnails
		if($result) {

			$params 	= JComponentHelper::getParams('com_jbsmartportfolio');
			$square 	= strtolower( $params->get('square', '600x600') );
			$rectangle 	= strtolower( $params->get('rectangle', '600x400') );
			$tower 		= strtolower( $params->get('tower', '600x800') );
			$cropratio 	= $params->get('cropratio', 4);

			if(!is_null($this->image)) {
				jimport( 'joomla.filesystem.file' );
				jimport( 'joomla.filesystem.folder' );
				jimport( 'joomla.image.image' );

				$image = JPATH_ROOT . '/' . $this->image;
				$path  = JPATH_ROOT . '/images/jbsmartportfolio/' . $this->alias;

				if(!file_exists($path)) {
					JFolder::create( $path, 0755 );
				}

				$sizes = array($square, $rectangle, $tower);
				$image = new JImage($image);
				$image->createThumbs($sizes, $cropratio, $path);
			}

		}
		
		
		return $result;
	}

	public function onAfterLoad(&$result) {

		if(!is_array($this->jbsmartportfolio_tag_id)) {
			if(!empty($this->jbsmartportfolio_tag_id)) {
				$this->jbsmartportfolio_tag_id = json_decode($this->jbsmartportfolio_tag_id, true);
			}
		}

		if(is_null($this->jbsmartportfolio_tag_id) || empty($this->jbsmartportfolio_tag_id)) {
			$this->jbsmartportfolio_tag_id = array();
		}
		
		if(!is_array($this->jbsmartportfolio_skill_id)) {
			if(!empty($this->jbsmartportfolio_skill_id)) {
				$this->jbsmartportfolio_skill_id = json_decode($this->jbsmartportfolio_skill_id, true);
			}
		}
		
		if(is_null($this->jbsmartportfolio_skill_id) || empty($this->jbsmartportfolio_skill_id)) {
			$this->jbsmartportfolio_skill_id = array();
		}
		
		if(!is_array($this->related_items)) {
			if(!empty($this->related_items)) {
				$this->related_items = json_decode($this->related_items, true);
			}
		}
		
		if(is_null($this->related_items) || empty($this->related_items)) {
			$this->related_items = array();
		}
		
		/* if($this->category_id !==''){
			$db = JFactory::getDbo ();
			$query = $db->getQuery ( true );
			$query->select ( 'title' )->from ( '#__categories' )->where ( 'id=' . $db->q ( $this->category_id) );
			$query->where ( 'extension ="com_jbsmartportfolio"' );
			$db->setQuery ( $query );
			$this->category_title = $db->loadResult ();
		} */
		
		return parent::onAfterLoad($result);
	}
}