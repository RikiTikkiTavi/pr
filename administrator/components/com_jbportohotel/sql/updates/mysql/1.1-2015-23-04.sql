ALTER TABLE `#__jbportohotel_items` ADD `language` VARCHAR(255) NOT NULL DEFAULT '*' AFTER `enabled`;
ALTER TABLE `#__jbportohotel_items` ADD `item_type` text NOT NULL AFTER `alias`;
ALTER TABLE `#__jbportohotel_items` ADD `alternative_img_1` text NOT NULL AFTER `image`;
ALTER TABLE `#__jbportohotel_items` ADD `alternative_img_2` text NOT NULL AFTER `image`;
ALTER TABLE `#__jbportohotel_items` ADD `alternative_img_3` text NOT NULL AFTER `image`;
ALTER TABLE `#__jbportohotel_items` ADD `alternative_img_4` text NOT NULL AFTER `image`;
ALTER TABLE `#__jbportohotel_items` ADD `beds` text NOT NULL AFTER `url`;
ALTER TABLE `#__jbportohotel_items` ADD `occupancy` int(11) NOT NULL AFTER `beds`;
ALTER TABLE `#__jbportohotel_items` ADD `size` int(11) NOT NULL AFTER `occupancy`;
ALTER TABLE `#__jbportohotel_items` ADD `hview` int(11) NOT NULL AFTER `size`;
ALTER TABLE `#__jbportohotel_items` ADD `rates` DECIMAL(9,6) NOT NULL AFTER `view`;
ALTER TABLE `#__jbportohotel_items` ADD `currency` text NOT NULL AFTER `rates`;
 
ALTER TABLE `#__jbportohotel_tags`  ADD `language` VARCHAR(255) NOT NULL DEFAULT '*' AFTER `alias`;
