<?php
?>
<?php if($this->item->client):?>
<h5 class="mt-sm mb-xs"><?php echo JText::_('JBSMARTPORTFOLIO_CLIENT');?></h5>
	
	<p>
		<?php echo $this->item->client;?>
		<?php if($this->item->client_url):?>
		<a href="<?php echo $this->item->client_url;?>" target="_blank">
			<i class="fa fa-external-link"></i>			 
		</a>
		<?php endif;?>
	</p>
	<?php endif;?>