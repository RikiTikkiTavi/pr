<?php 

$itemId = $this->itemId;
?>

<div class="row">
		<div class="sort-destination-loader sort-destination-loader-showing">
			<ul class="portfolio-list sort-destination jb-smartportfolio-items" data-sort-id="portfolio">
				<?php  foreach($this->items as $item):?>
				<?php					
				$tags = JbsmartportfolioHelper::getTags( $item->jbsmartportfolio_tag_id );
				$newtags = array();
				$filter = '';
				$groups = array();
				foreach ($tags as $tag) {
					$newtags[] 	 = $tag->title;
					$filter 	.= ' ' . $tag->alias;
					$groups[] 	.= '"' . $tag->alias . '"';
				} 
				$groups = implode(',', $groups);
			  $item->url = JRoute::_('index.php?option=com_jbsmartportfolio&view=item&id='.$item->jbsmartportfolio_item_id.':'.$item->alias . $itemId); ?>
									<li class="<?php echo $this->col;?> jb-smartportfolio-item isotope-item" data-groups='[<?php echo $groups; ?>]'>
										<div class="portfolio-item">
											<a href="<?php echo $item->url;?>">
												<span class="thumb-info thumb-info-centered-info thumb-info-no-borders">
													
													<span class="thumb-info-wrapper">
													<?php if($item->item_type =='slideshow'):?>
													<?php $images = JbsmartportfolioHelper::getItemImages($item);?>													
														<span class="owl-carousel owl-theme nav-inside m-none" data-plugin-options='{"items": 1, "margin": 10, "animateOut": "fadeOut", "autoplay": true, "autoplayTimeout": 3000}'>
													<?php foreach($images as $img):?>
														<span>
															<img class="jb-smartportfolio-img img-responsive" src="<?php echo $img;?>" alt="<?php echo $item->title; ?>" />															
														</span>
														<?php endforeach;?>											 
				 									</span>
													<?php else:?>
														<img class="jb-smartportfolio-img img-responsive" src="<?php echo JURI::root() . $item->image;?>" alt="<?php echo $item->title; ?>">
													<?php endif;?>
														
														<?php if($this->params->get('show_list_title',0) || $this->params->get('show_tags',0)):?>
														<span class="thumb-info-title">														
															<?php if($this->params->get('show_list_title',0)):?>
															<span class="thumb-info-inner"><?php echo $item->title;?></span>
															<?php endif;?>
															<?php if($this->params->get('show_list_tags',0)):?>
															 <span class="thumb-info-type"><?php echo implode(',',$newtags);?></span>
															 <?php endif;?> 
														</span>
														<?php endif;?>
														
														<?php if($this->params->get('show_item_link',0)):?>
														<span class="thumb-info-action">
															<span class="thumb-info-action-icon">
																<a href="<?php echo $item->url;?>"><i class="fa fa-link"></i></a>
															</span>
														</span>
														<?php endif;?>
													</span>
												</span>
											</a>
										</div>
									</li>
								<?php endforeach;?>							 
							</ul>
							<?php if ($this->pagination->get('pages.total') >1) { ?>
							<div class="pagination">
								<?php echo $this->pagination->getPagesLinks(); ?>
							</div>
							<?php } ?>
					</div>
				</div>
