<?php
/**
 * @version    CVS: 1.0.0
 * @package    com_jbeasyformmaker
 * @author     Priya Bose <support@joomlabuff.com>
 * @copyright  2016 www.joomlabuff.com
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
  
defined('_JEXEC') or die;
// Load FOF
// Include F0F
if(!defined('F0F_INCLUDED')) {
	require_once JPATH_LIBRARIES.'/f0f/include.php';
}
if(!defined('F0F_INCLUDED')) {
?>
   <h2>Incomplete installation detected</h2>
<?php
}

F0FDispatcher::getTmpInstance('com_jbeasyformmaker')->dispatch();
?>