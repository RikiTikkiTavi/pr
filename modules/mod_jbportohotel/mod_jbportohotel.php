<?php
/**
 * @package     JB Porto Hotel 
 * @subpackage  mod_jbportohotel
 *
 * @copyright   Copyright (C) 2010 - 2015 Joombuff. All rights reserved.
 * @license     GNU General Public License version 2 or later.
 */
defined ( '_JEXEC' ) or die ();

require_once JPATH_BASE . '/components/com_jbportohotel/helpers/helper.php';
require_once __DIR__ . '/helper.php';

// Load the method jquery script.
JHtml::_ ( 'jquery.framework' );

$doc = JFactory::getDocument ();
 

$media_path = JURI::root() . '/modules/mod_jbportohotel/media';

$app = JFactory::getApplication();
$doc->addStylesheet($media_path .'/assets/owl.carousel.min.css');
$doc->addStylesheet( $media_path .'/assets/owl.theme.default.min.css');
$doc->addScript($media_path .'/owl.carousel.min.js');


$items = ModjbportohotelHelper::getItems ( $params );
$moduleclass_sfx = htmlspecialchars ( $params->get ( 'moduleclass_sfx' ) );

$no_of_items = $params->get('no_of_items',1);
//require JModuleHelper::getLayoutPath ( 'mod_jbportohotel', $params->get ( 'layout', 'default' ) );
$slider =  $params->get('enable_slider',false);

$layout = $params->get ('slider_style', 'style1' );
$slider_navigation = $params->get('slider_navigation',0);
$slider_pagination = $params->get('slider_pagination',0);
$autoplay = $params->get('slider_autoplay',0);

$slider_navigation = $slider_navigation ? 'true' : 'false';
$slider_pagination = $slider_pagination ? 'true' : 'false';
 $autoplay = $autoplay ? 'true' : 'false'; 
 
//echo 'default_'. $layout; exit;
require (JModuleHelper::getLayoutPath ( 'mod_jbportohotel','default_'. $layout ));
