<?php
/**
 * @version    2.7.x
 * @package    K2
 * @author     JoomlaWorks http://www.joomlaworks.net
 * @copyright  Copyright (c) 2006 - 2016 JoomlaWorks Ltd. All rights reserved.
 * @license    GNU/GPL license: http://www.gnu.org/copyleft/gpl.html
 */

// no direct access
defined('_JEXEC') or die;
?>

<div id="k2ModuleBox<?php echo $module->id; ?>" class="k2ItemsBlock<?php if($params->get('moduleclass_sfx')) echo ' '.$params->get('moduleclass_sfx'); ?>">



	<?php if($params->get('itemPreText')): ?>
	<p class="modulePretext"><?php echo $params->get('itemPreText'); ?></p>
	<?php endif; ?>

	<?php if(count($items)): ?>
  <div class="container">
	 
	  
    <?php foreach ($items as $key=>$item):	?>
    <div class="col-md-6">
		
		<span class="thumb-info thumb-info-side-image thumb-info-no-zoom  thumb-info-blog-custom mt-xl">
			<span class="thumb-info-side-image-wrapper p-none hidden-xs">
				<a title="" href="<?php echo $item->link; ?>">
					<?php if($params->get('itemImage') && isset($item->image)): ?>
						<img class="img-responsive"  style="width: 235px;" src="<?php echo $item->image; ?>" alt="<?php echo K2HelperUtilities::cleanHtml($item->title); ?>"/>
					<?php endif; ?>
				</a>
				
			</span>
			
			<span class="thumb-info-caption">
				<span class="thumb-info-caption-text">
					<h2 class="mb-md mt-xs">
						<?php if($params->get('itemTitle')): ?>
							<a class="text-dark" title="" href="<?php echo $item->link; ?>"><?php echo $item->title; ?></a>
						<?php endif; ?>
					</h2>
					<span class="post-meta">
						<span>
							<?php if($params->get('itemDateCreated')): ?>
								<?php echo JHTML::_('date', $item->created, JText::_('K2_DATE_FORMAT_LC2')); ?>
							<?php endif; ?> | 
							
							<?php if($params->get('itemCategory')): ?>
								<a class="moduleItemCategory" href="<?php echo $item->categoryLink; ?>"><?php echo $item->categoryname; ?></a>
							<?php endif; ?>
							
							</span>
					</span>
					
						<?php if($params->get('itemIntroText')): ?>
							<p><?php echo $item->introtext; ?></p>
						<?php endif; ?>
					
					<?php if($params->get('itemReadMore') && $item->fulltext): ?>
						<a class="mt-md" href="<?php echo $item->link; ?>">
							<?php echo JText::_('K2_READ_MORE'); ?> <i class="fa fa-long-arrow-right"></i>
						</a>
					<?php endif; ?>
				</span>
			</span>
		</span>

      

     
      

      

      <?php if($params->get('itemTags') && count($item->tags)>0): ?>
      <div class="moduleItemTags">
      	<b><?php echo JText::_('K2_TAGS'); ?>:</b>
        <?php foreach ($item->tags as $tag): ?>
        <a href="<?php echo $tag->link; ?>"><?php echo $tag->name; ?></a>
        <?php endforeach; ?>
      </div>
      <?php endif; ?>

      <?php if($params->get('itemAttachments') && count($item->attachments)): ?>
			<div class="moduleAttachments">
				<?php foreach ($item->attachments as $attachment): ?>
				<a title="<?php echo K2HelperUtilities::cleanHtml($attachment->titleAttribute); ?>" href="<?php echo $attachment->link; ?>"><?php echo $attachment->title; ?></a>
				<?php endforeach; ?>
			</div>
      <?php endif; ?>

			

			
			
      <!-- Plugins: AfterDisplay -->
      <?php echo $item->event->AfterDisplay; ?>

      <!-- K2 Plugins: K2AfterDisplay -->
      <?php echo $item->event->K2AfterDisplay; ?>

      <div class="clr"></div>
    </div>
    <?php endforeach; ?>
    
  </div>
  <?php endif; ?>

	<?php if($params->get('itemCustomLink')): ?>
	<a class="moduleCustomLink" href="<?php echo $params->get('itemCustomLinkURL'); ?>" title="<?php echo K2HelperUtilities::cleanHtml($itemCustomLinkTitle); ?>"><?php echo $itemCustomLinkTitle; ?></a>
	<?php endif; ?>

	

</div>
