

/*===============================
/porto/components/com_sppagebuilder/assets/js/gmap.js
================================================================================*/;
function initSPPageBuilderGMap(){jQuery('.sppb-addon-gmap-canvas').each(function(index){var mapId=jQuery(this).attr('id'),zoom=Number(jQuery(this).attr('data-mapzoom')),mousescroll=(jQuery(this).attr('data-mousescroll')==='true')?true:false,maptype=jQuery(this).attr('data-maptype'),latlng={lat:Number(jQuery(this).attr('data-lat')),lng:Number(jQuery(this).attr('data-lng'))};var map=new google.maps.Map(document.getElementById(mapId),{center:latlng,zoom:zoom,scrollwheel:mousescroll});var marker=new google.maps.Marker({position:latlng,map:map});map.setMapTypeId(google.maps.MapTypeId[maptype]);});};jQuery(window).load(function(){initSPPageBuilderGMap();});


/*===============================
/porto/media/plg_captcha_recaptcha/js/recaptcha.min.js
================================================================================*/;
window.JoomlaInitReCaptcha2=function(){"use strict";var e=document.getElementsByClassName("g-recaptcha"),t,n;for(var r=0,i=e.length;r<i;r++)t=e[r],n=t.dataset?t.dataset:{sitekey:t.getAttribute("data-sitekey"),theme:t.getAttribute("data-theme"),size:t.getAttribute("data-size")},grecaptcha.render(t,n)};