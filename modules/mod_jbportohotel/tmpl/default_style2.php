<?php
/**
 * @package     JB Porto Hotel 
 * @subpackage  mod_jbportohotel
 *
 * @copyright   Copyright (C) 2010 - 2015 Joombuff. All rights reserved.
 * @license     GNU General Public License version 2 or later.
 */
defined ( '_JEXEC' ) or die ();
jimport ( 'joomla.filesystem.file' );
JHTML::_('behavior.modal');
 
$cparams = JComponentHelper::getParams ( 'com_jbportohotel' );
?>
<style>
.owl-carousel .modal{
	display:block !important;
 	position:unset !important;
}
</style>

 
<div   class="jb-smartportfolio jb-portohotel-view-items <?php echo $moduleclass_sfx; ?>">
	<div class="owl-carousel owl-theme" id="portfolio-style2-<?php echo $module->id;?>">
								
								<?php 
									foreach ( $items as $item ) :
									$item->category_title = ModjbportohotelHelper::getCategoryTitle($item->category_id);
									  $item->url = JRoute::_('index.php?option=com_jbportohotel&view=item&id='.$item->jbportohotel_item_id.':'.$item->alias. ModjbportohotelHelper::getItemid().'&tmpl=component'); ?>
									
							

								<div class="portfolio-item">
								  <a class="modal"  rel="{handler: 'iframe', size: {x: 1000, y: 600}}" href="<?php echo $item->url;?>"> 
										<span class="thumb-info thumb-info-lighten">
											<span class="thumb-info-wrapper">
												<img src="<?php echo JUri::root().'/'.$item->image;?>" class="img-responsive" alt="">
												<span class="thumb-info-title">
													<span class="thumb-info-inner"><?php echo $item->title;?></span>
													<span class="thumb-info-type"><?php echo $item->category_title;?></span>
												</span>
												<span class="thumb-info-action">
													<span class="thumb-info-action-icon"><i class="fa fa-plus-square"></i></span>
												</span>
											</span>
										</span>
									</a>								
								 
								</div>
							 
								
								<?php endforeach;?>
								 
							</div>
</div>
<script type="text/javascript">
 var $ = jQuery.noConflict();
 $("#portfolio-style2-<?php echo $module->id;?>").owlCarousel({        
        items:<?php echo $no_of_items;?>,
        mouseDrag:true,       
		startPosition:0,
		loop:true,
        nav:<?php echo $slider_navigation;?>,
        dots :<?php echo $slider_pagination;?>,
        autoplay : '<?php echo $autoplay;?>',
        margin:20,
        
      });
 
</script>