<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_content
 *
 * @copyright   Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined( '_JEXEC' ) or die;

JHtml::addIncludePath( JPATH_COMPONENT . '/helpers' );

JHtml::_( 'behavior.caption' );

jimport( 'joomla.application.module.helper' );
$dispatcher = JEventDispatcher::getInstance();

$this->category->text = $this->category->description;

$children = $this->children[$this->category->id];

function display_locks( $locks ) {
	for ( $l = 0; $l < $locks; $l ++ )
	{
		echo "<i class=\"fa fa-lock\" aria-hidden=\"true\"></i>";
	}
	for ( $o_l = 0; $o_l < 5 - $locks; $o_l ++ )
	{
		echo "<i class=\"fa fa-lock lock-light\" aria-hidden=\"true\"></i>";
	}
}

?>

<div class="row category-articles">
	<div class="container-fluid">

		<?php foreach ( $this->items as $key => &$item ) :


			$this->item = &$item;
			$this->loadTemplate( 'item' );

			$attribs = json_decode( $item->attribs );

			$article_icon  = $attribs->articleShortStory_icon;
			$article_locks = $attribs->articleShortStory_locks;
			$article_group = $attribs->articleShortStory_group;

			$images = json_decode( $item->images );
			$link   = JRoute::_( ContentHelperRoute::getCategoryRoute( $this->item->catslug ) ) . '/' . $item->id;

			?>

			<div class="row category-article-row">
				<div class="container vertical-align">
					<div class="col-md-4 col-sm-4 pr-news-card-col">
						<a class="pr-news-card-link" href="<?= $link ?>">
							<div class="pr-news-card"
							     style="background-image: url('/<?= $images->image_intro ?>');">
								<div class="pr-news-card-top">
								</div>
								<div class="pr-news-card-bottom">
									<div class="row">
										<div class="col-sm-4 col-xs-4 pr-news-card-icon">
											<i class="fa fa-2x <?= $article_icon ?>" aria-hidden="true"></i>
										</div>
										<div class="col-md-8 col-xs-8 pr-news-card-description">
											<div class="row pr-news-card-heading"><?= $item->title; ?></div>
											<div class="row pr-news-card-params">
												<div class="pr-news-card-params-lock">
													<?php display_locks( $article_locks ) ?>
												</div>
												<div class="pr-news-card-params-group">
													<i class="fa fa-users"
													   aria-hidden="true"></i> <?= $article_group; ?>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</a>
					</div>
					<div class="col-md-8 col-sm-8 col-xs-8">
						<div class="pr-news-description ">
							<h4><?= $item->title; ?></h4>
							<p>
								<?= $item->introtext; ?>
							</p>
						</div>
					</div>
				</div>
			</div>


		<?php endforeach; ?>

	</div><!-- /post-wrap -->
</div>