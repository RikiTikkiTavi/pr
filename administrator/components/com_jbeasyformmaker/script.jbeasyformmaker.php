<?php
/**
 * @version    CVS: 1.0.0
 * @package    com_jbeasyformmaker
 * @author     Priya Bose <support@joomlabuff.com>
 * @copyright  2016 www.joomlabuff.com
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
 
/** ensure this file is being included by a parent file */
defined ( '_JEXEC' ) or die ( 'Restricted access' );
jimport ( 'joomla.filesystem.file' );

// Load FOF if not already loaded
if (! defined ( 'F0F_INCLUDED' )) {
	$paths = array (
			(defined ( 'JPATH_LIBRARIES' ) ? JPATH_LIBRARIES : JPATH_ROOT . '/libraries') . '/f0f/include.php',
			__DIR__ . '/fof/include.php' 
	);
	
	foreach ( $paths as $filePath ) {
		if (! defined ( 'F0F_INCLUDED' ) && file_exists ( $filePath )) {
			@include_once $filePath;
		}
	}
}

// Pre-load the installer script class from our own copy of FOF
if (! class_exists ( 'F0FUtilsInstallscript', false )) {
	@include_once __DIR__ . '/fof/utils/installscript/installscript.php';
}

// Pre-load the database schema installer class from our own copy of FOF
if (! class_exists ( 'F0FDatabaseInstaller', false )) {
	@include_once __DIR__ . '/fof/database/installer.php';
}

// Pre-load the update utility class from our own copy of FOF
if (! class_exists ( 'F0FUtilsUpdate', false )) {
	@include_once __DIR__ . '/fof/utils/update/update.php';
}

// Pre-load the cache cleaner utility class from our own copy of FOF
if (! class_exists ( 'F0FUtilsCacheCleaner', false )) {
	
	@include_once __DIR__ . '/fof/utils/cache/cleaner.php';
}
class Com_JbeasyformmakerInstallerScript extends FOFUtilsInstallscript {
	
	/**
	 * The component's name
	 *
	 * @var string
	 */
	protected $componentName = 'com_jbeasyformmaker';
	
	/**
	 * The title of the component (printed on installation and uninstallation messages)
	 *
	 * @var string
	 */
	protected $componentTitle = 'JB Easy Form Maker';
	protected $minimumJoomlaVersion = '3.3.0';
	protected $installation_queue = array (
			'modules' => array (
					'site' => array (
							'mod_jbeasyformmaker' => array (
									'left',
									0 
							) 
					) 
			) 
	);
	
	/**
	 * Method to run after an install/update/uninstall method
	 *
	 * @param object $type
	 *        	type of change (install, update or discover_install)
	 * @param object $parent
	 *        	class calling this method
	 *        	
	 * @return void
	 */
	function postflight($type, $parent) {
		
		$db = JFactory::getDBO();
		$app = JFactory::getApplication('site');
		$status = new stdClass;
		$status->plugins = array();
		$src = $parent->getParent()->getPath('source');
		$manifest = $parent->getParent()->manifest;
		$modules = $manifest->xpath('modules/module');
		foreach ($modules as $module)
		{
			$name = (string)$module->attributes()->module;
			$client = (string)$module->attributes()->client;
			if (is_null($client))
			{
				$client = 'site';
			}
			($client == 'administrator') ? $path = $src.'/administrator/modules/'.$name : $path = $src.'/modules/site/'.$name;
			$installer = new JInstaller;
			$result = $installer->install($path);
			$status->modules[] = array('name' => $name, 'client' => $client, 'result' => $result);
		} 
		
		$fofInstallationStatus = $this->_installFOF ( $parent );
	}
	
	/**
	 * Check if FOF is already installed and install if not
	 *
	 * @param object $parent
	 *        	class calling this method
	 *        	
	 * @return array Array with performed actions summary
	 */
	private function _installFOF($parent) {
		$src = $parent->getParent ()->getPath ( 'source' );
		
		// Load dependencies
		JLoader::import ( 'joomla.filesystem.file' );
		JLoader::import ( 'joomla.utilities.date' );
		$source = $src . '/fof';
		
		if (! defined ( 'JPATH_LIBRARIES' )) {
			$target = JPATH_ROOT . '/libraries/f0f';
		} else {
			$target = JPATH_LIBRARIES . '/f0f';
		}
		$haveToInstallFOF = false;
		
		if (! is_dir ( $target )) {
			$haveToInstallFOF = true;
		} else {
			$fofVersion = array ();
			
			if (file_exists ( $target . '/version.txt' )) {
				$rawData = JFile::read ( $target . '/version.txt' );
				$info = explode ( "\n", $rawData );
				$fofVersion ['installed'] = array (
						'version' => trim ( $info [0] ),
						'date' => new JDate ( trim ( $info [1] ) ) 
				);
			} else {
				$fofVersion ['installed'] = array (
						'version' => '0.0',
						'date' => new JDate ( '2011-01-01' ) 
				);
			}
			
			$rawData = JFile::read ( $source . '/version.txt' );
			$info = explode ( "\n", $rawData );
			$fofVersion ['package'] = array (
					'version' => trim ( $info [0] ),
					'date' => new JDate ( trim ( $info [1] ) ) 
			);
			
			$haveToInstallFOF = $fofVersion ['package'] ['date']->toUNIX () > $fofVersion ['installed'] ['date']->toUNIX ();
		}
		
		$installedFOF = false;
		
		if ($haveToInstallFOF) {
			$versionSource = 'package';
			$installer = new JInstaller ();
			$installedFOF = $installer->install ( $source );
		} else {
			$versionSource = 'installed';
		}
		
		if (! isset ( $fofVersion )) {
			$fofVersion = array ();
			
			if (file_exists ( $target . '/version.txt' )) {
				$rawData = JFile::read ( $target . '/version.txt' );
				$info = explode ( "\n", $rawData );
				$fofVersion ['installed'] = array (
						'version' => trim ( $info [0] ),
						'date' => new JDate ( trim ( $info [1] ) ) 
				);
			} else {
				$fofVersion ['installed'] = array (
						'version' => '0.0',
						'date' => new JDate ( '2011-01-01' ) 
				);
			}
			
			$rawData = JFile::read ( $source . '/version.txt' );
			$info = explode ( "\n", $rawData );
			$fofVersion ['package'] = array (
					'version' => trim ( $info [0] ),
					'date' => new JDate ( trim ( $info [1] ) ) 
			);
			$versionSource = 'installed';
		}
		
		if (! ($fofVersion [$versionSource] ['date'] instanceof JDate)) {
			$fofVersion [$versionSource] ['date'] = new JDate ();
		}
		
		return array (
				'required' => $haveToInstallFOF,
				'installed' => $installedFOF,
				'version' => $fofVersion [$versionSource] ['version'],
				'date' => $fofVersion [$versionSource] ['date']->format ( 'Y-m-d' ) 
		);
	}
}
