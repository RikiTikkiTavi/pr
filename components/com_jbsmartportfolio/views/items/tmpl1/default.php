
<?php
/**
 * @package     JB Smart Portfolio
 *
 * @copyright   Copyright (C) 2016 Joomlabuff. All rights reserved.
 * @license     GNU General Public License version 2 or later.
 */

defined('_JEXEC') or die(); ?> 

<?php if(!empty($this->items)):?>
<!-- Load the header section -->
<?php echo $this->loadTemplate('header');?> 

 <div class="container jbsmartportfolio-list-items">	
 <?php if( $this->params->get('show_page_heading') && $this->params->get( 'page_heading' ) ):?>
 <h2><?php echo $this->params->get( 'page_heading');?></h2>
 <?php endif;?>
 <?php echo $this->loadTemplate('filters');?>
 	<hr>
 <?php echo $this->loadTemplate('body');?>
 <?php echo $this->loadTemplate('footer');?>
</div>
<?php else:?>
 <div class="container jbsmartportfolio-list-emptyitems">
 	<?php echo JText::_('COM_JBMSARTPORTFOLIO_NO_RECORDS');?>
 </div>	
<?php endif;?>
