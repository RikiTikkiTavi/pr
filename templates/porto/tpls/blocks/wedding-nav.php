<?php
/**
 * @package   T3 Blank
 * @copyright Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license   GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

// get params
$sitename  = $this->params->get('sitename');
$slogan    = $this->params->get('slogan', '');
$logotype  = $this->params->get('logotype', 'text');
$logoimage = $logotype == 'image' ? $this->params->get('logoimage', T3Path::getUrl('images/logo-dark.png', '', true)) : '';
$logoimgsm = ($logotype == 'image' && $this->params->get('enable_logoimage_sm', 0)) ? $this->params->get('logoimage_sm', T3Path::getUrl('images/logo-sm.png', '', true)) : false;
$logowidth = $this->params->get('logowidth');
$darkmenu  = $this->params->get('darkmenu');
$transpmenu  = $this->params->get('transpmenu');
$offcanvasstyle = $this->params->get('offcanvasstyle');
$containermenu = $this->params->get('containermenu');

if (!$sitename) {
	$sitename = JFactory::getConfig()->get('sitename');
}

$contpos = '';
if ($this->countModules('head-search')) {
	$contpos = ' style="position: relative;"';
}

?>

<div class="header-container header-nav header-nav-center" data-spy="affix" data-offset-top="110">
	<div class="header-nav-main custom-header-nav-main header-nav-main-effect-1 header-nav-main-sub-effect-1 background-color-light  mt-none">
	<!-- MAIN NAVIGATION -->
		<nav id="t3-mainnav" class="wrap navbar t3-mainnav">
				
			<?php if ( $offcanvasstyle != 'style-1' ) : ?>
			<div class="<?php echo $containermenu != 1 ? 'full-wrapper clearfix container' : 'container' ?>"<?php echo $contpos ?>>
			
				
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header">
				
					<?php if ($this->getParam('navigation_collapse_enable', 1) && $this->getParam('responsive', 1)) : ?>
						<?php $this->addScript(T3_URL.'/js/nav-collapse.js'); ?>
						<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".t3-navbar-collapse">
							<i class="fa fa-bars"></i>
						</button>
					<?php endif ?>

					<?php if ($this->getParam('addon_offcanvas_enable')) : ?>
						<?php $this->loadBlock ('off-canvas') ?>
					<?php endif ?>

				</div>

				<?php if ($this->getParam('navigation_collapse_enable')) : ?>
				<div class="t3-navbar-collapse navbar-collapse collapse onSmall"></div>
				<?php endif ?>
			
				<div class="t3-navbar navbar-collapse collapse">
					<jdoc:include type="<?php echo $this->getParam('navigation_type', 'megamenu') ?>" name="<?php echo $this->getParam('mm_type', 'mainmenu') ?>" />
				</div>

			</div>
			<?php else : ?>
			
			<!-- LOGO -->
			<h1 class="logo-<?php echo $logotype, ($logoimgsm ? ' logo-control' : '') ?>">
				<a href="<?php echo JURI::base(true) ?>" title="<?php echo strip_tags($sitename) ?>">
					<?php if($logotype == 'image'): ?>
						<img class="logo-img" src="<?php echo JURI::base(true) . '/' . $logoimage ?>" alt="<?php echo strip_tags($sitename) ?>" <?php echo $logowidth ? 'style="width: '.$logowidth.'px"' : '' ?> />
					<?php endif ?>
					<?php if($logoimgsm) : ?>
						<img class="logo-img-sm" src="<?php echo JURI::base(true) . '/' . $logoimgsm ?>" alt="<?php echo strip_tags($sitename) ?>" />
					<?php endif ?>
					<span><?php echo $sitename ?></span>
				</a>
				<small class="site-slogan"><?php echo $slogan ?></small>
			</h1>
			<!-- // LOGO -->
			<a href="#" class="fm-button"><span></span>Menu</a>
			<div class="fm-wrapper" id="fullscreen-menu">
				<div class="fm-wrapper-sub">
					<div class="fm-wrapper-sub-sub <?php $this->_c('menu-full') ?>">
						<jdoc:include type="modules" name="<?php $this->_p('menu-full') ?>" style="raw" />
					</div>
				</div>
				
			</div>
			<?php endif ?>
		</nav>
	</div>
</div>
<!-- //MAIN NAVIGATION -->
