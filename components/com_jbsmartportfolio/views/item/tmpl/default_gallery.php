<div class="row">	
 	<?php $images = JbsmartportfolioHelper::getItemImages($this->item);
 		//	print_r($images); exit;
 	?>	
 	<?php if(!empty($images)):?>			
	 
	<ul class="portfolio-list lightbox m-none" data-plugin-options='{"delegate": "a.lightbox-portfolio", "type": "image", "gallery": {"enabled": true}}'>
	 	<?php foreach($images as $pimg):?>
		 <li class="col-md-3 col-sm-6 col-xs-12">
			<div class="portfolio-item">
				<span class="thumb-info thumb-info-lighten thumb-info-centered-icons">
					<span class="thumb-info-wrapper"> 
						<img src="<?php echo $pimg;?>" class="img-responsive" alt=""> <span class="thumb-info-action">
							<a href="<?php echo $pimg;?>" class="lightbox-portfolio">
							<span class="thumb-info-action-icon thumb-info-action-icon-light">
							<i 	class="fa fa-search-plus"></i></span>
						</a>
					</span>
				</span>
				</span>
			</div>

		</li>
		 <?php endforeach;?>
 	</ul>
	 <?php endif;?>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="portfolio-info mt-xl pull-left">
			<div class="row">
				<div class="col-md-12 center">
					<?php echo $this->loadTemplate('niconlist');?>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-md-7">
		<h5 class="mt-lg mb-sm"><?php echo JText::_('JBSMARTPORTFOLO_PROJECT_DESCRIPTION');?></h5>

		<p class="mt-none mb-xlg"><?php echo $this->item->description; ?></p>
		<?php if($this->item->url):?>
		<a href="<?php echo $this->item->url;?>"
			class="btn btn-primary btn-icon"> <i class="fa fa-external-link"></i>
		 	<?php echo JText::_('JBSMARTPORTFOLIO_LIVE_PREVIEW');?>
		 </a>
		 <?php endif;?>
		 <span class="arrow hlb appear-animation" data-appear-animation="rotateInUpLeft"data-appear-animation-delay="800"></span>
		 
		 <ul class="portfolio-details">
			<?php if($this->params->get('show_skills',0)):?>
			<li>
				<?php echo $this->loadTemplate('skills');?>
			</li>
			<?php endif;?>	
			
		</ul>
	</div>
	<div class="col-md-5">	
		
		<?php if($this->params->get('show_clientinfo',0) && $this->item->client):?>
		<ul class="portfolio-details mt-none mb-xl">
			<li>
				<?php echo $this->loadTemplate('client');?>				
			</li>
		</ul>
			<?php endif;?>	
 		
 		<?php if($this->params->get('show_testimonial',0) && $this->item->author_name):?>
		<div class="testimonial testimonial-style-4">
			<?php if($this->item->author_quote):?>
			<blockquote>
				<p><?php echo $this->item->author_quote;?></p>
			</blockquote>
			<?php endif;?>
			<div class="testimonial-arrow-down"></div>
			<div class="testimonial-author">
				<?php if($this->item->author_image):?>
				<div class="testimonial-author-thumbnail">
					<img alt="" class="img-responsive img-circle" src="<?php echo $this->item->author_image;?>" />
				</div>
				<?php endif;?>				
				<p>
					<?php if($this->item->author_name):?>
					<strong><?php echo $this->item->author_name;?></strong> 
					<?php endif;?>
					<span><?php echo ($this->item->author_role) ? $this->item->author_role :'' ;?> - <?php echo ($this->item->author_company) ? $this->item->author_company : "";?> </span>
				</p>
			</div>
		</div>
		<?php endif;?>
		
		<?php if($this->params->get('show_social_share',0)):?>
			<h5 class="m-t-md"><?php echo JText::_('JBSMARTPORTFOLIO_ITEM_SHARE')?></h5>
			<?php echo $this->item->aftercontentDisplay; ?>		
		<?php endif; ?>
	</div>
</div>
<?php echo $this->loadTemplate('relateditems'); ?>