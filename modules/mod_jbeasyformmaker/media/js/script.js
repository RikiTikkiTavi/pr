var $jbFormbuilder = jQuery.noConflict();
$jbFormbuilder(document).ready(function() {
	$jbFormbuilder('.jbeasyformmaker-anyform-forms').each(function() {
		$jbFormbuilder(this).submit(function(e) {			 
			e.preventDefault();
			$jbFormbuilder('.formbuilder-form-error').remove();
			$jbFormbuilder('.jbanyform-error').hide();
			$jbFormbuilder('.formbuilder-form-success').hide();
			$jbFormbuilder('.jbeasyformmaker-notify-success-msg').remove();
			$jbFormbuilder('.jbeasyformmaker-notify-error-msg').remove();			
			var form = $jbFormbuilder(this); 
			var formData = form.serializeArray();
			$jbFormbuilder
						.ajax({
							url : 'index.php',
							data : formData,
							type : 'POST',
							dataType : 'json',			 
							success : function(json) {  
								if (json.status == 0) {
									form.find('.formbuilder-form-error').show();
									var vhtml = '';
									$jbFormbuilder(json.warnings).each(function(key, msg) {
														vhtml += '<p class=\'text text-error jbeasyformmaker-notify-error-msg\'>';
														vhtml += msg;
														vhtml += '</p>';
													});
									form.find('.formbuilder-form-error').append(vhtml);
									var t5 = setTimeout(function() {
										$jbFormbuilder(".formbuilder-form-error").fadeOut('slow');
									}, 6000);
								} else {									 
									jQuery('.formbuilder-form-success').show();
									var vhtml = '';									 
									$jbFormbuilder(json.warnings).each(
													function(key, msg) {
														vhtml += '<p class=\'text text-success jbeasyformmaker-notify-success-msg\'>';
														vhtml += msg;
														vhtml += '</p>';
													});
								 
									jQuery('.formbuilder-form-success').append(vhtml);
									$jbFormbuilder(form).find("input[type=text], textarea,input[type=number],select").val("");
									var t5 = setTimeout(function() {
										$jbFormbuilder(".formbuilder-form-success").fadeOut('slow');
									}, 6000);
								}

							}
						}); 
		});
	});
});

 
