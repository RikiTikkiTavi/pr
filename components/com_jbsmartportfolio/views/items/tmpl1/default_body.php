 <?php 
 
	$nomargin =  ($this->params->get('nomargin',0)) ? 'm-none p-none' : '';	

 	$div_layout_class="portfolio-item";
 	if($nomargin){
 		$div_layout_class ='portfolio-item m-none'; 		
 	}
 	
 	$span_layout_class = 'thumb-info thumb-info-centered-info'; 	
 	if($this->params->get('layout_type','mansory') == 'mansory'){
 		$span_layout_class = 'thumb-info thumb-info-centered-info thumb-info-no-borders';
 		if($this->params->get('nomargin',0) ==1){
 			$span_layout_class = 'thumb-info thumb-info-centered-info thumb-info-no-borders m-none';
 			
 		}
 	}elseif($this->params->get('layout_type','mansory') == 'grid'){ 
 		//thumb-info thumb-info-lighten thumb-info-no-zoom
 		$span_layout_class ='thumb-info thumb-info-lighten thumb-info-no-zoom';
 		
 		if($this->params->get('nomargin',0) ==1){																																				
 			//thumb-info thumb-info-centered-info thumb-info-no-borders m-none
 			$span_layout_class = 'thumb-info thumb-info-centered-info thumb-info-no-borders m-none';
 		}
 	} 																																			
 		
 ?>
<div class="sort-destination-loader1 sort-destination-loader-showing">
		<ul class="portfolio-list sort-destination jb-smartportfolio-items jb-smartportfolio-items-<?php echo $this->params->get('layout_type','masonry');?>" data-sort-id="portfolio">
				<?php  foreach($this->items as $item):?>
				<?php
					$item->category_title = JbsmartportfolioHelper::getCategoryTitle($item->category_id); 
					$tags = JbsmartportfolioHelper::getTags ( $item->jbsmartportfolio_tag_id );
					$newtags = array ();
					$filter = '';
					$groups = array ();
					foreach ( $tags as $tag ) {
						$newtags [] = $tag->title;
						$filter .= ' ' . $tag->alias;
						$groups [] .= '"' . $tag->alias . '"';
					}
					$groups = implode ( ',', $groups );
					$item->url = JRoute::_ ( 'index.php?option=com_jbsmartportfolio&view=item&id=' . $item->jbsmartportfolio_item_id . ':' . $item->alias . $this->itemId );
					?>
				<li class="<?php echo $this->col;?> jb-smartportfolio-item isotope-item  <?php echo $nomargin;?> " data-groups='[<?php echo $groups; ?>]'>
				<div class="<?php echo $div_layout_class;?>">
					<a href="<?php echo $item->url;?>">
						<?php if($item->item_type !== 'slideshow'):?>
					<span class="<?php $span_layout_class;?>">
					 <span class="thumb-info-wrapper"> 					 	
					 	<img class="jb-smartportfolio-img img-responsive" src="<?php echo JURI::root() . $item->image;?>" alt="<?php echo $item->title; ?>" />
								<span class="thumb-info-title"> 
									<?php if($this->params->get('show_list_title',0)):?>
									<span class="thumb-info-inner"><?php echo $item->title;?></span>
									<?php endif;?>									
									<?php if($this->params->get('show_list_cattitle',0)):?>
									<span class="thumb-info-type"><?php echo $item->category_title;?></span>
									<?php endif;?>
									
									<?php if($this->params->get('show_list_tags',0)):?>
									  <span class="thumb-info-type"><?php echo implode(',',$newtags);?></span>
									<?php endif;?> 
																	
								</span> 
								<?php if($this->params->get('show_hovericon',0)):?>
								<span class="thumb-info-action">
									<span class="thumb-info-action-icon"><i class="fa fa-link"></i></span>
								</span>
								<?php endif;?>
						</span>
					</span>
					<?php elseif($item->item_type =='slideshow'):?>
					<?php $images = JbsmartportfolioHelper::getItemImages($item);?>	
						<span class="<?php $span_layout_class;?>">
							<span class="thumb-info-wrapper">
								<span class="owl-carousel owl-theme nav-inside m-none" data-plugin-options='{"items": 1, "margin": 10, "animateOut": "fadeOut", "autoplay": true, "autoplayTimeout": 3000}'>
									<?php foreach($images as $img):?>
										<span>
											<img class="jb-smartportfolio-img img-responsive" src="<?php echo $img;?>" alt="<?php echo $item->title; ?>" />															
										</span>
									<?php endforeach;?>
								</span>
								<span class="thumb-info-title">
									<span class="thumb-info-inner"><?php echo $item->title;?></span> 
									<span class="thumb-info-type"><?php echo $item->category_title;?></span>							
								</span>
								
								<?php if($this->params->get('show_hovericon',0)):?>
								<span class="thumb-info-action">
									<span class="thumb-info-action-icon"><i class="fa fa-link"></i></span>
								</span>
								<?php endif;?>
							</span>
						</span>					
					<?php endif;?>
				</a>
				</div>
			</li>
		<?php endforeach;?>							 
		</ul>
</div> 
<?php if ($this->pagination->get('pages.total') >1) { ?>
<div class="pagination">
	<?php echo $this->pagination->getPagesLinks(); ?>
</div>
<?php } ?>
