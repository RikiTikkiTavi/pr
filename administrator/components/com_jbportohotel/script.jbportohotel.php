<?php
/**
 * @package     JB Porto Hotel 
 *
 * @copyright   Copyright (C) 2016 Joomlabuff. All rights reserved.
 * @license     GNU General Public License version 2 or later.
 */

defined('_JEXEC') or die();

jimport ( 'joomla.filesystem.file' );

// Load F0F if not already loaded
if (! defined ( 'F0F_INCLUDED' )) {
	$paths = array (
			(defined ( 'JPATH_LIBRARIES' ) ? JPATH_LIBRARIES : JPATH_ROOT . '/libraries') . '/f0f/include.php',
			__DIR__ . '/fof/include.php'
	);

	foreach ( $paths as $filePath ) {
		if (! defined ( 'F0F_INCLUDED' ) && file_exists ( $filePath )) {
			@include_once $filePath;
		}
	}
}

// Pre-load the installer script class from our own copy of F0F
if (! class_exists ( 'F0FUtilsInstallscript', false )) {
	@include_once __DIR__ . '/fof/utils/installscript/installscript.php';
}

// Pre-load the database schema installer class from our own copy of F0F
if (! class_exists ( 'F0FDatabaseInstaller', false )) {
	@include_once __DIR__ . '/fof/database/installer.php';
}

// Pre-load the update utility class from our own copy of F0F
if (! class_exists ( 'F0FUtilsUpdate', false )) {
	@include_once __DIR__ . '/fof/utils/update/update.php';
}

// Pre-load the cache cleaner utility class from our own copy of F0F
if (! class_exists ( 'F0FUtilsCacheCleaner', false )) {

	@include_once __DIR__ . '/fof/utils/cache/cleaner.php';
}
class Com_JbportohotelInstallerScript extends F0FUtilsInstallscript {
	
	
	
	 function postflight($type, $parent) {
		$db = JFactory::getDBO ();
		$app = JFactory::getApplication ( 'site' );
		$status = new stdClass ();
		$status->plugins = array ();
		$src = $parent->getParent ()->getPath ( 'source' );
		
		$manifest = $parent->getParent ()->manifest;
		$modules = $manifest->xpath ( 'modules/module' );
		// print_r($modules); exit;
		$plugins = $manifest->xpath ( 'plugins/plugin' );
		foreach ( $modules as $module ) {
			$name = ( string ) $module->attributes ()->module;
			$client = ( string ) $module->attributes ()->client;
			if (is_null ( $client )) {
				$client = 'site';
			}
			$path = $src . '/modules/' . $name;
			
			$installer = new JInstaller ();
			$result = $installer->install ( $path );
			
			$status->modules [] = array (
					'name' => $name,
					'client' => $client,
					'result' => $result 
			);
		}
		
		foreach ( $plugins as $plugin ) {
			$name = ( string ) $plugin->attributes ()->plugin;
			$group = ( string ) $plugin->attributes ()->group;
			$installer = new JInstaller ();
			$path = $src . '/plugins/' . $group . '/' . $name;
			$result = $installer->install ( $path );
			$status->plugins [] = array (
					'name' => $name,
					'group' => $group,
					'result' => $result 
			);
		}
		
		$fofInstallationStatus = $this->_installF0F ( $parent );
	}  
	
	/**
	 * Check if F0F is already installed and install if not
	 *
	 * @param object $parent
	 *        	class calling this method
	 *
	 * @return array Array with performed actions summary
	 */
	private function _installF0F($parent) {
		$src = $parent->getParent ()->getPath ( 'source' );
	
		// Load dependencies
		JLoader::import ( 'joomla.filesystem.file' );
		JLoader::import ( 'joomla.utilities.date' );
		$source = $src . '/fof';
	
		if (! defined ( 'JPATH_LIBRARIES' )) {
			$target = JPATH_ROOT . '/libraries/f0f';
		} else {
			$target = JPATH_LIBRARIES . '/f0f';
		}
		$haveToInstallF0F = false;
	
		if (! is_dir ( $target )) {
			$haveToInstallF0F = true;
		} else {
			$fofVersion = array ();
				
			if (file_exists ( $target . '/version.txt' )) {
				$rawData = JFile::read ( $target . '/version.txt' );
				$info = explode ( "\n", $rawData );
				$fofVersion ['installed'] = array (
						'version' => trim ( $info [0] ),
						'date' => new JDate ( trim ( $info [1] ) )
				);
			} else {
				$fofVersion ['installed'] = array (
						'version' => '0.0',
						'date' => new JDate ( '2011-01-01' )
				);
			}
				
			$rawData = JFile::read ( $source . '/version.txt' );
			$info = explode ( "\n", $rawData );
			$fofVersion ['package'] = array (
					'version' => trim ( $info [0] ),
					'date' => new JDate ( trim ( $info [1] ) )
			);
				
			$haveToInstallF0F = $fofVersion ['package'] ['date']->toUNIX () > $fofVersion ['installed'] ['date']->toUNIX ();
		}
	
		$installedF0F = false;
	
		if ($haveToInstallF0F) {
			$versionSource = 'package';
			$installer = new JInstaller ();
			$installedF0F = $installer->install ( $source );
		} else {
			$versionSource = 'installed';
		}
	
		if (! isset ( $fofVersion )) {
			$fofVersion = array ();
				
			if (file_exists ( $target . '/version.txt' )) {
				$rawData = JFile::read ( $target . '/version.txt' );
				$info = explode ( "\n", $rawData );
				$fofVersion ['installed'] = array (
						'version' => trim ( $info [0] ),
						'date' => new JDate ( trim ( $info [1] ) )
				);
			} else {
				$fofVersion ['installed'] = array (
						'version' => '0.0',
						'date' => new JDate ( '2011-01-01' )
				);
			}
				
			$rawData = JFile::read ( $source . '/version.txt' );
			$info = explode ( "\n", $rawData );
			$fofVersion ['package'] = array (
					'version' => trim ( $info [0] ),
					'date' => new JDate ( trim ( $info [1] ) )
			);
			$versionSource = 'installed';
		}
	
		if (! ($fofVersion [$versionSource] ['date'] instanceof JDate)) {
			$fofVersion [$versionSource] ['date'] = new JDate ();
		}
	
		return array (
				'required' => $haveToInstallF0F,
				'installed' => $installedF0F,
				'version' => $fofVersion [$versionSource] ['version'],
				'date' => $fofVersion [$versionSource] ['date']->format ( 'Y-m-d' )
		);
	}
public function preflight($type, $parent) {

	if(parent::preflight($type, $parent)) {

		$app = JFactory::getApplication();
		$configuration = JFactory::getConfig();
		$db = JFactory::getDbo();
		
		//remove duplicates from the product quantities table
		$query = 'ALTER TABLE #__jbportohotel_items MODIFY rates integer(11);';
		$this->_sqlexecute($query);
	}
}

private function _sqlexecute($query) {
	$db = JFactory::getDbo();
	$db->setQuery($query);
	try {
		$db->execute();
	}catch(Exception $e) {
		//do nothing as customer can do this very well by going to the tools menu
	}
}
	
}
