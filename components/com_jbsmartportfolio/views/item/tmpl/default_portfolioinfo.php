<?php
?>
<div class="portfolio-info">
	<div class="row">
		<div class="col-md-12 center">
			<?php echo $this->loadTemplate('niconlist');?>
		</div>		
		<h5 class="mt-lg mb-sm"><?php echo JText::_('JBSMARTPORTFOLO_PROJECT_DESCRIPTION');?></h5>
		
		<p class="mt-none mb-xlg"><?php echo $this->item->description; ?></p>
		
		<?php if($this->item->url):?>
		<a href="<?php echo $this->item->url;?>" target="_blank" class="btn btn-primary btn-icon">
			<i class="fa fa-external-link"></i>
		 	<?php echo JText::_('JBSMARTPORTFOLIO_LIVE_PREVIEW');?>
		 </a>
		 
		 <span class="arrow hlb appear-animation" data-appear-animation="rotateInUpLeft" data-appear-animation-delay="800"></span>
		<?php endif;?>
		 
		 
		  <ul class="portfolio-details ">
			<?php if($this->params->get('show_skills',0)):?>
			<li>
				<?php echo $this->loadTemplate('skills');?>
			</li>
			<?php endif;?>	
			<?php if($this->params->get('show_clientinfo',0)):?>
			<li>
				<?php echo $this->loadTemplate('client');?>				
			</li>
			<?php endif;?>
		</ul> 
		
		<?php if($this->params->get('show_testimonial',0)):?>
		
		<?php if($this->item->author_quote !=='' &&  $this->item->author_image !==''  && $this->item->author_name !=='' && $this->item->author_role !==''):?>
		<div class="testimonial testimonial-style-4">
			<?php if($this->item->author_quote !==''):?>
			<blockquote>
				<p><?php echo $this->item->author_quote;?></p>
			</blockquote>
			<?php endif;?>
			<div class="testimonial-arrow-down"></div>
			<div class="testimonial-author">
				<?php if($this->item->author_image !==''):?>
				<div class="testimonial-author-thumbnail">
					<img alt="" class="img-responsive img-circle" src="<?php echo $this->item->author_image;?>" />
				</div>
				<?php endif;?>
				
				<p>
					<?php if($this->item->author_name !==''):?>
					<strong><?php echo $this->item->author_name;?></strong> 
					<?php endif;?>
					<span><?php echo ($this->item->author_role) ? $this->item->author_role :'' ;?> - <?php echo ($this->item->author_company) ? $this->item->author_company : "";?> </span>
				</p>
			</div>
		</div>
		<?php endif;?>
		<?php endif;?>
		
		<?php if($this->params->get('show_social_share',0)):?>
			<h5 class="m-t-md"><?php echo JText::_('JBSMARTPORTFOLIO_ITEM_SHARE')?></h5>
			<?php echo $this->item->aftercontentDisplay; ?>		
		<?php endif; ?>

	</div>
</div>
