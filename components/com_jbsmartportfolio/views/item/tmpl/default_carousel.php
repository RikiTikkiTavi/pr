 
<div class="row">
	<div class="col-md-12"> 
			 <?php if($this->item->item_type =='slideshow'):?>
			 <?php $images = JbsmartportfolioHelper::getItemImages($this->item);?>
				<span class="owl-carousel-img owl-carousel owl-theme nav-inside m-none" data-plugin-options='{"items": 1, "margin": 10, "animateOut": "fadeOut", "autoplay": true, "autoplayTimeout": 3000}'>
				 <?php foreach($images as $img):?>
					 <span>
					 	<img class="jb-smartportfolio-img img-responsive" src="<?php echo $img;?>" alt="<?php echo $item->title; ?>" />															
					</span>
					 <?php endforeach;?>
				 </span>
				 <?php elseif($this->item->video): ?>
					 <div class="jb-smartportfolio-embed">
						 <iframe src="<?php echo $this->video_src; ?>" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>
					 </div>	
				<?php else:?>
				<span class="img-thumbnail">
					<img class="jb-smartportfolio-img img-responsive" src="<?php echo $this->item->image; ?>" alt="<?php echo $this->item->title; ?>">
				</span>			
				<?php endif;?> 
			</div>
	</div>
	
<div class="row">
	<div class="col-md-12">
		<div class="portfolio-info pull-left">
			<div class="row">
				<div class="col-md-12 center">
					 <?php echo $this->loadTemplate('niconlist');?>
				</div>
			</div>
		</div>
	</div>
</div>

					<div class="row">

						<div class="col-md-9">

							<h5 class="mt-lg mb-sm"><?php echo JText::_('JBSMARTPORTFOLO_PROJECT_DESCRIPTION');?></h5>
		
		<p class="mt-none mb-xlg"><?php echo $this->item->description; ?></p>
		<?php if($this->item->url):?>
		<a href="<?php echo $this->item->url;?>" class="btn btn-primary btn-icon">
			<i class="fa fa-external-link"></i>
		 	<?php echo JText::_('JBSMARTPORTFOLIO_LIVE_PREVIEW');?>
		 </a>
		 <?php endif;?>
		 <span class="arrow hlb appear-animation" data-appear-animation="rotateInUpLeft" data-appear-animation-delay="800"></span>
		
		 <?php if(!empty($this->item->jbsmartportfolio_skill_id)):?>
		 <?php $skills  = JbsmartportfolioHelper::getSkills($this->item->jbsmartportfolio_skill_id);?>
		  <ul class="portfolio-details">
			<li>
				<h5 class="mt-sm mb-xs"><?php echo JText::_('JBSMARTPORTFOLIO_SKILLS');?></h5>
				<ul class="list list-inline list-icons">								
					<?php foreach($skills as $skill):?>
					<li><i class="fa fa-check-circle"></i><?php echo $skill->title;?></li>
					<?php endforeach;?>														 
				</ul>
			</li>				
		</ul>
		<?php endif;?>	
		<?php if($this->params->get('show_clientinfo',0) && $this->item->client):?>		
		<ul class="portfolio-details mt-none mb-xl">
			<li>
		
		<h5 class="mt-sm mb-xs"><?php echo JText::_('JBSMARTPORTFOLIO_CLIENT');?></h5>
				<?php if($this->item->client !==''):?>
				<p>
					<?php echo $this->item->client;?>
						<a href="<?php echo $this->item->client_url;?>" target="_blank">
							<i class="fa fa-external-link"></i>
							<?php echo $this->item->client_url;?>
						</a>
				</p>
				<?php endif;?>
 		
 			</li>
 		</ul>
 		<?php endif;?>
 		
 		
		<?php if($this->params->get('show_social_share',0)):?>
			<h5 class="m-t-md"><?php echo JText::_('JBSMARTPORTFOLIO_ITEM_SHARE')?></h5>
			<?php echo $this->item->aftercontentDisplay; ?>		
		<?php endif; ?>
	</div>
	<div class="col-md-3">
		 <?php echo $this->loadTemplate('relateditems_carousel');?>		
	</div>
</div>

				


				
				
 