<?php
/**
 * @package     JB Smart Portfolio
 *
 * @copyright   Copyright (C) 2016 Joomlabuff. All rights reserved.
 * @license     GNU General Public License version 2 or later.
 */
defined ( '_JEXEC' ) or die ();


?>
<?php $related_items  = JbsmartportfolioHelper::getRelatedItems($this->item);?>	
 <?php if(!empty($related_items)): ?>
<div class="row">
	<div class="col-md-12">
		<h4 class="mb-md text-uppercase">
			 <?php echo JText::_('JBSMARTPORTFOLIO_ITEM_RELATED_WORKS');?>
		</h4>
		<div class="row">
			<ul id="portfolio-list-<?php echo $this->itemId;?>" class="portfolio-list owl-carousel owl-theme" data-plugin-options='{"items": 1, "margin": 10, "animateOut": "fadeOut", "autoplay": true, "autoplayTimeout": 3000}'>
			<?php foreach($related_items as $ritem):?>
			<?php
				$ritem->url = 'index.php?option=com_jbsmartportfolio&view=item&id=';
				$ritem->url .= $ritem->jbsmartportfolio_item_id . ':' . $ritem->alias . $this->itemId;
				//$ritem->rurl = JRoute::_ ( $ritem->url );				
				$rtags = JbsmartportfolioHelper::getTags ( $ritem->jbsmartportfolio_tag_id );
				$rnewtags = array ();
				foreach ( $rtags as $tag ) {
					$rnewtags[] = $tag->title;
				}
			?>
			 <li class="col-md-12">
			 	<div class="portfolio-item">
					<a href="<?php echo JRoute::_($ritem->url);?>"> 
						<span class="thumb-info thumb-info-no-zoom thumb-info-lighten">
							<span class="thumb-info-wrapper">
								<img  class="jb-smartportfolio-img img-responsive" src="<?php echo JURI::root() . $ritem->image;?>" alt="<?php echo $ritem->title; ?>" />
									<span class="thumb-info-title">
										<span class="thumb-info-inner"><?php echo $ritem->title;?></span>
										<span class="thumb-info-type"><?php echo implode(', ', $rnewtags); ?></span>
									 </span>
									 <span class="thumb-info-action">
									 	<span class="thumb-info-action-icon"><i class="fa fa-link"></i>
									 </span>
								</span>
							</span>
						</span>
						</a>
					</div>
			</li>
				 <?php endforeach;?>
			 </ul>

		</div> 
	</div>
</div>
<?php endif;?>

<?php
?>


<script type="text/javascript">
jQuery('#portfolio-list-<?php echo $this->itemId;?>').owlCarousel({"items": 1, "margin": 10, "animateOut": "fadeOut", "autoplay": true, "autoplayTimeout": 3000});

</script>

