<?php
/**
 * ------------------------------------------------------------------------
 * TP Runway template
 * ------------------------------------------------------------------------
 * Copyright (C) 2014-2022 Themeparrot.com. All Rights Reserved.
 * @license - Copyrighted Commercial Software
 * Author: Themeparrot
 * Websites:  http://www.themeparrot.com
 * ------------------------------------------------------------------------
 */

defined( '_JEXEC' ) or die;
?>


<?php if ( $this->countModules( 'blogmenu' ) ) : ?>
    <!-- blogmenu -->
    <div id="blogmenu" class="wrap t3-sl-1 <?php $this->_c( 'blogmenu' ) ?>">
        <div class="container"
        <jdoc:include type="modules" name="<?php $this->_p( 'blogmenu' ) ?>" style="T3Xhtml"/>
    </div>
    </div>
    <!-- //blogmenu -->
<?php endif ?>

<?php if ( $this->countModules( 'blog_pos1' ) ) : ?>
    <!-- blog_pos1 -->
    <div class="wrap t3-sl-1 <?php $this->_c( 'blog_pos1' ) ?>">
        <div class="container"
            <jdoc:include type="modules" name="<?php $this->_p( 'blog_pos1' ) ?>" style="T3Xhtml"/>
        </div>
    </div>
    <!-- //blog_pos1 -->
<?php endif ?>

<?php if ( $this->countModules( 'blog_pos2' ) ) : ?>
    <!-- blog_pos2 -->
    <div class="wrap t3-sl-1 <?php $this->_c( 'blog_pos2' ) ?>">
        <div class="container"
        <jdoc:include type="modules" name="<?php $this->_p( 'blog_pos2' ) ?>" style="T3Xhtml"/>
    </div>
    </div>
    <!-- //blog_pos2 -->
<?php endif ?>
