<?php
?>

<?php if(!empty($this->item->jbsmartportfolio_skill_id)):?>
<?php $skills  = JbsmartportfolioHelper::getSkills($this->item->jbsmartportfolio_skill_id);?>
<h5 class="mt-sm mb-xs"><?php echo JText::_('JBSMARTPORTFOLIO_SKILLS');?></h5>
<ul class="list list-inline list-icons">								
	<?php foreach($skills as $skill):?>
		<li><i class="fa fa-check-circle"></i><?php echo $skill->title;?></li>
	<?php endforeach;?>														 
</ul>
<?php endif;?>	