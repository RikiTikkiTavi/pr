<?php
/**
 * @package     JB Porto Hotel 
 * @subpackage  mod_jbportohotel
 *
 * @copyright   Copyright (C) 2010 - 2015 Joombuff. All rights reserved.
 * @license     GNU General Public License version 2 or later.
 */
defined ( '_JEXEC' ) or die ();
jimport ( 'joomla.filesystem.file' );
$doc = JFactory::getDocument (); 
$doc->addStylesheet ( JURI::root ( true ) . '/modules/mod_jbportohotel/theme-elements.css' );
// Params
$cparams = JComponentHelper::getParams ( 'com_jbportohotel' );
?>


<div id="mod-jb-smartportfolio" class="jb-smartportfolio jb-portohotel-view-items <?php echo $moduleclass_sfx; ?>">
	<ul class="image-gallery sort-destination full-width mb-none">
	
	<?php 
	foreach ( $items as $item ) :
		$tags = jbportohotelHelper::getTags ( $item->jbportohotel_tag_id );
		$newtags = array ();
		$groups = array ();
		foreach ( $tags as $tag ) {
			$newtags [] = $tag->title;	
			$groups [] .= '"' . $tag->alias . '"';
		}
		$groups = implode ( ',', $groups );		
		$item->url = JRoute::_('index.php?option=com_jbportohotel&view=item&id='.$item->jbportohotel_item_id.':'.$item->alias. ModjbportohotelHelper::getItemid()); ?>
		<li class="isotope-item">
			<div class="image-gallery-item mb-none">
			 	<a href="<?php echo $item->url;?>">
			 		<span class="thumb-info thumb-info-centered-info thumb-info-no-borders">
						<span class="thumb-info-wrapper"> 
							<img src="<?php echo JUri::root().'/'.$item->image;?>" class="img-responsive" alt="">
							
							<span class="thumb-info-title">
								<span class="thumb-info-inner"><?php echo $item->title;?></span> 
								<span class="thumb-info-type"><?php echo implode(', ', $newtags); ?></span>
							</span>
							<span class="thumb-info-action">
								<span class="thumb-info-action-icon"><i class="fa fa-link"></i></span>
							</span>
						</span>
					</span>
					</a>
				</div>
		</li>
	<?php endforeach;?>
	</ul> 
</div>

