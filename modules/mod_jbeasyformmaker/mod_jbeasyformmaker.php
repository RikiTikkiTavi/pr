<?php
/**
 * @copyright	@copyright	Copyright (c) 2016 Formbuilder. All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 */

// no direct access
defined('_JEXEC') or die;

// include the syndicate functions only once
require_once __DIR__ . '/helper.php';

$form = ModJBEasyFormMakerHelper::getCForm($params);
$menu_id =0;
$app = JFactory::getApplication();
if($app->getMenu()->getActive()) {
	$menu_id = $app->getMenu()->getActive()->id;
}

$document = JFactory::getDocument ();
JHtml::_ ( 'jquery.framework' );
$document->addScript ( JUri::root ( true ) . '/modules/mod_jbeasyformmaker/media/js/script.js', false, true );
$class_sfx = htmlspecialchars($params->get('class_sfx'));

require(JModuleHelper::getLayoutPath('mod_jbeasyformmaker', $params->get('layout', 'default')));