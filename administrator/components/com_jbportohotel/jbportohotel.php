<?php
/**
 * @package     JB Porto Hotel 
 *
 * @copyright   Copyright (C) 2016 Joomlabuff. All rights reserved.
 * @license     GNU General Public License version 2 or later.
 */

defined('_JEXEC') or die();
 

// Load F0F
// Include F0F
if(!defined('F0F_INCLUDED')) {
	require_once JPATH_LIBRARIES.'/f0f/include.php';
}
if(!defined('F0F_INCLUDED')) {
	?>
   <h2>Incomplete installation detected</h2>
<?php
}
 

F0FDispatcher::getTmpInstance('com_jbportohotel')->dispatch();