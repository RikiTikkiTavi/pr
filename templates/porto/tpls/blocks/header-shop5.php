<?php
/**
 * ------------------------------------------------------------------------
 * JB Porto template
 * ------------------------------------------------------------------------
 * Copyright (C) 2004-2011 J.O.O.M Solutions Co., Ltd. All Rights Reserved.
 * @license - Copyrighted Commercial Software
 * Author: J.O.O.M Solutions Co., Ltd
 * Websites:  http://www.joomlart.com -  http://www.joomlancers.com
 * This file may not be redistributed in whole or significant part.
 * ------------------------------------------------------------------------
 */

defined('_JEXEC') or die;

// get params
$sitename  = $this->params->get('sitename');
$slogan    = $this->params->get('slogan', '');
$logotype  = $this->params->get('logotype', 'text');
$logoimage = $logotype == 'image' ? $this->params->get('logoimage', T3Path::getUrl('images/logo.png', '', true)) : '';
$logoimgsm = ($logotype == 'image' && $this->params->get('enable_logoimage_sm', 0)) ? $this->params->get('logoimage_sm', T3Path::getUrl('images/logo-sm.png', '', true)) : false;

if (!$sitename) {
	$sitename = JFactory::getConfig()->get('sitename');
}

$headright = $this->countModules('head-search or cart');


?>

<!-- HEADER -->
<header id="header" class="wrap t3-header-wrap">

<div class="t3-header header-body jb-header-default">
	<div class="container">
		<div class="row">
			
			 <!-- LOGO -->
			 <div class="col-xs-6 col-sm-2 col-md-2 col-lg-2 logo">
				<div class="logo-<?php echo $logotype, ($logoimgsm ? ' logo-control' : '') ?>">
					<a href="<?php echo JURI::base(true) ?>" title="<?php echo strip_tags($sitename) ?>">
						<?php if($logotype == 'image'): ?>
							<img class="logo-img" src="<?php echo JURI::base(true) . '/' . $logoimage ?>" alt="<?php echo strip_tags($sitename) ?>" />
						<?php endif ?>
						<?php if($logoimgsm) : ?>
							<img class="logo-img-sm" src="<?php echo JURI::base(true) . '/' . $logoimgsm ?>" alt="<?php echo strip_tags($sitename) ?>" />
						<?php endif ?>
						<span><?php echo $sitename ?></span>
					</a>
					<small class="site-slogan"><?php echo $slogan ?></small>
					</div>
			 </div>
		    <!-- //LOGO -->
		    
			
				
				<div class="header-row">
					<!-- MAIN NAVIGATION -->
					<nav id="t3-mainnav" class="jb-navigation">
						
					<div class="navbar navbar-default t3-mainnav pull-left col-sm-4 hidden-sm hidden-xs hidden-md">
				
						<?php if ($this->getParam('navigation_collapse_enable')) : ?>
							<div class="t3-navbar-collapse navbar-collapse collapse"></div>
						<?php endif ?>
				
						<div class="t3-navbar header-nav-main-effect-1 header-nav-main-sub-effect-1 navbar-collapse collapse">
							<jdoc:include type="<?php echo $this->getParam('navigation_type', 'megamenu') ?>" name="<?php echo $this->getParam('mm_type', 'mainmenu') ?>" />
						</div>
				
					</div>
			
					<!-- Right side block starts -->
					<div class="header-right-block">
						<div class="t3-nav-btn">
							<!-- OFFCANVAS -->
							<?php if ($this->countModules('off-canvas')) : ?>
								<div class="pull-right">
								<?php if ($this->getParam('addon_offcanvas_enable')) : ?>
									<?php $this->loadBlock ('off-canvas') ?>
								<?php endif ?>
								</div>
							<?php endif ?>
							<!-- //OFFCANVAS -->
				
							<!-- Brand and toggle get grouped for better mobile display -->
							<div class="navbar-header pull-right hidden-xs">
							
								<?php if ($this->getParam('navigation_collapse_enable', 1) && $this->getParam('responsive', 1)) : ?>
									<?php $this->addScript(T3_URL.'/js/nav-collapse.js'); ?>
									<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".t3-navbar-collapse">
										<i class="fa fa-bars"></i>
									</button>
								<?php endif ?>
							</div>
							<!-- //Brand and toggle get grouped for better mobile display -->
							
							
							<div class="col-lg-4 col-md-5 col-sm-5">	
							<?php if ($this->countModules('topbar-right')) : ?>
								<!-- top-nav -->
									<div class="topbar-right jb-topbar-trans hidden-xs">
										<jdoc:include type="modules" name="<?php $this->_p('topbar-right') ?>" style="raw" />
									</div>
								<!-- //topbar-right -->
							<?php endif ?>
							</div>
							
							
							<div class="col-lg-1 col-md-2 col-sm-2">	
							<?php if ($this->countModules('cart')) : ?>
								<!-- cart -->
									<div class="cart">
										<jdoc:include type="modules" name="<?php $this->_p('cart') ?>" style="raw" />
									</div>
								<!-- //cart -->
							<?php endif ?>
							</div>
							<div class="col-lg-1 col-md-2 col-sm-2">	
							<?php if ($this->countModules('currency')) : ?>
								<!-- currency -->
									<div class="currency hidden-xs">
										<jdoc:include type="modules" name="<?php $this->_p('currency') ?>" style="raw" />
									</div>
								<!-- //currency -->
							<?php endif ?>
							</div>
							
							
						</div>
					</div>
					<!-- //Right side block ends -->
					
				

				
				
				
				
			</nav>
			<!-- //MAIN NAVIGATION -->
			
			
					
	</div>
				
   </div>
	</div>
	</div>
</header>
<!-- //HEADER -->
