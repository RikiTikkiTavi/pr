<?php
/**
 * @package     JB Porto Hotel 
 *
 * @copyright   Copyright (C) 2016 Joomlabuff. All rights reserved.
 * @license     GNU General Public License version 2 or later.
 */

defined('_JEXEC') or die();
 
require_once JPATH_COMPONENT . '/helpers/helper.php';
jbportohotelHelper::generateMeta($this->item);
$app = JFactory::getApplication(); 
JPluginHelper::importPlugin('jbportohotel');
$jitem  = $app->triggerEvent('onContentBeforeDisplay', array('com_jbportohotel.item', &$this->item, &$params, 1));
$menu 	= JFactory::getApplication()->getMenu();		
$itemId = '';
if(is_object($menu->getActive())) {
	$active = $menu->getActive();
   $itemId = $active->id;
}
 
$link = JRoute::_("index.php?Itemid={$itemId}");	
 
$doc = JFactory::getDocument();
$doc->addStylesheet( JURI::root(true) . '/components/com_jbportohotel/assets/css/jbportohotel.css' );

$doc = JFactory::getDocument(); 

 
	$app = JFactory::getApplication();
	$doc->addStylesheet( JURI::base(true) . '/templates/'.$app->getTemplate().'/sppagebuilder/addons/testimonialporto/media/assets/owl.carousel.min.css');
	$doc->addStylesheet( JURI::base(true) . '/templates/'.$app->getTemplate().'/sppagebuilder/addons/testimonialporto/media/assets/owl.theme.old_default.min.css');
	$doc->addScript( JURI::base(true) . '/templates/'.$app->getTemplate().'/sppagebuilder/addons/testimonialporto/media/owl.carousel.min.js');


$tags = jbportohotelHelper::getTags( (array) $this->item->jbportohotel_tag_id );
$newtags = array();
foreach ($tags as $tag) {
	$newtags[] 	 = '<a href="#">'.$tag->title.'</a>';
}

//video
if($this->item->video) {
	$video = parse_url($this->item->video);

	switch($video['host']) {
		case 'youtu.be':
		$video_id 	= trim($video['path'],'/');
		$video_src 	= '//www.youtube.com/embed/' . $video_id;
		break;

		case 'www.youtube.com':
		case 'youtube.com':
		parse_str($video['query'], $query);
		$video_id 	= $query['v'];
		$video_src 	= '//www.youtube.com/embed/' . $video_id;
		break;

		case 'vimeo.com':
		case 'www.vimeo.com':
		$video_id 	= trim($video['path'],'/');
		$video_src 	= "//player.vimeo.com/video/" . $video_id;
	}

}

?>

<div class="container">
<div id="jb-smartportfolio" class="jb-smartportfolio jb-portohotel-view-item"> 
<div class="row">
						<div class="col-md-12">
							<div class="portfolio-title">
								<div class="row">
									<div class="portfolio-nav-all col-md-1">
										<a href="<?php echo $link;?>" data-tooltip data-original-title="Back to list"><i class="fa fa-th"></i></a>
									</div>
									<div class="col-md-10 center">
										<h2 class="mb-none"><?php echo $this->item->title; ?></h2>
									</div>
									<div class="portfolio-nav col-md-1">
										<?php echo $this->item->pagination ; ?>
									</div>
								</div>
							</div>

							<hr class="tall">
						</div>
					</div>


<div class="row">
						<div class="col-md-4">
							
							 
							<div class="owl-carousel11 owl-thee" > 
									<div>	 
										<?php if($this->item->video): ?>
										<div class="jb-portohotel-embed">
											<iframe src="<?php echo $video_src; ?>" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>
										</div>
										<?php elseif($this->item->image): ?>
										<span class="img-thumbnail">
										<img class="jb-portohotel-img img-responsive" src="<?php echo $this->item->image; ?>" alt="<?php echo $this->item->title; ?>">
										<?php else: ?>
										<img class="jb-portohotel-img img-responsive" src="<?php echo $this->item->thumbnail; ?>" alt="<?php echo $this->item->title; ?>">
										 <?php endif;?> 

									</span>
								 </div>
								<div>
									<span class="img-thumbnail">
										<img alt="" class="img-responsive" src="img/projects/project-1-2.jpg">
									</span>
								</div>
							</div>

						</div>

						<div class="col-md-8">

							<div class="portfolio-info">
								<div class="row">
									<div class="col-md-12 center">
										<ul>
											<li>
												<a href="#" data-tooltip data-original-title="Like"><i class="fa fa-heart"></i>14</a>
											</li>
											<li>
												<i class="fa fa-calendar"></i>
												<?php echo JHtml::_('date', $this->item->created_on, JText::_('DATE_FORMAT_LC3')); ?>
												
											</li>
											<li>
												<i class="fa fa-tags"></i> <?php echo implode(', ', $newtags); ?>
											</li>
										</ul>
									</div>
								</div>
							</div> 
							 <?php echo $this->item->description; ?>						 

						</div>
					</div>
		</div>
</div>
	