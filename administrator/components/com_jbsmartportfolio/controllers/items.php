<?php 
// No direct access to this file
defined('_JEXEC') or die;
 
 
class JbsmartportfolioControllerItems extends F0FController
{
	
	 
	 /**
	  * Method to save a vote.
	  *
	  * @return  void
	  *
	  * @since   1.6
	  */
	 public function vote()
	 {
	 	// Check for request forgeries.
	 	JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));
	 
	 	$user_rating = $this->input->getInt('user_rating', -1);
	 
	 	if ($user_rating > -1)
	 	{
	 		$url = $this->input->getString('url', '');
	 		$id = $this->input->getInt('id', 0);
	 		$viewName = $this->input->getString('view', $this->default_view);
	 		$model = $this->getModel($viewName);
	 
	 		if ($model->storeVote($id, $user_rating))
	 		{
	 			$this->setRedirect($url, JText::_('COM_CONTENT_ARTICLE_VOTE_SUCCESS'));
	 		}
	 		else
	 		{
	 			$this->setRedirect($url, JText::_('COM_CONTENT_ARTICLE_VOTE_FAILURE'));
	 		}
	 	}
	 }
 

function setItems(){ 

		//get variant id
		$model =  F0FModel::getTmpInstance('Items' ,'JbsmartportfolioModel');

	//	$filter_category = $this->input->getInt('filter_category');

		$limit = $this->input->getInt('limit',0);
		$limitstart = $this->input->getInt('limitstart',0);

		/* $model->setState('filter_category',$filter_category);
		$model->setState('limit',$limit);
		$model->setState('limitstart',$limitstart);
		$model->setState('enabled',1);
		$model->setState('visible', 1); */
		
	//	F0FModel::getTmpInstance($type)->getItemList();
		$items = $model->getItemList();
		$catarray = array();
	 	
		//print_r($items); exit;
	 
		$categories = JHtmlCategory::options('com_jbsmartportfolio');
		
		 $layout = $this->input->getString('layout');
		 
		
		$view = $this->getThisView();
		$view->setModel($model, true);
		$view->set('state',$model->getState());
		$view->set('pagination',$model->getPagiantion());
		$view->set('total',$model->getTotal());
		$view->set('items',$items);
	//	$view->set('categories',$categories);
		$view->setLayout('items');
		$view->display();
	}
	
	 
}