<?php
/**
 * @package   T3 Blank
 * @copyright Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license   GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
?>

<!-- MAIN NAVIGATION -->
<div class="header-container header-nav header-nav-bar header-nav-bar-primary"  data-spy="affix" data-offset-top="110">
	
<nav id="t3-mainnav" class="wrap navbar jb-navbar-brand t3-mainnav">
	<div class="container">
<div class="header-nav-main header-nav-main-light header-nav-main-effect-1 header-nav-main-sub-effect-1">

		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
			<?php if ($this->getParam('addon_offcanvas_enable')) : ?>
				<?php $this->loadBlock ('off-canvas') ?>
			<?php endif ?>

		</div>

		<?php if ($this->getParam('navigation_collapse_enable')) : ?>
			<div class="t3-navbar-collapse navbar-collapse collapse"></div>
		<?php endif ?>

		<div class="<?php echo $mainnavsize ?> hidden-sm hidden-xs pull-left">
			<div class="t3-navbar navbar-collapse collapse">
				<jdoc:include type="<?php echo $this->getParam('navigation_type', 'megamenu') ?>" name="<?php echo $this->getParam('mm_type', 'mainmenu') ?>" />
			</div>
		</div>
</div>
	</div>
</nav>
</div>
<!-- //MAIN NAVIGATION -->
