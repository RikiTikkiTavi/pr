<?php
/**
 * @package     JB Smart Portfolio
 *
 * @copyright   Copyright (C) 2016 Joomlabuff. All rights reserved.
 * @license     GNU General Public License version 2 or later.
 */

defined('_JEXEC') or die();

JHtml::_('formbehavior.chosen', 'select');

//echo $this->getRenderedForm();

?>
<script>
jQuery

</script>

<style type="text/css">
.control-label > #item_type_ss{
	display:none;
}
</style>
<div class="smartportfolio_items">

<form id="adminForm" class="form-horizontal form-validate" name="adminForm" method="post" action="index.php">
	<input type="hidden" value="com_jbsmartportfolio" name="option">
	<input type="hidden" value="skill" name="view">
	<input type="hidden" id="task" value="" name="task">
	<input type="hidden" value="<?php echo $this->item->jbsmartportfolio_skill_id;?>" name="jbsmartportfolio_skill_id">
	<?php echo JHtml::_('form.token'); ?>
	
	
	<?php
        $fieldsets = $this->form->getFieldsets();
        $shortcode = $this->form->getValue('text');
        $tab_count = 0;
        foreach ($fieldsets as $key => $attr):
        
      /*   	 if($key =='relateditems'):
        	 		echo $this->loadTemplate('relateditems'); 
            	elseif($key =='skills'):
        			 echo $this->loadTemplate('skills');
        	else:
        	  */
            if ( $tab_count == 0 )
	            {
	                echo JHtml::_('bootstrap.startTabSet', 'configuration', array('active' => $attr->name));
	            }
	            echo JHtml::_('bootstrap.addTab', 'configuration', $attr->name, JText::_($attr->label, true));
	            ?>
	         
	            <div class="row-fluid">
	                <div class="span12">
	                    <?php
	                    $layout = '';
	                    $style = '';
	                    $fields = $this->form->getFieldset($attr->name);
	                    
	                    foreach ($fields as $key => $field):	                                        	
	                    ?>
	                        <div class="control-group <?php echo $layout; ?>" <?php echo $style; ?>>
	                            <div class="control-label"><?php echo $field->label; ?></div>
	                            
	                             	<div class="controls"><?php echo $field->input; ?>
	                            	<br />
	                            	<small class="muted"><?php echo JText::_($field->description); ?></small>
	                          </div>
	                        </div>
	                    <?php
	                    endforeach
	                    ?>
	                </div>
	            </div>
	             <?php
	            echo JHtml::_('bootstrap.endTab');
	            $tab_count++;
	            
       endforeach;
       
      
        ?>
        
        
        
         
	
</form>
</div>