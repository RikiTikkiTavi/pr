<?php
?>

<ul class="item-fav-icons-list">
 	<?php if($this->params->get('show_like_count',0)):?>
	 <li>
		 <?php echo $this->item->like_count_html;?>
					<!-- <a href="#" data-tooltip data-original-title="Like"><i class="fa fa-heart"></i></a> -->
	</li>
	<?php endif;?>
	
	<?php if($this->params->get('show_created_date',0)):?>
	<li>
		<i class="fa fa-calendar"></i>
		<?php echo JHtml::_('date', $this->item->created_on, JText::_('DATE_FORMAT_LC3')); ?>
	</li>
	<?php endif;?>
				
	<?php if($this->params->get('show_tags',0) && !empty($this->newtags)):?>
	<li><i class="fa fa-tags"></i>
		<?php echo implode(', ', $this->newtags); ?>
	</li>
	<?php endif;?>
</ul>