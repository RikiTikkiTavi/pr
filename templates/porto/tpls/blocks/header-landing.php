
<?php
/**
 * ------------------------------------------------------------------------
 * JB Porto template
 * ------------------------------------------------------------------------
 * Copyright (C) 2004-2011 J.O.O.M Solutions Co., Ltd. All Rights Reserved.
 * @license - Copyrighted Commercial Software
 * Author: J.O.O.M Solutions Co., Ltd
 * Websites:  http://www.joomlart.com -  http://www.joomlancers.com
 * This file may not be redistributed in whole or significant part.
 * ------------------------------------------------------------------------
 */

defined('_JEXEC') or die;

// get params
$sitename  = $this->params->get('sitename');
$slogan    = $this->params->get('slogan', '');
$logotype  = $this->params->get('logotype', 'text');
$logoimage = $logotype == 'image' ? $this->params->get('logoimage', T3Path::getUrl('images/logo.png', '', true)) : '';
$logoimgsm = ($logotype == 'image' && $this->params->get('enable_logoimage_sm', 0)) ? $this->params->get('logoimage_sm', T3Path::getUrl('images/logo-sm.png', '', true)) : false;

if (!$sitename) {
	$sitename = JFactory::getConfig()->get('sitename');
}

$headright = $this->countModules('head-search or languageswitcherload');



?>


<!-- HEADER -->
<header id="header" class="wrap t3-header-wrap">

<div class="t3-header header-body jb-header-default">
	<div class="container">
		<div class="row">
			
			 <!-- LOGO -->
			 <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2 logo">
				<div class="logo-<?php echo $logotype, ($logoimgsm ? ' logo-control' : '') ?>">
					<a href="<?php echo JURI::base(true) ?>" title="<?php echo strip_tags($sitename) ?>">
						<?php if($logotype == 'image'): ?>
							<img class="logo-img" src="<?php echo JURI::base(true) . '/' . $logoimage ?>" alt="<?php echo strip_tags($sitename) ?>" />
						<?php endif ?>
						<?php if($logoimgsm) : ?>
							<img class="logo-img-sm" src="<?php echo JURI::base(true) . '/' . $logoimgsm ?>" alt="<?php echo strip_tags($sitename) ?>" />
						<?php endif ?>
						<span><?php echo $sitename ?></span>
					</a>
					<small class="site-slogan"><?php echo $slogan ?></small>
					</div>
			 </div>
		    <!-- //LOGO -->
		    
		    <div class="header-column col-xs-12 col-sm-10 col-md-10 col-lg-10">
				<div class="header-nav header-nav-top-line navbar-default">
					<div class="header-nav-main header-nav-main-square header-nav-main-effect-3 header-nav-main-sub-effect-1">
					<!-- MAIN NAVIGATION -->
						<nav id="t3-mainnav" class="wrap navbar t3-mainnav">
								
							<!-- Brand and toggle get grouped for better mobile display -->
								
								

								<?php if ($this->getParam('navigation_collapse_enable')) : ?>
								<div class="t3-navbar-collapse navbar-collapse collapse onSmall"></div>
								<?php endif ?>
							
								
								
								<div class="t3-navbar pull-right hidden-sm hidden-xs hidden-md navbar-collapse collapse">
									<jdoc:include type="<?php echo $this->getParam('navigation_type', 'megamenu') ?>" name="<?php echo $this->getParam('mm_type', 'mainmenu') ?>" />
								</div>
								
								

							</div>
							
						</nav>
					</div>	
				</div>
			</div>
		 
		 </div>
	</div>
</div>
	

	  <!-- smooth scroll -->
  <script type="text/javascript">
    !function($) {

      var winurl = window.location.href.replace(/([^\#]*)(.*)/, '$1');

      window.reflow = function () {
        if('ontouchstart' in window){
          document.documentElement.style.width = (document.documentElement.offsetWidth + 1) + 'px';
          setTimeout(function () {
            document.documentElement.style.width = '';
          }, 0);
        }
      };

      $('.navbar ul li a').each (function (){
        var href = this.href,
          target = this.hash ? this.hash : href.replace(/.*(?=#[^\s]+$)/, ''),
          target_url = href.replace(/([^\#]*)(.*)/, '$1');

        if(this.hostname != window.location.hostname || target.substr(0, 1) != '#'){
          return;
        }

        $(this).attr('href', target).data('target', target);
      });

      $('.navbar ul li a').click (function(e) {
        if (!$(this).data('target')){
          return;
        } 

        var target = this.href.replace(/.*(?=#[^\s]+$)/, '');
        if (target && ($(target).length)) {
          e.preventDefault();
          
          $('html,body').animate({scrollTop: Math.max(0, 
                    $(target).offset().top
                     - ((!($('html').hasClass('off-canvas') && $('.btn-navbar').is(':visible')) && $('#t3-header').css('position') == 'fixed') ? $('#t3-header').height() : 0)
                      + 2)}, {
            duration: 800, 
            easing: 'easeInOutCubic',
            complete: window.reflow
          });
        } else { //not found
          var home = $('.navbar ul li a.home').attr('href');
          if(home){
            window.location.href = home.replace(/([^\#]*)(.*)/, '$1') + target;
          }
        }
      });

      $(document).ready(function(){
        var ftarget = window.location.href.replace(/.*(?=#[^\s]+$)/, '');

        if(ftarget.substr(0, 1) == '#'){
          ftarget = $(ftarget);

          if(ftarget.length){
            $('html,body').scrollTop(Math.max(0, ftarget.offset().top - ((!($('html').hasClass('off-canvas') && $('.btn-navbar').is(':visible')) && $('#t3-header').css('position') == 'fixed') ? $('#t3-header').height() : 0) + 1));
            window.reflow();
          }
        }

        var homelink = $('.navbar ul li a.home')[0];
        if(homelink){
          var home_url = homelink.href.replace(/([^\#]*)(.*)/, '$1'),
            home_target = homelink.hash ? homelink.hash : homelink.href.replace(/.*(?=#[^\s]+$)/, '');

          if(home_url == winurl){
            if(home_target.substr(0, 1) != '#'){
              homelink.href = home_target = '#onepage-home';
              $(homelink).data('target', home_target);
            }

            home_target = $(home_target);
            if(!home_target.length){
              home_target = $('<div id="onepage-home" style="width: 0; height: 0; visibility: hidden">').prependTo(document.body);
            }

          } else {
            home_target = null;
          }

          $(homelink).unbind('click').click(function(e){
            
            if(home_target){
              e.preventDefault();
              
              $('html,body').animate({scrollTop: Math.max(0, (home_target.offset().top - $('#t3-header').height() + 2))}, {
                duration: 800, 
                easing: 'easeInOutCubic',
                complete: window.reflow
              });
            }
          });
        }
      });
    }(window.jQuery);
  </script>
  <!-- //smooth scroll -->
	
</header>
<!-- //HEADER -->
						
