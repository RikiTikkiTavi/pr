<?php
/**
 * @version    CVS: 1.0.0
 * @package    com_jbeasyformmaker
 * @author     Priya Bose <support@joomlabuff.com>
 * @copyright  2016 www.joomlabuff.com
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
defined ( '_JEXEC' ) or die ();
class JBFormBuilderToolbar extends F0FToolbar {
	public function onCpanelsBrowse() {
		$option = $this->input->getCmd ( 'option', 'com_foobar' );
		$componentName = str_replace ( 'com_', '', $option );
		$subtitle_key = strtoupper ( $option . '_TITLE_' . F0FInflector::pluralize ( $this->input->getCmd ( 'view', 'cpanel' ) ) );
		JToolBarHelper::title ( JText::_ ( strtoupper ( $option ) ) . ': ' . JText::_ ( $subtitle_key ), $componentName );
		JToolBarHelper::preferences ( $option, 550, 875 );
	}
}
