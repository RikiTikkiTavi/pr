<?php
 
// No direct access to this file
defined('_JEXEC') or die;
require_once (JPATH_ADMINISTRATOR.'/components/com_jbsmartportfolio/helpers/jbsmartportfolio.php');
class JFormFieldApps extends  JFormField
{
	/**
	 * The field type.
	 *
	 * @var		string
	 */
	protected $type = 'Apps';

	protected function getInput(){

		//print_r($this);
		$html ='';
		
		$app = JFactory::getApplication();
		$id = $app->input->getInt('id');
		$item = F0FModel::getTmpInstance('Items' ,'JbsmartportfolioModel')->setId($id)->getItem();
		JPluginHelper::importPlugin('jbsmartportfolio');
		$results  = $app->triggerEvent('onAfterItemDisplayItemForm', array('com_jbsmartportfolio.item', &$item, array(), 1));
			
		$html ='';
		foreach($results as $result) {
			$html .= $result;
		}
		return $html; 	 
	}

}
