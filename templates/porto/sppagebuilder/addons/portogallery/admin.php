<?php
/**
 * @package SP Page Builder
 * @author JoomShaper http://www.joomshaper.com
 * @copyright Copyright (c) 2010 - 2016 JoomShaper
 * @license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 or later
*/
//no direct accees
defined ('_JEXEC') or die ('restricted aceess');

SpAddonsConfig::addonConfig(
	array( 
		'type'=>'content',
		'addon_name'=>'sp_portogallery',
		'title'=>JText::_('Porto Gallery'),
		'desc'=>JText::_('COM_SPPAGEBUILDER_ADDON_FSS_DESC'),
		'attr'=>array(
			'admin_label'=>array(
					'type'=>'text', 
					'title'=>JText::_('COM_SPPAGEBUILDER_ADDON_ADMIN_LABEL'),
					'desc'=>JText::_('COM_SPPAGEBUILDER_ADDON_ADMIN_LABEL_DESC'),
					'std'=> ''
				), 
			/*
			'title'=>array(
					'type'=>'text',
					'title'=>JText::_('COM_SPPAGEBUILDER_ADDON_TITLE'),
					'desc'=>JText::_('COM_SPPAGEBUILDER_ADDON_TITLE_DESC'),
					'std'=>  ''
				),
				
		  'description'=>array(
				'type'=>'textarea', 
				'title'=> 'Description',
				'desc'=> 'Enter the description',
				'std'=>'',
				), */ 
			'block_1'=>array(
				'type'=>'text', 
				'title'=> 'Block 1',
				 'desc'=> 'Enter the title',
				'std'=>'Strategy',
				), 
				
			'block_1_image'=>array(
				'type'=>'media', 
				'title'=>JText::_('COM_SPPAGEBUILDER_ADDON_IMAGE'),
				'desc'=>JText::_('COM_SPPAGEBUILDER_ADDON_IMAGE_DESC'),
				), 
			
			'block_2'=>array(
				'type'=>'text', 
				'title'=> 'Block 2',
				'desc'=> 'Enter the title',
				'std'=>'Planning',
				),  
				
			'block_2_image'=>array(
				'type'=>'media', 
				'title'=>JText::_('COM_SPPAGEBUILDER_ADDON_IMAGE'),
				'desc'=>JText::_('COM_SPPAGEBUILDER_ADDON_IMAGE_DESC'),
				),  
			 
			 'block_3'=>array(
				'type'=>'text', 
				'title'=> 'Block 3',
				'desc'=> 'Enter the title',
				'std'=>'Build',
				), 
			
			'block_3_image'=>array(
				'type'=>'media', 
				'title'=>JText::_('COM_SPPAGEBUILDER_ADDON_IMAGE'),
				'desc'=>JText::_('COM_SPPAGEBUILDER_ADDON_IMAGE_DESC'),
				), 
		 'block_4'=>array(
				'type'=>'text', 
				'title'=> 'Block 3',
				'desc'=> 'Enter the title',
				'std'=>'Build',
				), 
			
		'block_4_image'=>array(
				'type'=>'media', 
				'title'=>JText::_('COM_SPPAGEBUILDER_ADDON_IMAGE'),
				'desc'=>JText::_('COM_SPPAGEBUILDER_ADDON_IMAGE_DESC'),
				), 
	 
	   'block_5'=>array(
				'type'=>'text', 
				'title'=> 'Block 3',
				'desc'=> 'Enter the title',
				'std'=>'Build',
				), 
			
	  'block_5_image'=>array(
				'type'=>'media', 
				'title'=>JText::_('COM_SPPAGEBUILDER_ADDON_IMAGE'),
				'desc'=>JText::_('COM_SPPAGEBUILDER_ADDON_IMAGE_DESC'),
				), 
			 				 
			'class'=>array(
				'type'=>'text', 
				'title'=>JText::_('COM_SPPAGEBUILDER_ADDON_CLASS'),
				'desc'=>JText::_('COM_SPPAGEBUILDER_ADDON_CLASS_DESC'),
				'std'=> ''
				),
			)
		)
	);
