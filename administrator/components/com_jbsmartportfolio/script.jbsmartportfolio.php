<?php
/**
 * @package     JB Smart Portfolio
 *
 * @copyright   Copyright (C) 2016 Joomlabuff. All rights reserved.
 * @license     GNU General Public License version 2 or later.
 */
defined ( '_JEXEC' ) or die ();
jimport ( 'joomla.filesystem.file' );

// Load FOF if not already loaded
if (! defined ( 'F0F_INCLUDED' )) {
	$paths = array (
			(defined ( 'JPATH_LIBRARIES' ) ? JPATH_LIBRARIES : JPATH_ROOT . '/libraries') . '/f0f/include.php',
			__DIR__ . '/fof/include.php'
	);

	foreach ( $paths as $filePath ) {
		if (! defined ( 'F0F_INCLUDED' ) && file_exists ( $filePath )) {
			@include_once $filePath;
		}
	}
}

// Pre-load the installer script class from our own copy of FOF
if (! class_exists ( 'F0FUtilsInstallscript', false )) {
	@include_once __DIR__ . '/fof/utils/installscript/installscript.php';
}

// Pre-load the database schema installer class from our own copy of FOF
if (! class_exists ( 'F0FDatabaseInstaller', false )) {
	@include_once __DIR__ . '/fof/database/installer.php';
}

// Pre-load the update utility class from our own copy of FOF
if (! class_exists ( 'F0FUtilsUpdate', false )) {
	@include_once __DIR__ . '/fof/utils/update/update.php';
}

// Pre-load the cache cleaner utility class from our own copy of FOF
if (! class_exists ( 'F0FUtilsCacheCleaner', false )) {

	@include_once __DIR__ . '/fof/utils/cache/cleaner.php';
}
class Com_JbsmartportfolioInstallerScript extends FOFUtilsInstallscript {
	function postflight($type, $parent) {
		$db = JFactory::getDBO ();
		$app = JFactory::getApplication ( 'site' );
		$status = new stdClass ();
		$status->plugins = array ();
		$src = $parent->getParent ()->getPath ( 'source' );
		
		$manifest = $parent->getParent ()->manifest;
		$modules = $manifest->xpath ( 'modules/module' );
		// print_r($modules); exit;
		$plugins = $manifest->xpath ( 'plugins/plugin' );
		foreach ( $modules as $module ) {
			$name = ( string ) $module->attributes ()->module;
			$client = ( string ) $module->attributes ()->client;
			if (is_null ( $client )) {
				$client = 'site';
			}
			$path = $src . '/modules/' . $name;
			
			$installer = new JInstaller ();
			$result = $installer->install ( $path );
			
			$status->modules [] = array (
					'name' => $name,
					'client' => $client,
					'result' => $result 
			);
		}
		
		foreach ( $plugins as $plugin ) {
			$name = ( string ) $plugin->attributes ()->plugin;
			$group = ( string ) $plugin->attributes ()->group;
			$installer = new JInstaller ();
			$path = $src . '/plugins/' . $group . '/' . $name;
			$result = $installer->install ( $path );
			$status->plugins [] = array (
					'name' => $name,
					'group' => $group,
					'result' => $result 
			);
		}
		
		$fofInstallationStatus = $this->_installFOF ( $parent );
	}
	

	/**
	 * Check if FOF is already installed and install if not
	 *
	 * @param object $parent
	 *        	class calling this method
	 *
	 * @return array Array with performed actions summary
	 */
	private function _installFOF($parent) {
		$src = $parent->getParent ()->getPath ( 'source' );
	
		// Load dependencies
		JLoader::import ( 'joomla.filesystem.file' );
		JLoader::import ( 'joomla.utilities.date' );
		$source = $src . '/fof';
	
		if (! defined ( 'JPATH_LIBRARIES' )) {
			$target = JPATH_ROOT . '/libraries/f0f';
		} else {
			$target = JPATH_LIBRARIES . '/f0f';
		}
		$haveToInstallFOF = false;
	
		if (! is_dir ( $target )) {
			$haveToInstallFOF = true;
		} else {
			$fofVersion = array ();
	
			if (file_exists ( $target . '/version.txt' )) {
				$rawData = JFile::read ( $target . '/version.txt' );
				$info = explode ( "\n", $rawData );
				$fofVersion ['installed'] = array (
						'version' => trim ( $info [0] ),
						'date' => new JDate ( trim ( $info [1] ) )
				);
			} else {
				$fofVersion ['installed'] = array (
						'version' => '0.0',
						'date' => new JDate ( '2011-01-01' )
				);
			}
	
			$rawData = JFile::read ( $source . '/version.txt' );
			$info = explode ( "\n", $rawData );
			$fofVersion ['package'] = array (
					'version' => trim ( $info [0] ),
					'date' => new JDate ( trim ( $info [1] ) )
			);
	
			$haveToInstallFOF = $fofVersion ['package'] ['date']->toUNIX () > $fofVersion ['installed'] ['date']->toUNIX ();
		}
	
		$installedFOF = false;
	
		if ($haveToInstallFOF) {
			$versionSource = 'package';
			$installer = new JInstaller ();
			$installedFOF = $installer->install ( $source );
		} else {
			$versionSource = 'installed';
		}
	
		if (! isset ( $fofVersion )) {
			$fofVersion = array ();
	
			if (file_exists ( $target . '/version.txt' )) {
				$rawData = JFile::read ( $target . '/version.txt' );
				$info = explode ( "\n", $rawData );
				$fofVersion ['installed'] = array (
						'version' => trim ( $info [0] ),
						'date' => new JDate ( trim ( $info [1] ) )
				);
			} else {
				$fofVersion ['installed'] = array (
						'version' => '0.0',
						'date' => new JDate ( '2011-01-01' )
				);
			}
	
			$rawData = JFile::read ( $source . '/version.txt' );
			$info = explode ( "\n", $rawData );
			$fofVersion ['package'] = array (
					'version' => trim ( $info [0] ),
					'date' => new JDate ( trim ( $info [1] ) )
			);
			$versionSource = 'installed';
		}
	
		if (! ($fofVersion [$versionSource] ['date'] instanceof JDate)) {
			$fofVersion [$versionSource] ['date'] = new JDate ();
		}
	
		return array (
				'required' => $haveToInstallFOF,
				'installed' => $installedFOF,
				'version' => $fofVersion [$versionSource] ['version'],
				'date' => $fofVersion [$versionSource] ['date']->format ( 'Y-m-d' )
		);
	}
	
	public function preflight($type, $parent) {
		if (parent::preflight ( $type, $parent )) {
			
			$app = JFactory::getApplication ();
			$configuration = JFactory::getConfig ();
			$db = JFactory::getDbo ();
			
			// remove duplicates from the product quantities table
			// $query = 'ALTER TABLE #__jbsmartportfolio_items MODIFY rates integer(11);';
			$query = "ALTER TABLE `#__jbsmartportfolio_items` ADD `language` VARCHAR(255) NOT NULL DEFAULT '*' AFTER `enabled`;";
			$this->_sqlexecute ( $query );
			$query = "ALTER TABLE `#__jbsmartportfolio_items` ADD `item_type` text int(11) NOT NULL AFTER `alias`;";
			$this->_sqlexecute ( $query );
			$query = "ALTER TABLE `#__jbsmartportfolio_items` ADD `alternative_img_1` text NOT NULL AFTER `image`;";
			$this->_sqlexecute ( $query );
			$query = "ALTER TABLE `#__jbsmartportfolio_items` ADD `alternative_img_2` text NOT NULL AFTER `alternative_img_1`;";
			$this->_sqlexecute ( $query );
			$query = "ALTER TABLE `#__jbsmartportfolio_items` ADD `alternative_img_3` text NOT NULL AFTER `alternative_img_2`;";
			$this->_sqlexecute ( $query );
			$query = "ALTER TABLE `#__jbsmartportfolio_items` ADD `alternative_img_4` text NOT NULL AFTER `alternative_img_3`;";
			$this->_sqlexecute ( $query );
			$query = "ALTER TABLE `#__jbsmartportfolio_items` ADD `alternative_img_5` text NOT NULL AFTER `alternative_img_4`;";
			$this->_sqlexecute ( $query );
			$query = "ALTER TABLE `#__jbsmartportfolio_items` ADD `alternative_img_6` text NOT NULL AFTER `alternative_img_5`;";
			$this->_sqlexecute ( $query );
			$query = "ALTER TABLE `#__jbsmartportfolio_items` ADD `alternative_img_7` text NOT NULL AFTER `alternative_img_6`;";
			$this->_sqlexecute ( $query );
			$query = "ALTER TABLE `#__jbsmartportfolio_items` ADD `alternative_img_8` text NOT NULL AFTER `alternative_img_7`;";
			$this->_sqlexecute ( $query );
			$query = "ALTER TABLE `#__jbsmartportfolio_items` ADD `jbsmartportfolio_skill_id` text NOT NULL AFTER `jbsmartportfolio_tag_id`;";
			$this->_sqlexecute ( $query );
			$query = "ALTER TABLE `#__jbsmartportfolio_items` ADD `related_items` text NOT NULL AFTER `jbsmartportfolio_skill_id`;";
			$this->_sqlexecute ( $query );
			$query = "ALTER TABLE `#__jbsmartportfolio_items` ADD `url` text NOT NULL AFTER `jbsmartportfolio_item_tag`;";
			$this->_sqlexecute ( $query );
			$query = "ALTER TABLE `#__jbsmartportfolio_items` ADD `location` text NOT NULL AFTER `url`;";
			$this->_sqlexecute ( $query );
			$query = "ALTER TABLE `#__jbsmartportfolio_items` ADD `client` varchar(255) NOT NULL AFTER `location`;";
			$this->_sqlexecute ( $query );
			$query = "ALTER TABLE `#__jbsmartportfolio_items` ADD `client_url` text NOT NULL AFTER `client`;";
			$this->_sqlexecute ( $query );
			$query = "ALTER TABLE `#__jbsmartportfolio_items` ADD `author_name` varchar(255) NOT NULL AFTER `client`;";
			$this->_sqlexecute ( $query );
			$query = "ALTER TABLE `#__jbsmartportfolio_items` ADD `author_image` text NOT NULL AFTER `author_name`;";
			$this->_sqlexecute ( $query );
			$query = "ALTER TABLE `#__jbsmartportfolio_items` ADD `author_role` varchar(255) NOT NULL AFTER `author_image`;";
			$this->_sqlexecute ( $query );
			$query = "ALTER TABLE `#__jbsmartportfolio_items` ADD `author_quote` text NOT NULL AFTER `author_role`;";
			$this->_sqlexecute ( $query );
			$query = "ALTER TABLE `#__jbsmartportfolio_items` ADD `author_company` varchar(255) NOT NULL AFTER `author_quote`;";
			$this->_sqlexecute ( $query );
			$query = "ALTER TABLE `#__jbsmartportfolio_items` ADD  `social_share` int(11) NOT NULL AFTER `author_company`;";
			$this->_sqlexecute ( $query );
			$query = "ALTER TABLE `#__jbsmartportfolio_items` ADD  `like_count` int(11) NOT NULL AFTER `social_share`;";
			$this->_sqlexecute ( $query );
			
			$query = "CREATE TABLE IF NOT EXISTS `#__jbsmartportfolio_skills` (
				  `jbsmartportfolio_skill_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
				  `title` varchar(255) NOT NULL,
				  `enabled` tinyint(3) NOT NULL DEFAULT '1',
				  `ordering` int(10) NOT NULL DEFAULT '0',
				  `created_by` bigint(20) NOT NULL DEFAULT '0',
				  `created_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
				  `modified_by` bigint(20) NOT NULL DEFAULT '0',
				  `modified_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
				  `locked_by` bigint(20) NOT NULL DEFAULT '0',
				  `locked_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
				  PRIMARY KEY (`jbsmartportfolio_skill_id`)
				) ENGINE=InnoDB  DEFAULT CHARSET=utf8;";
			$this->_sqlexecute ( $query );
			$query = "CREATE TABLE IF NOT EXISTS `#__jbsmartportfolio_rating` (
					  `item_id` int(11) NOT NULL DEFAULT '0',
					  `rating_sum` int(10) unsigned NOT NULL DEFAULT '0',
					  `rating_count` int(10) unsigned NOT NULL DEFAULT '0',
					  `lastip` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
					  PRIMARY KEY (`item_id`)
					) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;";
			$this->_sqlexecute ( $query );
		}
	}
	private function _sqlexecute($query) {
		$db = JFactory::getDbo ();
		$db->setQuery ( $query );
		try {
			$db->execute ();
		} catch ( Exception $e ) {
			// do nothing as customer can do this very well by going to the tools menu
		}
	}
}
