<?php
/**
 * @package     JB Porto Hotel 
 * @subpackage  mod_jbportohotel
 *
 * @copyright   Copyright (C) 2010 - 2015 Joombuff. All rights reserved.
 * @license     GNU General Public License version 2 or later.
 */
defined ( '_JEXEC' ) or die ();
jimport ( 'joomla.filesystem.file' );
//$doc = JFactory::getDocument (); 
//$doc->addStylesheet ( JURI::root ( true ) . '/modules/mod_jbportohotel/theme-elements.css' );
// Params
$cparams = JComponentHelper::getParams ( 'com_jbportohotel' );
?>

<?php  $slider =  $params->get('enable_slider',false); 
 // owl-carousel owl-theme ?>
 
<div  class="jb-smartportfolio jb-portohotel-view-items <?php echo $moduleclass_sfx; ?>">
	<div class="owl-carousel owl-theme mb-none" id="portfolio-style1-<?php echo $module->id;?>">
	 <?php  foreach ( $items as $item ) :
			 $item->url = JRoute::_('index.php?option=com_jbportohotel&view=item&id='.$item->jbportohotel_item_id.':'.$item->alias. ModjbportohotelHelper::getItemid()); ?>
		 <div>
		 <a href="<?php echo $item->url;?>">
			 <img src="<?php echo JUri::root().'/'.$item->image;?>" class="img-responsive" alt="">
		 </a>
		 </div>
	 	<?php endforeach;?>
								 
 	</div>
</div>
<script type="text/javascript">
var $ = jQuery.noConflict();
  $("#portfolio-style1-<?php echo $module->id;?>").owlCarousel({
        items:<?php echo $no_of_items;?>,
        nav:<?php echo $slider_navigation;?>,
        dots :<?php echo $slider_pagination;?>,
        autoplay : <?php echo $autoplay;?>       
        
      });
 
</script>