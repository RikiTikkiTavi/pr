<?php
/**
 * @version    CVS: 1.0.0
 * @package    com_jbeasyformmaker
 * @author     Priya Bose <support@joomlabuff.com>
 * @copyright  2016 www.joomlabuff.com
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access to this file
defined ( '_JEXEC' ) or die ();

/* class JFormFieldFieldtypes extends JFormField */
class JFormFieldFormmodal extends JFormField {
	protected $type = 'formmodal';
	public function getInput() {
		//$html = '
		
		
		return $html;
	}
	public function getLabel() {
		return '';
	}
	public function shortcodes() {
		return array (
				'text' => JText::_ ( 'FIELD_TEXT' ),
				'textarea' => JText::_ ( 'FIELD_TEXTAREA' ),
				'checkbox' => JText::_ ( 'FIELD_CHECKBOX' ),
				'list' => JText::_ ( 'FIELD_LIST' ),
				'count' => JText::_ ( 'FIELD_COUNT' ),
				'date' => JText::_ ( 'FIELD_DATE' ),
				'number' => JText::_ ( 'FIELD_NUMBER' ),
				'submit' => JText::_ ( 'FIELD_SUBMIT' ),
				'response' => JText::_ ( 'FIELD_RESPONSE' ) 
		);
	}
}