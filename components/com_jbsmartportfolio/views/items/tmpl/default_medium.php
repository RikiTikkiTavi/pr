<?php

$dispatcher = JEventDispatcher::getInstance();
JPluginHelper::importPlugin('jbsmartportfolio');

?>

<div class="sort-destination-loader1 sort-destination-loader-showing">
	<ul class="portfolio-list sort-destination" data-sort-id="portfolio">
		<li class="col-md-12 isotope-item mt-xl brands">
			<?php  foreach($this->items as $item):?>
				<?php
					$item->category_title = JbsmartportfolioHelper::getCategoryTitle($item->category_id); 
					$tags = JbsmartportfolioHelper::getTags ( $item->jbsmartportfolio_tag_id );
					$newtags = array ();
					$filter = '';
					$groups = array ();
					foreach ( $tags as $tag ) {
						$newtags [] = $tag->title;
						$filter .= ' ' . $tag->alias;
						$groups [] .= '"' . $tag->alias . '"';
					}
					$groups = implode ( ',', $groups );
					$item->url = JRoute::_ ( 'index.php?option=com_jbsmartportfolio&view=item&id=' . $item->jbsmartportfolio_item_id . ':' . $item->alias . $this->itemId );
					//$results = $dispatcher->trigger('onAfterContentDisplay', array('com_jbsmartportfolio.item', &$item, &$this->params));
					//$item->aftercontentDisplay = trim(implode("\n", $results));
					
					$results = $dispatcher->trigger('onAfterContentTitle', array('com_jbsmartportfolio.item', &$item, &$this->params));
					$item->like_count_html = trim(implode("\n", $results));
					?>
		
			<div class="row">
				<div class="col-md-6">
					<div class="portfolio-item">
						<a href="<?php echo $item->url;?>">
						<span class="thumb-info thumb-info-no-zoom thumb-info-lighten">
							<span class="thumb-info-wrapper">
								<img class="jb-smartportfolio-img img-responsive" src="<?php echo JURI::root() . $item->image;?>" alt="<?php echo $item->title; ?>" />
								<span class="thumb-info-action">
									<span class="thumb-info-action-icon"><i class="fa fa-link"></i></span>
								</span>
							</span>
						</span>
						</a>
					</div>
				</div>
				<div class="col-md-6">
					<div class="portfolio-info">
						<div class="row">
							<div class="col-md-12 center">																
								<ul class="item-fav-icons-list">
							 	<?php if($this->params->get('show_like_count',0)):?>
								 <li>
									 <?php echo $item->like_count_html;?>
											
								</li>
								<?php endif;?>
								
								<?php if($this->params->get('show_created_date',0)):?>
								<li>
									<i class="fa fa-calendar"></i>
									<?php echo JHtml::_('date', $item->created_on, JText::_('DATE_FORMAT_LC3')); ?>
								</li>
								<?php endif;?>
											
								<?php if($this->params->get('show_tags',0) && !empty($newtags)):?>
								<li><i class="fa fa-tags"></i>
									<?php echo implode(', ', $newtags); ?>
								</li>
								<?php endif;?>
							</ul>
							</div>
						</div>
					</div>

					<h4 class="heading-primary"><?php echo $item->title;?></h4>
					<p class="mt-xlg"><?php echo $item->description;?></p>
					<a href="<?php echo $item->url;?>" class="btn btn-primary">Learn More</a>				
					
						<?php if(!empty($item->jbsmartportfolio_skill_id)):?>
						<?php $skills  = JbsmartportfolioHelper::getSkills($item->jbsmartportfolio_skill_id);?>
						
						<ul class="portfolio-details">
							<li>
								<p>
									<strong><?php echo JText::_('JBSMARTPORTFOLIO_SKILLS');?></strong>
								</p>								
							<ul class="list list-inline list-icons">
							<?php foreach($skills as $skill):?>
								<li><i class="fa fa-check-circle"></i><?php echo $skill->title;?></li>
							<?php endforeach;?>						
							</ul>								 
						</ul>
						<?php endif;?>					 
				</div>
			</div>
			<?php endforeach;?>			
		</li>
	</ul>
</div>

