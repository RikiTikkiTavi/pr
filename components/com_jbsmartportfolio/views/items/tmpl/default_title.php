<?php
/**
 * @package     JB Smart Portfolio
 *
 * @copyright   Copyright (C) 2016 Joomlabuff. All rights reserved.
 * @license     GNU General Public License version 2 or later.
 */
defined ( '_JEXEC' ) or die ();
?>
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="portfolio-title">
				<div class="row">
					<div class="portfolio-nav-all col-md-1">
							<?php if($this->params->get('show_back_to_list',0)):?>							
								<a href="<?php echo $this->link;?>" data-tooltip data-original-title="Back to list">
									<i class="fa fa-th"></i>
								</a>
							<?php endif;?>	
					 </div>
					<div class="col-md-10 center">
						 <?php if($this->params->get('show_sview_title',0)):?>
						 <h2 class="mb-none"><?php echo $this->item->title; ?></h2>
						 <?php endif;?>
					 </div>
					<div class="portfolio-nav col-md-1">
					 <?php if($this->params->get('show_navigation')):?>
						<?php echo $this->item->pagination ; ?>
					 <?php endif;?>
					 </div>
				</div>
			</div> 
		</div>
	</div>
</div>
