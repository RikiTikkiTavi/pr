<?php
/**
 * @package     JB Porto Hotel 
 *
 * @copyright   Copyright (C) 2016 Joomlabuff. All rights reserved.
 * @license     GNU General Public License version 2 or later.
 */

defined('_JEXEC') or die();

JLoader::register('JHtmlString', JPATH_LIBRARIES.'/joomla/html/html/string.php');

class JbportohotelHelper {

	public static function generateMeta($item) {
		$document = JFactory::getDocument();
		$app = JFactory::getApplication();
		$menus = $app->getMenu();
		$menu = $menus->getActive();
		$title = null;

		$document->setTitle($item->title);
		$document->addCustomTag('<meta content="website" property="og:type"/>');
		$document->addCustomTag('<meta content="'.JURI::current().'" property="og:url" />');
		$document->setDescription( JHtml::_('string.truncate', $item->description, 155, false, false ) );
		$document->addCustomTag('<meta content="'. $item->title .'" property="og:title" />');
		$document->addCustomTag('<meta content="'. JURI::root().$item->image.'" property="og:image" />');
		$document->addCustomTag('<meta content="'. JHtml::_('string.truncate', $item->description, 155, false, false ) .'" property="og:description" />');
	
		return true;
	}

	public static function getTags($ids) {
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);

		if(!is_array($ids)) {
			$ids = (array) json_decode($ids);
		}

		$ids = implode(',', $ids);

		$query->select($db->quoteName(array('jbportohotel_tag_id', 'title', 'alias')));
		$query->from($db->quoteName('#__jbportohotel_tags'));
		$query->where($db->quoteName('jbportohotel_tag_id')." IN (" .$ids . ")");
	 	$query->where('language in (' . $db->quote(JFactory::getLanguage()->getTag()) . ',' . $db->quote('*') . ')');
		
		$db->setQuery($query);

		return $db->loadObjectList();
	}


	public static function getTagList($items) {
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);

		$tags = array();

		foreach ($items as $item) {
			$itemtags = json_decode( $item->jbportohotel_tag_id );
			foreach ($itemtags as $itemtag) {
				$tags[] = $itemtag;
			}
		}

		$json = json_encode(array_unique($tags));

		$result = self::getTags( $json );

		return $result;
	}
	
	/**
	 * Method to get category title
	 * @param int $id
	 */
	public static function getCategoryTitle($id) {
		$db = JFactory::getDbo ();
		$query = $db->getQuery ( true );
		$query->select ( 'title' )->from ( '#__categories' )->where ( 'id=' . $db->q ( $id ) );
		$query->where ( 'extension ="com_jbportohotel"' );
		$db->setQuery ( $query );
		return $category_title = $db->loadResult ();
	}
	
	public static function getItemImages($item) {
		$images = array();
	
		$image_path = JURI::root();
			
		if($item->image !==''){
			$images[] = $image_path.'/'.$item->alternative_img_1;
		}
			
			
		if($item->alternative_img_1 !=='' ){
			$images[] = $image_path.'/'.$item->alternative_img_1;
		}
	
	 /* 
			
		if($item->alternative_img_2 !=='' ){
			$images[] =$image_path.'/'.$item->alternative_img_2;
		}
			
			
		if($item->alternative_img_3 !=='' ){
			$images[] = $image_path.'/'.$item->alternative_img_3;
		}
			
			
		if($item->alternative_img_4 !==''  ){
			$images[] = $image_path.'/'.$item->alternative_img_4;
		} */
			
		 
			
		return  $images;
	
	}
	
	 
}
