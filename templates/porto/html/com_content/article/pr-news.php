<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_content
 *
 * @copyright   Copyright (C) 2005 - 2018 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

// Create a shortcut for params.
$params = $this->item->params;
JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
$canEdit = $this->item->params->get('access-edit');
$info    = $params->get('info_block_position', 0);

// Check if associations are implemented. If they are, define the parameter.
$assocParam = (JLanguageAssociations::isEnabled() && $params->get('show_associations'));


$document   = JFactory::getDocument();
$document->setTitle($this->item->title );
?>

<div class="container">
	<div class="row">
		<div class="col-md-12">
			<h2><?=$this->item->title?></h2>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<?=$this->item->fulltext?>
		</div>
	</div>
</div>
