<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_articles_latest
 *
 * @copyright   Copyright (C) 2005 - 2018 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined( '_JEXEC' ) or die;

JHtml::addIncludePath( JPATH_COMPONENT . '/helpers' );

JHtml::_( 'behavior.caption' );

jimport( 'joomla.application.module.helper' );

$news_quantity = count( $list );
if ( $news_quantity > 6 )
{
	$list_1          = array_slice( $list, 0, 6 );
	$list_2_quantity = $news_quantity - 6;
	$list_2          = array_slice( $list, 6, $list_2_quantity );
}
else
{
	$list_1 = $list;
	$list_2 = [];
}
?>

<div class="row pr-latest-news-block">
    <div class="col-sm-6 col-md-6">
		<?php foreach ( $list_1 as $item ):
			$link = $item->link
			?>
            <div class="feature-box">
                <div class="feature-box-icon">
                    <i class="fa fa-newspaper-o" aria-hidden="true"></i>
                </div>
                <div class="feature-box-info">
                    <h4 class="heading-primary mb-none"><a href="<?= $link ?>"><?= $item->title ?></a></h4>
                    <?= $item->introtext ?>
                </div>
            </div>
		<?php endforeach ?>
    </div>
    <div class="col-sm-6 col-md-6">
	    <?php foreach ( $list_2 as $item ):
		    $link = JRoute::_( ContentHelperRoute::getCategoryRoute( $item->catslug ) ) . '/' . $item->id
		    ?>
            <div class="feature-box">
                <div class="feature-box-icon">
                    <i class="fa fa-newspaper-o" aria-hidden="true"></i>
                </div>
                <div class="feature-box-info">
                    <h4 class="heading-primary mb-none"><a href="<?= $link ?>"><?= $item->title ?></a></h4>
                    <?= $item->introtext ?>
                </div>
            </div>
	    <?php endforeach ?>
    </div>
</div>

