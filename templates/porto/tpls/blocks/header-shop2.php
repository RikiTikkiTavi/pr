
<?php
/**
 * ------------------------------------------------------------------------
 * JB Porto template
 * ------------------------------------------------------------------------
 * Copyright (C) 2004-2011 J.O.O.M Solutions Co., Ltd. All Rights Reserved.
 * @license - Copyrighted Commercial Software
 * Author: J.O.O.M Solutions Co., Ltd
 * Websites:  http://www.joomlart.com -  http://www.joomlancers.com
 * This file may not be redistributed in whole or significant part.
 * ------------------------------------------------------------------------
 */

defined('_JEXEC') or die;

// get params
$sitename  = $this->params->get('sitename');
$slogan    = $this->params->get('slogan', '');
$logotype  = $this->params->get('logotype', 'text');
$logoimage = $logotype == 'image' ? $this->params->get('logoimage', T3Path::getUrl('images/logo.png', '', true)) : '';
$logoimgsm = ($logotype == 'image' && $this->params->get('enable_logoimage_sm', 0)) ? $this->params->get('logoimage_sm', T3Path::getUrl('images/logo-sm.png', '', true)) : false;

if (!$sitename) {
	$sitename = JFactory::getConfig()->get('sitename');
}

$headright = $this->countModules('head-search or languageswitcherload');


?>

<!-- HEADER -->
<header>
	<div class="jb-header header-body">
		<div class="container">
			<div class="row">
				 <!-- LOGO -->
				 <div class="col-xs-5 col-sm-3 col-md-2 col-lg-2 logo">
					<div class="logo-<?php echo $logotype, ($logoimgsm ? ' logo-control' : '') ?>">
						<a href="<?php echo JURI::base(true) ?>" title="<?php echo strip_tags($sitename) ?>">
							<?php if($logotype == 'image'): ?>
								<img class="logo-img" src="<?php echo JURI::base(true) . '/' . $logoimage ?>" alt="<?php echo strip_tags($sitename) ?>" />
							<?php endif ?>
							<?php if($logoimgsm) : ?>
								<img class="logo-img-sm" src="<?php echo JURI::base(true) . '/' . $logoimgsm ?>" alt="<?php echo strip_tags($sitename) ?>" />
							<?php endif ?>
							<span><?php echo $sitename ?></span>
						</a>
						<small class="site-slogan"><?php echo $slogan ?></small>
						</div>
				 </div>
				<!-- //LOGO -->
				<div class="shop-header-block">
					<div class="col-xs-12 col-sm-8 col-md-6 col-lg-6 hidden-xs">
						<?php if ($this->countModules('vm-search')) : ?>
							<!-- vm-search -->
								<jdoc:include type="modules" name="<?php $this->_p('vm-search') ?>" style="raw" />
							<!-- //vm-search -->
						<?php endif ?>
					</div>
				
					<div class="col-xs-6 col-sm-8 col-md-6 col-lg-4">
						
							<?php if ($this->countModules('cart')) : ?>
								<!-- cart -->
									<div class="cart">
										<jdoc:include type="modules" name="<?php $this->_p('cart') ?>" style="raw" />
									</div>
								<!-- //cart -->
							<?php endif ?>
						
					</div>
				</div>
				
				
				
				
			</div>
	  </div>
	</div>
</header>
<!-- //HEADER -->
