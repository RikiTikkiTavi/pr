<?php
/**
 * @package     JB Smart Portfolio
 *
 * @copyright   Copyright (C) 2016 Joomlabuff. All rights reserved.
 * @license     GNU General Public License version 2 or later.
 */

defined('_JEXEC') or die();
 
require_once JPATH_COMPONENT . '/helpers/helper.php';
JbsmartportfolioHelper::generateMeta($this->item);

$app = JFactory::getApplication(); 
JPluginHelper::importPlugin('jbsmartportfolio');
$jitem  = $app->triggerEvent('onContentBeforeDisplay', array('com_jbsmartportfolio.item', &$this->item, &$params, 1));
$menu 	= JFactory::getApplication()->getMenu();		
$this->itemId = '';
if(is_object($menu->getActive())) {
	$active = $menu->getActive();
   $this->itemId = $active->id;
}
 
$this->params = $menu->getParams($this->itemId);

$doc = JFactory::getDocument();
//$doc->addScript('http://code.jquery.com/jquery-latest.min.js');
$this->link = JRoute::_("index.php?Itemid={$this->itemId}");	

$doc->addStylesheet( JURI::root(true) . '/components/com_jbsmartportfolio/assets/css/jbsmartportfolio.css' );

 
 

$doc->addScript( JURI::root(true) . '/components/com_jbsmartportfolio/assets/owl.carousel/owl.carousel.min.js' );
$doc->addStylesheet( JURI::root(true) . '/components/com_jbsmartportfolio/assets/owl.carousel/assets/owl.carousel.min.css' );
$doc->addStylesheet( JURI::root(true) . '/components/com_jbsmartportfolio/assets/owl.carousel/assets/owl.theme.default.min.css' );

$doc->addStylesheet(JURI::root(true) . '/components/com_jbsmartportfolio/assets/magnific-popup/css/magnific-popup.min.css');
$doc->addScript(JURI::root(true) . '/components/com_jbsmartportfolio/assets/magnific-popup/js/jquery.magnific-popup.min.js');
$doc->addScript(JURI::root(true) . '/components/com_jbsmartportfolio/assets/isotope/jquery.isotope.min.js');?>
 

<?php 
$doc->addScript(JURI::root(true) . '/components/com_jbsmartportfolio/assets/stickysidebar/stickySidebar.js'); 
$doc->addStyleSheet(JURI::root(true) . '/components/com_jbsmartportfolio/assets/stickysidebar/style.css');
$tags = JbsmartportfolioHelper::getTags( (array) $this->item->jbsmartportfolio_tag_id );
$newtags = array();
foreach ($tags as $tag) {
	$newtags[] 	 = '<a href="#">'.$tag->title.'</a>';
}
$this->video_src=""; 
//video
if($this->item->video) {
	$video = parse_url($this->item->video);
	
	switch($video['host']) {
		case 'youtu.be':
		$video_id 	= trim($video['path'],'/');
		$this->video_src 	= '//www.youtube.com/embed/' . $video_id;
		break;

		case 'www.youtube.com':
		case 'youtube.com':
		parse_str($video['query'], $query);
		$video_id 	= $query['v'];
		$this->video_src 	= '//www.youtube.com/embed/' . $video_id;
		break;

		case 'vimeo.com':
		case 'www.vimeo.com':
		$video_id 	= trim($video['path'],'/');
		$this->video_src 	= "//player.vimeo.com/video/" . $video_id;
	}

}

$this->tags = JbsmartportfolioHelper::getTags ( $this->item->jbsmartportfolio_tag_id );
$this->newtags = array ();
foreach ( $this->tags as $tag ) {
	$this->newtags[] = $tag->title;
}

$dispatcher = JEventDispatcher::getInstance();
JPluginHelper::importPlugin('jbsmartportfolio');
$results = $dispatcher->trigger('onAfterContentDisplay', array('com_jbsmartportfolio.item', &$this->item, &$this->params));
$this->item->aftercontentDisplay = trim(implode("\n", $results));

$results = $dispatcher->trigger('onAfterContentTitle', array('com_jbsmartportfolio.item', &$this->item, &$this->params));
$this->item->like_count_html = trim(implode("\n", $results));
?>
<style>
.btn-favourite {
     background:none!important;
     border:none; 
     padding:0!important;
     font: inherit;
     /*border is optional*/
     border-bottom:none; 
     cursor: pointer;
}
.item-fav-icons-list li{
  list-style:none;
  display:inline;
  margin:5px !important;
  border-right:1px solid #000000; 
  
}
.form-favourite{
width: 420px; height: 0px;
}
</style>
 
 