<?php 

JHtml::_('behavior.modal'); 
?>
<style>
.modal{
	display:block;
	position:relative;
	}
</style>
 <div class="owl-carousel owl-theme manual featured-products-carousel" id="mod-jb-vm-ps-<?php echo $module->id;?>">
	<?php foreach($products as $product):?>
	<?php $url = JRoute::_ ('index.php?option=com_virtuemart&view=productdetails&virtuemart_product_id=' . $product->virtuemart_product_id . '&virtuemart_category_id=' .$product->virtuemart_category_id); 
	
	$quickview = JRoute::_ ('index.php?option=com_virtuemart&view=productdetails&virtuemart_product_id=' . $product->virtuemart_product_id . '&virtuemart_category_id=' .$product->virtuemart_category_id.'&amp;tmpl=component');
	?>
	<div class="product">
		<figure class="product-image-area">
			<?php
				if (!empty($product->images[0])) {
					$image = $product->images[0]->displayMediaThumb ('class="featuredProductImage"', FALSE);
				} else {
					$image = '';
				}
				echo JHTML::_ ('link', JRoute::_ ('index.php?option=com_virtuemart&view=productdetails&virtuemart_product_id=' . $product->virtuemart_product_id . '&virtuemart_category_id=' . $product->virtuemart_category_id), $image, array('title' => $product->product_name));
				echo '<div class="clear"></div>';
				?>
			 
			<a href="<?php echo $quickview;?>" class="modal product-quickview">
				<i class="fa fa-share-square-o"></i>
				<span><?php echo JText::_( 'MOD_JB_VIRTUEMART_PRODUCTSLIDER_QUICKVIEW' ); ?></span>
			</a>  
			 
		</figure>
		 <div class="product-details-area">
							<h2 class="product-name">
								<a href="<?php echo $url ?>" title="Product Name">
									<?php echo $product->product_name; ?>
								</a>
							</h2>
							 
							<div class="product-ratings">
								<?php echo shopFunctionsF::renderVmSubLayout('rating',array('showRating'=>1,'product'=>$product)); ?>
								 
							</div>
						 <?php if ($show_price and  isset($product->prices)):?>
							<?php echo shopFunctionsF::renderVmSubLayout('prices',array('product'=>$product,'currency'=>$currency)); ?>
						  <?php endif;?>
										 

							<div class="product-actions">
								<?php if ($show_addtocart):?>
								<a href="<?php echo $url ?>" class="addtocart" title="Add to Cart">
									<i class="fa fa-shopping-cart"></i>
									<span>Add to Cart</span>
								</a> 
								<?php endif;?>
  
							</div>
						</div>
		</div>
 	<?php endforeach;?>				
</div>
 <script type="text/javascript">
 jQuery('#mod-jb-vm-ps-<?php echo $module->id;?>').owlCarousel({				 	 			
			loop:'<?php echo $loop ;?>', 
			nav:'<?php echo $nav ;?>',
			navText: ["",""],
			dots:'<?php echo $pagination;?>',
			autoplay:'<?php echo $autoplay;?>',
			responsiveClass:true,
			responsive:{
			0:{
				items:1,
				nav:true
			},
			600:{
				items:1,
				nav:false
			},
			700:{
				items:<?php echo $items ;?>,
				nav:false
			},
			1000:{
				items:<?php echo $items ;?>,
				nav:false,
				loop:false
			}
    }
			
			
			
		}); 	
 
 </script>
