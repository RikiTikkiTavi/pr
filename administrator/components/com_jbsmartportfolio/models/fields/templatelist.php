<?php
 
defined('_JEXEC') or die('Restricted access');

jimport('joomla.html.html');
jimport('joomla.form.formfield');
jimport('joomla.form.helper');
JFormHelper::loadFieldClass('list');

 
class JFormFieldTemplateList extends JFormFieldList {

	protected $type = 'TemplateList';

	public function getInput() {


		jimport('joomla.filesystem.folder');
		$mainframe = JFactory::getApplication();
		$fieldName =  $this->name ;
		$componentPath =  JPATH_SITE.'/components/com_jbsmartportfolio/views/templates/';
		$componentFolders = JFolder::folders($componentPath);
		$db = JFactory::getDBO();
		$query = "SELECT template FROM #__template_styles WHERE client_id = 0 AND home = 1";
		$db->setQuery($query);
		$defaultemplate = $db->loadResult();
		if (JFolder::exists(JPATH_SITE.'/templates/'.$defaultemplate.'/html/com_jbsmartportfolio/templates/'))
		{
			$templatePath = JPATH_SITE.'/templates/'.$defaultemplate.'/html/com_jbsmartportfolio/templates/';
		}
		else
		{
			 $templatePath = JPATH_SITE.'/templates/'.$defaultemplate.'/com_jbsmartportfolio/templates/';
			 //$templatePath = $componentPath;
		}

	//	echo $templatePath; exit;
		if (JFolder::exists($templatePath))
		{
			$templateFolders = JFolder::folders($templatePath);
			$folders = @array_merge($templateFolders, $componentFolders);
			$folders = @array_unique($folders);
		}
		else
		{
			$folders = $componentFolders;
		}
		
		$exclude = 'default';
		$options = array();
		foreach ($folders as $folder)
		{
			if (preg_match(chr(1).$exclude.chr(1), $folder))
			{
				continue;
			}
			if($folder != 'tmpl') {
				$options[] = JHTML::_('select.option', $folder, $folder);
			}
		}

		array_unshift($options, JHTML::_('select.option', 'default', JText::_('JOOMLA_USE_DEFAULT')));

		return JHTML::_('select.genericlist', $options, $fieldName, 'class="inputbox"', 'value', 'text', $this->value, $this->control_name.$this->name);
	}

}
