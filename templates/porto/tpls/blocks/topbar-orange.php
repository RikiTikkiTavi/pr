<?php
/**
 * @package   jb_creativ
 * @copyright Copyright (C) 2015 - 2023 Open Source Matters, Inc. All rights reserved.
 * @license   GNU General Public License version 2 or later; see LICENSE.txt
 */
 
 defined('_JEXEC') or die;
 ?>
 
<!-- TOPBAR -->
<?php if ($this->countModules('topbar-left') || $this->countModules('head-search') ) : ?>
 <div id="jb-topbar" class="wrap header-top topbar-orange hidden-xs">
	<div class="container">
		<div class="row">
			<div class="pull-right-remove">
		<?php if ($this->countModules('topbar-left')) : ?>
			<div class="col-xs-12 col-sm-6 col-md-6  text-left t3-topbar-1  <?php $this->_c('topbar-left') ?>">
				<jdoc:include type="modules" name="<?php $this->_p('topbar-left') ?>" style="T3Xhtml" />
			</div>
		<?php endif ?>
		
		<div class="right-block">
			<?php if ($this->countModules('head-search')) : ?>
				<!-- HEAD SEARCH -->
					<div class="head-search">
						<jdoc:include type="modules" name="<?php $this->_p('head-search') ?>" style="raw" />
					</div>
				<!-- //HEAD SEARCH -->
			<?php endif ?>
		</div>
		</div>
		</div>
	</div>
</div>
<?php endif ?>
 <!-- //TOPBAR -->
