
<?php
/**
 * @package     JB Porto Hotel 
 *
 * @copyright   Copyright (C) 2016 Joomlabuff. All rights reserved.
 * @license     GNU General Public License version 2 or later.
 */

defined('_JEXEC') or die();

require_once JPATH_COMPONENT . '/helpers/helper.php';
jimport( 'joomla.filesystem.file' );
$layout_type = $this->params->get('layout_type', 'default');

//$doc = JFactory::getDocument ();
//$doc->addStylesheet ( JURI::root ( true ) . '/modules/mod_jbportohotel/theme-elements.css' );

$columns = $this->params->get('columns', 3);

$col ='col-md-6';
if($columns == 2){
	$col ='col-md-6';
}elseif($columns == 3){
	$col ='col-md-4';
}elseif($columns == 4){
	$col ='col-md-3';
}elseif($columns == 5){
	$col ='col-md-1-5';
}elseif($columns == 6){
	$col ='col-md-2';
}

//Load the method jquery script.
JHtml::_('jquery.framework');

//Params
$params 	= JComponentHelper::getParams('com_jbportohotel');
$square 	= strtolower( $params->get('square', '600x600') );
$rectangle 	= strtolower( $params->get('rectangle', '600x400') );
$tower 		= strtolower( $params->get('tower', '600x800') );

//Add js and css files
$doc = JFactory::getDocument();
$doc->addStylesheet( JURI::root(true) . '/components/com_jbportohotel/assets/css/featherlight.min.css' );
$doc->addStylesheet( JURI::root(true) . '/components/com_jbportohotel/assets/css/jbportohotel.css' );
$doc->addScript( JURI::root(true) . '/components/com_jbportohotel/assets/js/jquery.shuffle.modernizr.min.js' );
$doc->addScript( JURI::root(true) . '/components/com_jbportohotel/assets/js/featherlight.min.js' );
$doc->addScript( JURI::root(true) . '/components/com_jbportohotel/assets/js/jbportohotel.js' );

 

$menu 	= JFactory::getApplication()->getMenu();
$itemId = '';
if(is_object($menu->getActive())) {
	$active = $menu->getActive();
	$itemId = '&Itemid=' . $active->id;
}
 
?>
 <div class="container">
		<?php if( $this->params->get('show_page_heading') && $this->params->get( 'page_heading' ) ):?>
		<h1><?php echo $this->params->get( 'page_heading');?></h1>
	<?php endif;?>

	<?php if($this->params->get('show_filter', 1)): ?>
		<div class="jb-portohotel-filter">
			<ul   class="nav nav-pills sort-source" data-sort-id="portfolio" data-option-key="filter" data-plugin-options='{"layoutMode": "masonry", "filter": "*"}'>
				<li data-option-value="*" class="active" data-group="all"><a href="#"><?php echo JText::_('COM_JBPORTOHOTEL_SHOW_ALL'); ?></a></li>
				<?php $filters = jbportohotelHelper::getTagList( $this->items ); ?>
				<?php   foreach ($filters as $filter): ?>
				<li  data-option-value=".<?php echo $filter->alias; ?>" data-group="<?php echo $filter->alias; ?>">
					<a href="#"><?php echo $filter->title; ?></a>
				</li>
				<?php  endforeach ?>
			</ul>
		</div>
	<?php endif; ?>
	<hr>
	<div class="row">
		<div class="sort-destination-loader1 sort-destination-loader-showing">
			<ul class="portfolio-list sort-destination jb-portohotel-items" data-sort-id="portfolio">
				<?php 
				
				
				foreach($this->items as $item):?>
				<?php				
				
								
								$tags = jbportohotelHelper::getTags( $item->jbportohotel_tag_id );
								$newtags = array();
								$filter = '';
								$groups = array();
								foreach ($tags as $tag) {
									$newtags[] 	 = $tag->title;
									$filter 	.= ' ' . $tag->alias;
									$groups[] 	.= '"' . $tag->alias . '"';
								}
								$groups = implode(',', $groups);
					?>
								<?php $item->url = JRoute::_('index.php?option=com_jbportohotel&view=item&id='.$item->jbportohotel_item_id.':'.$item->alias . $itemId); ?>
									<li class="<?php echo $col;?> jb-portohotel-item isotope-item " data-groups='[<?php echo $groups; ?>]'>
										<div class="portfolio-item">
											<a href="<?php echo $item->url;?>">
												<span class="thumb-info thumb-info-centered-info thumb-info-no-borders">
													<span class="thumb-info-wrapper">
														<img class="jb-portohotel-img img-responsive" src="<?php echo JURI::root() . $item->image;?>" alt="<?php echo $item->title; ?>">
														
														<span class="thumb-info-title">
															<span class="thumb-info-inner"><?php echo $item->title;?></span>
															<span class="thumb-info-type"><?php echo implode(',',$newtags);?></span>
														</span>
														<span class="thumb-info-action">
															<span class="thumb-info-action-icon"><i class="fa fa-link"></i></span>
														</span>
													</span>
												</span>
											</a>
										</div>
									</li>
								<?php endforeach;?>							 
							</ul>
							<?php if ($this->pagination->get('pages.total') >1) { ?>
							<div class="pagination">
								<?php echo $this->pagination->getPagesLinks(); ?>
							</div>
							<?php } ?>
					</div>
				</div>
			</div>
		</div>

