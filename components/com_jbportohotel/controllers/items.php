<?php
/**
 * @package     JB Porto Hotel 
 *
 * @copyright   Copyright (C) 2016 Joomlabuff. All rights reserved.
 * @license     GNU General Public License version 2 or later.
 */
defined('_JEXEC') or die();
 
class JbportohotelControllerItems extends F0FController
{

	public function __construct($config = array())
	{
		parent::__construct($config);
	} 
	 

	public function onBeforeBrowse()
	{
		
		$app		= JFactory::getApplication();
		$params		= $app->getParams();
		$menu 	= JFactory::getApplication()->getMenu();
		$active = $menu->getActive();
		$menu_params = $menu->getParams($active->id);

		$this->getThisModel()->limit( $params->get('limit', 12) );
		$this->getThisModel()->limitstart($this->input->getInt('limitstart', 0));
	
		return true;
	}
	
 
}
