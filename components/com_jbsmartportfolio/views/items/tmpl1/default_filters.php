
<?php
/**
 * @package     JB Smart Portfolio
 *
 * @copyright   Copyright (C) 2016 Joomlabuff. All rights reserved.
 * @license     GNU General Public License version 2 or later.
 */

defined('_JEXEC') or die(); ?>
<?php if($this->params->get('show_filter', 1)): ?>
		 <div class="jb-smartportfolio-filter">  
			<ul   class="nav nav-pills sort-source" data-sort-id="portfolio" data-option-key="filter" data-plugin-options='{"layoutMode": "masonry", "filter": "*"}'>
				<li data-option-value="*" class="active" data-group="all"><a href="#"><?php echo JText::_('COM_JBSMARTPORTFOLIO_SHOW_ALL'); ?></a></li>
				<?php $filters = JbsmartportfolioHelper::getTagList( $this->items ); ?>
				
				<?php   foreach ($filters as $filter): ?>
				<li  data-option-value=".<?php echo $filter->alias; ?>" data-group="<?php echo $filter->alias; ?>">
					<a href="#"><?php echo $filter->title; ?></a>
				</li>
				<?php  endforeach ?>
			</ul>
		  </div> 
	<?php endif; ?>
