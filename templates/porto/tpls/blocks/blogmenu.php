<?php
/**
 * ------------------------------------------------------------------------
 * TP Runway template
 * ------------------------------------------------------------------------
 * Copyright (C) 2014-2022 Themeparrot.com. All Rights Reserved.
 * @license - Copyrighted Commercial Software
 * Author: Themeparrot
 * Websites:  http://www.themeparrot.com
 * ------------------------------------------------------------------------
 */

defined('_JEXEC') or die;
?>

<?php if ($this->countModules('blogmenu')) : ?>
	<!-- MASTHEAD -->
	<div id="blogmenu" class="wrap t3-sl-1 <?php $this->_c('blogmenu') ?>">
		<div class="container"
			<jdoc:include type="modules" name="<?php $this->_p('blogmenu') ?>" style="T3Xhtml" />
		</div>
	</div>
	<!-- //MASTHEAD -->
<?php endif ?>
