<?php
/**
 * ------------------------------------------------------------------------
 * TP Runway template
 * ------------------------------------------------------------------------
 * Copyright (C) 2014-2022 Themeparrot.com. All Rights Reserved.
 * @license - Copyrighted Commercial Software
 * Author: Themeparrot
 * Websites:  http://www.themeparrot.com
 * ------------------------------------------------------------------------
 */

defined('_JEXEC') or die;
?>

<?php if ($this->countModules('masthead')) : ?>
	<!-- MASTHEAD -->
	<div class="wrap t3-sl-1 jb-breadcrumb page-header <?php $this->_c('masthead') ?>">
		<div class="container">
			<jdoc:include type="modules" name="<?php $this->_p('masthead') ?>" style="T3Xhtml" />
		</div>
	</div>
	<!-- //MASTHEAD -->
<?php endif ?>
