<?php
/**
 *
 * Show the products in a category
 *
 * @package    VirtueMart
 * @subpackage
 * @author RolandD
 * @author Max Milbers
 * @todo add pagination
 * @link http://www.virtuemart.net
 * @copyright Copyright (c) 2004 - 2010 VirtueMart Team. All rights reserved.
 * @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
 * VirtueMart is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 * @version $Id: default.php 8811 2015-03-30 23:11:08Z Milbo $
 */

defined ('_JEXEC') or die('Restricted access');

$app 	  = JFactory::getApplication();
$template = $app->getTemplate();
 
//$show_vmcategory_image = $helix3->getParam('show_vmcategory_image', 1);

?>
<style type="text/css">


.orderby-displaynumber {
  margin: 0 0 30px;
  padding: 6px 0;
}
.orderby-displaynumber .view-mode a {
  width: 35px;
  height: 35px;
  text-align: center;
  line-height: 35px;
  border: 1px solid #e5e5e5;
  display: block;
  float: left;
  margin-left: 8px;
}
.rtl .orderby-displaynumber .view-mode a {
  float: right;
}
.rtl .orderby-displaynumber .view-mode a {
  margin-right: 8px;
  margin-left: 0;
}
.orderby-displaynumber .view-mode a span {
  font-size: 0;
  display: none;
}
.orderby-displaynumber .view-mode .mode-grid {
  background: url(../images/icons/grid.png) center no-repeat;
}
.orderby-displaynumber .view-mode .mode-list {
  margin-left: 4px;
  background: url(../images/icons/list.png) center no-repeat;
}
.rtl .orderby-displaynumber .view-mode .mode-list {
  margin-right: 4px;
  margin-left: 0;
}


.product-i {
  margin: 0 0 25px !important;
}
.product-i .vm-product-media-container {
  position: relative;
}
.product-i .item-i {
  padding: 0 15px;
  position: relative;
}
.product-i .product-fields {
  display: none;
}
.product-i .price-rating {
  text-align: center;
}
.product-i .product-status .label-pro {
  position: absolute;
  z-index: 1;
  text-align: center;
  color: #fff;
  width: 50px;
  top: 30px;
  line-height: 28px;
  font-size: 13px;
}
.product-i .product-status .label-pro.status-hot {
  background: #fd5b4e;
}
.product-i .product-status .status-sale,
.product-i .product-status .status-new-sale {
  left: 12px;
}
.rtl .product-i .product-status .status-sale,
.rtl .product-i .product-status .status-new-sale {
  left: auto;
  right: 12px;
}
.product-i .product-status .status-new {
  right: 12px;
}
.rtl .product-i .product-status .status-new {
  left: 12px;
  right: auto;
}
.product-i .image-block {
  position: relative;
  overflow: hidden;
}
.product-i .image-block a {
  display: block;
}
.product-i .image-block .pro-image {
  -webkit-transition: all 0.5s ease 0s;
  -moz-transition: all 0.5s ease 0s;
  -o-transition: all 0.5s ease 0s;
  transition: all 0.5s ease 0s;
  position: relative;
}
.product-i .image-block .pro-image:before {
  content: "";
  position: absolute;
  height: 0;
  width: 100%;
  background-color: rgba(34,34,34,0.3);
  bottom: 100%;
  left: 0;
  transition: 0.5s;
}
.product-i .image-block .second-image {
  position: absolute;
  top: 20%;
  transition: all 0.7s;
  max-width: 100%;
  left: 0;
  opacity: 0;
}
.rtl .product-i .image-block .second-image {
  left: auto;
  right: 0;
}
.product-i .text-block {
  overflow: hidden;
  padding: 20px 0;
  margin-bottom: 1px;
}
.product-i .text-block .product-title {
  font-size: 14px;
  text-transform: uppercase;
  text-align: center;
  transition: 0.7s;
  margin-top: 0;
}
.product-i .text-block .price-rating {
  transition: 0.7s;
}
.product-i .text-block .box-review {
  margin-bottom: 20px;
}
.product-i .text-block .product_s_desc {
  clear: both;
  display: none;
}
.product-i .quickview {
  position: absolute;
  top: 40%;
  width: 100%;
  left: 0;
  display: inherit;
  opacity: 0;
  text-align: center;
}
.product-i .btn-quickview {
  width: 60px;
  height: 60px;
  border-radius: 100%;
  background: #fff;
  margin: auto;
  border: 0;
}
.product-i .btn-quickview i {
  border-radius: 100%;
  line-height: 43px;
  display: block;
}
.product-i .btn-quickview i:before {
  font-size: 41px;
  content: "+";
  font-weight: 500;
  color: #000;
  line-height: 53px;
}
.product-i .btn-quickview a {
  border-radius: 100%;
  display: block;
  line-height: 0px;
}
.product-i .btn-quickview span {
  font-size: 0px;
}
.product-i .btn-actions {
  display: inline-block;
  margin: auto;
}
.product-i .actions {
  opacity: 0;
  transition: 0.7s;
  position: relative;
  display: inline-flex;
  position: absolute;
  width: 100%;
  left: 0;
  margin-top: -20px;
  text-align: center;
}
.product-i .actions i {
  line-height: 36px;
  display: block;
}
.product-i .actions .btn-wishlist i {
  line-height: 30px;
}
.product-i .actions .btn-wishlist i:before {
  content: '\f15f';
  font-family: 'Material-Design-Iconic-Font';
  font-size: 16px;
  line-height: 35px;
}
.product-i .actions .addtocart-area {
  display: inline-block;
  line-height: 30px;
  margin: 0 10px;
}
.product-i .actions .addtocart-area .quantity-box,
.product-i .actions .addtocart-area .lbl_qty,
.product-i .actions .addtocart-area .quantity-controls {
  display: none;
}
.product-i .actions .addtocart-area .notify {
  border: 2px solid #e5e5e5;
  padding: 6px 25px;
  line-height: 3;
}
.product-i .actions .btn-group {
  width: 40px;
  height: 40px;
  overflow: hidden;
  text-align: center;
  border: 2px solid #e5e5e5;
}
.product-i .actions .btn-group span {
  display: none;
  visibility: hidden;
}
.product-i .actions .btn-group a,
.product-i .actions .btn-group i {
  color: #222;
  display: inline-block;
}
.product-i .actions .btn-details i {
  line-height: 30px;
}
.product-i .actions .btn-details i:before {
  content: '\f1c3';
  font-family: 'Material-Design-Iconic-Font';
  font-size: 20px;
  line-height: 36px;
}
.product-i .actions .addtofav_aws_icon,
.product-i .actions .addtocart-button {
  background: transparent;
  text-transform: none;
  border: none;
  -webkit-border-radius: 0;
  -moz-border-radius: 0;
  border-radius: 0;
}
.product-i .actions span.addtocart-button {
  position: relative;
  width: auto;
  display: block;
  height: 40px;
  border: 2px solid #e5e5e5;
}
.product-i .actions span.addtocart-button .addtocart-button {
  padding: 0 15px;
  color: #222;
  text-transform: uppercase;
  line-height: 36px !important;
}
.product-i .product-price {
  display: inline-block;
  margin-top: 3px;
  font-size: 16px;
  line-height: 20px;
}
.product-i .product-price .price-crossed {
  font-size: 14px;
}
.product-i .item-i:hover {
  border-color: #ff6131;
}
.product-i .item-i:hover .image-block .pro-image {
  top: 0;
  opacity: 1;
}
.product-i .item-i:hover .pro-image:before {
  content: "";
  height: 100%;
  top: 0;
}
.product-i .item-i:hover .actions {
  opacity: 1;
  margin-top: -58px;
}
.product-i .item-i:hover .quickview {
  opacity: 1;
}
.product-i .item-i:hover .product-title,
.product-i .item-i:hover .price-rating {
  opacity: 0;
}
.product-i .timer-grid {
  width: 100%;
  position: absolute;
  bottom: 0;
  padding: 0 25px;
  left: 0;
}
.product-i .timer-grid .box-time-date {
  width: 33%;
  padding: 10px 0;
  margin-bottom: 10px;
  font-size: 14px;
  line-height: 20px;
  font-weight: 500;
  text-align: center;
  text-transform: capitalize;
  background: rgba(0,0,0,0.5);
  color: #fff;
  border-right: 1px solid rgba(241,241,241,0.9);
  font-family: Arial;
  white-space: nowrap;
  float: left;
}
.product-i .timer-grid .box-time-date .number {
  display: block;
  font-size: 16px;
  font-weight: 700;
  padding: 0 8px;
  text-align: center;
}
.product-i .timer-grid .sec {
  border-right: 0;
}
.product-i .timer-grid .day {
  width: 100%;
  background: #ff6131;
  text-align: left;
  line-height: 25px;
  padding-left: 60px;
  font-size: 16px;
  font-weight: 600;
  position: relative;
}
.product-i .timer-grid .day .number {
  display: inline;
  width: auto;
}
.product-i .timer-grid .day .number:before {
  display: block;
  position: absolute;
  top: 0;
  left: 0;
  width: 45px;
  height: 45px;
  background: #ca2f00;
  text-align: center;
  font-size: 20px;
  line-height: 45px;
  content: "\f017";
  font-family: FontAwesome;
}
.has-counter .item-i {
  margin-bottom: 25px;
  border: 6px solid rgba(63,180,251,0.1);
  transition: 0.5s;
}
.has-counter .item-i .btn-quickview a,
.has-counter .item-i .btn-details a {
  color: #fff !important;
}
.has-counter .product-i {
  border: 0;
}
.has-counter .product-i:hover .btn-groups {
  bottom: 36.5%;
}
.has-counter .product-i .product-title {
  margin-top: 0;
}
.has-counter .product-i .actions span.addtocart-button {
  height: 28px;
}
.has-counter .product-i .actions span.addtocart-button .addtocart-button {
  line-height: 26px !important;
  font-size: 11px !important;
  padding: 0 8px;
}
.has-counter .product-i .actions .btn-groups .btn-quickview,
.has-counter .product-i .actions .btn-groups .btn-wishlist,
.has-counter .product-i .actions .btn-groups .btn-details {
  height: 28px;
  width: 28px;
}
.has-counter .product-i .actions .btn-groups .btn-quickview i,
.has-counter .product-i .actions .btn-groups .btn-wishlist i,
.has-counter .product-i .actions .btn-groups .btn-details i {
  line-height: 26px;
}
.has-counter .product-i .ratingbox {
  margin-bottom: 7px;
}
.has-counter .product-i .text-block {
  padding: 20px 25px 0px 25px;
}
.has-counter .owl-item {
  border-right: 0;
  margin-right: 0;
}
#fancybox-content {
  background: #fff;
}
#fancybox-content .continue_link,
#fancybox-content .showcart {
  background-color: #2f2f2f;
  color: #ffffff;
  display: inline-block;
  margin: 10px 0 0;
  padding: 5px 20px;
  text-align: center;
  text-transform: capitalize;
  width: auto;
}
.listing-view .list-product {
  margin-bottom: 50px;
}
.listing-view .product-row .product-i {
  border: 0;
  margin-bottom: 30px !important;
  padding: 0;
}
.listing-view .product-row .product-i .actions .btn-groups {
  display: inline-block;
}
.listing-view.vm_list_view .list-product {
  border-top: 0;
}
.listing-view.vm_list_view .product-i {
  width: 100%;
}
.listing-view.vm_list_view .product-i .item-i {
  *zoom: 1;
}
.listing-view.vm_list_view .product-i .item-i:before,
.listing-view.vm_list_view .product-i .item-i:after {
  content: " ";
  display: table;
}
.listing-view.vm_list_view .product-i .item-i:after {
  clear: both;
}
.listing-view.vm_list_view .product-i .item-i:before,
.listing-view.vm_list_view .product-i .item-i:after {
  display: table;
  content: "";
  line-height: 0;
}
.listing-view.vm_list_view .product-i .item-i:after {
  clear: both;
}
.listing-view.vm_list_view .product-i .item-i:hover .price-rating,
.listing-view.vm_list_view .product-i .item-i:hover .product-title {
  opacity: 1;
}
.listing-view.vm_list_view .product-i .text-block {
  border: 2px solid #e5e5e5;
  padding: 30px;
  margin-top: 20px;
  border-left: 0;
}
.rtl .listing-view.vm_list_view .product-i .text-block {
  border-right: 0;
  border-left: none;
}
.listing-view.vm_list_view .product-i .price-rating {
  text-align: left;
}
.rtl .listing-view.vm_list_view .product-i .price-rating {
  text-align: right;
}
.listing-view.vm_list_view .product-i .text-block-inner {
  text-align: left;
}
.rtl .listing-view.vm_list_view .product-i .text-block-inner {
  text-align: right;
}
.listing-view.vm_list_view .product-i .text-block-inner .product-title,
.listing-view.vm_list_view .product-i .text-block-inner .vm-product-rating-container {
  text-align: left;
}
.rtl .listing-view.vm_list_view .product-i .text-block-inner .product-title,
.rtl .listing-view.vm_list_view .product-i .text-block-inner .vm-product-rating-container {
  text-align: right;
}
.listing-view.vm_list_view .product-i .text-block-inner .product-title {
  font-size: 18px;
  transition: 0s;
}
.listing-view.vm_list_view .product-i .text-block-inner .product-price {
  margin-bottom: 5px;
}
.listing-view.vm_list_view .product-i .text-block-inner .product-price .PricesalesPrice {
  font-size: 22px;
  margin: 0;
}
.listing-view.vm_list_view .product-i .text-block-inner .product-price .price-crossed {
  font-size: 18px;
  margin-right: 12px;
}
.rtl .listing-view.vm_list_view .product-i .text-block-inner .product-price .price-crossed {
  margin-left: 12px;
  margin-right: 0;
}
.listing-view.vm_list_view .product-i .text-block-inner .box-review {
  margin-bottom: 5px;
}
.listing-view.vm_list_view .product-i .text-block-inner .ratingbox {
  margin: 0;
  display: inline-block;
  margin-top: -4px;
}
.listing-view.vm_list_view .product-i .text-block-inner .amount {
  display: initial;
}
.listing-view.vm_list_view .product-i .text-block-inner .amount:before {
  content: "|";
  font-size: 12px;
  color: #e5e5e5;
  margin: 0 10px;
}
.listing-view.vm_list_view .product-i .vm-product-media-container {
  float: left;
  max-width: 272px;
}
.rtl .listing-view.vm_list_view .product-i .vm-product-media-container {
  float: right;
}
.listing-view.vm_list_view .product-i .product_s_desc {
  display: block;
}
.listing-view.vm_list_view .product-i .actions {
  margin-top: 30px;
  opacity: 1;
  position: initial;
}
.listing-view.vm_list_view .product-i .actions .btn-actions {
  margin: initial;
}
.listing-view.vm_list_view .product-i .actions .btn-groups {
  display: inline-block;
}
.listing-view.vm_list_view .product-i .vm-product-descr-container-1 {
  min-height: 77px;
}
.listing-view.vm_list_view .product-i:hover .btn-groups {
  bottom: 25%;
}
.vm-product-container {
  margin: 0 0 50px;
}
.vm-product-media-container .additional-images {
  margin-top: 10px;
  padding-left: 0px;
  padding-right: 0px;
}
.vm-product-media-container .owl-theme {
  padding: 0 25px;
  margin: 20px 0 0;
  width: auto;
}
.vm-product-media-container .owl-theme .owl-wrapper-outer {
  width: auto;
}
.vm-product-media-container .owl-theme .owl-item .item {
  border: 1px solid #e5e5e5;
  margin-left: 8px;
  margin-right: 8px;
}
.vm-product-media-container .owl-theme .owl-controls .owl-buttons {
  opacity: 100;
  filter: alpha(opacity=10000);
  opacity: 1;
  filter: alpha(opacity=100);
}
.vm-product-media-container .owl-theme .owl-controls .owl-buttons div {
  width: 30px;
  opacity: 0;
}
.vm-product-media-container .owl-theme.style1 .owl-controls .owl-buttons:before {
  display: none;
}
.vm-product-media-container .owl-theme.style1 .owl-controls .owl-buttons >div {
  font-size: 20px;
  width: 30px;
}
.vm-product-media-container .owl-theme.style1 .owl-controls .owl-buttons >div:before {
  top: 9px;
}
.vm-product-media-container .owl-theme.style1 .owl-controls .owl-buttons >div i {
  position: absolute;
  top: 50%;
  -webkit-transform: translate(0,-50%);
  -ms-transform: translate(0,-50%);
  -o-transform: translate(0,-50%);
  transform: translate(0,-50%);
  -moz-transform: translate(0,-50%);
  left: 0;
  right: 0;
  margin: auto;
  display: none;
}
.vm-product-media-container .owl-theme.style1 .owl-controls .owl-buttons >div:hover {
  color: #fff;
}
.vm-product-media-container .owl-theme.style1 .owl-controls .owl-buttons .owl-prev {
  left: -4px;
}
.vm-product-media-container .owl-theme.style1 .owl-controls .owl-buttons .owl-next {
  right: -4px;
}
.vm-product-media-container:hover .owl-controls .owl-buttons div {
  opacity: 1;
}
#vmproductcompare_overlay {
  height: auto;
}
.vm-product-details-container .vm-product-details-inner {
  position: relative;
}
.vm-product-details-container .vm-product-details-inner .dropdown-menu {
  min-height: 55px;
}
.vm-product-details-container .vm-product-details-inner .dropdown-menu li a {
  height: auto;
  display: inline;
}
.vm-product-details-container .product-neighbours {
  position: absolute;
  right: 0;
  top: 5px;
  margin: 0 -2px;
}
.rtl .vm-product-details-container .product-neighbours {
  left: 0;
  right: auto;
}
.vm-product-details-container .product-neighbours a {
  border-radius: 50%;
  width: 18px;
  height: 17px;
  margin: 0 2px;
  padding: 0;
  display: block;
  float: left;
  text-indent: -9999px;
  overflow: hidden;
}
.rtl .vm-product-details-container .product-neighbours a {
  float: right;
}
.vm-product-details-container .product-neighbours a:hover {
  opacity: 80;
  filter: alpha(opacity=8000);
  opacity: 0.8;
  filter: alpha(opacity=80);
}
.vm-product-details-container .product-neighbours a.previous-page {
  background-image: url("../images/icons/prev1.png");
}
.vm-product-details-container .product-neighbours a.next-page {
  background-image: url("../images/icons/next1.png");
}
.vm-product-details-container .ratingbox {
  margin: 0;
}
.vm-product-details-container h1 {
  font-size: 20px;
  padding-right: 30px;
  margin-top: 30px;
  border-bottom: 1px solid #e5e5e5;
  border-top: 1px solid #e5e5e5;
  padding: 15px 0;
  text-transform: uppercase;
}
.rtl .vm-product-details-container h1 {
  padding-left: 30px;
  padding-right: 0;
}
.vm-product-details-container .icons .btn-group {
  margin-top: 5px;
}
.vm-product-details-container .vm-product-rating-container {
  color: rgba(34,34,34,0.5);
  margin: 0 0 10px;
  border-bottom: 1px solid #e5e5e5;
  padding-bottom: 10px;
  padding-top: 5px;
}
.vm-product-details-container .vm-product-rating-container .separator {
  margin: 0 4px;
  color: #919191;
  font-weight: 700;
  font-size: 13px;
}
.vm-product-details-container .vm-product-rating-container .to_review,
.vm-product-details-container .vm-product-rating-container .in-stock {
  font-size: 13px;
  display: inline;
}
.vm-product-details-container .vm-product-rating-container .to_review span,
.vm-product-details-container .vm-product-rating-container .in-stock span {
  color: #919191;
}
.vm-product-details-container .back-to-category {
  font-size: 13px;
}
.vm-product-details-container .product-price {
  *zoom: 1;
  font-size: 22px;
  margin: 15px 0 10px;
  font-weight: 500;
}
.vm-product-details-container .product-price:before,
.vm-product-details-container .product-price:after {
  content: " ";
  display: table;
}
.vm-product-details-container .product-price:after {
  clear: both;
}
.vm-product-details-container .product-price:before,
.vm-product-details-container .product-price:after {
  display: table;
  content: "";
  line-height: 0;
}
.vm-product-details-container .product-price:after {
  clear: both;
}
.vm-product-details-container .product-price .PricesalesPrice {
  font-size: 35px;
}
.vm-product-details-container .product-short-description {
  padding: 10px 0;
  margin-bottom: 10px;
  color: rgba(34,34,34,0.8);
}
.vm-product-details-container .product-short-description h3 {
  font-size: 18px;
  font-weight: 500;
  text-transform: uppercase;
}
.vm-product-details-container .product-fields .product-field {
  *zoom: 1;
  margin: 0 0 20px;
  width: 50%;
  float: left;
  padding-right: 20px;
}
.vm-product-details-container .product-fields .product-field:before,
.vm-product-details-container .product-fields .product-field:after {
  content: " ";
  display: table;
}
.vm-product-details-container .product-fields .product-field:after {
  clear: both;
}
.vm-product-details-container .product-fields .product-field:before,
.vm-product-details-container .product-fields .product-field:after {
  display: table;
  content: "";
  line-height: 0;
}
.vm-product-details-container .product-fields .product-field:after {
  clear: both;
}
.rtl .vm-product-details-container .product-fields .product-field {
  padding-left: 20px;
  padding-right: 0;
}
.vm-product-details-container .product-fields .product-field >span,
.vm-product-details-container .product-fields .product-field .product-field-display {
  float: left;
}
.rtl .vm-product-details-container .product-fields .product-field >span,
.rtl .vm-product-details-container .product-fields .product-field .product-field-display {
  float: right;
}
.vm-product-details-container .product-fields .product-field >span {
  width: 50px;
  margin: 5px 0;
}
.vm-product-details-container .product-fields .product-field >span .product-fields-title {
  color: #393939;
}
.vm-product-details-container .product-fields .product-field >span .product-fields-title:after {
  content: "*";
  font-weight: 600;
}
.vm-product-details-container .product-fields .product-field >span .product-fields-title strong {
  font-weight: 600;
}
.vm-product-details-container .product-fields .product-field .strong {
  font-weight: 300;
}
.vm-product-details-container .product-fields .product-field .chzn-single {
  -webkit-border-radius: 0;
  -moz-border-radius: 0;
  border-radius: 0;
}
.vm-product-details-container .product-fields .product-field .chzn-container {
  margin-bottom: 0;
  display: block;
  width: 80% !important;
}
.vm-product-details-container .product-fields .product-field .chzn-container .chzn-single {
  height: 36px;
  line-height: 36px;
  background: transparent;
  border: 1px solid #e5e5e5;
}
.vm-product-details-container .product-fields .product-field .chzn-container .chzn-single span {
  margin-right: 40px;
}
.vm-product-details-container .product-fields .product-field .chzn-container .chzn-single div {
  width: 30px;
  height: 30px;
  top: 2px;
  right: 2px;
  padding: 4px 10px;
  line-height: 25px;
}
.rtl .vm-product-details-container .product-fields .product-field .chzn-container .chzn-single div {
  left: 2px;
  right: auto;
}
.vm-product-details-container .product-fields .product-field .chzn-container .chzn-single div b {
  display: block;
}
.vm-product-details-container .product-fields .product-field .chzn-container .chzn-single div:before {
  content: "\f107";
  font-family: 'FontAwesome';
}
.vm-product-details-container .addtocart-bar {
  padding: 20px 0;
  *zoom: 1;
}
.vm-product-details-container .addtocart-bar:before,
.vm-product-details-container .addtocart-bar:after {
  content: " ";
  display: table;
}
.vm-product-details-container .addtocart-bar:after {
  clear: both;
}
.vm-product-details-container .addtocart-bar:before,
.vm-product-details-container .addtocart-bar:after {
  display: table;
  content: "";
  line-height: 0;
}
.vm-product-details-container .addtocart-bar:after {
  clear: both;
}
.vm-product-details-container .addtocart-bar .lbl_qty {
  float: left;
  line-height: 40px;
  margin-right: 10px;
  margin-bottom: 0;
}
.rtl .vm-product-details-container .addtocart-bar .lbl_qty {
  float: right;
}
.rtl .vm-product-details-container .addtocart-bar .lbl_qty {
  margin-left: 10px;
  margin-right: 0;
}
.vm-product-details-container .addtocart-bar .lbl_qty {
  display: none;
}
.vm-product-details-container .addtocart-bar .addtocart-button {
  background: transparent;
  text-transform: none;
  -webkit-border-radius: 0;
  -moz-border-radius: 0;
  border-radius: 0;
  float: left;
}
.rtl .vm-product-details-container .addtocart-bar .addtocart-button {
  float: right;
}
.vm-product-details-container .addtocart-bar span.addtocart-button {
  position: relative;
  width: auto;
  display: block;
  height: 40px;
  line-height: 39px;
  border-radius: 0;
}
.vm-product-details-container .addtocart-bar span.addtocart-button .addtocart-button {
  padding: 0 22px;
  color: #222;
  text-transform: uppercase;
  border: 2px solid #e5e5e5;
  height: 40px;
}
.vm-product-details-container .btn-groups {
  padding: 10px 0;
}
.vm-product-details-container .btn-groups .btn-group {
  width: 40px;
  height: 40px;
  margin-right: 3px;
}
.rtl .vm-product-details-container .btn-groups .btn-group {
  margin-left: 3px;
  margin-right: 0;
}
.vm-product-details-container .btn-groups .btn-group #vmcompare {
  padding: 0;
}
.vm-product-details-container .btn-groups .btn-group #vmcompare a {
  display: block;
}
.vm-product-details-container .btn-groups .btn-group #vmcompare a:before {
  content: "\f021" !important;
  font-family: FontAwesome;
}
.vm-product-details-container .btn-groups .btn-group #wk_compare .add_continue a {
  font-size: 14px;
}
.vm-product-details-container .btn-groups .btn-group #wk_compare .add_compare a {
  font-size: 14px;
  display: inline-block;
  background: #ddd;
}
.vm-product-details-container .btn-groups .btn-group .addtofav_aws_icon,
.vm-product-details-container .btn-groups .btn-group #vmcompare a,
.vm-product-details-container .btn-groups .btn-group a {
  height: 40px;
  width: auto;
  padding: 5px 11px;
  line-height: 27px;
  -webkit-border-radius: 0;
  -moz-border-radius: 0;
  border-radius: 0;
  border: 2px solid #e5e5e5;
  font-size: 0px;
  display: block;
  background: #fff;
}
.vm-product-details-container .btn-groups .btn-group .addtofav_aws_icon:before,
.vm-product-details-container .btn-groups .btn-group .addtofav_aws_icon i,
.vm-product-details-container .btn-groups .btn-group #vmcompare a:before,
.vm-product-details-container .btn-groups .btn-group #vmcompare a i,
.vm-product-details-container .btn-groups .btn-group a:before,
.vm-product-details-container .btn-groups .btn-group a i {
  font-size: 14px;
  line-height: 29px;
}
.vm-product-details-container .btn-groups .btn-wishlist i:before {
  content: '\f15f';
  font-family: 'Material-Design-Iconic-Font';
  font-size: 16px;
  line-height: 29px;
}
.vm-product-details-container .btn-groups .btn-wishlist .addtofav_aws_icon_active {
  background: #0091d5;
  height: 40px;
  width: 40px;
  text-align: center;
  color: #fff;
  position: absolute;
  top: 0;
  left: 0;
  outline: 0;
}
.vm-product-details-container .btn-groups .btn-wishlist .addtofav_aws_icon_active i {
  line-height: 39px;
  margin-left: 4px;
}
.vm-product-details-container .btn-groups .btn-email a:before {
  content: "\f003";
  font-family: FontAwesome;
}
.vm-product-details-container .addthis_toolbox {
  padding: 20px 0 0;
}
.quantity-box {
  position: relative;
  float: left;
  margin-right: 30px;
}
.rtl .quantity-box {
  float: right;
}
.rtl .quantity-box {
  margin-left: 30px;
  margin-right: 0;
}
.quantity-box .quantity-input {
  font-size: 14px;
  line-height: 40px;
  font-weight: 500;
  text-align: center;
  float: left;
  padding: 3px 5px;
  width: 70px;
  height: 40px;
  margin: 0px;
  -webkit-border-radius: 0;
  -moz-border-radius: 0;
  border-radius: 0;
  border: 1px solid #e5e5e5;
  box-shadow: none;
}
.rtl .quantity-box .quantity-input {
  float: right;
}
.quantity-box .quantity-input:focus {
  -webkit-box-shadow: none;
  box-shadow: none;
  -moz-box-shadow: none;
}
.quantity-box .quantity-minus,
.quantity-box .quantity-plus {
  float: left;
  width: 20px;
  height: 20px;
  border: 0;
  position: absolute;
}
.rtl .quantity-box .quantity-minus,
.rtl .quantity-box .quantity-plus {
  float: right;
}
.quantity-box .quantity-minus {
  background: #383838 url(../images/general/minus-white-1.png) center center no-repeat !important;
  right: -20px;
  bottom: 0;
}
.rtl .quantity-box .quantity-minus {
  left: -20px;
  right: auto;
}
.quantity-box .quantity-plus {
  background: #383838 url(../images/general/plus-white-1.png) center center no-repeat !important;
}
.zoomContainer .zoomLens {
  border: 1px solid #eadbdb !important;
}
.zoomContainer .zoomWindowContainer div.zoomWindow {
  border-color: #eee !important;
}
.productdetails .vm-product-media-container .main-image {
  border: 4px solid rgba(63,180,251,0.1);
}
.productdetails .nav-tabs {
  *zoom: 1;
  border: 0;
}
.productdetails .nav-tabs:before,
.productdetails .nav-tabs:after {
  content: " ";
  display: table;
}
.productdetails .nav-tabs:after {
  clear: both;
}
.productdetails .nav-tabs:before,
.productdetails .nav-tabs:after {
  display: table;
  content: "";
  line-height: 0;
}
.productdetails .nav-tabs:after {
  clear: both;
}
.productdetails .nav-tabs li {
  float: left;
  border: none;
}
.rtl .productdetails .nav-tabs li {
  float: right;
}
.productdetails .nav-tabs li a {
  line-height: 23px;
  color: #222;
  margin: 0;
  -webkit-border-radius: 0;
  -moz-border-radius: 0;
  border-radius: 0;
  text-transform: uppercase;
  font-size: 16px;
  border: 1px solid #e5e5e5;
  padding: 12px 30px;
}
.productdetails .nav-tabs li a:focus {
  border-left: none;
}
.rtl .productdetails .nav-tabs li:first-child a {
  border-left: none;
}
.rtl .productdetails .nav-tabs li:last-child a {
  border-left: 1px solid #e5e5e5;
}
.productdetails .nav-tabs li:last-child a {
  border-left: 0;
}
.productdetails .tab-content {
  border: 1px solid #e5e5e5;
  padding: 40px 25px;
}
.productdetails .related_slider {
  margin-left: -15px !important;
  margin-right: -15px !important;
}
.productdetails .related_slider .owl-item {
  padding: 0 15px;
}
.productdetails .related_slider .owl-buttons:before {
  display: none;
}
.productdetails .related_slider .owl-buttons >div {
  height: 30px;
  width: 30px;
}
.related_slider .owl-item .vm-prices-block {
  text-align: left;
}
.rtl .related_slider .owl-item .vm-prices-block {
  text-align: right;
}
h1.header {
  font-size: 28px;
  line-height: 28px;
  font-weight: normal;
  overflow: hidden;
  width: 100%;
}
h1.header span {
  width: 100%;
  display: inline-block;
  border-bottom: 1px solid #eee;
  margin: 0 0 25px;
  padding-bottom: 10px;
}
#form-login {
  margin: 15px 0;
}
.buttonBar {
  padding-top: 10px;
  border-top: 3px solid #888;
  width: 100%;
  display: inline-block;
  margin-top: 20px;
  margin-bottom: 10px;
}
.buttonBar #reg_text {
  margin-bottom: 5px;
}
#com-form-login .form-list > li {
  margin-bottom: 10px;
}
#com-form-login .form-list label em {
  margin-right: 4px;
}
.new-users .content h2 {
  background: url(../images/general/i_page1.gif) no-repeat;
}
.login-users .content h2 {
  background: url(../images/general/i_page2.gif) no-repeat;
}
.row-set .content {
  width: 100%;
  padding: 14px 21px;
  display: inline-block;
}
.row-set .content h2 {
  font-weight: normal;
  font-size: 15px;
  margin: 0 0 30px;
  padding: 0 0 5px 23px;
  border-bottom: 1px solid #e5e5e5;
  background-position: 0 1px;
  background-repeat: no-repeat;
  text-transform: uppercase;
  color: #444443;
  display: inline-block;
  width: 100%;
}
.account-login .row-set .content {
  min-height: 345px;
  border-bottom: 0;
  margin-bottom: 0;
}
.account-login .row-set .content h2 {
  margin: 0 0 14px;
}
.account-login .buttons-set {
  text-align: right;
  border: 1px solid #e5e5e5;
  margin: 0;
  padding: 8px 13px;
  display: inline-block;
  width: 100%;
}
.account-login .buttons-set > div {
  margin-right: 7px;
}
.account-login .buttons-set a {
  font-size: 13px;
  line-height: 34px;
}
.user-details .row {
  margin-bottom: 15px;
}
.cart-view h3,
.cart-view .h3 {
  font-size: 22px;
}
.cart-view form {
  display: inline-block;
  width: 100%;
  margin-bottom: 30px;
}
table.cart-summary tbody {
  border: 1px solid #e5e5e5;
}
table.cart-summary tr th {
  border: 1px solid #e5e5e5;
  padding: 15px 0 8px;
}
.billto-shipto h3 {
  margin: 0 0 10px;
}
.billto-shipto h3 i {
  color: #fff;
  display: inline-block;
  margin-right: 5px;
  font-size: 20px;
  height: 40px;
  line-height: 40px;
  text-align: center;
  width: 40px;
  background-color: #b4b4b4;
}
.billto-shipto a.details:hover {
  text-decoration: underline;
}
.billto-shipto .billto-inner,
.billto-shipto .shipto-inner {
  min-height: 150px;
}
.billto-shipto .output-shipto a {
  color: #898989 !important;
}
.billto-shipto .output-shipto a:hover {
  color:  !important;
}
.billto-shipto .output-shipto input {
  float: left;
  position: initial;
  margin-left: 0;
  margin-top: 4px;
  margin-right: 5px;
}
.cart-view h3 {
  color: #444;
}
.block-border {
  display: inline-block;
  width: 100%;
  border: 1px solid #e5e5e5;
  padding: 21px;
  margin-bottom: 30px;
}
.table-footer {
  display: inline-block;
  width: 100%;
}
.table-footer h3,
.table-footer .h3 {
  font-size: 14px;
  margin: 10px 0;
}
.shipment input[type="radio"],
.payment input[type="radio"] {
  float: left;
  margin-top: 4px;
  width: 4%;
}
.shipment label,
.payment label {
  float: left;
  width: 96%;
  margin-bottom: 30px;
}
.shipment .vm-display.vm-price-value,
.payment .vm-display.vm-price-value {
  display: inline-block;
}
.total-block span,
.total-block div {
  font-size: 20px;
  display: inline-block;
}
.total-block .priceColor2 {
  margin-right: 30px;
}
.total-block .title {
  font-weight: 500;
  margin-right: 5px;
}
.vmshipment_name,
.vmpayment_name {
  font-weight: 400;
  line-height: 1.35;
  display: block;
  font-size: 16px;
  color: #444;
}
.vmCartPaymentLogo {
  display: inline-block;
  float: left;
  min-width: 40px;
}
.vmCartPaymentLogo img {
  display: inline-block;
  float: left;
  margin-right: 15px;
}
a.terms-of-service {
  text-decoration: underline;
}
#checkoutForm fieldset {
  width: 100%;
  float: left;
  margin: 0;
  display: inline-block;
}
#checkoutForm .product-name a {
  font-size: 13px;
  font-weight: 500;
  text-transform: uppercase;
}
#checkoutForm .vm-continue-shopping {
  width: 50%;
  float: left;
  padding-bottom: 0;
}
#checkoutForm .vir_quantity {
  text-align: center;
}
#checkoutForm .vir_quantity input.quantity-input {
  width: 50px !important;
  padding: 2px;
  display: inline-block;
  border: 1px solid #ddd;
  height: 30px;
  text-align: center;
  border-radius: 0;
  -webkit-box-shadow: none;
  box-shadow: none;
  -moz-box-shadow: none;
}
#checkoutForm .vir_quantity .vm2-add_quantity_cart,
#checkoutForm .vir_quantity .vm2-remove_from_cart {
  width: 30px;
  height: 30px;
  padding: 2px;
  -webkit-box-shadow: none;
  box-shadow: none;
  -moz-box-shadow: none;
}
#checkoutForm .vm-fieldset-pricelist {
  width: 100%;
  margin: 0 0 30px 0;
}
#checkoutForm .tb-total {
  line-height: 30px;
  text-transform: uppercase;
}
#checkoutForm .tb-total .total-title {
  padding: 15px 20px;
  border-bottom: 1px solid #d9dde3;
  background-color: #f5f5f5;
}
#checkoutForm .tb-total .total-title .total {
  display: inline-block;
}
#checkoutForm .tb-total .continue_link {
  color: #fff;
  float: left;
  line-height: 35px;
  border: 0 !important;
  background: #313133;
  padding: 5px 15px;
  font-size: 13px;
  text-transform: initial;
}
#checkoutForm .tb-total .price div {
  display: inline-block;
}
#checkoutForm .tb-total .price span {
  line-height: 38px;
}
#checkoutForm .price-coupon {
  margin-top: 10px;
}
#checkoutForm .price-coupon div {
  display: inline-block;
}
#checkoutForm .price-coupon > span {
  font-weight: bold;
}
#checkoutForm .cart-summary {
  width: 100%;
}
#checkoutForm .cart-summary .product-name {
  padding: 10px 5px;
}
#checkoutForm .cart-summary tr th {
  font-weight: normal;
  border-right: 1px solid #f2f2f2;
  padding: 10px;
  white-space: nowrap;
  vertical-align: middle;
  text-align: center;
  text-transform: capitalize;
  background: transparent;
}
#checkoutForm .cart-summary .cart-images {
  width: 100%;
  text-align: center;
  float: left;
  padding: 10px;
}
#checkoutForm .cart-summary .cart-images img {
  display: inline-block;
  float: none;
  margin: 0 auto;
  max-width: 150px;
  width: auto;
}
#checkoutForm .cart-summary tbody td {
  border-bottom: 1px solid #f2f2f2;
  border-right: 1px solid #f2f2f2;
  text-align: center;
}
#checkoutForm .cart-summary tbody .vm-customfield-cart {
  text-align: center;
  line-height: 20px;
  padding: 20px 0px;
}
#checkoutForm .cart-summary tbody .vm-customfield-cart span {
  float: none;
}
#checkoutForm .cart-summary tfoot {
  text-align: left;
}
#checkoutForm .cart-summary tfoot a:hover {
  text-decoration: underline;
}
#checkoutForm .cart-summary tfoot .checkoutStep {
  font-size: 18px;
  font-weight: 600;
  margin: 10px 0 15px;
}
#checkoutForm .cart-summary tfoot .shipment h3,
#checkoutForm .cart-summary tfoot .payment h3 {
  font-size: 13px;
  margin: 0;
}
#checkoutForm .cart-summary tfoot .shipment .buttonBar-left,
#checkoutForm .cart-summary tfoot .payment .buttonBar-left {
  float: right;
}
#checkoutForm .cart-summary tfoot .sectiontableentry2 td .tb-tfoot {
  border: 1px dashed #ccc;
}
#checkoutForm .cart-summary tfoot .sectiontableentry1 td .sectiontableentry1-inner {
  float: left;
  width: 100%;
  padding: 20px;
  border: 1px dashed #ccc;
  -webkit-box-sizing: border-box;
  -moz-box-sizing: border-box;
  box-sizing: border-box;
}
#checkoutForm .cart-summary .tb-tfoot {
  padding: 20px;
}
#checkoutForm .cart-summary .tb-tfoot input {
  margin: 0;
  height: 37px;
  border: 1px solid #f2f2f2;
  -webkit-box-shadow: none;
  box-shadow: none;
  -moz-box-shadow: none;
  -webkit-border-radius: 0;
  -moz-border-radius: 0;
  border-radius: 0;
  -webkit-box-sizing: border-box;
  -moz-box-sizing: border-box;
  box-sizing: border-box;
}
#checkoutForm .cart-summary .tb-tfoot .details-button input {
  background-image: none;
}
#checkoutForm .cart-summary .tb-tfoot .coupon {
  width: 300px;
  line-height: 37px;
}
#checkoutForm .checkout-button-top {
  display: inline-block;
  float: right;
  text-align: right;
  border: 0;
  width: 100%;
  padding: 20px 40px 10px;
}
#checkoutForm .vm-fieldset-customer-note {
  width: 100%;
  margin: 0 0 30px 0;
  border: 1px solid #e5e5e5;
  float: left;
  padding: 20px;
  -webkit-box-sizing: border-box;
  -moz-box-sizing: border-box;
  box-sizing: border-box;
}
#checkoutForm .vm-fieldset-customer-note span.customer-note {
  font-weight: 400;
  font-size: 14px;
  border-bottom: 1px solid #f1f1f1;
  padding-bottom: 3px;
  display: block;
}
#checkoutForm .vm-fieldset-customer-note span.customer-note:before {
  margin-right: 5px;
  content: "\f133";
  font: normal normal normal 14px/1 FontAwesome;
  font-size: inherit;
  text-rendering: auto;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
  transform: translate(0,0);
}
#checkoutForm .vm-fieldset-customer-note textarea#customer_note_field {
  height: 120px;
  margin: 20px 0;
  overflow: auto;
  width: 100%;
  border: 1px solid #f1f1f1;
  -webkit-box-shadow: none;
  box-shadow: none;
  -moz-box-shadow: none;
  -webkit-border-radius: 0;
  -moz-border-radius: 0;
  border-radius: 0;
}
#checkoutForm .vm-fieldset-customer-note .vm-fieldset-tos input.terms-of-service {
  float: left;
}
#checkoutForm .vm-fieldset-customer-note .vm-fieldset-tos span.cart {
  float: left;
}
#checkoutForm .vm-fieldset-customer-note .vm-fieldset-tos div.terms-of-service {
  width: 100%;
  float: left;
}
#checkoutForm .vm-fieldset-customer-note .vm-fieldset-tos div.terms-of-service .vmicon {
  display: none;
}
#checkoutForm .vm-fieldset-customer-note .vm-fieldset-tos div.terms-of-service a:hover {
  text-decoration: underline;
}
.login .radio label,
.login .checkbox label {
  position: relative;
}
.login .radio label input[type="checkbox"],
.login .checkbox label input[type="checkbox"] {
  left: 0;
  top: 5px;
}
.login .btn-primary {
  border-radius: 20px;
}
.form-links {
  margin-bottom: 30px;
}
.site .ui-tooltip {
  display: none;
}
#ui-tabs ul#tabs {
  border-bottom: 2px solid #eee;
  list-style-type: none;
  overflow: visible;
  margin: 0;
  margin-bottom: 20px;
  padding: 20px 0 0;
}
#ui-tabs ul#tabs li {
  color: #444;
  font-size: 16px;
  font-weight: 600;
  line-height: 1;
  text-align: center;
  text-transform: uppercase;
  text-decoration: none;
  position: relative;
  display: inline-block;
  margin: 0;
  margin-right: 20px;
  padding: 10px 5px;
  border: 0;
  border-radius: 0;
  cursor: pointer;
  background: none;
  background-image: none;
  -webkit-box-shadow: none;
  box-shadow: none;
  -moz-box-shadow: none;
}
#ui-tabs ul#tabs li:after {
  content: "";
  width: 100%;
  border-bottom: 2px solid transparent;
  position: absolute;
  left: 0;
  bottom: -2px;
}
#ui-tabs ul#tabs li.current {
  display: inline-block;
}
#ui-tabs ul#tabs li:focus {
  outline: 0;
  outline-offset: 0;
  -webkit-box-shadow: none;
  box-shadow: none;
  -moz-box-shadow: none;
}
.post_payment_payment_name .vmpayment_name {
  display: inline;
}
tr,
td {
  padding: 4px;
  border: 1px solid #c0c0c0;
}
.order-view {
  margin-bottom: 25px;
  display: inline-block;
  width: 100%;
}
.order-view #com-form-order-submit input {
  margin-top: 20px;
}
#userForm fieldset input[type="radio"] {
  float: left;
  margin-right: 5px;
  margin-top: 14px;
}
#userForm fieldset label {
  display: inline;
  margin-bottom: 10px;
}
#userForm fieldset .vmshipment_name,
#userForm fieldset .vmpayment_name {
  margin-top: 10px;
}
.product-related-products .sp-module-title {
  position: relative;
  margin-top: 60px;
  text-align: center;
  margin-bottom: 60px;
}
.product-related-products .sp-module-title h3 {
  margin-top: 0;
  margin-bottom: 25px;
  display: inline-block;
  background: #fff;
  z-index: 10;
  font-size: 24px;
  position: relative;
}
.product-related-products .sp-module-title h3:before {
  content: "R";
  display: block;
  padding: 0 50px;
  font-size: 85px;
  line-height: 78px;
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%,-50%);
  color: rgba(34,34,34,0.1);
  border-bottom: 2px dotted #e9ac4c;
}
.product-related-products .product-i .text-block .vm-prices-block {
  text-align: center;
}
.product-related-products .owl-prev,
.product-related-products .owl-next {
  line-height: 2px !important;
}
.userfields_info {
  font-weight: bold;
  display: block;
  margin: 0px 0px 8px 0px;
}
.adminForm.user-details {
  margin-top: 30px;
}
#checkoutForm .cart-summary tbody td {
  border: 1px solid #f2f2f2;
}
#checkoutForm .vm-fieldset-pricelist {
  overflow-x: auto;
}
#paymentForm input[type="radio"] {
  float: left;
  margin-right: 4px;
  margin-top: 4px;
}
@media (max-width: 760px) {
  .width30 {
    width: 100%;
    margin-bottom: 15px;
  }
  .width30:last-child {
    margin-bottom: 0;
  }
  #checkoutForm .cart-summary tbody td {
    border: 1px solid #f2f2f2;
    min-width: 10em;
  }
}
.fav_table .prod_name {
  font-size: 15px;
  font-weight: normal;
  color: #444;
}
.fav_table .fav_heading {
  background-color: #fafafa;
}
.fav_table .fav_col {
  border: 1px solid #e5e5e5;
  padding: 15px 5px;
}
.fav_table .fav_col h4 {
  font-size: 16px;
}
.fav_table .fav_col img {
  display: inline-block;
}
.fav_table .fav_col .quantity-box .quantity-input {
  height: 35px;
  line-height: 35px;
}
.fav_table .fav_col .fav_addtocart-bar .quantity-box {
  border: 0;
}
.fav_table .fav_col .fav_addtocart-bar span.quantity-controls {
  position: relative;
}
.fav_table .fav_col .fav_addtocart-bar span.quantity-controls input {
  position: absolute;
  float: left;
  color: #c0c0c0;
  font-size: 14px;
  width: 20px;
  height: 18px;
  line-height: 8px;
  padding: 0;
  border: 1px solid #e5e5e5;
  top: 0;
}
.fav_table .fav_col .fav_addtocart-bar span.quantity-controls .quantity-minus {
  top: 17px;
}
.fav_addtocart-bar span.quantity-controls {
  float: left;
  display: inline-block;
}
.fav_addtocart-bar span.quantity-controls input {
  padding: 0 15px;
  background-position: center center;
  background-repeat: no-repeat;
  background-color: #f1f1f1;
}
.fav_addtocart-bar span.quantity-controls input.quantity-plus {
  background: transparent url(../images/general/qty-decrease.png) no-repeat 0 0 !important;
  top: 8px;
  background-position: center center !important;
}
.fav_addtocart-bar span.quantity-controls input.quantity-minus {
  background: transparent url(../images/general/qty-increase.png) no-repeat 0 0 !important;
  bottom: -28px;
  background-position: center center !important;
}
.fav_addtocart-bar .addtocart-button {
  margin-left: 35px;
}
.fav_addtocart-bar .addtocart-button input {
  font-size: 14px;
  margin: 0;
  text-transform: uppercase;
  height: 35px;
}
.fav_addtocart-bar .addtocart-button:hover input {
  color: #fff !important;
}
.fav_addtocart-bar .quantity-box {
  margin-right: 0;
  border-right: 1px solid #e5e5e5;
}
.fav_addtocart-bar .quantity-box .quantity-input {
  width: 44px !important;
}
.compare_home {
  font-size: 24px;
}
.shop_home {
  display: inline-block;
  margin: 0;
  float: right;
}
.compare-main {
  margin-top: 15px;
}
.compare-main img {
  height: 100%;
}
.compare-main > h2 {
  font-size: 18px;
}
.compare-main .comapre-details-inner {
  padding: 0 20px;
  height: 50px;
  line-height: 50px;
}
.compare-main .product-details-inner {
  padding: 0 20px;
  height: 50px;
  line-height: 50px;
}
.compare-main .product-details-inner.remove {
  padding-left: 42px;
  background-position: 15px center;
}
.compare-main .grayrating {
  background-position: 15px center !important;
  height: 48px;
  line-height: 48px;
}
.compare-main .addtocart-button input:hover {
  color: #fff !important;
}
.reset-value {
  height: auto !important;
  border: none;
}
#searchMedia {
  height: 35px !important;
}
.category-view {
  margin-bottom: 30px;
}
.category-view .category-inner {
  border: 1px solid rgba(63,180,251,0.1);
  transition: 0.5s;
}
.category-view .category-inner .category-title {
  font-size: 14px;
  color: #fff;
  text-align: center;
  font-weight: 700;
  margin: 0;
  padding: 15px 0;
  background: #ff6131;
}
.category-view .category-inner:hover {
  border: 1px solid #ff6131;
}
.product-summary .width30 {
  width: 40%;
  float: left;
}
.product-summary .width70 {
  width: 60%;
  float: right;
  padding: 15px;
}
.ask-a-question-view .form-field textarea.field {
  width: 100%;
  float: left;
  min-height: 150px;
}
.ask-a-question-view tr,
.ask-a-question-view td {
  width: 100%;
  float: left;
  border: 0;
}
.ask-a-question-view .submit .floatleft {
  margin-top: 35px;
}
.ask-a-question-view .submit .text-right {
  margin-top: 0px;
  float: right;
  margin-right: 10px;
}
#fancybox-title {
  width: 100% !important;
}
@media (min-width: 1200px) {
  .sppb-container,
  .container {
    width: 1200px;
  }
  .rev_slider_wrapper .btn-slider {
    font-size: 14px;
    width: 160px;
    height: 42px;
    line-height: 38px;
  }
}
@media (max-width: 1200px) {
  .sp-megamenu-parent >li >a {
    padding: 5px 2px;
    font-size: 13px;
  }
  .rev_slider_wrapper .btn-slider {
    font-size: 14px;
    width: 160px;
    height: 42px;
    line-height: 38px;
  }
  .slideshow1 .large_bg_black:after {
    display: none;
  }
  #sp-search {
    width: 100%;
    text-align: center;
    display: inline-block;
  }
  #sp-search .sp-column {
    display: inline-block;
  }
  #sp-search .mod_search .sp-vmsearch {
    padding: 15px 0;
    padding-top: 0;
    margin: 0;
  }
  .style-home2 #sp-logo,
  .style-home2 #sp-minicart {
    width: 50%;
    float: left;
    margin-top: 30px;
  }
  .vina-carousel-virtuemart.default1 .product-i .text-block {
    padding: 0 15px;
  }
  .listing-view .text-block .actions {
    display: block;
  }
  .listing-view .text-block .actions .btn-quickview {
    margin-left: 0px;
  }
}
@media (min-width: 992px) and (max-width: 1199px) {
  .sppb-container,
  .container {
    width: 970px;
  }
  .corporate-about .block-wrapper {
    text-align: center;
    width: 100%;
  }
}
@media (max-width: 1199px) {
  .contact-form.title-bg-white .sppb-col-sm-6 {
    width: 100%;
    float: left;
    margin-top: 30px;
  }
  .contact-form .sppb-col-sm-6 {
    width: 100%;
    margin-bottom: 10px;
  }
  .contact-form .sppb-col-sm-6 .qlform {
    margin: auto;
  }
  .contact-form .sppb-col-sm-6 .sppb-addon-title {
    text-align: center;
    margin-bottom: 0;
  }
  .contact-form .sppb-col-sm-6 .sppb-addon-title:before,
  .contact-form .sppb-col-sm-6 .sppb-addon-title:after {
    display: none;
  }
  .contact-form .sppb-col-sm-6 #contac-form,
  .contact-form .sppb-col-sm-6 .control-group {
    text-align: center;
  }
  .title-bg-white .sppb-carousel-inner,
  .title-bg-white .sppb-carousel-controls,
  .title-bg-white .sppb-addon-text-block h3 {
    float: none;
    margin: 35px auto;
  }
}
@media (min-width: 768px) and (max-width: 991px) {
  #sp-left,
  #sp-component,
  #sp-right {
    width: 100%;
  }
}
@media (max-width: 991px) {
  .title1-product .sppb-col-sm-4,
  .title1-product .sppb-col-sm-8,
  .title1-betseller .sppb-col-sm-4,
  .title1-betseller .sppb-col-sm-8 {
    width: 100%;
  }
  .title1-betseller .sppb-col-sm-4 .title1 {
    margin-top: 50px;
    position: relative;
  }
  .list-carousel-virtuemart .sppb-col-sm-4 {
    width: 100%;
    margin-top: 30px;
  }
  .sppb-col-sm-5,
  .sppb-col-sm-7,
  .sppb-col-sm-3,
  .sppb-col-sm-9,
  #sp-position1,
  #sp-position2,
  #sp-position3,
  #sp-brand {
    width: 100%;
  }
  .sppb-col-sm-5 .social-icons,
  .sppb-col-sm-7 .social-icons,
  .sppb-col-sm-3 .social-icons,
  .sppb-col-sm-9 .social-icons,
  #sp-position1 .social-icons,
  #sp-position2 .social-icons,
  #sp-position3 .social-icons,
  #sp-brand .social-icons {
    float: none;
    padding: 0;
    padding-top: 20px;
  }
  .sppb-col-sm-5,
  .sppb-col-sm-7 {
    width: 100%;
  }
  .sppb-col-sm-5 .sppb-animated,
  .sppb-col-sm-7 .sppb-animated {
    margin-top: 60px;
  }
  .welcome-wrapper .sppb-addon-text-block {
    text-align: center;
  }
  #sp-position1 {
    text-align: center;
  }
  .pb-categories-product .sppb-addon-container {
    margin-top: 30px;
  }
  .pb-categories-product .sppb-col-sm-3 {
    width: 50%;
    float: left;
  }
  .pb-categories-product .sppb-col-sm-3 a img {
    width: 100%;
  }
  #sp-header.style-home2 {
    padding: 30px 0;
  }
  #sp-bottom-wrapper {
    background: #1a1a1a;
  }
  #sp-bottom-wrapper #sp-bottom3,
  #sp-bottom-wrapper #sp-bottom1,
  #sp-bottom-wrapper #sp-bottom4,
  #sp-bottom-wrapper #sp-bottom2 {
    float: left;
    width: 50%;
    border: 0;
    padding-left: 15px;
  }
  .vina-carousel-virtuemart.default1 .product-i .image-block {
    margin-left: 0;
  }
  .vina-carousel-content img {
    width: 100%;
  }
  .corporate-about .block-wrapper {
    text-align: center;
  }
  #sp-top-wrapper .menu li,
  .sp-module.modules-top .bootstrap-select .btn {
    padding: 1px 10px;
  }
}
@media (max-width: 767px) {
  #sp-top2,
  #sp-top1,
  #sp-top3 {
    display: inline-block;
    float: left;
    width: 100%;
    border-bottom: 1px solid #e5e5e5;
    text-align: center;
  }
  #sp-top2 .sp-column,
  #sp-top1 .sp-column,
  #sp-top3 .sp-column {
    display: inline-block;
  }
  #sp-top2 .sp-column .sp-contact-info .sp-contact-email,
  #sp-top1 .sp-column .sp-contact-info .sp-contact-email,
  #sp-top3 .sp-column .sp-contact-info .sp-contact-email {
    display: none;
  }
  #sp-footer1,
  #sp-footer2 {
    width: 100%;
    text-align: center !important;
  }
  #sp-footer1 .payment,
  #sp-footer2 .payment {
    float: none !important;
    margin: auto;
    display: inline-block;
    margin-top: 20px;
  }
  .sp-module.modules-top .cur_box .btn,
  #sp-top-wrapper .menu .parent {
    border: 0;
    padding: 0 15px;
  }
  #sp-logo .logo {
    margin: auto;
  }
  #offcanvas-toggler {
    text-align: left;
  }
  #sp-menu,
  #sp-minicart {
    width: 50%;
    float: left;
  }
  .sp-module.mod_search .sp-vmsearch .search-input-wrapper {
    width: 200px;
  }
  .sp-vmsearch.sp-vmsearch-slideToggle .sp-vmsearch-content {
    width: 380px;
  }
  #sp-search {
    width: 100%;
  }
  #sp-search .vmCartModule {
    margin: auto;
    display: table;
  }
  #sp-search .mod_search .sp-vmsearch {
    padding: 10px 0;
  }
  #sp-search .mod_search .sp-vmsearch .search-input-wrapper {
    width: 160px;
  }
  .rev_slider_wrapper .btn-slider {
    font-size: 14px;
    width: 130px;
    height: 33px;
    line-height: 29px;
  }
  .style-home2 #sp-minicart,
  .style-home2 #sp-logo {
    text-align: center;
    margin-bottom: 10px;
  }
  .style-home2 #sp-minicart .vmCartModule,
  .style-home2 #sp-logo .vmCartModule {
    display: inline-block;
  }
  .style-home2 #sp-minicart .logo,
  .style-home2 #sp-logo .logo {
    margin: auto;
  }
  #sp-header-wrapper.style-home2 #sp-menu {
    width: 100%;
  }
  #sp-header-wrapper.style-home2 #sp-menu #offcanvas-toggler {
    text-align: center;
  }
  .banner-static {
    width: 100%;
    margin-bottom: 20px;
  }
  .banner-static a {
    text-align: center;
  }
  .banner-static a img {
    margin: auto;
  }
  .sptab-vert .sptab .tabs_buttons {
    width: 100%;
    margin: 0 0 30px;
  }
  .sptab-vert .sptab .items_mask {
    width: 100%;
  }
  .sptab-horz .sptab ul.tabs_container {
    position: static;
  }
  .sptab-horz .sptab ul.tabs_container li.tab {
    width: 100%;
    border: none;
    border-top: 1px solid #e5e5e5;
    margin: 0 !important;
  }
  #sp-top-wrapper #login-form {
    float: left;
    width: 300px;
  }
  .rtl #sp-top-wrapper #login-form {
    float: right;
  }
  #sp-position-wrapper .social-icons {
    margin-top: 20px;
  }
  .banner-static-wrapper >.sppb-container >.sppb-row >div {
    width: 100%;
    margin-bottom: 20px;
  }
  .banner-static-wrapper >.sppb-container >.sppb-row >div .position-banner1 {
    display: none;
  }
  .works-wrapper {
    margin: 20px 0 30px 0 !important;
  }
  .banner-home4 {
    width: 50%;
  }
  .sptab-horz .items_mask >div {
    margin-right: 4px;
  }
  .sptab-vert .sppb-addon-content {
    margin-right: 3px;
  }
  .listing-view.vm_list_view .text-block .actions {
    display: block;
  }
  .listing-view.vm_list_view .text-block .actions .btn-quickview {
    margin-left: 0px;
  }
  .vina-carousel-virtuemart.default1 .product-i .media-body {
    padding-left: 10px;
  }
  .rtl .vina-carousel-virtuemart.default1 .product-i .media-body {
    padding-right: 10px;
    padding-left: 0;
  }
  .vina-carousel-virtuemart.default1 .product-i .image-block {
    margin-left: 0;
  }
  .title2.title3 .betseller-products .product-i .actions {
    display: flex;
  }
  div.modal {
    top: auto;
    left: 0;
  }
  .title-bg-white .sppb-carousel-inner,
  .title-bg-white .sppb-carousel-controls,
  .title-bg-white .sppb-addon-text-block h3 {
    width: 320px;
  }
}
@media (max-width: 480px) {
  #sp-logo {
    padding-bottom: 30px;
  }
  #sp-bottom-wrapper {
    background: #1a1a1a;
  }
  #sp-bottom-wrapper #sp-bottom3,
  #sp-bottom-wrapper #sp-bottom1,
  #sp-bottom-wrapper #sp-bottom4,
  #sp-bottom-wrapper #sp-bottom2 {
    width: 100%;
    padding: 30px 35px 15px;
  }
  #sp-header.style-home2 .sp-module.mod_search .sp-vmsearch {
    border: 1px solid #e5e5e5;
  }
  #sp-header.style-home2 .sp-module.mod_search .search-button-wrapper {
    float: right;
    border-left: 1px solid #e5e5e5;
    margin-right: 3px;
  }
  #sp-header.style-home2 .sp-module.mod_search .sp-vmsearch-categorybox {
    float: none !important;
    border-bottom: 1px solid #e5e5e5;
  }
  #sp-header.style-home2 .sp-module.mod_search .sp-vmsearch-categorybox .sp-vmsearch-categories1 {
    border: 0 !important;
  }
  .sppb-addon-raw-html .timer-hot-deal .box-time-date {
    height: 65px;
    width: 55px;
  }
  .sppb-addon-raw-html .timer-hot-deal .box-time-date span {
    font-size: 18px;
  }
  .sp-module.mod_search .sp-vmsearch .search-input-wrapper {
    width: 124px;
  }
  .sp-vmsearch.sp-vmsearch-slideToggle .sp-vmsearch-content {
    width: 300px;
    top: 100%;
    right: -50px;
  }
  .vina-carousel-content.style-1 .text-block,
  .vina-carousel-content.style-1 .inner-text-block,
  .vina-carousel-content.style-1 .image-block {
    margin: 0px;
  }
  .vina-carousel-content.style-1 .major_color {
    position: initial;
  }
  .vina-carousel-content.style-1 .major_color .intro-day {
    font-size: 50px;
  }
  #sp-menu .block-mini-cart,
  #sp-minicart .block-mini-cart {
    float: none;
  }
  #sp-menu .block-mini-cart .mini-cart-body,
  #sp-minicart .block-mini-cart .mini-cart-body {
    padding: 7px 0;
  }
  #sp-menu .block-mini-cart .mini-cart-title,
  #sp-minicart .block-mini-cart .mini-cart-title {
    padding-left: 15px;
    border-radius: 4px;
    height: 30px;
  }
  #sp-menu .block-mini-cart .mini-cart-title .count-item,
  #sp-minicart .block-mini-cart .mini-cart-title .count-item {
    line-height: 29px;
  }
  #sp-menu .block-mini-cart .mini-cart-title:before,
  #sp-minicart .block-mini-cart .mini-cart-title:before {
    display: none;
  }
  #sp-menu .block-mini-cart .mini-cart-title .simple-handbag:before,
  #sp-minicart .block-mini-cart .mini-cart-title .simple-handbag:before {
    display: none;
  }
  #sp-menu .vmCartModule,
  #sp-minicart .vmCartModule {
    float: right;
  }
  .pb-categories-product .sppb-col-sm-3 {
    width: 100%;
  }
  #sp-search .mod_search .sp-vmsearch .search-input-wrapper {
    width: 110px;
  }
  .listing-view.vm_list_view .text-block {
    width: 100%;
  }
  .listing-view.vm_list_view .text-block .actions {
    display: block;
  }
  .acymailing_module .acymailing_form .inputbox {
    width: 270px !important;
  }
  .social-icons li {
    margin: 0 3px;
  }
  .featured-product .sptab.sptab_red .tabs_mask {
    height: auto !important;
  }
  .featured-product .sptab.sptab_red .tabs_mask ul li {
    width: 100%;
    margin: 10px 0 !important;
  }
  .featured-product .sptab.sptab_red .tabs_mask ul li:before {
    left: 49%;
  }
  .vm-product-details-container .vm-product-details-inner {
    margin-top: 20px;
  }
  .style-home2 #sp-minicart .vmCartModule {
    float: none;
  }
  .style-home2 #sp-minicart .vmCartModule .mini-cart-title {
    padding-left: 0;
  }
  .style-home2 #sp-minicart,
  .style-home2 #sp-logo {
    width: 100%;
  }
  .vina-carousel-virtuemart.default1 .product-i .text-block {
    padding: 0px;
  }
  .sp-page-title h2 {
    font-size: 18px;
  }
  .vm-product-details-container .product-fields .product-field {
    width: 100%;
  }
  .orderby-displaynumber .display-number,
  .orderby-displaynumber .orderlistcontainer {
    float: left !important;
    margin-top: 15px;
  }
  #sp-header-wrapper-sticky-wrapper.is-sticky #sp-logo {
    display: none;
  }
  #sp-header-wrapper-sticky-wrapper.is-sticky #sp-header-wrapper {
    padding: 13px 0 7px;
  }
}
@media (max-width: 321px) {
  .corporate-about .zmdi {
    margin-left: 30px;
  }
  .corporate-about .icon:before {
    left: 20px;
  }
  .corporate-about .block-wrapper {
    padding-bottom: 0;
  }
  .sp-vmsearch.sp-vmsearch-slideToggle .sp-vmsearch-content {
    right: -50px;
  }
  .title1-betseller {
    margin-top: 50px !important;
  }
  .social-icons li a {
    width: 40px;
    height: 40px;
    line-height: 30px;
  }
  .vina-carousel-content.style-1 .title {
    font-size: 15px;
    font-weight: 600;
  }
  .vina-carousel-content.style-1 .date-article .article-info {
    float: none;
    padding-bottom: 3px;
  }
  .title-bg-white .sppb-carousel-inner,
  .title-bg-white .sppb-carousel-controls,
  .title-bg-white .sppb-addon-text-block h3 {
    width: 280px;
  }
  .welcome-wrapper {
    margin: 20px 0 50px 0 !important;
  }
}

</style>

<div class="category-view">
	<?php
	$js = "
		jQuery(document).ready(function () {
			jQuery('.orderlistcontainer').hover(
				function() { jQuery(this).find('.orderlist').stop().show()},
				function() { jQuery(this).find('.orderlist').stop().hide()}
			)
		});
	";
	vmJsApi::addJScript('vm.hover',$js);
	?>

	<?php  if($this->category->category_name) { ?>
		<?php  // Category Name ?>
		<div class="vm-page-title vm-category-title">
			<h1 class="hidden"><?php echo $this->category->category_name; ?></h1>
		</div>
		<?php // Category Image
		if( $show_vmcategory_image && !empty($this->category->images[0]->file_url_thumb) && isset($this->category->images[0]->file_url_thumb) ) { ?>
			<div class="cat_image">
				<?php echo $this->category->images[0]->displayMediaThumb("",false); ?>
			</div>
		<?php  } ?>
	<?php } ?>		

	<?php
	// Category Description
	if (empty($this->keyword) and !empty($this->category)) { ?>
		<?php if ($this->category->category_description) {?>
			<div class="category_description">
				<?php echo $this->category->category_description; ?>
			</div>
		<?php }?>
	<?php } ?>
	
	<?php // Show child categories
	if (VmConfig::get ('showCategory', 1) and empty($this->keyword)) {
		if (!empty($this->category->haschildren)) {
			echo ShopFunctionsF::renderVmSubLayout('categories',array('categories'=>$this->category->children));
		}
	} ?>

	<?php if($this->showproducts){ ?>
		<div id="vm-products-category" class="browse-view">
			<?php
			if (!empty($this->keyword)) {
				//id taken in the view.html.php could be modified
				$category_id  = vRequest::getInt ('virtuemart_category_id', 0); ?>
				<h3><?php echo $this->keyword; ?></h3>
				
				<form action="<?php echo JRoute::_ ('index.php?option=com_virtuemart&view=category&limitstart=0', FALSE); ?>" method="get">
					<!--BEGIN Search Box -->
					<div class="virtuemart_search">
						<?php echo $this->searchcustom ?>
						<br/>
						<?php echo $this->searchCustomValues ?>
						<input name="keyword" class="inputbox" type="text" size="20" value="<?php echo $this->keyword ?>"/>
						<input type="submit" value="<?php echo vmText::_ ('COM_VIRTUEMART_SEARCH') ?>" class="button" onclick="this.form.keyword.focus();"/>
					</div>
					<input type="hidden" name="search" value="true"/>
					<input type="hidden" name="view" value="category"/>
					<input type="hidden" name="option" value="com_virtuemart"/>
					<input type="hidden" name="virtuemart_category_id" value="<?php echo $category_id; ?>"/>

				</form>
				<!-- End Search Box -->
			<?php  } ?>

			<?php // Show orderby & displaynumber ?>
			<div class="orderby-displaynumber">
				<div class="view-mode pull-left">
					<a href="#" onclick="javascript:viewMode('grid');" class="mode-grid <?php echo  ($this->productsLayout == 'products') ? 'active' : ''; ?>" title="Grid">
					
					<i class="fa fa-th" aria-hidden="true"></i>
<span aria-hidden="true" data-icon="&#xe06a;"></span></a>
					<a href="#" onclick="javascript:viewMode('list');" class="mode-list	<?php echo  ($this->productsLayout == 'products_horizon') ? 'active' : ''; ?>" title="List">
					<i class="fa fa-list" aria-hidden="true"></i>

					<span aria-hidden="true" data-icon="&#xe067;"></span></a>
				</div>
				<div class="orderby-displaynumber-inner pull-right">
					<div class="pull-left vm-order-list">
						<?php echo $this->orderByList['orderby']; ?>
						<?php echo $this->orderByList['manufacturer']; ?>
					</div>
					
					<!--<div class="vm-pagination vm-pagination-top">
						<?php //echo $this->vmPagination->getPagesLinks (); ?>
						<span class="vm-page-counter"><?php //echo $this->vmPagination->getPagesCounter (); ?></span>
					</div> -->
					
					<div class="pull-right display-number">
						<?php echo $this->vmPagination->getResultsCounter (); ?>
						<?php echo $this->vmPagination->getLimitBox ($this->category->limit_list_step); ?>
					</div>
				</div>
				<div class="clear"></div>
			</div> <!-- end of orderby-displaynumber -->

			

			<!--<h1 class="title-category"><?php echo vmText::_($this->category->category_name); ?></h1>-->

			<?php
			if (!empty($this->products)) {
				$products = array();
				$products[0] = $this->products;
				echo shopFunctionsF::renderVmSubLayout($this->productsLayout,array('products'=>$products,'currency'=>$this->currency,'products_per_row'=>$this->perRow,'showRating'=>$this->showRating));
			?>
				<?php if ($this->vmPagination->getPagesCounter ()) { ?>
					<div class="vm-pagination vm-pagination-bottom">
						<span class="vm-page-counter"><?php echo $this->vmPagination->getPagesCounter (); ?></span>
						<?php echo $this->vmPagination->getPagesLinks (); ?>					
					</div>					
				<?php } ?>					
			<?php
			} elseif (!empty($this->keyword)) {
				echo vmText::_ ('COM_VIRTUEMART_NO_RESULT') . ($this->keyword ? ' : (' . $this->keyword . ')' : '');
			}
			?>
		</div>
	<?php } ?>
</div>

<?php
	$j = "Virtuemart.container = jQuery('.category-view');
	Virtuemart.containerSelector = '.category-view';";
	vmJsApi::addJScript('ajaxContent',$j);
?>
<!-- end browse-view -->
<script>
function viewMode(mode) {
	console.log(mode);
	if(mode == 'list') {	
		jQuery('#vm-products-category .view-mode').find('a').removeClass('active');
		jQuery('.list-product').addClass('vm_list_view');		
		jQuery('#vm-products-category').find('a.mode-list').addClass('active');
	} else {	
		jQuery('#vm-products-category .view-mode').find('a').removeClass('active');
		jQuery('.list-product').removeClass('vm_list_view');		
		jQuery('#vm-products-category').find('a.mode-grid').addClass('active');
	}
}

</script>
