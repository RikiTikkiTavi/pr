<?php
/**
 * @version    CVS: 1.0.0
 * @package    com_jbeasyformmaker
 * @author     Priya Bose <support@joomlabuff.com>
 * @copyright  2016 www.joomlabuff.com
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined ( '_JEXEC' ) or die ();

/* class JFormFieldFieldtypes extends JFormField */
class JFormFieldCFormlist extends JFormField {
	protected $type = 'cformlist';
	public function getInput() {
		$html = array ();
		// $model = FOFModel::getTmpInstance ( 'Cforms', 'FormbuilderModal' );
		$list = self::getCforms();
		$options = array ();
		foreach ( $list as $item ) {
			$options [$item->jbeasyformmaker_cform_id] = $item->title;
		}
		
		$html [] = JHtml::_ ( 'select.genericlist', $options, $this->name, array(), 'value', 'text', $this->value, $this->id );
		return implode ( $html );
	}
	public function getCforms() {
		$db = JFactory::getDbo ();
		$query = $db->getQuery ( true );
		$query->select ( '*' );
		$query->from ( '#__jbeasyformmaker_cforms' );
		$db->setQuery ( $query );
		return $db->loadObjectList ();
	}
}