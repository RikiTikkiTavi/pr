/**
 * @package     JB Smart Portfolio
 *
 * @copyright   Copyright (C) 2016 Joomlabuff. All rights reserved.
 * @license     GNU General Public License version 2 or later.
 */

jQuery(function($) {

	 var $container 	= $('.jb-smartportfolio-items');
	
	$(window).load(function() {
		var $sizer = $container.find('.shuffle__sizer');
				 
		$container.shuffle({
			itemSelector: '.jb-smartportfolio-item',
			sequentialFadeDelay: 150,
			sizer: $sizer
		});
		
	});
  
	

	// Filters
	$('.jb-smartportfolio-filter li a').on('click', function(event){
		event.preventDefault();
		var $self = $(this);
		var $this = $(this).parent();
		
		if($this.hasClass('active')) {
			return;
		}

		$self.closest('ul').children().removeClass('active');
		$self.parent().addClass('active');

		$container.shuffle( 'shuffle', $this.data('group') );
	});

});

