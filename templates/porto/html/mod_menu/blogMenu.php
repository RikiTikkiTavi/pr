<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_menu
 *
 * @copyright   Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined( '_JEXEC' ) or die;

$id = '';

if ( $tagId = $params->get( 'tag_id', '' ) ) {
	$id = ' id="' . $tagId . '"';
}

// The menu class is deprecated. Use nav instead
?>

<div class="row">
  <div class="col-md-12">
    <ul class="nav nav-pills<?php echo $class_sfx; ?>"<?php echo $id; ?>>
	<?php foreach ( $list as $i => &$item ) {
		$class = 'item-' . $item->id;

		if ( $item->id == $active_id || ( $item->type === 'alias' && $item->params->get( 'aliasoptions' ) == $active_id ) ) {
			$class .= ' active';
		}

		echo '<li class="nav-item ' . $class . '">';

		switch ( $item->type ) :
			case 'separator':
			case 'component':
			case 'heading':
			case 'url':
				require JModuleHelper::getLayoutPath( 'mod_menu', 'default_' . $item->type );
				break;

			default:
				require JModuleHelper::getLayoutPath( 'mod_menu', 'default_url' );
				break;
		endswitch;

		// The next item is deeper.
		if ( $item->deeper ) {
			echo '<ul class="nav-child unstyled small">';
		} // The next item is shallower.
        elseif ( $item->shallower ) {
			echo '</li>';
			echo str_repeat( '</ul></li>', $item->level_diff );
		} // The next item is on the same level.
		else {
			echo '</li>';
		}
	}
	?></ul>
</div>
</div>
