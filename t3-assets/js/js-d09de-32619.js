

/*===============================
/porto/media/system/js/core.js
================================================================================*/;
Joomla=window.Joomla||{},Joomla.editors=Joomla.editors||{},Joomla.editors.instances=Joomla.editors.instances||{},function(a,b){"use strict";a.submitform=function(a,c,d){c||(c=b.getElementById("adminForm")),a&&(c.task.value=a),c.noValidate=!d,c.setAttribute("novalidate",!d);var e=b.createElement("input");e.style.display="none",e.type="submit",c.appendChild(e).click(),c.removeChild(e)},a.submitbutton=function(b){a.submitform(b)},a.JText={strings:{},_:function(a,b){return"undefined"!=typeof this.strings[a.toUpperCase()]?this.strings[a.toUpperCase()]:b},load:function(a){for(var b in a)a.hasOwnProperty(b)&&(this.strings[b.toUpperCase()]=a[b]);return this}},a.replaceTokens=function(a){if(/^[0-9A-F]{32}$/i.test(a)){var d,e,f,c=b.getElementsByTagName("input");for(d=0,f=c.length;d<f;d++)e=c[d],"hidden"==e.type&&"1"==e.value&&32==e.name.length&&(e.name=a)}},a.isEmail=function(a){var b=/^[\w.!#$%&‚Äô*+\/=?^`{|}~-]+@[a-z0-9-]+(?:\.[a-z0-9-]{2,})+$/i;return b.test(a)},a.checkAll=function(a,b){if(!a.form)return!1;b=b?b:"cb";var d,e,f,c=0;for(d=0,f=a.form.elements.length;d<f;d++)e=a.form.elements[d],e.type==a.type&&0===e.id.indexOf(b)&&(e.checked=a.checked,c+=e.checked?1:0);return a.form.boxchecked&&(a.form.boxchecked.value=c),!0},a.renderMessages=function(c){a.removeMessages();var e,f,g,h,i,j,k,l,d=b.getElementById("system-message-container");for(e in c)if(c.hasOwnProperty(e)){f=c[e],g=b.createElement("div"),l="notice"==e?"alert-info":"alert-"+e,l="message"==e?"alert-success":l,g.className="alert "+l;var m=b.createElement("button");for(m.setAttribute("type","button"),m.setAttribute("data-dismiss","alert"),m.className="close",m.innerHTML="×",g.appendChild(m),h=a.JText._(e),"undefined"!=typeof h&&(i=b.createElement("h4"),i.className="alert-heading",i.innerHTML=a.JText._(e),g.appendChild(i)),j=f.length-1;j>=0;j--)k=b.createElement("div"),k.innerHTML=f[j],g.appendChild(k);d.appendChild(g)}},a.removeMessages=function(){for(var a=b.getElementById("system-message-container");a.firstChild;)a.removeChild(a.firstChild);a.style.display="none",a.offsetHeight,a.style.display=""},a.ajaxErrorsMessages=function(b,c,d){var e={};if("parsererror"==c){for(var f=b.responseText.trim(),g=[],h=f.length-1;h>=0;h--)g.unshift(["&#",f[h].charCodeAt(),";"].join(""));f=g.join(""),e.error=[a.JText._("JLIB_JS_AJAX_ERROR_PARSE").replace("%s",f)]}else"nocontent"==c?e.error=[a.JText._("JLIB_JS_AJAX_ERROR_NO_CONTENT")]:"timeout"==c?e.error=[a.JText._("JLIB_JS_AJAX_ERROR_TIMEOUT")]:"abort"==c?e.error=[a.JText._("JLIB_JS_AJAX_ERROR_CONNECTION_ABORT")]:e.error=[a.JText._("JLIB_JS_AJAX_ERROR_OTHER").replace("%s",b.status)];return e},a.isChecked=function(a,c){if("undefined"==typeof c&&(c=b.getElementById("adminForm")),c.boxchecked.value=a?parseInt(c.boxchecked.value)+1:parseInt(c.boxchecked.value)-1,c.elements["checkall-toggle"]){var e,f,g,d=!0;for(e=0,g=c.elements.length;e<g;e++)if(f=c.elements[e],"checkbox"==f.type&&"checkall-toggle"!=f.name&&!f.checked){d=!1;break}c.elements["checkall-toggle"].checked=d}},a.popupWindow=function(a,b,c,d,e){var f=(screen.width-c)/2,g=(screen.height-d)/2,h="height="+d+",width="+c+",top="+g+",left="+f+",scrollbars="+e+",resizable";window.open(a,b,h).window.focus()},a.tableOrdering=function(c,d,e,f){"undefined"==typeof f&&(f=b.getElementById("adminForm")),f.filter_order.value=c,f.filter_order_Dir.value=d,a.submitform(e,f)},window.writeDynaList=function(a,c,d,e,f,g){var k,l,m,h="<select "+a+">",i=d==e,j=0;for(l in c)c.hasOwnProperty(l)&&(m=c[l],m[0]==d&&(k="",(i&&f==m[1]||!i&&0===j)&&(k='selected="selected"'),h+='<option value="'+m[1]+'" '+k+">"+m[2]+"</option>",j++));h+="</select>",g?g.innerHTML=h:b.writeln(h)},window.changeDynaList=function(a,c,d,e,f){for(var i,j,k,l,g=b.adminForm[a],h=d==e;g.firstChild;)g.removeChild(g.firstChild);i=0;for(j in c)c.hasOwnProperty(j)&&(k=c[j],k[0]==d&&(l=new Option,l.value=k[1],l.text=k[2],(h&&f==l.value||!h&&0===i)&&(l.selected=!0),g.options[i++]=l));g.length=i},window.radioGetCheckedValue=function(a){if(!a)return"";var c,b=a.length;if(void 0===b)return a.checked?a.value:"";for(c=0;c<b;c++)if(a[c].checked)return a[c].value;return""},window.getSelectedValue=function(a,c){var d=b[a][c],e=d.selectedIndex;return null!==e&&e>-1?d.options[e].value:null},window.listItemTask=function(a,c){var f,d=b.adminForm,e=0,g=d[a];if(!g)return!1;for(;;){if(f=d["cb"+e],!f)break;f.checked=!1,e++}return g.checked=!0,d.boxchecked.value=1,window.submitform(c),!1},window.submitbutton=function(b){a.submitbutton(b)},window.submitform=function(b){a.submitform(b)},window.saveorder=function(a,b){window.checkAll_button(a,b)},window.checkAll_button=function(c,d){d=d?d:"saveorder";var e,f;for(e=0;e<=c;e++){if(f=b.adminForm["cb"+e],!f)return void alert("You cannot change the order of items, as an item in the list is `Checked Out`");f.checked=!0}a.submitform(d)},a.loadingLayer=function(c,d){if(c=c||"show",d=d||b.body,"load"==c){var e=b.getElementsByTagName("body")[0].getAttribute("data-basepath")||"",f=b.createElement("div");f.id="loading-logo",f.style.position="fixed",f.style.top="0",f.style.left="0",f.style.width="100%",f.style.height="100%",f.style.opacity="0.8",f.style.filter="alpha(opacity=80)",f.style.overflow="hidden",f.style["z-index"]="10000",f.style.display="none",f.style["background-color"]="#fff",f.style["background-image"]='url("'+e+'/media/jui/images/ajax-loader.gif")',f.style["background-position"]="center",f.style["background-repeat"]="no-repeat",f.style["background-attachment"]="fixed",d.appendChild(f)}else b.getElementById("loading-logo")||a.loadingLayer("load",d),b.getElementById("loading-logo").style.display="show"==c?"block":"none";return b.getElementById("loading-logo")}}(Joomla,document);


/*===============================
/porto/components/com_sppagebuilder/assets/js/sppagebuilder.js
================================================================================*/;
+function($){'use strict';var dismiss='[data-dismiss="sppb-alert"]'
var SPPBAlert=function(el){$(el).on('click',dismiss,this.close)}
SPPBAlert.VERSION='3.2.0'
SPPBAlert.prototype.close=function(e){var $this=$(this)
var selector=$this.attr('data-target')
if(!selector){selector=$this.attr('href')
selector=selector&&selector.replace(/.*(?=#[^\s]*$)/,'')}
var $parent=$(selector)
if(e)e.preventDefault()
if(!$parent.length){$parent=$this.hasClass('sppb-alert')?$this:$this.parent()}
$parent.trigger(e=$.Event('close.sppb.alert'))
if(e.isDefaultPrevented())return
$parent.removeClass('in')
function removeElement(){$parent.detach().trigger('closed.sppb.alert').remove()}
$.support.transition&&$parent.hasClass('sppb-fade')?$parent.one('bsTransitionEnd',removeElement).emulateTransitionEnd(150):removeElement()}
function Plugin(option){return this.each(function(){var $this=$(this)
var data=$this.data('sppb.alert')
if(!data)$this.data('sppb.alert',(data=new SPPBAlert(this)))
if(typeof option=='string')data[option].call($this)})}
var old=$.fn.spbalert
$.fn.spbalert=Plugin
$.fn.spbalert.Constructor=SPPBAlert
$.fn.spbalert.noConflict=function(){$.fn.spbalert=old
return this}
$(document).on('click.sppb.alert.data-api',dismiss,SPPBAlert.prototype.close)}(jQuery);+function($){'use strict';var SPPBCarousel=function(element,options){this.$element=$(element).on('keydown.sppb.carousel',$.proxy(this.keydown,this))
this.$indicators=this.$element.find('.sppb-carousel-indicators')
this.options=options
this.paused=this.sliding=this.interval=this.$active=this.$items=null
this.options.pause=='hover'&&this.$element.on('mouseenter.sppb.carousel',$.proxy(this.pause,this)).on('mouseleave.sppb.carousel',$.proxy(this.cycle,this))}
SPPBCarousel.VERSION='3.2.0'
SPPBCarousel.DEFAULTS={interval:5000,pause:'hover',wrap:true}
SPPBCarousel.prototype.keydown=function(e){switch(e.which){case 37:this.prev();break
case 39:this.next();break
default:return}
e.preventDefault()}
SPPBCarousel.prototype.cycle=function(e){e||(this.paused=false)
this.interval&&clearInterval(this.interval)
this.options.interval&&!this.paused&&(this.interval=setInterval($.proxy(this.next,this),this.options.interval))
return this}
SPPBCarousel.prototype.getItemIndex=function(item){this.$items=item.parent().children('.sppb-item')
return this.$items.index(item||this.$active)}
SPPBCarousel.prototype.to=function(pos){var that=this
var activeIndex=this.getItemIndex(this.$active=this.$element.find('.sppb-item.active'))
if(pos>(this.$items.length-1)||pos<0)return
if(this.sliding)return this.$element.one('slid.sppb.carousel',function(){that.to(pos)})
if(activeIndex==pos)return this.pause().cycle()
return this.slide(pos>activeIndex?'next':'prev',$(this.$items[pos]))}
SPPBCarousel.prototype.pause=function(e){e||(this.paused=true)
if(this.$element.find('.next, .prev').length&&$.support.transition){this.$element.trigger($.support.transition.end)
this.cycle(true)}
this.interval=clearInterval(this.interval)
return this}
SPPBCarousel.prototype.next=function(){if(this.sliding)return
return this.slide('next')}
SPPBCarousel.prototype.prev=function(){if(this.sliding)return
return this.slide('prev')}
SPPBCarousel.prototype.slide=function(type,next){var $active=this.$element.find('.sppb-item.active')
var $next=next||$active[type]()
var isCycling=this.interval
var direction=type=='next'?'left':'right'
var fallback=type=='next'?'first':'last'
var that=this
if(!$next.length){if(!this.options.wrap)return
$next=this.$element.find('.sppb-item')[fallback]()}
if($next.hasClass('active'))return(this.sliding=false)
var relatedTarget=$next[0]
var slideEvent=$.Event('slide.sppb.carousel',{relatedTarget:relatedTarget,direction:direction})
this.$element.trigger(slideEvent)
if(slideEvent.isDefaultPrevented())return
this.sliding=true
isCycling&&this.pause()
if(this.$indicators.length){this.$indicators.find('.active').removeClass('active')
var $nextIndicator=$(this.$indicators.children()[this.getItemIndex($next)])
$nextIndicator&&$nextIndicator.addClass('active')}
var slidEvent=$.Event('slid.sppb.carousel',{relatedTarget:relatedTarget,direction:direction})
if($.support.transition&&this.$element.hasClass('sppb-slide')){$next.addClass(type)
$next[0].offsetWidth
$active.addClass(direction)
$next.addClass(direction)
$active.one('bsTransitionEnd',function(){$next.removeClass([type,direction].join(' ')).addClass('active')
$active.removeClass(['active',direction].join(' '))
that.sliding=false
setTimeout(function(){that.$element.trigger(slidEvent)},0)}).emulateTransitionEnd($active.css('transition-duration').slice(0,-1)*1000)}else{$active.removeClass('active')
$next.addClass('active')
this.sliding=false
this.$element.trigger(slidEvent)}
isCycling&&this.cycle()
return this}
function Plugin(option){return this.each(function(){var $this=$(this)
var data=$this.data('sppb.carousel')
var options=$.extend({},SPPBCarousel.DEFAULTS,$this.data(),typeof option=='object'&&option)
var action=typeof option=='string'?option:options.slide
if(!data)$this.data('sppb.carousel',(data=new SPPBCarousel(this,options)))
if(typeof option=='number')data.to(option)
else if(action)data[action]()
else if(options.interval)data.pause().cycle()})}
var old=$.fn.sppbcarousel
$.fn.sppbcarousel=Plugin
$.fn.sppbcarousel.Constructor=SPPBCarousel
$.fn.sppbcarousel.noConflict=function(){$.fn.sppbcarousel=old
return this}
$(document).ready(function(){$('.sppb-carousel').each(function(index){var items=$(this).find('.sppb-item');var id='sppb-carousel'+(index+1);var controller_items='';$(this).attr('id',id);for(var i=0;i<items.length;i++){if(i==0){controller_items+='<li data-sppb-target="#'+id+'" class="active" data-sppb-slide-to="0"></li>';}else{controller_items+='\n<li data-sppb-target="#'+id+'" data-sppb-slide-to="'+i+'"></li>';}};$(this).find('>.sppb-carousel-indicators').html(controller_items);$(this).find('.sppb-carousel-control').attr('href','#'+id);$(this).find('.sppb-item').first().addClass('active');});})
$(document).on('click.sppb.carousel.data-api','[data-slide], [data-sppb-slide-to]',function(e){var href
var $this=$(this)
var $target=$($this.attr('data-sppb-target')||(href=$this.attr('href'))&&href.replace(/.*(?=#[^\s]+$)/,''))
if(!$target.hasClass('sppb-carousel'))return
var options=$.extend({},$target.data(),$this.data())
var slideIndex=$this.attr('data-sppb-slide-to')
if(slideIndex)options.interval=false
Plugin.call($target,options)
if(slideIndex){$target.data('sppb.carousel').to(slideIndex)}
e.preventDefault()})
$(window).on('load',function(){$('[data-sppb-ride="sppb-carousel"]').each(function(){var $carousel=$(this)
Plugin.call($carousel,$carousel.data())})})}(jQuery);+function($){'use strict';$(document).on('click','.sppb-panel-heading',function(event){event.preventDefault();var $this=$(this);var $items=$this.closest('.sppb-panel-group').find('>div');var $handlers=$items.find('.sppb-panel-heading');var $panels=$items.find('.sppb-panel-collapse');if($(this).hasClass('active'))
{$(this).removeClass('active');$panels.slideUp();}
else
{$handlers.removeClass('active');$panels.slideUp();$(this).addClass('active').next().slideDown();}});}(jQuery);+function($){'use strict';var SPPBTab=function(element){this.element=$(element)}
SPPBTab.VERSION='3.2.0'
SPPBTab.prototype.show=function(){var $this=this.element
var $ul=$this.closest('ul:not(.dropdown-menu)')
var selector=$this.data('target')
if(!selector){selector=$this.attr('href')
selector=selector&&selector.replace(/.*(?=#[^\s]*$)/,'')}
if($this.parent('li').hasClass('active'))return
var previous=$ul.find('.active:last a')[0]
var e=$.Event('show.sppb.tab',{relatedTarget:previous})
$this.trigger(e)
if(e.isDefaultPrevented())return
var $target=$(selector)
this.activate($this.closest('li'),$ul)
this.activate($target,$target.parent(),function(){$this.trigger({type:'shown.sppb.tab',relatedTarget:previous})})}
SPPBTab.prototype.activate=function(element,container,callback){var $active=container.find('> .active')
var transition=callback&&$.support.transition&&$active.hasClass('sppb-fade')
function next(){$active.removeClass('active').find('> .dropdown-menu > .active').removeClass('active')
element.addClass('active')
if(transition){element[0].offsetWidth
element.addClass('in')}else{element.removeClass('sppb-fade')}
if(element.parent('.dropdown-menu')){element.closest('li.dropdown').addClass('active')}
callback&&callback()}
transition?$active.one('bsTransitionEnd',next).emulateTransitionEnd(150):next()
$active.removeClass('in')}
function Plugin(option){return this.each(function(){var $this=$(this)
var data=$this.data('sppb.tab')
if(!data)$this.data('sppb.tab',(data=new SPPBTab(this)))
if(typeof option=='string')data[option]()})}
var old=$.fn.sppbtab
$.fn.sppbtab=Plugin
$.fn.sppbtab.Constructor=SPPBTab
$.fn.sppbtab.noConflict=function(){$.fn.sppbtab=old
return this}
$(document).ready(function(){$('.sppb-tab').each(function(index){var id='sppb-tab'+(index+1);$(this).find('>.sppb-nav').children().each(function(index){$(this).find('>a').attr('href','#'+id+'-'+(index+1))});$(this).find('>.sppb-tab-content').children().each(function(index){$(this).attr('id',id+'-'+(index+1))});});});$(document).on('click.sppb.tab.data-api','[data-toggle="sppb-tab"], [data-toggle="sppb-pill"]',function(e){e.preventDefault()
Plugin.call($(this),'show')})}(jQuery);+function($){'use strict';var SPPBTooltip=function(element,options){this.type=this.options=this.enabled=this.timeout=this.hoverState=this.$element=null
this.init('sppbtooltip',element,options)}
SPPBTooltip.VERSION='3.2.0'
SPPBTooltip.DEFAULTS={animation:true,placement:'top',selector:false,template:'<div class="sppb-tooltip" role="tooltip"><div class="sppb-tooltip-arrow"></div><div class="sppb-tooltip-inner"></div></div>',trigger:'hover focus',title:'',delay:0,html:false,container:false,viewport:{selector:'body',padding:0}}
SPPBTooltip.prototype.init=function(type,element,options){this.enabled=true
this.type=type
this.$element=$(element)
this.options=this.getOptions(options)
this.$viewport=this.options.viewport&&$(this.options.viewport.selector||this.options.viewport)
var triggers=this.options.trigger.split(' ')
for(var i=triggers.length;i--;){var trigger=triggers[i]
if(trigger=='click'){this.$element.on('click.'+this.type,this.options.selector,$.proxy(this.toggle,this))}else if(trigger!='manual'){var eventIn=trigger=='hover'?'mouseenter':'focusin'
var eventOut=trigger=='hover'?'mouseleave':'focusout'
this.$element.on(eventIn+'.'+this.type,this.options.selector,$.proxy(this.enter,this))
this.$element.on(eventOut+'.'+this.type,this.options.selector,$.proxy(this.leave,this))}}
this.options.selector?(this._options=$.extend({},this.options,{trigger:'manual',selector:''})):this.fixTitle()}
SPPBTooltip.prototype.getDefaults=function(){return SPPBTooltip.DEFAULTS}
SPPBTooltip.prototype.getOptions=function(options){options=$.extend({},this.getDefaults(),this.$element.data(),options)
if(options.delay&&typeof options.delay=='number'){options.delay={show:options.delay,hide:options.delay}}
return options}
SPPBTooltip.prototype.getDelegateOptions=function(){var options={}
var defaults=this.getDefaults()
this._options&&$.each(this._options,function(key,value){if(defaults[key]!=value)options[key]=value})
return options}
SPPBTooltip.prototype.enter=function(obj){var self=obj instanceof this.constructor?obj:$(obj.currentTarget).data('sppb.'+this.type)
if(!self){self=new this.constructor(obj.currentTarget,this.getDelegateOptions())
$(obj.currentTarget).data('sppb.'+this.type,self)}
clearTimeout(self.timeout)
self.hoverState='in'
if(!self.options.delay||!self.options.delay.show)return self.show()
self.timeout=setTimeout(function(){if(self.hoverState=='in')self.show()},self.options.delay.show)}
SPPBTooltip.prototype.leave=function(obj){var self=obj instanceof this.constructor?obj:$(obj.currentTarget).data('sppb.'+this.type)
if(!self){self=new this.constructor(obj.currentTarget,this.getDelegateOptions())
$(obj.currentTarget).data('sppb.'+this.type,self)}
clearTimeout(self.timeout)
self.hoverState='out'
if(!self.options.delay||!self.options.delay.hide)return self.hide()
self.timeout=setTimeout(function(){if(self.hoverState=='out')self.hide()},self.options.delay.hide)}
SPPBTooltip.prototype.show=function(){var e=$.Event('show.sppb.'+this.type)
if(this.hasContent()&&this.enabled){this.$element.trigger(e)
var inDom=$.contains(document.documentElement,this.$element[0])
if(e.isDefaultPrevented()||!inDom)return
var that=this
var $tip=this.tip()
var tipId=this.getUID(this.type)
this.setContent()
$tip.attr('id',tipId)
this.$element.attr('aria-describedby',tipId)
if(this.options.animation)$tip.addClass('sppb-fade')
var placement=typeof this.options.placement=='function'?this.options.placement.call(this,$tip[0],this.$element[0]):this.options.placement
var autoToken=/\s?auto?\s?/i
var autoPlace=autoToken.test(placement)
if(autoPlace)placement=placement.replace(autoToken,'')||'top'
$tip.detach().css({top:0,left:0,display:'block'}).addClass(placement).data('sppb.'+this.type,this)
this.options.container?$tip.appendTo(this.options.container):$tip.insertAfter(this.$element)
var pos=this.getPosition()
var actualWidth=$tip[0].offsetWidth
var actualHeight=$tip[0].offsetHeight
if(autoPlace){var orgPlacement=placement
var $parent=this.$element.parent()
var parentDim=this.getPosition($parent)
placement=placement=='bottom'&&pos.top+pos.height+actualHeight-parentDim.scroll>parentDim.height?'top':placement=='top'&&pos.top-parentDim.scroll-actualHeight<0?'bottom':placement=='right'&&pos.right+actualWidth>parentDim.width?'left':placement=='left'&&pos.left-actualWidth<parentDim.left?'right':placement
$tip.removeClass(orgPlacement).addClass(placement)}
var calculatedOffset=this.getCalculatedOffset(placement,pos,actualWidth,actualHeight)
this.applyPlacement(calculatedOffset,placement)
var complete=function(){that.$element.trigger('shown.sppb.'+that.type)
that.hoverState=null}
$.support.transition&&this.$tip.hasClass('sppb-')?$tip.one('bsTransitionEnd',complete).emulateTransitionEnd(150):complete()}}
SPPBTooltip.prototype.applyPlacement=function(offset,placement){var $tip=this.tip()
var width=$tip[0].offsetWidth
var height=$tip[0].offsetHeight
var marginTop=parseInt($tip.css('margin-top'),10)
var marginLeft=parseInt($tip.css('margin-left'),10)
if(isNaN(marginTop))marginTop=0
if(isNaN(marginLeft))marginLeft=0
offset.top=offset.top+marginTop
offset.left=offset.left+marginLeft
$.offset.setOffset($tip[0],$.extend({using:function(props){$tip.css({top:Math.round(props.top),left:Math.round(props.left)})}},offset),0)
$tip.addClass('in')
var actualWidth=$tip[0].offsetWidth
var actualHeight=$tip[0].offsetHeight
if(placement=='top'&&actualHeight!=height){offset.top=offset.top+height-actualHeight}
var delta=this.getViewportAdjustedDelta(placement,offset,actualWidth,actualHeight)
if(delta.left)offset.left+=delta.left
else offset.top+=delta.top
var arrowDelta=delta.left?delta.left*2-width+actualWidth:delta.top*2-height+actualHeight
var arrowPosition=delta.left?'left':'top'
var arrowOffsetPosition=delta.left?'offsetWidth':'offsetHeight'
$tip.offset(offset)
this.replaceArrow(arrowDelta,$tip[0][arrowOffsetPosition],arrowPosition)}
SPPBTooltip.prototype.replaceArrow=function(delta,dimension,position){this.arrow().css(position,delta?(50*(1-delta/dimension)+'%'):'')}
SPPBTooltip.prototype.setContent=function(){var $tip=this.tip()
var title=this.getTitle()
$tip.find('.sppb-tooltip-inner')[this.options.html?'html':'text'](title)
$tip.removeClass('sppb-fade in top bottom left right')}
SPPBTooltip.prototype.hide=function(){var that=this
var $tip=this.tip()
var e=$.Event('hide.sppb.'+this.type)
this.$element.removeAttr('aria-describedby')
function complete(){if(that.hoverState!='in')$tip.detach()
that.$element.trigger('hidden.sppb.'+that.type)}
this.$element.trigger(e)
if(e.isDefaultPrevented())return
$tip.removeClass('in')
$.support.transition&&this.$tip.hasClass('sppb-fade')?$tip.one('bsTransitionEnd',complete).emulateTransitionEnd(150):complete()
this.hoverState=null
return this}
SPPBTooltip.prototype.fixTitle=function(){var $e=this.$element
if($e.attr('title')||typeof($e.attr('data-original-title'))!='string'){$e.attr('data-original-title',$e.attr('title')||'').attr('title','')}}
SPPBTooltip.prototype.hasContent=function(){return this.getTitle()}
SPPBTooltip.prototype.getPosition=function($element){$element=$element||this.$element
var el=$element[0]
var isBody=el.tagName=='BODY'
return $.extend({},(typeof el.getBoundingClientRect=='function')?el.getBoundingClientRect():null,{scroll:isBody?document.documentElement.scrollTop||document.body.scrollTop:$element.scrollTop(),width:isBody?$(window).width():$element.outerWidth(),height:isBody?$(window).height():$element.outerHeight()},isBody?{top:0,left:0}:$element.offset())}
SPPBTooltip.prototype.getCalculatedOffset=function(placement,pos,actualWidth,actualHeight){return placement=='bottom'?{top:pos.top+pos.height,left:pos.left+pos.width/2-actualWidth/2}:placement=='top'?{top:pos.top-actualHeight,left:pos.left+pos.width/2-actualWidth/2}:placement=='left'?{top:pos.top+pos.height/2-actualHeight/2,left:pos.left-actualWidth}:{top:pos.top+pos.height/2-actualHeight/2,left:pos.left+pos.width}}
SPPBTooltip.prototype.getViewportAdjustedDelta=function(placement,pos,actualWidth,actualHeight){var delta={top:0,left:0}
if(!this.$viewport)return delta
var viewportPadding=this.options.viewport&&this.options.viewport.padding||0
var viewportDimensions=this.getPosition(this.$viewport)
if(/right|left/.test(placement)){var topEdgeOffset=pos.top-viewportPadding-viewportDimensions.scroll
var bottomEdgeOffset=pos.top+viewportPadding-viewportDimensions.scroll+actualHeight
if(topEdgeOffset<viewportDimensions.top){delta.top=viewportDimensions.top-topEdgeOffset}else if(bottomEdgeOffset>viewportDimensions.top+viewportDimensions.height){delta.top=viewportDimensions.top+viewportDimensions.height-bottomEdgeOffset}}else{var leftEdgeOffset=pos.left-viewportPadding
var rightEdgeOffset=pos.left+viewportPadding+actualWidth
if(leftEdgeOffset<viewportDimensions.left){delta.left=viewportDimensions.left-leftEdgeOffset}else if(rightEdgeOffset>viewportDimensions.width){delta.left=viewportDimensions.left+viewportDimensions.width-rightEdgeOffset}}
return delta}
SPPBTooltip.prototype.getTitle=function(){var title
var $e=this.$element
var o=this.options
title=$e.attr('data-original-title')||(typeof o.title=='function'?o.title.call($e[0]):o.title)
return title}
SPPBTooltip.prototype.getUID=function(prefix){do prefix+=~~(Math.random()*1000000)
while(document.getElementById(prefix))
return prefix}
SPPBTooltip.prototype.tip=function(){return(this.$tip=this.$tip||$(this.options.template))}
SPPBTooltip.prototype.arrow=function(){return(this.$arrow=this.$arrow||this.tip().find('.sppb-tooltip-arrow'))}
SPPBTooltip.prototype.validate=function(){if(!this.$element[0].parentNode){this.hide()
this.$element=null
this.options=null}}
SPPBTooltip.prototype.enable=function(){this.enabled=true}
SPPBTooltip.prototype.disable=function(){this.enabled=false}
SPPBTooltip.prototype.toggleEnabled=function(){this.enabled=!this.enabled}
SPPBTooltip.prototype.toggle=function(e){var self=this
if(e){self=$(e.currentTarget).data('sppb.'+this.type)
if(!self){self=new this.constructor(e.currentTarget,this.getDelegateOptions())
$(e.currentTarget).data('sppb.'+this.type,self)}}
self.tip().hasClass('in')?self.leave(self):self.enter(self)}
SPPBTooltip.prototype.destroy=function(){clearTimeout(this.timeout)
this.hide().$element.off('.'+this.type).removeData('sppb.'+this.type)}
function Plugin(option){return this.each(function(){var $this=$(this)
var data=$this.data('sppb.tooltip')
var options=typeof option=='object'&&option
if(!data&&option=='destroy')return
if(!data)$this.data('sppb.tooltip',(data=new SPPBTooltip(this,options)))
if(typeof option=='string')data[option]()})}
var old=$.fn.sppbtooltip
$.fn.sppbtooltip=Plugin
$.fn.sppbtooltip.Constructor=SPPBTooltip
$.fn.sppbtooltip.noConflict=function(){$.fn.sppbtooltip=old
return this}}(jQuery);+function($){'use strict';var SPPBPopover=function(element,options){this.init('sppbpopover',element,options)}
if(!$.fn.sppbtooltip)throw new Error('Popover requires tooltip.js')
SPPBPopover.VERSION='3.2.0'
SPPBPopover.DEFAULTS=$.extend({},$.fn.sppbtooltip.Constructor.DEFAULTS,{placement:'right',trigger:'click',content:'',template:'<div class="sppb-popover" role="tooltip"><div class="arrow"></div><h3 class="sppb-popover-title"></h3><div class="sppb-popover-content"></div></div>'})
SPPBPopover.prototype=$.extend({},$.fn.sppbtooltip.Constructor.prototype)
SPPBPopover.prototype.constructor=SPPBPopover
SPPBPopover.prototype.getDefaults=function(){return SPPBPopover.DEFAULTS}
SPPBPopover.prototype.setContent=function(){var $tip=this.tip()
var title=this.getTitle()
var content=this.getContent()
$tip.find('.sppb-popover-title')[this.options.html?'html':'text'](title)
$tip.find('.sppb-popover-content').empty()[this.options.html?(typeof content=='string'?'html':'append'):'text'](content)
$tip.removeClass('sppb-fade top bottom left right in')
if(!$tip.find('.sppb-popover-title').html())$tip.find('.sppb-popover-title').hide()}
SPPBPopover.prototype.hasContent=function(){return this.getTitle()||this.getContent()}
SPPBPopover.prototype.getContent=function(){var $e=this.$element
var o=this.options
return $e.attr('data-content')||(typeof o.content=='function'?o.content.call($e[0]):o.content)}
SPPBPopover.prototype.arrow=function(){return(this.$arrow=this.$arrow||this.tip().find('.arrow'))}
SPPBPopover.prototype.tip=function(){if(!this.$tip)this.$tip=$(this.options.template)
return this.$tip}
function Plugin(option){return this.each(function(){var $this=$(this)
var data=$this.data('sppb.popover')
var options=typeof option=='object'&&option
if(!data&&option=='destroy')return
if(!data)$this.data('sppb.popover',(data=new SPPBPopover(this,options)))
if(typeof option=='string')data[option]()})}
var old=$.fn.sppbpopover
$.fn.sppbpopover=Plugin
$.fn.sppbpopover.Constructor=SPPBPopover
$.fn.sppbpopover.noConflict=function(){$.fn.sppbpopover=old
return this}}(jQuery);+function($){'use strict';function transitionEnd(){var el=document.createElement('bootstrap')
var transEndEventNames={WebkitTransition:'webkitTransitionEnd',MozTransition:'transitionend',OTransition:'oTransitionEnd otransitionend',transition:'transitionend'}
for(var name in transEndEventNames){if(el.style[name]!==undefined){return{end:transEndEventNames[name]}}}
return false}
$.fn.emulateTransitionEnd=function(duration){var called=false
var $el=this
$(this).one('bsTransitionEnd',function(){called=true})
var callback=function(){if(!called)$($el).trigger($.support.transition.end)}
setTimeout(callback,duration)
return this}
$(function(){$.support.transition=transitionEnd()
if(!$.support.transition)return
$.event.special.bsTransitionEnd={bindType:$.support.transition.end,delegateType:$.support.transition.end,handle:function(e){if($(e.target).is(this))return e.handleObj.handler.apply(this,arguments)}}})}(jQuery);if(typeof jQuery!='undefined'&&typeof MooTools!='undefined'){(function($){$(document).ready(function(){$('.sppb-carousel').each(function(index,element){$(this)[index].slide=null;});});})(jQuery);};(function($,window,document,navigator){"use strict";var pluginName="vide",defaults={volume:1,playbackRate:1,muted:true,loop:true,autoplay:true,position:"50% 50%"};var iOS=/iPad|iPhone|iPod/i.test(navigator.userAgent),android=/Android/i.test(navigator.userAgent);$[pluginName]={lookup:[]};var parseOptions=function(str){var obj={},clearedStr,arr;clearedStr=str.replace(/\s*:\s*/g,":").replace(/\s*,\s*/g,",");arr=clearedStr.split(",");var i,len,val;for(i=0,len=arr.length;i<len;i++){arr[i]=arr[i].split(":");val=arr[i][1];if(!val){val=undefined;}
if(typeof val==="string"||val instanceof String){val=val==="true"||(val==="false"?false:val);}
if(typeof val==="string"||val instanceof String){val=!isNaN(val)?+val:val;}
obj[arr[i][0]]=val;}
return obj;};var parsePosition=function(str){str=""+str;var args=str.split(/\s+/),x="50%",y="50%";for(var i=0,len=args.length,arg;i<len;i++){arg=args[i];if(arg==="left"){x="0%";}else if(arg==="right"){x="100%";}else if(arg==="top"){y="0%";}else if(arg==="bottom"){y="100%";}else if(arg==="center"){if(i===0){x="50%";}else{y="50%";}}else{if(i===0){x=arg;}else{y=arg;}}}
return{x:x,y:y};};function Vide(element,path,options){this.element=$(element);this._defaults=defaults;this._name=pluginName;path=path.replace(/\.\w*$/,"");this.settings=$.extend({},defaults,options);this.path=path;this.init();}
Vide.prototype.init=function(){var that=this;this.wrapper=$("<div>");var position=parsePosition(this.settings.position);this.wrapper.css({"position":"absolute","z-index":-1,"top":0,"left":0,"bottom":0,"right":0,"overflow":"hidden","-webkit-background-size":"cover","-moz-background-size":"cover","-o-background-size":"cover","background-size":"cover","background-repeat":"no-repeat","background-position":position.x+" "+position.y});if($(this.element).data('vide-image')){this.wrapper.css("background-image","url("+$(this.element).data('vide-image')+")");}
if(this.element.css("position")==="static"){this.element.css("position","relative");}
this.element.prepend(this.wrapper);if(!iOS&&!android){var video_src='';if($(this.element).data('vide-mp4')){video_src+="<source src='"+$(this.element).data('vide-mp4')+"' type='video/mp4'>";}
if($(this.element).data('vide-ogv')){video_src+="<source src='"+$(this.element).data('vide-ogv')+"' type='video/ogg'>";}
this.video=$("<video>"+video_src+"</video>");this.video.css("visibility","hidden");this.video.prop({autoplay:this.settings.autoplay,loop:this.settings.loop,volume:this.settings.volume,muted:this.settings.muted,playbackRate:this.settings.playbackRate});this.wrapper.append(this.video);this.video.css({"margin":"auto","position":"absolute","z-index":-1,"top":position.y,"left":position.x,"-webkit-transform":"translate(-"+position.x+", -"+position.y+")","-ms-transform":"translate(-"+position.x+", -"+position.y+")","transform":"translate(-"+position.x+", -"+position.y+")"});this.video.bind("loadedmetadata."+pluginName,function(){that.video.css("visibility","visible");that.resize();});$(this.element).bind("resize."+pluginName,function(){that.resize();});}};Vide.prototype.getVideoObject=function(){return this.video?this.video[0]:null;};Vide.prototype.resize=function(){if(!this.video){return;}
var videoHeight=this.video[0].videoHeight,videoWidth=this.video[0].videoWidth;var wrapperHeight=this.wrapper.height(),wrapperWidth=this.wrapper.width();if(wrapperWidth/videoWidth>wrapperHeight/videoHeight){this.video.css({"width":wrapperWidth+2,"height":"auto"});}else{this.video.css({"width":"auto","height":wrapperHeight+2});}};Vide.prototype.destroy=function(){this.element.unbind(pluginName);if(this.video){this.video.unbind(pluginName);}
delete $[pluginName].lookup[this.index];this.element.removeData(pluginName);this.wrapper.remove();};$.fn[pluginName]=function(path,options){var instance;this.each(function(){instance=$.data(this,pluginName);if(instance){instance.destroy();}
instance=new Vide(this,path,options);instance.index=$[pluginName].lookup.push(instance)-1;$.data(this,pluginName,instance);});return this;};$(document).ready(function(){$(window).bind("resize."+pluginName,function(){for(var len=$[pluginName].lookup.length,instance,i=0;i<len;i++){instance=$[pluginName].lookup[i];if(instance){instance.resize();}}});$(document).find("[data-"+pluginName+"-bg]").each(function(i,element){var $element=$(element),options=$element.data(pluginName+"-options"),path='';if(!options){options={};}else{options=parseOptions(options);}
$element[pluginName](path,options);});});})(window.jQuery,window,document,navigator);
/*! WOW - v1.0.1 - 2014-08-15
 * Copyright (c) 2014 Matthieu Aussaguel; Licensed MIT */
;(function(){var a,b,c,d=function(a,b){return function(){return a.apply(b,arguments)}},e=[].indexOf||function(a){for(var b=0,c=this.length;c>b;b++)
if(b in this&&this[b]===a)return b;return-1};b=function(){function a(){}
return a.prototype.extend=function(a,b){var c,d;for(c in b)d=b[c],null==a[c]&&(a[c]=d);return a},a.prototype.isMobile=function(a){return /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(a)},a}(),c=this.WeakMap||this.MozWeakMap||(c=function(){function a(){this.keys=[],this.values=[]}
return a.prototype.get=function(a){var b,c,d,e,f;for(f=this.keys,b=d=0,e=f.length;e>d;b=++d)
if(c=f[b],c===a)return this.values[b]},a.prototype.set=function(a,b){var c,d,e,f,g;for(g=this.keys,c=e=0,f=g.length;f>e;c=++e)
if(d=g[c],d===a)return void(this.values[c]=b);return this.keys.push(a),this.values.push(b)},a}()),a=this.MutationObserver||this.WebkitMutationObserver||this.MozMutationObserver||(a=function(){function a(){console.warn("MutationObserver is not supported by your browser."),console.warn("WOW.js cannot detect dom mutations, please call .sync() after loading new content.")}
return a.notSupported=!0,a.prototype.observe=function(){},a}()),this.SPPBWOW=function(){function f(a){null==a&&(a={}),this.scrollCallback=d(this.scrollCallback,this),this.scrollHandler=d(this.scrollHandler,this),this.start=d(this.start,this),this.scrolled=!0,this.config=this.util().extend(a,this.defaults),this.animationNameCache=new c}
return f.prototype.defaults={boxClass:"sppb-wow",animateClass:"sppb-animated",offset:0,mobile:!0,live:!0},f.prototype.init=function(){var a;return this.element=window.document.documentElement,"interactive"===(a=document.readyState)||"complete"===a?this.start():document.addEventListener("DOMContentLoaded",this.start),this.finished=[]},f.prototype.start=function(){var b,c,d,e;if(this.stopped=!1,this.boxes=function(){var a,c,d,e;for(d=this.element.querySelectorAll("."+this.config.boxClass),e=[],a=0,c=d.length;c>a;a++)b=d[a],e.push(b);return e}.call(this),this.all=function(){var a,c,d,e;for(d=this.boxes,e=[],a=0,c=d.length;c>a;a++)b=d[a],e.push(b);return e}.call(this),this.boxes.length)
if(this.disabled())this.resetStyle();else{for(e=this.boxes,c=0,d=e.length;d>c;c++)b=e[c],this.applyStyle(b,!0);window.addEventListener("scroll",this.scrollHandler,!1),window.addEventListener("resize",this.scrollHandler,!1),this.interval=setInterval(this.scrollCallback,50)}
return this.config.live?new a(function(a){return function(b){var c,d,e,f,g;for(g=[],e=0,f=b.length;f>e;e++)d=b[e],g.push(function(){var a,b,e,f;for(e=d.addedNodes||[],f=[],a=0,b=e.length;b>a;a++)c=e[a],f.push(this.doSync(c));return f}.call(a));return g}}(this)).observe(document.body,{childList:!0,subtree:!0}):void 0},f.prototype.stop=function(){return this.stopped=!0,window.removeEventListener("scroll",this.scrollHandler,!1),window.removeEventListener("resize",this.scrollHandler,!1),null!=this.interval?clearInterval(this.interval):void 0},f.prototype.sync=function(){return a.notSupported?this.doSync(this.element):void 0},f.prototype.doSync=function(a){var b,c,d,f,g;if(!this.stopped){if(null==a&&(a=this.element),1!==a.nodeType)return;for(a=a.parentNode||a,f=a.querySelectorAll("."+this.config.boxClass),g=[],c=0,d=f.length;d>c;c++)b=f[c],e.call(this.all,b)<0?(this.applyStyle(b,!0),this.boxes.push(b),this.all.push(b),g.push(this.scrolled=!0)):g.push(void 0);return g}},f.prototype.show=function(a){return this.applyStyle(a),a.className=""+a.className+" "+this.config.animateClass},f.prototype.applyStyle=function(a,b){var c,d,e;return d=a.getAttribute("data-sppb-wow-duration"),c=a.getAttribute("data-sppb-wow-delay"),e=a.getAttribute("data-sppb-wow-iteration"),this.animate(function(f){return function(){return f.customStyle(a,b,d,c,e)}}(this))},f.prototype.animate=function(){return"requestAnimationFrame"in window?function(a){return window.requestAnimationFrame(a)}:function(a){return a()}}(),f.prototype.resetStyle=function(){var a,b,c,d,e;for(d=this.boxes,e=[],b=0,c=d.length;c>b;b++)a=d[b],e.push(a.setAttribute("style","visibility: visible;"));return e},f.prototype.customStyle=function(a,b,c,d,e){return b&&this.cacheAnimationName(a),a.style.visibility=b?"hidden":"visible",c&&this.vendorSet(a.style,{animationDuration:c}),d&&this.vendorSet(a.style,{animationDelay:d}),e&&this.vendorSet(a.style,{animationIterationCount:e}),this.vendorSet(a.style,{animationName:b?"none":this.cachedAnimationName(a)}),a},f.prototype.vendors=["moz","webkit"],f.prototype.vendorSet=function(a,b){var c,d,e,f;f=[];for(c in b)d=b[c],a[""+c]=d,f.push(function(){var b,f,g,h;for(g=this.vendors,h=[],b=0,f=g.length;f>b;b++)e=g[b],h.push(a[""+e+c.charAt(0).toUpperCase()+c.substr(1)]=d);return h}.call(this));return f},f.prototype.vendorCSS=function(a,b){var c,d,e,f,g,h;for(d=window.getComputedStyle(a),c=d.getPropertyCSSValue(b),h=this.vendors,f=0,g=h.length;g>f;f++)e=h[f],c=c||d.getPropertyCSSValue("-"+e+"-"+b);return c},f.prototype.animationName=function(a){var b;try{b=this.vendorCSS(a,"animation-name").cssText}catch(c){b=window.getComputedStyle(a).getPropertyValue("animation-name")}
return"none"===b?"":b},f.prototype.cacheAnimationName=function(a){return this.animationNameCache.set(a,this.animationName(a))},f.prototype.cachedAnimationName=function(a){return this.animationNameCache.get(a)},f.prototype.scrollHandler=function(){return this.scrolled=!0},f.prototype.scrollCallback=function(){var a;return!this.scrolled||(this.scrolled=!1,this.boxes=function(){var b,c,d,e;for(d=this.boxes,e=[],b=0,c=d.length;c>b;b++)a=d[b],a&&(this.isVisible(a)?this.show(a):e.push(a));return e}.call(this),this.boxes.length||this.config.live)?void 0:this.stop()},f.prototype.offsetTop=function(a){for(var b;void 0===a.offsetTop;)a=a.parentNode;for(b=a.offsetTop;a=a.offsetParent;)b+=a.offsetTop;return b},f.prototype.isVisible=function(a){var b,c,d,e,f;return c=a.getAttribute("data-sppb-wow-offset")||this.config.offset,f=window.pageYOffset,e=f+Math.min(this.element.clientHeight,innerHeight)-c,d=this.offsetTop(a),b=d+a.clientHeight,e>=d&&b>=f},f.prototype.util=function(){return null!=this._util?this._util:this._util=new b},f.prototype.disabled=function(){return!this.config.mobile&&this.util().isMobile(navigator.userAgent)},f}()}).call(this);jQuery(function($){new SPPBWOW().init();});(function($){var inviewObjects={},viewportSize,viewportOffset,d=document,w=window,documentElement=d.documentElement,expando=$.expando,timer;$.event.special.inview={add:function(data){inviewObjects[data.guid+"-"+this[expando]]={data:data,$element:$(this)};if(!timer&&!$.isEmptyObject(inviewObjects)){timer=setInterval(checkInView,250);}},remove:function(data){try{delete inviewObjects[data.guid+"-"+this[expando]];}catch(e){}
if($.isEmptyObject(inviewObjects)){clearInterval(timer);timer=null;}}};function getViewportSize(){var mode,domObject,size={height:w.innerHeight,width:w.innerWidth};if(!size.height){mode=d.compatMode;if(mode||!$.support.boxModel){domObject=mode==='CSS1Compat'?documentElement:d.body;size={height:domObject.clientHeight,width:domObject.clientWidth};}}
return size;}
function getViewportOffset(){return{top:w.pageYOffset||documentElement.scrollTop||d.body.scrollTop,left:w.pageXOffset||documentElement.scrollLeft||d.body.scrollLeft};}
function checkInView(){var $elements=$(),elementsLength,i=0;$.each(inviewObjects,function(i,inviewObject){var selector=inviewObject.data.selector,$element=inviewObject.$element;$elements=$elements.add(selector?$element.find(selector):$element);});elementsLength=$elements.length;if(elementsLength){viewportSize=viewportSize||getViewportSize();viewportOffset=viewportOffset||getViewportOffset();for(;i<elementsLength;i++){if(!$.contains(documentElement,$elements[i])){continue;}
var $element=$($elements[i]),elementSize={height:$element.height(),width:$element.width()},elementOffset=$element.offset(),inView=$element.data('inview'),visiblePartX,visiblePartY,visiblePartsMerged;if(!viewportOffset||!viewportSize){return;}
if(elementOffset.top+elementSize.height>viewportOffset.top&&elementOffset.top<viewportOffset.top+viewportSize.height&&elementOffset.left+elementSize.width>viewportOffset.left&&elementOffset.left<viewportOffset.left+viewportSize.width){visiblePartX=(viewportOffset.left>elementOffset.left?'right':(viewportOffset.left+viewportSize.width)<(elementOffset.left+elementSize.width)?'left':'both');visiblePartY=(viewportOffset.top>elementOffset.top?'bottom':(viewportOffset.top+viewportSize.height)<(elementOffset.top+elementSize.height)?'top':'both');visiblePartsMerged=visiblePartX+"-"+visiblePartY;if(!inView||inView!==visiblePartsMerged){$element.data('inview',visiblePartsMerged).trigger('inview',[true,visiblePartX,visiblePartY]);}}else if(inView){$element.data('inview',false).trigger('inview',[false]);}}}}
$(w).bind("scroll resize scrollstop",function(){viewportSize=viewportOffset=null;});if(!documentElement.addEventListener&&documentElement.attachEvent){documentElement.attachEvent("onfocusin",function(){viewportOffset=null;});}
$(document).on('inview','.sppb-progress',function(event,visible,visiblePartX,visiblePartY){var $bar=$(this).find('.sppb-progress-bar');if(visible){$bar.css('width',$bar.data('width'));$(this).unbind('inview');}});$.fn.sppbanimateNumbers=function(stop,commas,duration,ease){return this.each(function(){var $this=$(this);var start=parseInt($this.text().replace(/,/g,""));commas=(commas===undefined)?true:commas;$({value:start}).animate({value:stop},{duration:duration==undefined?1000:duration,easing:ease==undefined?"swing":ease,step:function(){$this.text(Math.floor(this.value));if(commas){$this.text($this.text().replace(/(\d)(?=(\d\d\d)+(?!\d))/g,"$1,"));}},complete:function(){if(parseInt($this.text())!==stop){$this.text(stop);if(commas){$this.text($this.text().replace(/(\d)(?=(\d\d\d)+(?!\d))/g,"$1,"));}}}});});};$(document).on('inview','.sppb-animated-number',function(event,visible,visiblePartX,visiblePartY){var $this=$(this);if(visible){$this.sppbanimateNumbers($this.data('digit'),false,$this.data('duration'));$this.unbind('inview');}});$(document).on('inview','.sppb-pie-chart',function(event,visible,visiblePartX,visiblePartY){var $this=$(this);if(visible){$this.easyPieChart({barColor:$this.data('barcolor'),trackColor:$this.data('trackcolor'),scaleColor:false,lineWidth:$this.data('width'),size:$this.data('size'),onStep:function(from,to,percent){$this.find('.sppb-chart-percent > span').text(Math.round(percent)+'%');}});$this.unbind('inview');}});})(jQuery);jQuery(function($){$(document).on('submit','.sppb-ajaxt-contact-form',function(event){event.preventDefault();var $self=$(this);var value=$(this).serializeArray();var request={'option':'com_sppagebuilder','task':'ajax','addon':'ajax_contact','data':value};$.ajax({type:'POST',data:request,beforeSend:function(){$self.find('.fa').addClass('fa-spinner fa-spin');},success:function(response){$self.find('.fa-spin').removeClass('fa-spinner fa-spin');$self.trigger('reset');$self.next('.sppb-ajax-contact-status').html($.parseJSON(response).data).fadeIn().delay(4000).fadeOut(500);;}});return false;});});jQuery(function($){$(document).on('click','.sppb-magnific-popup',function(event){event.preventDefault();var $this=$(this);$this.magnificPopup({type:$this.data('popup_type'),mainClass:$this.data('mainclass')}).magnificPopup('open');});});


/*===============================
/porto/plugins/system/t3/base-bs3/bootstrap/js/bootstrap.js
================================================================================*/;
/*!
 * Bootstrap v3.3.6 (http://getbootstrap.com)
 * Copyright 2011-2015 Twitter, Inc.
 * Licensed under the MIT license
 */
if(typeof jQuery==='undefined'){throw new Error('Bootstrap\'s JavaScript requires jQuery')}
+function($){'use strict';var version=$.fn.jquery.split(' ')[0].split('.')
if((version[0]<2&&version[1]<9)||(version[0]==1&&version[1]==9&&version[2]<1)||(version[0]>2)){throw new Error('Bootstrap\'s JavaScript requires jQuery version 1.9.1 or higher, but lower than version 3')}}(jQuery);+function($){'use strict';function transitionEnd(){var el=document.createElement('bootstrap')
var transEndEventNames={WebkitTransition:'webkitTransitionEnd',MozTransition:'transitionend',OTransition:'oTransitionEnd otransitionend',transition:'transitionend'}
for(var name in transEndEventNames){if(el.style[name]!==undefined){return{end:transEndEventNames[name]}}}
return false}
$.fn.emulateTransitionEnd=function(duration){var called=false
var $el=this
$(this).one('bsTransitionEnd',function(){called=true})
var callback=function(){if(!called)$($el).trigger($.support.transition.end)}
setTimeout(callback,duration)
return this}
$(function(){$.support.transition=transitionEnd()
if(!$.support.transition)return
$.event.special.bsTransitionEnd={bindType:$.support.transition.end,delegateType:$.support.transition.end,handle:function(e){if($(e.target).is(this))return e.handleObj.handler.apply(this,arguments)}}})}(jQuery);+function($){'use strict';var dismiss='[data-dismiss="alert"]'
var Alert=function(el){$(el).on('click',dismiss,this.close)}
Alert.VERSION='3.3.6'
Alert.TRANSITION_DURATION=150
Alert.prototype.close=function(e){var $this=$(this)
var selector=$this.attr('data-target')
if(!selector){selector=$this.attr('href')
selector=selector&&selector.replace(/.*(?=#[^\s]*$)/,'')}
var $parent=$(selector)
if(e)e.preventDefault()
if(!$parent.length){$parent=$this.closest('.alert')}
$parent.trigger(e=$.Event('close.bs.alert'))
if(e.isDefaultPrevented())return
$parent.removeClass('in')
function removeElement(){$parent.detach().trigger('closed.bs.alert').remove()}
$.support.transition&&$parent.hasClass('fade')?$parent.one('bsTransitionEnd',removeElement).emulateTransitionEnd(Alert.TRANSITION_DURATION):removeElement()}
function Plugin(option){return this.each(function(){var $this=$(this)
var data=$this.data('bs.alert')
if(!data)$this.data('bs.alert',(data=new Alert(this)))
if(typeof option=='string')data[option].call($this)})}
var old=$.fn.alert
$.fn.alert=Plugin
$.fn.alert.Constructor=Alert
$.fn.alert.noConflict=function(){$.fn.alert=old
return this}
$(document).on('click.bs.alert.data-api',dismiss,Alert.prototype.close)}(jQuery);+function($){'use strict';var Button=function(element,options){this.$element=$(element)
this.options=$.extend({},Button.DEFAULTS,options)
this.isLoading=false}
Button.VERSION='3.3.6'
Button.DEFAULTS={loadingText:'loading...'}
Button.prototype.setState=function(state){var d='disabled'
var $el=this.$element
var val=$el.is('input')?'val':'html'
var data=$el.data()
state+='Text'
if(data.resetText==null)$el.data('resetText',$el[val]())
setTimeout($.proxy(function(){$el[val](data[state]==null?this.options[state]:data[state])
if(state=='loadingText'){this.isLoading=true
$el.addClass(d).attr(d,d)}else if(this.isLoading){this.isLoading=false
$el.removeClass(d).removeAttr(d)}},this),0)}
Button.prototype.toggle=function(){var changed=true
var $parent=this.$element.closest('[data-toggle="buttons"]')
if($parent.length){var $input=this.$element.find('input')
if($input.prop('type')=='radio'){if($input.prop('checked'))changed=false
$parent.find('.active').removeClass('active')
this.$element.addClass('active')}else if($input.prop('type')=='checkbox'){if(($input.prop('checked'))!==this.$element.hasClass('active'))changed=false
this.$element.toggleClass('active')}
$input.prop('checked',this.$element.hasClass('active'))
if(changed)$input.trigger('change')}else{this.$element.attr('aria-pressed',!this.$element.hasClass('active'))
this.$element.toggleClass('active')}}
function Plugin(option){return this.each(function(){var $this=$(this)
var data=$this.data('bs.button')
var options=typeof option=='object'&&option
if(!data)$this.data('bs.button',(data=new Button(this,options)))
if(option=='toggle')data.toggle()
else if(option)data.setState(option)})}
var old=$.fn.button
$.fn.button=Plugin
$.fn.button.Constructor=Button
$.fn.button.noConflict=function(){$.fn.button=old
return this}
$(document).on('click.bs.button.data-api','[data-toggle^="button"]',function(e){var $btn=$(e.target)
if(!$btn.hasClass('btn'))$btn=$btn.closest('.btn')
Plugin.call($btn,'toggle')
if(!($(e.target).is('input[type="radio"]')||$(e.target).is('input[type="checkbox"]')))e.preventDefault()}).on('focus.bs.button.data-api blur.bs.button.data-api','[data-toggle^="button"]',function(e){$(e.target).closest('.btn').toggleClass('focus',/^focus(in)?$/.test(e.type))})}(jQuery);+function($){'use strict';var Carousel=function(element,options){this.$element=$(element)
this.$indicators=this.$element.find('.carousel-indicators')
this.options=options
this.paused=null
this.sliding=null
this.interval=null
this.$active=null
this.$items=null
this.options.keyboard&&this.$element.on('keydown.bs.carousel',$.proxy(this.keydown,this))
this.options.pause=='hover'&&!('ontouchstart'in document.documentElement)&&this.$element.on('mouseenter.bs.carousel',$.proxy(this.pause,this)).on('mouseleave.bs.carousel',$.proxy(this.cycle,this))}
Carousel.VERSION='3.3.6'
Carousel.TRANSITION_DURATION=600
Carousel.DEFAULTS={interval:5000,pause:'hover',wrap:true,keyboard:true}
Carousel.prototype.keydown=function(e){if(/input|textarea/i.test(e.target.tagName))return
switch(e.which){case 37:this.prev();break
case 39:this.next();break
default:return}
e.preventDefault()}
Carousel.prototype.cycle=function(e){e||(this.paused=false)
this.interval&&clearInterval(this.interval)
this.options.interval&&!this.paused&&(this.interval=setInterval($.proxy(this.next,this),this.options.interval))
return this}
Carousel.prototype.getItemIndex=function(item){this.$items=item.parent().children('.item')
return this.$items.index(item||this.$active)}
Carousel.prototype.getItemForDirection=function(direction,active){var activeIndex=this.getItemIndex(active)
var willWrap=(direction=='prev'&&activeIndex===0)||(direction=='next'&&activeIndex==(this.$items.length-1))
if(willWrap&&!this.options.wrap)return active
var delta=direction=='prev'?-1:1
var itemIndex=(activeIndex+delta)%this.$items.length
return this.$items.eq(itemIndex)}
Carousel.prototype.to=function(pos){var that=this
var activeIndex=this.getItemIndex(this.$active=this.$element.find('.item.active'))
if(pos>(this.$items.length-1)||pos<0)return
if(this.sliding)return this.$element.one('slid.bs.carousel',function(){that.to(pos)})
if(activeIndex==pos)return this.pause().cycle()
return this.slide(pos>activeIndex?'next':'prev',this.$items.eq(pos))}
Carousel.prototype.pause=function(e){e||(this.paused=true)
if(this.$element.find('.next, .prev').length&&$.support.transition){this.$element.trigger($.support.transition.end)
this.cycle(true)}
this.interval=clearInterval(this.interval)
return this}
Carousel.prototype.next=function(){if(this.sliding)return
return this.slide('next')}
Carousel.prototype.prev=function(){if(this.sliding)return
return this.slide('prev')}
Carousel.prototype.slide=function(type,next){var $active=this.$element.find('.item.active')
var $next=next||this.getItemForDirection(type,$active)
var isCycling=this.interval
var direction=type=='next'?'left':'right'
var that=this
if($next.hasClass('active'))return(this.sliding=false)
var relatedTarget=$next[0]
var slideEvent=$.Event('slide.bs.carousel',{relatedTarget:relatedTarget,direction:direction})
this.$element.trigger(slideEvent)
if(slideEvent.isDefaultPrevented())return
this.sliding=true
isCycling&&this.pause()
if(this.$indicators.length){this.$indicators.find('.active').removeClass('active')
var $nextIndicator=$(this.$indicators.children()[this.getItemIndex($next)])
$nextIndicator&&$nextIndicator.addClass('active')}
var slidEvent=$.Event('slid.bs.carousel',{relatedTarget:relatedTarget,direction:direction})
if($.support.transition&&this.$element.hasClass('slide')){$next.addClass(type)
$next[0].offsetWidth
$active.addClass(direction)
$next.addClass(direction)
$active.one('bsTransitionEnd',function(){$next.removeClass([type,direction].join(' ')).addClass('active')
$active.removeClass(['active',direction].join(' '))
that.sliding=false
setTimeout(function(){that.$element.trigger(slidEvent)},0)}).emulateTransitionEnd(Carousel.TRANSITION_DURATION)}else{$active.removeClass('active')
$next.addClass('active')
this.sliding=false
this.$element.trigger(slidEvent)}
isCycling&&this.cycle()
return this}
function Plugin(option){return this.each(function(){var $this=$(this)
var data=$this.data('bs.carousel')
var options=$.extend({},Carousel.DEFAULTS,$this.data(),typeof option=='object'&&option)
var action=typeof option=='string'?option:options.slide
if(!data)$this.data('bs.carousel',(data=new Carousel(this,options)))
if(typeof option=='number')data.to(option)
else if(action)data[action]()
else if(options.interval)data.pause().cycle()})}
var old=$.fn.carousel
$.fn.carousel=Plugin
$.fn.carousel.Constructor=Carousel
$.fn.carousel.noConflict=function(){$.fn.carousel=old
return this}
var clickHandler=function(e){var href
var $this=$(this)
var $target=$($this.attr('data-target')||(href=$this.attr('href'))&&href.replace(/.*(?=#[^\s]+$)/,''))
if(!$target.hasClass('carousel'))return
var options=$.extend({},$target.data(),$this.data())
var slideIndex=$this.attr('data-slide-to')
if(slideIndex)options.interval=false
Plugin.call($target,options)
if(slideIndex){$target.data('bs.carousel').to(slideIndex)}
e.preventDefault()}
$(document).on('click.bs.carousel.data-api','[data-slide]',clickHandler).on('click.bs.carousel.data-api','[data-slide-to]',clickHandler)
$(window).on('load',function(){$('[data-ride="carousel"]').each(function(){var $carousel=$(this)
Plugin.call($carousel,$carousel.data())})})}(jQuery);+function($){'use strict';var Collapse=function(element,options){this.$element=$(element)
this.options=$.extend({},Collapse.DEFAULTS,options)
this.$trigger=$('[data-toggle="collapse"][href="#'+element.id+'"],'+'[data-toggle="collapse"][data-target="#'+element.id+'"]')
this.transitioning=null
if(this.options.parent){this.$parent=this.getParent()}else{this.addAriaAndCollapsedClass(this.$element,this.$trigger)}
if(this.options.toggle)this.toggle()}
Collapse.VERSION='3.3.6'
Collapse.TRANSITION_DURATION=350
Collapse.DEFAULTS={toggle:true}
Collapse.prototype.dimension=function(){var hasWidth=this.$element.hasClass('width')
return hasWidth?'width':'height'}
Collapse.prototype.show=function(){if(this.transitioning||this.$element.hasClass('in'))return
var activesData
var actives=this.$parent&&this.$parent.children('.panel').children('.in, .collapsing')
if(actives&&actives.length){activesData=actives.data('bs.collapse')
if(activesData&&activesData.transitioning)return}
var startEvent=$.Event('show.bs.collapse')
this.$element.trigger(startEvent)
if(startEvent.isDefaultPrevented())return
if(actives&&actives.length){Plugin.call(actives,'hide')
activesData||actives.data('bs.collapse',null)}
var dimension=this.dimension()
this.$element.removeClass('collapse').addClass('collapsing')[dimension](0).attr('aria-expanded',true)
this.$trigger.removeClass('collapsed').attr('aria-expanded',true)
this.transitioning=1
var complete=function(){this.$element.removeClass('collapsing').addClass('collapse in')[dimension]('')
this.transitioning=0
this.$element.trigger('shown.bs.collapse')}
if(!$.support.transition)return complete.call(this)
var scrollSize=$.camelCase(['scroll',dimension].join('-'))
this.$element.one('bsTransitionEnd',$.proxy(complete,this)).emulateTransitionEnd(Collapse.TRANSITION_DURATION)[dimension](this.$element[0][scrollSize])}
Collapse.prototype.hide=function(){if(this.transitioning||!this.$element.hasClass('in'))return
var startEvent=$.Event('hide.bs.collapse')
this.$element.trigger(startEvent)
if(startEvent.isDefaultPrevented())return
var dimension=this.dimension()
this.$element[dimension](this.$element[dimension]())[0].offsetHeight
this.$element.addClass('collapsing').removeClass('collapse in').attr('aria-expanded',false)
this.$trigger.addClass('collapsed').attr('aria-expanded',false)
this.transitioning=1
var complete=function(){this.transitioning=0
this.$element.removeClass('collapsing').addClass('collapse').trigger('hidden.bs.collapse')}
if(!$.support.transition)return complete.call(this)
this.$element
[dimension](0).one('bsTransitionEnd',$.proxy(complete,this)).emulateTransitionEnd(Collapse.TRANSITION_DURATION)}
Collapse.prototype.toggle=function(){this[this.$element.hasClass('in')?'hide':'show']()}
Collapse.prototype.getParent=function(){return $(this.options.parent).find('[data-toggle="collapse"][data-parent="'+this.options.parent+'"]').each($.proxy(function(i,element){var $element=$(element)
this.addAriaAndCollapsedClass(getTargetFromTrigger($element),$element)},this)).end()}
Collapse.prototype.addAriaAndCollapsedClass=function($element,$trigger){var isOpen=$element.hasClass('in')
$element.attr('aria-expanded',isOpen)
$trigger.toggleClass('collapsed',!isOpen).attr('aria-expanded',isOpen)}
function getTargetFromTrigger($trigger){var href
var target=$trigger.attr('data-target')||(href=$trigger.attr('href'))&&href.replace(/.*(?=#[^\s]+$)/,'')
return $(target)}
function Plugin(option){return this.each(function(){var $this=$(this)
var data=$this.data('bs.collapse')
var options=$.extend({},Collapse.DEFAULTS,$this.data(),typeof option=='object'&&option)
if(!data&&options.toggle&&/show|hide/.test(option))options.toggle=false
if(!data)$this.data('bs.collapse',(data=new Collapse(this,options)))
if(typeof option=='string')data[option]()})}
var old=$.fn.collapse
$.fn.collapse=Plugin
$.fn.collapse.Constructor=Collapse
$.fn.collapse.noConflict=function(){$.fn.collapse=old
return this}
$(document).on('click.bs.collapse.data-api','[data-toggle="collapse"]',function(e){var $this=$(this)
if(!$this.attr('data-target'))e.preventDefault()
var $target=getTargetFromTrigger($this)
var data=$target.data('bs.collapse')
var option=data?'toggle':$this.data()
Plugin.call($target,option)})}(jQuery);+function($){'use strict';var backdrop='.dropdown-backdrop'
var toggle='[data-toggle="dropdown"]'
var Dropdown=function(element){$(element).on('click.bs.dropdown',this.toggle)}
Dropdown.VERSION='3.3.6'
function getParent($this){var selector=$this.attr('data-target')
if(!selector){selector=$this.attr('href')
selector=selector&&/#[A-Za-z]/.test(selector)&&selector.replace(/.*(?=#[^\s]*$)/,'')}
var $parent=selector&&$(selector)
return $parent&&$parent.length?$parent:$this.parent()}
function clearMenus(e){if(e&&e.which===3)return
$(backdrop).remove()
$(toggle).each(function(){var $this=$(this)
var $parent=getParent($this)
var relatedTarget={relatedTarget:this}
if(!$parent.hasClass('open'))return
if(e&&e.type=='click'&&/input|textarea/i.test(e.target.tagName)&&$.contains($parent[0],e.target))return
$parent.trigger(e=$.Event('hide.bs.dropdown',relatedTarget))
if(e.isDefaultPrevented())return
$this.attr('aria-expanded','false')
$parent.removeClass('open').trigger($.Event('hidden.bs.dropdown',relatedTarget))})}
Dropdown.prototype.toggle=function(e){var $this=$(this)
if($this.is('.disabled, :disabled'))return
var $parent=getParent($this)
var isActive=$parent.hasClass('open')
clearMenus()
if(!isActive){if('ontouchstart'in document.documentElement&&!$parent.closest('.navbar-nav').length){$(document.createElement('div')).addClass('dropdown-backdrop').insertAfter($(this)).on('click',clearMenus)}
var relatedTarget={relatedTarget:this}
$parent.trigger(e=$.Event('show.bs.dropdown',relatedTarget))
if(e.isDefaultPrevented())return
$this.trigger('focus').attr('aria-expanded','true')
$parent.toggleClass('open').trigger($.Event('shown.bs.dropdown',relatedTarget))}
return false}
Dropdown.prototype.keydown=function(e){if(!/(38|40|27|32)/.test(e.which)||/input|textarea/i.test(e.target.tagName))return
var $this=$(this)
e.preventDefault()
e.stopPropagation()
if($this.is('.disabled, :disabled'))return
var $parent=getParent($this)
var isActive=$parent.hasClass('open')
if(!isActive&&e.which!=27||isActive&&e.which==27){if(e.which==27)$parent.find(toggle).trigger('focus')
return $this.trigger('click')}
var desc=' li:not(.disabled):visible a'
var $items=$parent.find('.dropdown-menu'+desc)
if(!$items.length)return
var index=$items.index(e.target)
if(e.which==38&&index>0)index--
if(e.which==40&&index<$items.length-1)index++
if(!~index)index=0
$items.eq(index).trigger('focus')}
function Plugin(option){return this.each(function(){var $this=$(this)
var data=$this.data('bs.dropdown')
if(!data)$this.data('bs.dropdown',(data=new Dropdown(this)))
if(typeof option=='string')data[option].call($this)})}
var old=$.fn.dropdown
$.fn.dropdown=Plugin
$.fn.dropdown.Constructor=Dropdown
$.fn.dropdown.noConflict=function(){$.fn.dropdown=old
return this}
$(document).on('click.bs.dropdown.data-api',clearMenus).on('click.bs.dropdown.data-api','.dropdown form',function(e){e.stopPropagation()}).on('click.bs.dropdown.data-api',toggle,Dropdown.prototype.toggle).on('keydown.bs.dropdown.data-api',toggle,Dropdown.prototype.keydown).on('keydown.bs.dropdown.data-api','.dropdown-menu',Dropdown.prototype.keydown)}(jQuery);+function($){'use strict';var Modal=function(element,options){this.options=options
this.$body=$(document.body)
this.$element=$(element)
this.$dialog=this.$element.find('.modal-dialog')
this.$backdrop=null
this.isShown=null
this.originalBodyPad=null
this.scrollbarWidth=0
this.ignoreBackdropClick=false
if(this.options.remote){this.$element.find('.modal-content').load(this.options.remote,$.proxy(function(){this.$element.trigger('loaded.bs.modal')},this))}}
Modal.VERSION='3.3.6'
Modal.TRANSITION_DURATION=300
Modal.BACKDROP_TRANSITION_DURATION=150
Modal.DEFAULTS={backdrop:true,keyboard:true,show:true}
Modal.prototype.toggle=function(_relatedTarget){return this.isShown?this.hide():this.show(_relatedTarget)}
Modal.prototype.show=function(_relatedTarget){var that=this
var e=$.Event('show.bs.modal',{relatedTarget:_relatedTarget})
this.$element.trigger(e)
if(this.isShown||e.isDefaultPrevented())return
this.isShown=true
this.checkScrollbar()
this.setScrollbar()
this.$body.addClass('modal-open')
this.escape()
this.resize()
this.$element.on('click.dismiss.bs.modal','[data-dismiss="modal"]',$.proxy(this.hide,this))
this.$dialog.on('mousedown.dismiss.bs.modal',function(){that.$element.one('mouseup.dismiss.bs.modal',function(e){if($(e.target).is(that.$element))that.ignoreBackdropClick=true})})
this.backdrop(function(){var transition=$.support.transition&&that.$element.hasClass('fade')
if(!that.$element.parent().length){that.$element.appendTo(that.$body)}
that.$element.show().scrollTop(0)
that.adjustDialog()
if(transition){that.$element[0].offsetWidth}
that.$element.addClass('in')
that.enforceFocus()
var e=$.Event('shown.bs.modal',{relatedTarget:_relatedTarget})
transition?that.$dialog.one('bsTransitionEnd',function(){that.$element.trigger('focus').trigger(e)}).emulateTransitionEnd(Modal.TRANSITION_DURATION):that.$element.trigger('focus').trigger(e)})}
Modal.prototype.hide=function(e){if(e)e.preventDefault()
e=$.Event('hide.bs.modal')
this.$element.trigger(e)
if(!this.isShown||e.isDefaultPrevented())return
this.isShown=false
this.escape()
this.resize()
$(document).off('focusin.bs.modal')
this.$element.removeClass('in').off('click.dismiss.bs.modal').off('mouseup.dismiss.bs.modal')
this.$dialog.off('mousedown.dismiss.bs.modal')
$.support.transition&&this.$element.hasClass('fade')?this.$element.one('bsTransitionEnd',$.proxy(this.hideModal,this)).emulateTransitionEnd(Modal.TRANSITION_DURATION):this.hideModal()}
Modal.prototype.enforceFocus=function(){$(document).off('focusin.bs.modal').on('focusin.bs.modal',$.proxy(function(e){if(this.$element[0]!==e.target&&!this.$element.has(e.target).length){this.$element.trigger('focus')}},this))}
Modal.prototype.escape=function(){if(this.isShown&&this.options.keyboard){this.$element.on('keydown.dismiss.bs.modal',$.proxy(function(e){e.which==27&&this.hide()},this))}else if(!this.isShown){this.$element.off('keydown.dismiss.bs.modal')}}
Modal.prototype.resize=function(){if(this.isShown){$(window).on('resize.bs.modal',$.proxy(this.handleUpdate,this))}else{$(window).off('resize.bs.modal')}}
Modal.prototype.hideModal=function(){var that=this
this.$element.hide()
this.backdrop(function(){that.$body.removeClass('modal-open')
that.resetAdjustments()
that.resetScrollbar()
that.$element.trigger('hidden.bs.modal')})}
Modal.prototype.removeBackdrop=function(){this.$backdrop&&this.$backdrop.remove()
this.$backdrop=null}
Modal.prototype.backdrop=function(callback){var that=this
var animate=this.$element.hasClass('fade')?'fade':''
if(this.isShown&&this.options.backdrop){var doAnimate=$.support.transition&&animate
this.$backdrop=$(document.createElement('div')).addClass('modal-backdrop '+animate).appendTo(this.$body)
this.$element.on('click.dismiss.bs.modal',$.proxy(function(e){if(this.ignoreBackdropClick){this.ignoreBackdropClick=false
return}
if(e.target!==e.currentTarget)return
this.options.backdrop=='static'?this.$element[0].focus():this.hide()},this))
if(doAnimate)this.$backdrop[0].offsetWidth
this.$backdrop.addClass('in')
if(!callback)return
doAnimate?this.$backdrop.one('bsTransitionEnd',callback).emulateTransitionEnd(Modal.BACKDROP_TRANSITION_DURATION):callback()}else if(!this.isShown&&this.$backdrop){this.$backdrop.removeClass('in')
var callbackRemove=function(){that.removeBackdrop()
callback&&callback()}
$.support.transition&&this.$element.hasClass('fade')?this.$backdrop.one('bsTransitionEnd',callbackRemove).emulateTransitionEnd(Modal.BACKDROP_TRANSITION_DURATION):callbackRemove()}else if(callback){callback()}}
Modal.prototype.handleUpdate=function(){this.adjustDialog()}
Modal.prototype.adjustDialog=function(){var modalIsOverflowing=this.$element[0].scrollHeight>document.documentElement.clientHeight
this.$element.css({paddingLeft:!this.bodyIsOverflowing&&modalIsOverflowing?this.scrollbarWidth:'',paddingRight:this.bodyIsOverflowing&&!modalIsOverflowing?this.scrollbarWidth:''})}
Modal.prototype.resetAdjustments=function(){this.$element.css({paddingLeft:'',paddingRight:''})}
Modal.prototype.checkScrollbar=function(){var fullWindowWidth=window.innerWidth
if(!fullWindowWidth){var documentElementRect=document.documentElement.getBoundingClientRect()
fullWindowWidth=documentElementRect.right-Math.abs(documentElementRect.left)}
this.bodyIsOverflowing=document.body.clientWidth<fullWindowWidth
this.scrollbarWidth=this.measureScrollbar()}
Modal.prototype.setScrollbar=function(){var bodyPad=parseInt((this.$body.css('padding-right')||0),10)
this.originalBodyPad=document.body.style.paddingRight||''
if(this.bodyIsOverflowing)this.$body.css('padding-right',bodyPad+this.scrollbarWidth)}
Modal.prototype.resetScrollbar=function(){this.$body.css('padding-right',this.originalBodyPad)}
Modal.prototype.measureScrollbar=function(){var scrollDiv=document.createElement('div')
scrollDiv.className='modal-scrollbar-measure'
this.$body.append(scrollDiv)
var scrollbarWidth=scrollDiv.offsetWidth-scrollDiv.clientWidth
this.$body[0].removeChild(scrollDiv)
return scrollbarWidth}
function Plugin(option,_relatedTarget){return this.each(function(){var $this=$(this)
var data=$this.data('bs.modal')
var options=$.extend({},Modal.DEFAULTS,$this.data(),typeof option=='object'&&option)
if(!data)$this.data('bs.modal',(data=new Modal(this,options)))
if(typeof option=='string')data[option](_relatedTarget)
else if(options.show)data.show(_relatedTarget)})}
var old=$.fn.modal
$.fn.modal=Plugin
$.fn.modal.Constructor=Modal
$.fn.modal.noConflict=function(){$.fn.modal=old
return this}
$(document).on('click.bs.modal.data-api','[data-toggle="modal"]',function(e){var $this=$(this)
var href=$this.attr('href')
var $target=$($this.attr('data-target')||(href&&href.replace(/.*(?=#[^\s]+$)/,'')))
var option=$target.data('bs.modal')?'toggle':$.extend({remote:!/#/.test(href)&&href},$target.data(),$this.data())
if($this.is('a'))e.preventDefault()
$target.one('show.bs.modal',function(showEvent){if(showEvent.isDefaultPrevented())return
$target.one('hidden.bs.modal',function(){$this.is(':visible')&&$this.trigger('focus')})})
Plugin.call($target,option,this)})}(jQuery);+function($){'use strict';var Tooltip=function(element,options){this.type=null
this.options=null
this.enabled=null
this.timeout=null
this.hoverState=null
this.$element=null
this.inState=null
this.init('tooltip',element,options)}
Tooltip.VERSION='3.3.6'
Tooltip.TRANSITION_DURATION=150
Tooltip.DEFAULTS={animation:true,placement:'top',selector:false,template:'<div class="tooltip" role="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner"></div></div>',trigger:'hover focus',title:'',delay:0,html:false,container:false,viewport:{selector:'body',padding:0}}
Tooltip.prototype.init=function(type,element,options){this.enabled=true
this.type=type
this.$element=$(element)
this.options=this.getOptions(options)
this.$viewport=this.options.viewport&&$($.isFunction(this.options.viewport)?this.options.viewport.call(this,this.$element):(this.options.viewport.selector||this.options.viewport))
this.inState={click:false,hover:false,focus:false}
if(this.$element[0]instanceof document.constructor&&!this.options.selector){throw new Error('`selector` option must be specified when initializing '+this.type+' on the window.document object!')}
var triggers=this.options.trigger.split(' ')
for(var i=triggers.length;i--;){var trigger=triggers[i]
if(trigger=='click'){this.$element.on('click.'+this.type,this.options.selector,$.proxy(this.toggle,this))}else if(trigger!='manual'){var eventIn=trigger=='hover'?'mouseenter':'focusin'
var eventOut=trigger=='hover'?'mouseleave':'focusout'
this.$element.on(eventIn+'.'+this.type,this.options.selector,$.proxy(this.enter,this))
this.$element.on(eventOut+'.'+this.type,this.options.selector,$.proxy(this.leave,this))}}
this.options.selector?(this._options=$.extend({},this.options,{trigger:'manual',selector:''})):this.fixTitle()}
Tooltip.prototype.getDefaults=function(){return Tooltip.DEFAULTS}
Tooltip.prototype.getOptions=function(options){options=$.extend({},this.getDefaults(),this.$element.data(),options)
if(options.delay&&typeof options.delay=='number'){options.delay={show:options.delay,hide:options.delay}}
return options}
Tooltip.prototype.getDelegateOptions=function(){var options={}
var defaults=this.getDefaults()
this._options&&$.each(this._options,function(key,value){if(defaults[key]!=value)options[key]=value})
return options}
Tooltip.prototype.enter=function(obj){var self=obj instanceof this.constructor?obj:$(obj.currentTarget).data('bs.'+this.type)
if(!self){self=new this.constructor(obj.currentTarget,this.getDelegateOptions())
$(obj.currentTarget).data('bs.'+this.type,self)}
if(obj instanceof $.Event){self.inState[obj.type=='focusin'?'focus':'hover']=true}
if(self.tip().hasClass('in')||self.hoverState=='in'){self.hoverState='in'
return}
clearTimeout(self.timeout)
self.hoverState='in'
if(!self.options.delay||!self.options.delay.show)return self.show()
self.timeout=setTimeout(function(){if(self.hoverState=='in')self.show()},self.options.delay.show)}
Tooltip.prototype.isInStateTrue=function(){for(var key in this.inState){if(this.inState[key])return true}
return false}
Tooltip.prototype.leave=function(obj){var self=obj instanceof this.constructor?obj:$(obj.currentTarget).data('bs.'+this.type)
if(!self){self=new this.constructor(obj.currentTarget,this.getDelegateOptions())
$(obj.currentTarget).data('bs.'+this.type,self)}
if(obj instanceof $.Event){self.inState[obj.type=='focusout'?'focus':'hover']=false}
if(self.isInStateTrue())return
clearTimeout(self.timeout)
self.hoverState='out'
if(!self.options.delay||!self.options.delay.hide)return self.hide()
self.timeout=setTimeout(function(){if(self.hoverState=='out')self.hide()},self.options.delay.hide)}
Tooltip.prototype.show=function(){var e=$.Event('show.bs.'+this.type)
if(this.hasContent()&&this.enabled){this.$element.trigger(e)
var inDom=$.contains(this.$element[0].ownerDocument.documentElement,this.$element[0])
if(e.isDefaultPrevented()||!inDom)return
var that=this
var $tip=this.tip()
var tipId=this.getUID(this.type)
this.setContent()
$tip.attr('id',tipId)
this.$element.attr('aria-describedby',tipId)
if(this.options.animation)$tip.addClass('fade')
var placement=typeof this.options.placement=='function'?this.options.placement.call(this,$tip[0],this.$element[0]):this.options.placement
var autoToken=/\s?auto?\s?/i
var autoPlace=autoToken.test(placement)
if(autoPlace)placement=placement.replace(autoToken,'')||'top'
$tip.detach().css({top:0,left:0,display:'block'}).addClass(placement).data('bs.'+this.type,this)
this.options.container?$tip.appendTo(this.options.container):$tip.insertAfter(this.$element)
this.$element.trigger('inserted.bs.'+this.type)
var pos=this.getPosition()
var actualWidth=$tip[0].offsetWidth
var actualHeight=$tip[0].offsetHeight
if(autoPlace){var orgPlacement=placement
var viewportDim=this.getPosition(this.$viewport)
placement=placement=='bottom'&&pos.bottom+actualHeight>viewportDim.bottom?'top':placement=='top'&&pos.top-actualHeight<viewportDim.top?'bottom':placement=='right'&&pos.right+actualWidth>viewportDim.width?'left':placement=='left'&&pos.left-actualWidth<viewportDim.left?'right':placement
$tip.removeClass(orgPlacement).addClass(placement)}
var calculatedOffset=this.getCalculatedOffset(placement,pos,actualWidth,actualHeight)
this.applyPlacement(calculatedOffset,placement)
var complete=function(){var prevHoverState=that.hoverState
that.$element.trigger('shown.bs.'+that.type)
that.hoverState=null
if(prevHoverState=='out')that.leave(that)}
$.support.transition&&this.$tip.hasClass('fade')?$tip.one('bsTransitionEnd',complete).emulateTransitionEnd(Tooltip.TRANSITION_DURATION):complete()}}
Tooltip.prototype.applyPlacement=function(offset,placement){var $tip=this.tip()
var width=$tip[0].offsetWidth
var height=$tip[0].offsetHeight
var marginTop=parseInt($tip.css('margin-top'),10)
var marginLeft=parseInt($tip.css('margin-left'),10)
if(isNaN(marginTop))marginTop=0
if(isNaN(marginLeft))marginLeft=0
offset.top+=marginTop
offset.left+=marginLeft
$.offset.setOffset($tip[0],$.extend({using:function(props){$tip.css({top:Math.round(props.top),left:Math.round(props.left)})}},offset),0)
$tip.addClass('in')
var actualWidth=$tip[0].offsetWidth
var actualHeight=$tip[0].offsetHeight
if(placement=='top'&&actualHeight!=height){offset.top=offset.top+height-actualHeight}
var delta=this.getViewportAdjustedDelta(placement,offset,actualWidth,actualHeight)
if(delta.left)offset.left+=delta.left
else offset.top+=delta.top
var isVertical=/top|bottom/.test(placement)
var arrowDelta=isVertical?delta.left*2-width+actualWidth:delta.top*2-height+actualHeight
var arrowOffsetPosition=isVertical?'offsetWidth':'offsetHeight'
$tip.offset(offset)
this.replaceArrow(arrowDelta,$tip[0][arrowOffsetPosition],isVertical)}
Tooltip.prototype.replaceArrow=function(delta,dimension,isVertical){this.arrow().css(isVertical?'left':'top',50*(1-delta/dimension)+'%').css(isVertical?'top':'left','')}
Tooltip.prototype.setContent=function(){var $tip=this.tip()
var title=this.getTitle()
$tip.find('.tooltip-inner')[this.options.html?'html':'text'](title)
$tip.removeClass('fade in top bottom left right')}
Tooltip.prototype.hide=function(callback){var that=this
var $tip=$(this.$tip)
var e=$.Event('hide.bs.'+this.type)
function complete(){if(that.hoverState!='in')$tip.detach()
that.$element.removeAttr('aria-describedby').trigger('hidden.bs.'+that.type)
callback&&callback()}
this.$element.trigger(e)
if(e.isDefaultPrevented())return
$tip.removeClass('in')
$.support.transition&&$tip.hasClass('fade')?$tip.one('bsTransitionEnd',complete).emulateTransitionEnd(Tooltip.TRANSITION_DURATION):complete()
this.hoverState=null
return this}
Tooltip.prototype.fixTitle=function(){var $e=this.$element
if($e.attr('title')||typeof $e.attr('data-original-title')!='string'){$e.attr('data-original-title',$e.attr('title')||'').attr('title','')}}
Tooltip.prototype.hasContent=function(){return this.getTitle()}
Tooltip.prototype.getPosition=function($element){$element=$element||this.$element
var el=$element[0]
var isBody=el.tagName=='BODY'
var elRect=el.getBoundingClientRect()
if(elRect.width==null){elRect=$.extend({},elRect,{width:elRect.right-elRect.left,height:elRect.bottom-elRect.top})}
var elOffset=isBody?{top:0,left:0}:$element.offset()
var scroll={scroll:isBody?document.documentElement.scrollTop||document.body.scrollTop:$element.scrollTop()}
var outerDims=isBody?{width:$(window).width(),height:$(window).height()}:null
return $.extend({},elRect,scroll,outerDims,elOffset)}
Tooltip.prototype.getCalculatedOffset=function(placement,pos,actualWidth,actualHeight){return placement=='bottom'?{top:pos.top+pos.height,left:pos.left+pos.width/2-actualWidth/2}:placement=='top'?{top:pos.top-actualHeight,left:pos.left+pos.width/2-actualWidth/2}:placement=='left'?{top:pos.top+pos.height/2-actualHeight/2,left:pos.left-actualWidth}:{top:pos.top+pos.height/2-actualHeight/2,left:pos.left+pos.width}}
Tooltip.prototype.getViewportAdjustedDelta=function(placement,pos,actualWidth,actualHeight){var delta={top:0,left:0}
if(!this.$viewport)return delta
var viewportPadding=this.options.viewport&&this.options.viewport.padding||0
var viewportDimensions=this.getPosition(this.$viewport)
if(/right|left/.test(placement)){var topEdgeOffset=pos.top-viewportPadding-viewportDimensions.scroll
var bottomEdgeOffset=pos.top+viewportPadding-viewportDimensions.scroll+actualHeight
if(topEdgeOffset<viewportDimensions.top){delta.top=viewportDimensions.top-topEdgeOffset}else if(bottomEdgeOffset>viewportDimensions.top+viewportDimensions.height){delta.top=viewportDimensions.top+viewportDimensions.height-bottomEdgeOffset}}else{var leftEdgeOffset=pos.left-viewportPadding
var rightEdgeOffset=pos.left+viewportPadding+actualWidth
if(leftEdgeOffset<viewportDimensions.left){delta.left=viewportDimensions.left-leftEdgeOffset}else if(rightEdgeOffset>viewportDimensions.right){delta.left=viewportDimensions.left+viewportDimensions.width-rightEdgeOffset}}
return delta}
Tooltip.prototype.getTitle=function(){var title
var $e=this.$element
var o=this.options
title=$e.attr('data-original-title')||(typeof o.title=='function'?o.title.call($e[0]):o.title)
return title}
Tooltip.prototype.getUID=function(prefix){do prefix+=~~(Math.random()*1000000)
while(document.getElementById(prefix))
return prefix}
Tooltip.prototype.tip=function(){if(!this.$tip){this.$tip=$(this.options.template)
if(this.$tip.length!=1){throw new Error(this.type+' `template` option must consist of exactly 1 top-level element!')}}
return this.$tip}
Tooltip.prototype.arrow=function(){return(this.$arrow=this.$arrow||this.tip().find('.tooltip-arrow'))}
Tooltip.prototype.enable=function(){this.enabled=true}
Tooltip.prototype.disable=function(){this.enabled=false}
Tooltip.prototype.toggleEnabled=function(){this.enabled=!this.enabled}
Tooltip.prototype.toggle=function(e){var self=this
if(e){self=$(e.currentTarget).data('bs.'+this.type)
if(!self){self=new this.constructor(e.currentTarget,this.getDelegateOptions())
$(e.currentTarget).data('bs.'+this.type,self)}}
if(e){self.inState.click=!self.inState.click
if(self.isInStateTrue())self.enter(self)
else self.leave(self)}else{self.tip().hasClass('in')?self.leave(self):self.enter(self)}}
Tooltip.prototype.destroy=function(){var that=this
clearTimeout(this.timeout)
this.hide(function(){that.$element.off('.'+that.type).removeData('bs.'+that.type)
if(that.$tip){that.$tip.detach()}
that.$tip=null
that.$arrow=null
that.$viewport=null})}
function Plugin(option){return this.each(function(){var $this=$(this)
var data=$this.data('bs.tooltip')
var options=typeof option=='object'&&option
if(!data&&/destroy|hide/.test(option))return
if(!data)$this.data('bs.tooltip',(data=new Tooltip(this,options)))
if(typeof option=='string')data[option]()})}
var old=$.fn.tooltip
$.fn.tooltip=Plugin
$.fn.tooltip.Constructor=Tooltip
$.fn.tooltip.noConflict=function(){$.fn.tooltip=old
return this}}(jQuery);+function($){'use strict';var Popover=function(element,options){this.init('popover',element,options)}
if(!$.fn.tooltip)throw new Error('Popover requires tooltip.js')
Popover.VERSION='3.3.6'
Popover.DEFAULTS=$.extend({},$.fn.tooltip.Constructor.DEFAULTS,{placement:'right',trigger:'click',content:'',template:'<div class="popover" role="tooltip"><div class="arrow"></div><h3 class="popover-title"></h3><div class="popover-content"></div></div>'})
Popover.prototype=$.extend({},$.fn.tooltip.Constructor.prototype)
Popover.prototype.constructor=Popover
Popover.prototype.getDefaults=function(){return Popover.DEFAULTS}
Popover.prototype.setContent=function(){var $tip=this.tip()
var title=this.getTitle()
var content=this.getContent()
$tip.find('.popover-title')[this.options.html?'html':'text'](title)
$tip.find('.popover-content').children().detach().end()[this.options.html?(typeof content=='string'?'html':'append'):'text'](content)
$tip.removeClass('fade top bottom left right in')
if(!$tip.find('.popover-title').html())$tip.find('.popover-title').hide()}
Popover.prototype.hasContent=function(){return this.getTitle()||this.getContent()}
Popover.prototype.getContent=function(){var $e=this.$element
var o=this.options
return $e.attr('data-content')||(typeof o.content=='function'?o.content.call($e[0]):o.content)}
Popover.prototype.arrow=function(){return(this.$arrow=this.$arrow||this.tip().find('.arrow'))}
function Plugin(option){return this.each(function(){var $this=$(this)
var data=$this.data('bs.popover')
var options=typeof option=='object'&&option
if(!data&&/destroy|hide/.test(option))return
if(!data)$this.data('bs.popover',(data=new Popover(this,options)))
if(typeof option=='string')data[option]()})}
var old=$.fn.popover
$.fn.popover=Plugin
$.fn.popover.Constructor=Popover
$.fn.popover.noConflict=function(){$.fn.popover=old
return this}}(jQuery);+function($){'use strict';function ScrollSpy(element,options){this.$body=$(document.body)
this.$scrollElement=$(element).is(document.body)?$(window):$(element)
this.options=$.extend({},ScrollSpy.DEFAULTS,options)
this.selector=(this.options.target||'')+' .nav li > a'
this.offsets=[]
this.targets=[]
this.activeTarget=null
this.scrollHeight=0
this.$scrollElement.on('scroll.bs.scrollspy',$.proxy(this.process,this))
this.refresh()
this.process()}
ScrollSpy.VERSION='3.3.6'
ScrollSpy.DEFAULTS={offset:10}
ScrollSpy.prototype.getScrollHeight=function(){return this.$scrollElement[0].scrollHeight||Math.max(this.$body[0].scrollHeight,document.documentElement.scrollHeight)}
ScrollSpy.prototype.refresh=function(){var that=this
var offsetMethod='offset'
var offsetBase=0
this.offsets=[]
this.targets=[]
this.scrollHeight=this.getScrollHeight()
if(!$.isWindow(this.$scrollElement[0])){offsetMethod='position'
offsetBase=this.$scrollElement.scrollTop()}
this.$body.find(this.selector).map(function(){var $el=$(this)
var href=$el.data('target')||$el.attr('href')
var $href=/^#./.test(href)&&$(href)
return($href&&$href.length&&$href.is(':visible')&&[[$href[offsetMethod]().top+offsetBase,href]])||null}).sort(function(a,b){return a[0]-b[0]}).each(function(){that.offsets.push(this[0])
that.targets.push(this[1])})}
ScrollSpy.prototype.process=function(){var scrollTop=this.$scrollElement.scrollTop()+this.options.offset
var scrollHeight=this.getScrollHeight()
var maxScroll=this.options.offset+scrollHeight-this.$scrollElement.height()
var offsets=this.offsets
var targets=this.targets
var activeTarget=this.activeTarget
var i
if(this.scrollHeight!=scrollHeight){this.refresh()}
if(scrollTop>=maxScroll){return activeTarget!=(i=targets[targets.length-1])&&this.activate(i)}
if(activeTarget&&scrollTop<offsets[0]){this.activeTarget=null
return this.clear()}
for(i=offsets.length;i--;){activeTarget!=targets[i]&&scrollTop>=offsets[i]&&(offsets[i+1]===undefined||scrollTop<offsets[i+1])&&this.activate(targets[i])}}
ScrollSpy.prototype.activate=function(target){this.activeTarget=target
this.clear()
var selector=this.selector+'[data-target="'+target+'"],'+
this.selector+'[href="'+target+'"]'
var active=$(selector).parents('li').addClass('active')
if(active.parent('.dropdown-menu').length){active=active.closest('li.dropdown').addClass('active')}
active.trigger('activate.bs.scrollspy')}
ScrollSpy.prototype.clear=function(){$(this.selector).parentsUntil(this.options.target,'.active').removeClass('active')}
function Plugin(option){return this.each(function(){var $this=$(this)
var data=$this.data('bs.scrollspy')
var options=typeof option=='object'&&option
if(!data)$this.data('bs.scrollspy',(data=new ScrollSpy(this,options)))
if(typeof option=='string')data[option]()})}
var old=$.fn.scrollspy
$.fn.scrollspy=Plugin
$.fn.scrollspy.Constructor=ScrollSpy
$.fn.scrollspy.noConflict=function(){$.fn.scrollspy=old
return this}
$(window).on('load.bs.scrollspy.data-api',function(){$('[data-spy="scroll"]').each(function(){var $spy=$(this)
Plugin.call($spy,$spy.data())})})}(jQuery);+function($){'use strict';var Tab=function(element){this.element=$(element)}
Tab.VERSION='3.3.6'
Tab.TRANSITION_DURATION=150
Tab.prototype.show=function(){var $this=this.element
var $ul=$this.closest('ul:not(.dropdown-menu)')
var selector=$this.data('target')
if(!selector){selector=$this.attr('href')
selector=selector&&selector.replace(/.*(?=#[^\s]*$)/,'')}
if($this.parent('li').hasClass('active'))return
var $previous=$ul.find('.active:last a')
var hideEvent=$.Event('hide.bs.tab',{relatedTarget:$this[0]})
var showEvent=$.Event('show.bs.tab',{relatedTarget:$previous[0]})
$previous.trigger(hideEvent)
$this.trigger(showEvent)
if(showEvent.isDefaultPrevented()||hideEvent.isDefaultPrevented())return
var $target=$(selector)
this.activate($this.closest('li'),$ul)
this.activate($target,$target.parent(),function(){$previous.trigger({type:'hidden.bs.tab',relatedTarget:$this[0]})
$this.trigger({type:'shown.bs.tab',relatedTarget:$previous[0]})})}
Tab.prototype.activate=function(element,container,callback){var $active=container.find('> .active')
var transition=callback&&$.support.transition&&($active.length&&$active.hasClass('fade')||!!container.find('> .fade').length)
function next(){$active.removeClass('active').find('> .dropdown-menu > .active').removeClass('active').end().find('[data-toggle="tab"]').attr('aria-expanded',false)
element.addClass('active').find('[data-toggle="tab"]').attr('aria-expanded',true)
if(transition){element[0].offsetWidth
element.addClass('in')}else{element.removeClass('fade')}
if(element.parent('.dropdown-menu').length){element.closest('li.dropdown').addClass('active').end().find('[data-toggle="tab"]').attr('aria-expanded',true)}
callback&&callback()}
$active.length&&transition?$active.one('bsTransitionEnd',next).emulateTransitionEnd(Tab.TRANSITION_DURATION):next()
$active.removeClass('in')}
function Plugin(option){return this.each(function(){var $this=$(this)
var data=$this.data('bs.tab')
if(!data)$this.data('bs.tab',(data=new Tab(this)))
if(typeof option=='string')data[option]()})}
var old=$.fn.tab
$.fn.tab=Plugin
$.fn.tab.Constructor=Tab
$.fn.tab.noConflict=function(){$.fn.tab=old
return this}
var clickHandler=function(e){e.preventDefault()
Plugin.call($(this),'show')}
$(document).on('click.bs.tab.data-api','[data-toggle="tab"]',clickHandler).on('click.bs.tab.data-api','[data-toggle="pill"]',clickHandler)}(jQuery);+function($){'use strict';var Affix=function(element,options){this.options=$.extend({},Affix.DEFAULTS,options)
this.$target=$(this.options.target).on('scroll.bs.affix.data-api',$.proxy(this.checkPosition,this)).on('click.bs.affix.data-api',$.proxy(this.checkPositionWithEventLoop,this))
this.$element=$(element)
this.affixed=null
this.unpin=null
this.pinnedOffset=null
this.checkPosition()}
Affix.VERSION='3.3.6'
Affix.RESET='affix affix-top affix-bottom'
Affix.DEFAULTS={offset:0,target:window}
Affix.prototype.getState=function(scrollHeight,height,offsetTop,offsetBottom){var scrollTop=this.$target.scrollTop()
var position=this.$element.offset()
var targetHeight=this.$target.height()
if(offsetTop!=null&&this.affixed=='top')return scrollTop<offsetTop?'top':false
if(this.affixed=='bottom'){if(offsetTop!=null)return(scrollTop+this.unpin<=position.top)?false:'bottom'
return(scrollTop+targetHeight<=scrollHeight-offsetBottom)?false:'bottom'}
var initializing=this.affixed==null
var colliderTop=initializing?scrollTop:position.top
var colliderHeight=initializing?targetHeight:height
if(offsetTop!=null&&scrollTop<=offsetTop)return'top'
if(offsetBottom!=null&&(colliderTop+colliderHeight>=scrollHeight-offsetBottom))return'bottom'
return false}
Affix.prototype.getPinnedOffset=function(){if(this.pinnedOffset)return this.pinnedOffset
this.$element.removeClass(Affix.RESET).addClass('affix')
var scrollTop=this.$target.scrollTop()
var position=this.$element.offset()
return(this.pinnedOffset=position.top-scrollTop)}
Affix.prototype.checkPositionWithEventLoop=function(){setTimeout($.proxy(this.checkPosition,this),1)}
Affix.prototype.checkPosition=function(){if(!this.$element.is(':visible'))return
var height=this.$element.height()
var offset=this.options.offset
var offsetTop=offset.top
var offsetBottom=offset.bottom
var scrollHeight=Math.max($(document).height(),$(document.body).height())
if(typeof offset!='object')offsetBottom=offsetTop=offset
if(typeof offsetTop=='function')offsetTop=offset.top(this.$element)
if(typeof offsetBottom=='function')offsetBottom=offset.bottom(this.$element)
var affix=this.getState(scrollHeight,height,offsetTop,offsetBottom)
if(this.affixed!=affix){if(this.unpin!=null)this.$element.css('top','')
var affixType='affix'+(affix?'-'+affix:'')
var e=$.Event(affixType+'.bs.affix')
this.$element.trigger(e)
if(e.isDefaultPrevented())return
this.affixed=affix
this.unpin=affix=='bottom'?this.getPinnedOffset():null
this.$element.removeClass(Affix.RESET).addClass(affixType).trigger(affixType.replace('affix','affixed')+'.bs.affix')}
if(affix=='bottom'){this.$element.offset({top:scrollHeight-height-offsetBottom})}}
function Plugin(option){return this.each(function(){var $this=$(this)
var data=$this.data('bs.affix')
var options=typeof option=='object'&&option
if(!data)$this.data('bs.affix',(data=new Affix(this,options)))
if(typeof option=='string')data[option]()})}
var old=$.fn.affix
$.fn.affix=Plugin
$.fn.affix.Constructor=Affix
$.fn.affix.noConflict=function(){$.fn.affix=old
return this}
$(window).on('load',function(){$('[data-spy="affix"]').each(function(){var $spy=$(this)
var data=$spy.data()
data.offset=data.offset||{}
if(data.offsetBottom!=null)data.offset.bottom=data.offsetBottom
if(data.offsetTop!=null)data.offset.top=data.offsetTop
Plugin.call($spy,data)})})}(jQuery);


/*===============================
/porto/media/system/js/calendar.js
================================================================================*/;
Calendar=function(d,c,f,a){this.activeDiv=null;this.currentDateEl=null;this.getDateStatus=null;this.getDateToolTip=null;this.getDateText=null;this.timeout=null;this.onSelected=f||null;this.onClose=a||null;this.dragging=false;this.hidden=false;this.minYear=1970;this.maxYear=2050;this.dateFormat=Calendar._TT.DEF_DATE_FORMAT;this.ttDateFormat=Calendar._TT.TT_DATE_FORMAT;this.isPopup=true;this.weekNumbers=true;this.firstDayOfWeek=typeof d=="number"?d:Calendar._FD;this.showsOtherMonths=false;this.dateStr=c;this.ar_days=null;this.showsTime=false;this.time24=true;this.yearStep=2;this.hiliteToday=true;this.multiple=null;this.table=null;this.element=null;this.tbody=null;this.firstdayname=null;this.monthsCombo=null;this.yearsCombo=null;this.hilitedMonth=null;this.activeMonth=null;this.hilitedYear=null;this.activeYear=null;this.dateClicked=false;if(typeof Calendar._SDN=="undefined"){if(typeof Calendar._SDN_len=="undefined"){Calendar._SDN_len=3}var b=new Array();for(var e=8;e>0;){b[--e]=Calendar._DN[e].substr(0,Calendar._SDN_len)}Calendar._SDN=b;if(typeof Calendar._SMN_len=="undefined"){Calendar._SMN_len=3}b=new Array();for(var e=12;e>0;){b[--e]=Calendar._MN[e].substr(0,Calendar._SMN_len)}Calendar._SMN=b}};Calendar._C=null;Calendar.is_ie=(/msie/i.test(navigator.userAgent)&&!/opera/i.test(navigator.userAgent));Calendar.is_ie5=(Calendar.is_ie&&/msie 5\.0/i.test(navigator.userAgent));Calendar.is_opera=/opera/i.test(navigator.userAgent);Calendar.is_khtml=/Konqueror|Safari|KHTML/i.test(navigator.userAgent);Calendar.getAbsolutePos=function(e){var a=0,d=0;var c=/^div$/i.test(e.tagName);if(c&&e.scrollLeft){a=e.scrollLeft}if(c&&e.scrollTop){d=e.scrollTop}var f={x:e.offsetLeft-a,y:e.offsetTop-d};if(e.offsetParent){var b=this.getAbsolutePos(e.offsetParent);f.x+=b.x;f.y+=b.y}return f};Calendar.isRelated=function(c,a){var d=a.relatedTarget;if(!d){var b=a.type;if(b=="mouseover"){d=a.fromElement}else{if(b=="mouseout"){d=a.toElement}}}while(d){if(d==c){return true}d=d.parentNode}return false};Calendar.removeClass=function(e,d){if(!(e&&e.className)){return}var a=e.className.split(" ");var b=new Array();for(var c=a.length;c>0;){if(a[--c]!=d){b[b.length]=a[c]}}e.className=b.join(" ")};Calendar.addClass=function(b,a){Calendar.removeClass(b,a);b.className+=" "+a};Calendar.getElement=function(a){var b=Calendar.is_ie?window.event.srcElement:a.currentTarget;while(b.nodeType!=1||/^div$/i.test(b.tagName)){b=b.parentNode}return b};Calendar.getTargetElement=function(a){var b=Calendar.is_ie?window.event.srcElement:a.target;while(b.nodeType!=1){b=b.parentNode}return b};Calendar.stopEvent=function(a){a||(a=window.event);if(Calendar.is_ie){a.cancelBubble=true;a.returnValue=false}else{a.preventDefault();a.stopPropagation()}return false};Calendar.addEvent=function(a,c,b){if(a.attachEvent){a.attachEvent("on"+c,b)}else{if(a.addEventListener){a.addEventListener(c,b,true)}else{a["on"+c]=b}}};Calendar.removeEvent=function(a,c,b){if(a.detachEvent){a.detachEvent("on"+c,b)}else{if(a.removeEventListener){a.removeEventListener(c,b,true)}else{a["on"+c]=null}}};Calendar.createElement=function(c,b){var a=null;if(document.createElementNS){a=document.createElementNS("http://www.w3.org/1999/xhtml",c)}else{a=document.createElement(c)}if(typeof b!="undefined"){b.appendChild(a)}return a};Calendar._add_evs=function(el){with(Calendar){addEvent(el,"mouseover",dayMouseOver);addEvent(el,"mousedown",dayMouseDown);addEvent(el,"mouseout",dayMouseOut);if(is_ie){addEvent(el,"dblclick",dayMouseDblClick);el.setAttribute("unselectable",true)}}};Calendar.findMonth=function(a){if(typeof a.month!="undefined"){return a}else{if(typeof a.parentNode.month!="undefined"){return a.parentNode}}return null};Calendar.findYear=function(a){if(typeof a.year!="undefined"){return a}else{if(typeof a.parentNode.year!="undefined"){return a.parentNode}}return null};Calendar.showMonthsCombo=function(){var e=Calendar._C;if(!e){return false}var e=e;var f=e.activeDiv;var d=e.monthsCombo;if(e.hilitedMonth){Calendar.removeClass(e.hilitedMonth,"hilite")}if(e.activeMonth){Calendar.removeClass(e.activeMonth,"active")}var c=e.monthsCombo.getElementsByTagName("div")[e.date.getMonth()];Calendar.addClass(c,"active");e.activeMonth=c;var b=d.style;b.display="block";if(f.navtype<0){b.left=f.offsetLeft+"px"}else{var a=d.offsetWidth;if(typeof a=="undefined"){a=50}b.left=(f.offsetLeft+f.offsetWidth-a)+"px"}b.top=(f.offsetTop+f.offsetHeight)+"px"};Calendar.showYearsCombo=function(d){var a=Calendar._C;if(!a){return false}var a=a;var c=a.activeDiv;var f=a.yearsCombo;if(a.hilitedYear){Calendar.removeClass(a.hilitedYear,"hilite")}if(a.activeYear){Calendar.removeClass(a.activeYear,"active")}a.activeYear=null;var b=a.date.getFullYear()+(d?1:-1);var j=f.firstChild;var h=false;for(var e=12;e>0;--e){if(b>=a.minYear&&b<=a.maxYear){j.innerHTML=b;j.year=b;j.style.display="block";h=true}else{j.style.display="none"}j=j.nextSibling;b+=d?a.yearStep:-a.yearStep}if(h){var k=f.style;k.display="block";if(c.navtype<0){k.left=c.offsetLeft+"px"}else{var g=f.offsetWidth;if(typeof g=="undefined"){g=50}k.left=(c.offsetLeft+c.offsetWidth-g)+"px"}k.top=(c.offsetTop+c.offsetHeight)+"px"}};Calendar.tableMouseUp=function(ev){var cal=Calendar._C;if(!cal){return false}if(cal.timeout){clearTimeout(cal.timeout)}var el=cal.activeDiv;if(!el){return false}var target=Calendar.getTargetElement(ev);ev||(ev=window.event);Calendar.removeClass(el,"active");if(target==el||target.parentNode==el){Calendar.cellClick(el,ev)}var mon=Calendar.findMonth(target);var date=null;if(mon){date=new Date(cal.date);if(mon.month!=date.getMonth()){date.setMonth(mon.month);cal.setDate(date);cal.dateClicked=false;cal.callHandler()}}else{var year=Calendar.findYear(target);if(year){date=new Date(cal.date);if(year.year!=date.getFullYear()){date.setFullYear(year.year);cal.setDate(date);cal.dateClicked=false;cal.callHandler()}}}with(Calendar){removeEvent(document,"mouseup",tableMouseUp);removeEvent(document,"mouseover",tableMouseOver);removeEvent(document,"mousemove",tableMouseOver);cal._hideCombos();_C=null;return stopEvent(ev)}};Calendar.tableMouseOver=function(n){var a=Calendar._C;if(!a){return}var c=a.activeDiv;var j=Calendar.getTargetElement(n);if(j==c||j.parentNode==c){Calendar.addClass(c,"hilite active");Calendar.addClass(c.parentNode,"rowhilite")}else{if(typeof c.navtype=="undefined"||(c.navtype!=50&&(c.navtype==0||Math.abs(c.navtype)>2))){Calendar.removeClass(c,"active")}Calendar.removeClass(c,"hilite");Calendar.removeClass(c.parentNode,"rowhilite")}n||(n=window.event);if(c.navtype==50&&j!=c){var m=Calendar.getAbsolutePos(c);var p=c.offsetWidth;var o=n.clientX;var q;var l=true;if(o>m.x+p){q=o-m.x-p;l=false}else{q=m.x-o}if(q<0){q=0}var f=c._range;var h=c._current;var g=Math.floor(q/10)%f.length;for(var e=f.length;--e>=0;){if(f[e]==h){break}}while(g-->0){if(l){if(--e<0){e=f.length-1}}else{if(++e>=f.length){e=0}}}var b=f[e];c.innerHTML=b;a.onUpdateTime()}var d=Calendar.findMonth(j);if(d){if(d.month!=a.date.getMonth()){if(a.hilitedMonth){Calendar.removeClass(a.hilitedMonth,"hilite")}Calendar.addClass(d,"hilite");a.hilitedMonth=d}else{if(a.hilitedMonth){Calendar.removeClass(a.hilitedMonth,"hilite")}}}else{if(a.hilitedMonth){Calendar.removeClass(a.hilitedMonth,"hilite")}var k=Calendar.findYear(j);if(k){if(k.year!=a.date.getFullYear()){if(a.hilitedYear){Calendar.removeClass(a.hilitedYear,"hilite")}Calendar.addClass(k,"hilite");a.hilitedYear=k}else{if(a.hilitedYear){Calendar.removeClass(a.hilitedYear,"hilite")}}}else{if(a.hilitedYear){Calendar.removeClass(a.hilitedYear,"hilite")}}}return Calendar.stopEvent(n)};Calendar.tableMouseDown=function(a){if(Calendar.getTargetElement(a)==Calendar.getElement(a)){return Calendar.stopEvent(a)}};Calendar.calDragIt=function(b){var c=Calendar._C;if(!(c&&c.dragging)){return false}var e;var d;if(Calendar.is_ie){d=window.event.clientY+document.body.scrollTop;e=window.event.clientX+document.body.scrollLeft}else{e=b.pageX;d=b.pageY}c.hideShowCovered();var a=c.element.style;a.left=(e-c.xOffs)+"px";a.top=(d-c.yOffs)+"px";return Calendar.stopEvent(b)};Calendar.calDragEnd=function(ev){var cal=Calendar._C;if(!cal){return false}cal.dragging=false;with(Calendar){removeEvent(document,"mousemove",calDragIt);removeEvent(document,"mouseup",calDragEnd);tableMouseUp(ev)}cal.hideShowCovered()};Calendar.dayMouseDown=function(ev){var el=Calendar.getElement(ev);if(el.disabled){return false}var cal=el.calendar;cal.activeDiv=el;Calendar._C=cal;if(el.navtype!=300){with(Calendar){if(el.navtype==50){el._current=el.innerHTML;addEvent(document,"mousemove",tableMouseOver)}else{addEvent(document,Calendar.is_ie5?"mousemove":"mouseover",tableMouseOver)}addClass(el,"hilite active");addEvent(document,"mouseup",tableMouseUp)}}else{if(cal.isPopup){cal._dragStart(ev)}}if(el.navtype==-1||el.navtype==1){if(cal.timeout){clearTimeout(cal.timeout)}cal.timeout=setTimeout("Calendar.showMonthsCombo()",250)}else{if(el.navtype==-2||el.navtype==2){if(cal.timeout){clearTimeout(cal.timeout)}cal.timeout=setTimeout((el.navtype>0)?"Calendar.showYearsCombo(true)":"Calendar.showYearsCombo(false)",250)}else{cal.timeout=null}}return Calendar.stopEvent(ev)};Calendar.dayMouseDblClick=function(a){Calendar.cellClick(Calendar.getElement(a),a||window.event);if(Calendar.is_ie){document.selection.empty()}};Calendar.dayMouseOver=function(b){var a=Calendar.getElement(b);if(Calendar.isRelated(a,b)||Calendar._C||a.disabled){return false}if(a.ttip){if(a.ttip.substr(0,1)=="_"){a.ttip=a.caldate.print(a.calendar.ttDateFormat)+a.ttip.substr(1)}a.calendar.tooltips.innerHTML=a.ttip}if(a.navtype!=300){Calendar.addClass(a,"hilite");if(a.caldate){Calendar.addClass(a.parentNode,"rowhilite");var c=a.calendar;if(c&&c.getDateToolTip){var e=a.caldate;window.status=e;a.title=c.getDateToolTip(e,e.getFullYear(),e.getMonth(),e.getDate())}}}return Calendar.stopEvent(b)};Calendar.dayMouseOut=function(ev){with(Calendar){var el=getElement(ev);if(isRelated(el,ev)||_C||el.disabled){return false}removeClass(el,"hilite");if(el.caldate){removeClass(el.parentNode,"rowhilite")}if(el.calendar){el.calendar.tooltips.innerHTML=_TT.SEL_DATE}}};Calendar.cellClick=function(e,o){var c=e.calendar;var h=false;var l=false;var f=null;if(typeof e.navtype=="undefined"){if(c.currentDateEl){Calendar.removeClass(c.currentDateEl,"selected");Calendar.addClass(e,"selected");h=(c.currentDateEl==e);if(!h){c.currentDateEl=e}}c.date.setDateOnly(e.caldate);f=c.date;var b=!(c.dateClicked=!e.otherMonth);if(!b&&!c.currentDateEl&&c.multiple){c._toggleMultipleDate(new Date(f))}else{l=!e.disabled}if(b){c._init(c.firstDayOfWeek,f)}}else{if(e.navtype==200){Calendar.removeClass(e,"hilite");c.callCloseHandler();return}f=new Date(c.date);if(e.navtype==0){f.setDateOnly(new Date())}c.dateClicked=false;var n=f.getFullYear();var g=f.getMonth();function a(q){var r=f.getDate();var i=f.getMonthDays(q);if(r>i){f.setDate(i)}f.setMonth(q)}switch(e.navtype){case 400:Calendar.removeClass(e,"hilite");var p=Calendar._TT.ABOUT;if(typeof p!="undefined"){p+=c.showsTime?Calendar._TT.ABOUT_TIME:""}else{p='Help and about box text is not translated into this language.\nIf you know this language and you feel generous please update\nthe corresponding file in "lang" subdir to match calendar-en.js\nand send it back to <mihai_bazon@yahoo.com> to get it into the distribution  ;-)\n\nThank you!\nhttp://dynarch.com/mishoo/calendar.epl\n'}alert(p);return;case-2:if(n>c.minYear){f.setFullYear(n-1)}break;case-1:if(g>0){a(g-1)}else{if(n-->c.minYear){f.setFullYear(n);a(11)}}break;case 1:if(g<11){a(g+1)}else{if(n<c.maxYear){f.setFullYear(n+1);a(0)}}break;case 2:if(n<c.maxYear){f.setFullYear(n+1)}break;case 100:c.setFirstDayOfWeek(e.fdow);return;case 50:var k=e._range;var m=e.innerHTML;for(var j=k.length;--j>=0;){if(k[j]==m){break}}if(o&&o.shiftKey){if(--j<0){j=k.length-1}}else{if(++j>=k.length){j=0}}var d=k[j];e.innerHTML=d;c.onUpdateTime();return;case 0:if((typeof c.getDateStatus=="function")&&c.getDateStatus(f,f.getFullYear(),f.getMonth(),f.getDate())){return false}break}if(!f.equalsTo(c.date)){c.setDate(f);l=true}else{if(e.navtype==0){l=h=true}}}if(l){o&&c.callHandler()}if(h){Calendar.removeClass(e,"hilite");o&&c.callCloseHandler()}};Calendar.prototype.create=function(n){var m=null;if(!n){m=document.getElementsByTagName("body")[0];this.isPopup=true}else{m=n;this.isPopup=false}this.date=this.dateStr?new Date(this.dateStr):new Date();var q=Calendar.createElement("table");this.table=q;q.cellSpacing=0;q.cellPadding=0;q.calendar=this;Calendar.addEvent(q,"mousedown",Calendar.tableMouseDown);var a=Calendar.createElement("div");this.element=a;a.className="calendar";if(this.isPopup){a.style.position="absolute";a.style.display="none"}a.appendChild(q);var k=Calendar.createElement("thead",q);var o=null;var r=null;var b=this;var e=function(s,j,i){o=Calendar.createElement("td",r);o.colSpan=j;o.className="button";if(i!=0&&Math.abs(i)<=2){o.className+=" nav"}Calendar._add_evs(o);o.calendar=b;o.navtype=i;o.innerHTML="<div unselectable='on'>"+s+"</div>";return o};r=Calendar.createElement("tr",k);var c=6;(this.isPopup)&&--c;(this.weekNumbers)&&++c;e("?",1,400).ttip=Calendar._TT.INFO;this.title=e("",c,300);this.title.className="title";if(this.isPopup){this.title.ttip=Calendar._TT.DRAG_TO_MOVE;this.title.style.cursor="move";e("&#x00d7;",1,200).ttip=Calendar._TT.CLOSE}r=Calendar.createElement("tr",k);r.className="headrow";this._nav_py=e("&#x00ab;",1,-2);this._nav_py.ttip=Calendar._TT.PREV_YEAR;this._nav_pm=e("&#x2039;",1,-1);this._nav_pm.ttip=Calendar._TT.PREV_MONTH;this._nav_now=e(Calendar._TT.TODAY,this.weekNumbers?4:3,0);this._nav_now.ttip=Calendar._TT.GO_TODAY;this._nav_nm=e("&#x203a;",1,1);this._nav_nm.ttip=Calendar._TT.NEXT_MONTH;this._nav_ny=e("&#x00bb;",1,2);this._nav_ny.ttip=Calendar._TT.NEXT_YEAR;r=Calendar.createElement("tr",k);r.className="daynames";if(this.weekNumbers){o=Calendar.createElement("td",r);o.className="name wn";o.innerHTML=Calendar._TT.WK}for(var h=7;h>0;--h){o=Calendar.createElement("td",r);if(!h){o.navtype=100;o.calendar=this;Calendar._add_evs(o)}}this.firstdayname=(this.weekNumbers)?r.firstChild.nextSibling:r.firstChild;this._displayWeekdays();var g=Calendar.createElement("tbody",q);this.tbody=g;for(h=6;h>0;--h){r=Calendar.createElement("tr",g);if(this.weekNumbers){o=Calendar.createElement("td",r)}for(var f=7;f>0;--f){o=Calendar.createElement("td",r);o.calendar=this;Calendar._add_evs(o)}}if(this.showsTime){r=Calendar.createElement("tr",g);r.className="time";o=Calendar.createElement("td",r);o.className="time";o.colSpan=2;o.innerHTML=Calendar._TT.TIME||"&#160;";o=Calendar.createElement("td",r);o.className="time";o.colSpan=this.weekNumbers?4:3;(function(){function t(C,E,D,F){var A=Calendar.createElement("span",o);A.className=C;A.innerHTML=E;A.calendar=b;A.ttip=Calendar._TT.TIME_PART;A.navtype=50;A._range=[];if(typeof D!="number"){A._range=D}else{for(var B=D;B<=F;++B){var z;if(B<10&&F>=10){z="0"+B}else{z=""+B}A._range[A._range.length]=z}}Calendar._add_evs(A);return A}var x=b.date.getHours();var i=b.date.getMinutes();var y=!b.time24;var j=(x>12);if(y&&j){x-=12}var v=t("hour",x,y?1:0,y?12:23);var u=Calendar.createElement("span",o);u.innerHTML=":";u.className="colon";var s=t("minute",i,0,59);var w=null;o=Calendar.createElement("td",r);o.className="time";o.colSpan=2;if(y){w=t("ampm",j?"pm":"am",["am","pm"])}else{o.innerHTML="&#160;"}b.onSetTime=function(){var A,z=this.date.getHours(),B=this.date.getMinutes();if(y){A=(z>=12);if(A){z-=12}if(z==0){z=12}w.innerHTML=A?"pm":"am"}v.innerHTML=(z<10)?("0"+z):z;s.innerHTML=(B<10)?("0"+B):B};b.onUpdateTime=function(){var A=this.date;var B=parseInt(v.innerHTML,10);if(y){if(/pm/i.test(w.innerHTML)&&B<12){B+=12}else{if(/am/i.test(w.innerHTML)&&B==12){B=0}}}var C=A.getDate();var z=A.getMonth();var D=A.getFullYear();A.setHours(B);A.setMinutes(parseInt(s.innerHTML,10));A.setFullYear(D);A.setMonth(z);A.setDate(C);this.dateClicked=false;this.callHandler()}})()}else{this.onSetTime=this.onUpdateTime=function(){}}var l=Calendar.createElement("tfoot",q);r=Calendar.createElement("tr",l);r.className="footrow";o=e(Calendar._TT.SEL_DATE,this.weekNumbers?8:7,300);o.className="ttip";if(this.isPopup){o.ttip=Calendar._TT.DRAG_TO_MOVE;o.style.cursor="move"}this.tooltips=o;a=Calendar.createElement("div",this.element);this.monthsCombo=a;a.className="combo";for(h=0;h<Calendar._MN.length;++h){var d=Calendar.createElement("div");d.className=Calendar.is_ie?"label-IEfix":"label";d.month=h;d.innerHTML=Calendar._SMN[h];a.appendChild(d)}a=Calendar.createElement("div",this.element);this.yearsCombo=a;a.className="combo";for(h=12;h>0;--h){var p=Calendar.createElement("div");p.className=Calendar.is_ie?"label-IEfix":"label";a.appendChild(p)}this._init(this.firstDayOfWeek,this.date);m.appendChild(this.element)};Calendar._keyEvent=function(k){var a=window._dynarch_popupCalendar;if(!a||a.multiple){return false}(Calendar.is_ie)&&(k=window.event);var i=(Calendar.is_ie||k.type=="keypress"),l=k.keyCode;if(k.ctrlKey){switch(l){case 37:i&&Calendar.cellClick(a._nav_pm);break;case 38:i&&Calendar.cellClick(a._nav_py);break;case 39:i&&Calendar.cellClick(a._nav_nm);break;case 40:i&&Calendar.cellClick(a._nav_ny);break;default:return false}}else{switch(l){case 32:Calendar.cellClick(a._nav_now);break;case 27:i&&a.callCloseHandler();break;case 37:case 38:case 39:case 40:if(i){var e,m,j,g,c,d;e=l==37||l==38;d=(l==37||l==39)?1:7;function b(){c=a.currentDateEl;var n=c.pos;m=n&15;j=n>>4;g=a.ar_days[j][m]}b();function f(){var n=new Date(a.date);n.setDate(n.getDate()-d);a.setDate(n)}function h(){var n=new Date(a.date);n.setDate(n.getDate()+d);a.setDate(n)}while(1){switch(l){case 37:if(--m>=0){g=a.ar_days[j][m]}else{m=6;l=38;continue}break;case 38:if(--j>=0){g=a.ar_days[j][m]}else{f();b()}break;case 39:if(++m<7){g=a.ar_days[j][m]}else{m=0;l=40;continue}break;case 40:if(++j<a.ar_days.length){g=a.ar_days[j][m]}else{h();b()}break}break}if(g){if(!g.disabled){Calendar.cellClick(g)}else{if(e){f()}else{h()}}}}break;case 13:if(i){Calendar.cellClick(a.currentDateEl,k)}break;default:return false}}return Calendar.stopEvent(k)};Calendar.prototype._init=function(m,w){var v=new Date(),q=v.getFullYear(),y=v.getMonth(),b=v.getDate();this.table.style.visibility="hidden";var h=w.getFullYear();if(h<this.minYear){h=this.minYear;w.setFullYear(h)}else{if(h>this.maxYear){h=this.maxYear;w.setFullYear(h)}}this.firstDayOfWeek=m;this.date=new Date(w);var x=w.getMonth();var A=w.getDate();var z=w.getMonthDays();w.setDate(1);var r=(w.getDay()-this.firstDayOfWeek)%7;if(r<0){r+=7}w.setDate(-r);w.setDate(w.getDate()+1);var e=this.tbody.firstChild;var k=Calendar._SMN[x];var o=this.ar_days=new Array();var n=Calendar._TT.WEEKEND;var d=this.multiple?(this.datesCells={}):null;for(var t=0;t<6;++t,e=e.nextSibling){var a=e.firstChild;if(this.weekNumbers){a.className="day wn";a.innerHTML=w.getWeekNumber();a=a.nextSibling}e.className="daysrow";var u=false,f,c=o[t]=[];for(var s=0;s<7;++s,a=a.nextSibling,w.setDate(f+1)){f=w.getDate();var g=w.getDay();a.className="day";a.pos=t<<4|s;c[s]=a;var l=(w.getMonth()==x);if(!l){if(this.showsOtherMonths){a.className+=" othermonth";a.otherMonth=true}else{a.className="emptycell";a.innerHTML="&#160;";a.disabled=true;continue}}else{a.otherMonth=false;u=true}a.disabled=false;a.innerHTML=this.getDateText?this.getDateText(w,f):f;if(d){d[w.print("%Y%m%d")]=a}if(this.getDateStatus){var p=this.getDateStatus(w,h,x,f);if(p===true){a.className+=" disabled";a.disabled=true}else{if(/disabled/i.test(p)){a.disabled=true}a.className+=" "+p}}if(!a.disabled){a.caldate=new Date(w);a.ttip="_";if(!this.multiple&&l&&f==A&&this.hiliteToday){a.className+=" selected";this.currentDateEl=a}if(w.getFullYear()==q&&w.getMonth()==y&&f==b){a.className+=" today";a.ttip+=Calendar._TT.PART_TODAY}if(n.indexOf(g.toString())!=-1){a.className+=a.otherMonth?" oweekend":" weekend"}}}if(!(u||this.showsOtherMonths)){e.className="emptyrow"}}this.title.innerHTML=Calendar._MN[x]+", "+h;this.onSetTime();this.table.style.visibility="visible";this._initMultipleDates()};Calendar.prototype._initMultipleDates=function(){if(this.multiple){for(var b in this.multiple){var a=this.datesCells[b];var c=this.multiple[b];if(!c){continue}if(a){a.className+=" selected"}}}};Calendar.prototype._toggleMultipleDate=function(b){if(this.multiple){var c=b.print("%Y%m%d");var a=this.datesCells[c];if(a){var e=this.multiple[c];if(!e){Calendar.addClass(a,"selected");this.multiple[c]=b}else{Calendar.removeClass(a,"selected");delete this.multiple[c]}}}};Calendar.prototype.setDateToolTipHandler=function(a){this.getDateToolTip=a};Calendar.prototype.setDate=function(a){if(!a.equalsTo(this.date)){this._init(this.firstDayOfWeek,a)}};Calendar.prototype.refresh=function(){this._init(this.firstDayOfWeek,this.date)};Calendar.prototype.setFirstDayOfWeek=function(a){this._init(a,this.date);this._displayWeekdays()};Calendar.prototype.setDateStatusHandler=Calendar.prototype.setDisabledHandler=function(a){this.getDateStatus=a};Calendar.prototype.setRange=function(b,c){this.minYear=b;this.maxYear=c};Calendar.prototype.callHandler=function(){if(this.onSelected){this.onSelected(this,this.date.print(this.dateFormat))}};Calendar.prototype.callCloseHandler=function(){if(this.onClose){this.onClose(this)}this.hideShowCovered()};Calendar.prototype.destroy=function(){var a=this.element.parentNode;a.removeChild(this.element);Calendar._C=null;window._dynarch_popupCalendar=null};Calendar.prototype.reparent=function(b){var a=this.element;a.parentNode.removeChild(a);b.appendChild(a)};Calendar._checkCalendar=function(b){var c=window._dynarch_popupCalendar;if(!c){return false}var a=Calendar.is_ie?Calendar.getElement(b):Calendar.getTargetElement(b);for(;a!=null&&a!=c.element;a=a.parentNode){}if(a==null){window._dynarch_popupCalendar.callCloseHandler();return Calendar.stopEvent(b)}};Calendar.prototype.show=function(){var e=this.table.getElementsByTagName("tr");for(var d=e.length;d>0;){var f=e[--d];Calendar.removeClass(f,"rowhilite");var c=f.getElementsByTagName("td");for(var b=c.length;b>0;){var a=c[--b];Calendar.removeClass(a,"hilite");Calendar.removeClass(a,"active")}}this.element.style.display="block";this.hidden=false;if(this.isPopup){window._dynarch_popupCalendar=this;Calendar.addEvent(document,"keydown",Calendar._keyEvent);Calendar.addEvent(document,"keypress",Calendar._keyEvent);Calendar.addEvent(document,"mousedown",Calendar._checkCalendar)}this.hideShowCovered()};Calendar.prototype.hide=function(){if(this.isPopup){Calendar.removeEvent(document,"keydown",Calendar._keyEvent);Calendar.removeEvent(document,"keypress",Calendar._keyEvent);Calendar.removeEvent(document,"mousedown",Calendar._checkCalendar)}this.element.style.display="none";this.hidden=true;this.hideShowCovered()};Calendar.prototype.showAt=function(a,c){var b=this.element.style;b.left=a+"px";b.top=c+"px";this.show()};Calendar.prototype.showAtElement=function(c,d){var a=this;var e=Calendar.getAbsolutePos(c);if(!d||typeof d!="string"){this.showAt(e.x,e.y+c.offsetHeight);return true}function b(i){if(i.x<0){i.x=0}if(i.y<0){i.y=0}var j=document.createElement("div");var h=j.style;h.position="absolute";h.right=h.bottom=h.width=h.height="0px";document.body.appendChild(j);var g=Calendar.getAbsolutePos(j);document.body.removeChild(j);if(Calendar.is_ie){g.y+=(document.documentElement&&document.documentElement.scrollTop)||document.body.scrollTop;g.x+=(document.documentElement&&document.documentElement.scrollLeft)||document.body.scrollLeft}else{g.y+=window.scrollY;g.x+=window.scrollX}var f=i.x+i.width-g.x;if(f>0){i.x-=f}f=i.y+i.height-g.y;if(f>0){i.y-=f}}this.element.style.display="block";Calendar.continuation_for_the_khtml_browser=function(){var f=a.element.offsetWidth;var i=a.element.offsetHeight;a.element.style.display="none";var g=d.substr(0,1);var j="l";if(d.length>1){j=d.substr(1,1)}switch(g){case"T":e.y-=i;break;case"B":e.y+=c.offsetHeight;break;case"C":e.y+=(c.offsetHeight-i)/2;break;case"t":e.y+=c.offsetHeight-i;break;case"b":break}switch(j){case"L":e.x-=f;break;case"R":e.x+=c.offsetWidth;break;case"C":e.x+=(c.offsetWidth-f)/2;break;case"l":e.x+=c.offsetWidth-f;break;case"r":break}e.width=f;e.height=i+40;a.monthsCombo.style.display="none";b(e);a.showAt(e.x,e.y)};if(Calendar.is_khtml){setTimeout("Calendar.continuation_for_the_khtml_browser()",10)}else{Calendar.continuation_for_the_khtml_browser()}};Calendar.prototype.setDateFormat=function(a){this.dateFormat=a};Calendar.prototype.setTtDateFormat=function(a){this.ttDateFormat=a};Calendar.prototype.parseDate=function(b,a){if(!a){a=this.dateFormat}this.setDate(Date.parseDate(b,a))};Calendar.prototype.hideShowCovered=function(){if(!Calendar.is_ie&&!Calendar.is_opera){return}function b(k){var i=k.style.visibility;if(!i){if(document.defaultView&&typeof(document.defaultView.getComputedStyle)=="function"){if(!Calendar.is_khtml){i=document.defaultView.getComputedStyle(k,"").getPropertyValue("visibility")}else{i=""}}else{if(k.currentStyle){i=k.currentStyle.visibility}else{i=""}}}return i}var s=new Array("applet","iframe","select");var c=this.element;var a=Calendar.getAbsolutePos(c);var f=a.x;var d=c.offsetWidth+f;var r=a.y;var q=c.offsetHeight+r;for(var h=s.length;h>0;){var g=document.getElementsByTagName(s[--h]);var e=null;for(var l=g.length;l>0;){e=g[--l];a=Calendar.getAbsolutePos(e);var o=a.x;var n=e.offsetWidth+o;var m=a.y;var j=e.offsetHeight+m;if(this.hidden||(o>d)||(n<f)||(m>q)||(j<r)){if(!e.__msh_save_visibility){e.__msh_save_visibility=b(e)}e.style.visibility=e.__msh_save_visibility}else{if(!e.__msh_save_visibility){e.__msh_save_visibility=b(e)}e.style.visibility="hidden"}}}};Calendar.prototype._displayWeekdays=function(){var b=this.firstDayOfWeek;var a=this.firstdayname;var d=Calendar._TT.WEEKEND;for(var c=0;c<7;++c){a.className="day name";var e=(c+b)%7;if(c){a.ttip=Calendar._TT.DAY_FIRST.replace("%s",Calendar._DN[e]);a.navtype=100;a.calendar=this;a.fdow=e;Calendar._add_evs(a)}if(d.indexOf(e.toString())!=-1){Calendar.addClass(a,"weekend")}a.innerHTML=Calendar._SDN[(c+b)%7];a=a.nextSibling}};Calendar.prototype._hideCombos=function(){this.monthsCombo.style.display="none";this.yearsCombo.style.display="none"};Calendar.prototype._dragStart=function(ev){if(this.dragging){return}this.dragging=true;var posX;var posY;if(Calendar.is_ie){posY=window.event.clientY+document.body.scrollTop;posX=window.event.clientX+document.body.scrollLeft}else{posY=ev.clientY+window.scrollY;posX=ev.clientX+window.scrollX}var st=this.element.style;this.xOffs=posX-parseInt(st.left);this.yOffs=posY-parseInt(st.top);with(Calendar){addEvent(document,"mousemove",calDragIt);addEvent(document,"mouseup",calDragEnd)}};Date._MD=new Array(31,28,31,30,31,30,31,31,30,31,30,31);Date.SECOND=1000;Date.MINUTE=60*Date.SECOND;Date.HOUR=60*Date.MINUTE;Date.DAY=24*Date.HOUR;Date.WEEK=7*Date.DAY;Date.parseDate=function(l,c){var n=new Date();var o=0;var e=-1;var k=0;var q=l.split(/\W+/);var p=c.match(/%./g);var h=0,g=0;var r=0;var f=0;for(h=0;h<q.length;++h){if(!q[h]){continue}switch(p[h]){case"%d":case"%e":k=parseInt(q[h],10);break;case"%m":e=parseInt(q[h],10)-1;break;case"%Y":case"%y":o=parseInt(q[h],10);(o<100)&&(o+=(o>29)?1900:2000);break;case"%b":case"%B":for(g=0;g<12;++g){if(Calendar._MN[g].substr(0,q[h].length).toLowerCase()==q[h].toLowerCase()){e=g;break}}break;case"%H":case"%I":case"%k":case"%l":r=parseInt(q[h],10);break;case"%P":case"%p":if(/pm/i.test(q[h])&&r<12){r+=12}else{if(/am/i.test(q[h])&&r>=12){r-=12}}break;case"%M":f=parseInt(q[h],10);break}}if(isNaN(o)){o=n.getFullYear()}if(isNaN(e)){e=n.getMonth()}if(isNaN(k)){k=n.getDate()}if(isNaN(r)){r=n.getHours()}if(isNaN(f)){f=n.getMinutes()}if(o!=0&&e!=-1&&k!=0){return new Date(o,e,k,r,f,0)}o=0;e=-1;k=0;for(h=0;h<q.length;++h){if(q[h].search(/[a-zA-Z]+/)!=-1){var s=-1;for(g=0;g<12;++g){if(Calendar._MN[g].substr(0,q[h].length).toLowerCase()==q[h].toLowerCase()){s=g;break}}if(s!=-1){if(e!=-1){k=e+1}e=s}}else{if(parseInt(q[h],10)<=12&&e==-1){e=q[h]-1}else{if(parseInt(q[h],10)>31&&o==0){o=parseInt(q[h],10);(o<100)&&(o+=(o>29)?1900:2000)}else{if(k==0){k=q[h]}}}}}if(o==0){o=n.getFullYear()}if(e!=-1&&k!=0){return new Date(o,e,k,r,f,0)}return n};Date.prototype.getMonthDays=function(b){var a=this.getFullYear();if(typeof b=="undefined"){b=this.getMonth()}if(((0==(a%4))&&((0!=(a%100))||(0==(a%400))))&&b==1){return 29}else{return Date._MD[b]}};Date.prototype.getDayOfYear=function(){var a=new Date(this.getFullYear(),this.getMonth(),this.getDate(),0,0,0);var c=new Date(this.getFullYear(),0,0,0,0,0);var b=a-c;return Math.floor(b/Date.DAY)};Date.prototype.getWeekNumber=function(){var c=new Date(this.getFullYear(),this.getMonth(),this.getDate(),0,0,0);var b=c.getDay();c.setDate(c.getDate()-(b+6)%7+3);var a=c.valueOf();c.setMonth(0);c.setDate(4);return Math.round((a-c.valueOf())/(7*86400000))+1};Date.prototype.equalsTo=function(a){return((this.getFullYear()==a.getFullYear())&&(this.getMonth()==a.getMonth())&&(this.getDate()==a.getDate())&&(this.getHours()==a.getHours())&&(this.getMinutes()==a.getMinutes()))};Date.prototype.setDateOnly=function(a){var b=new Date(a);this.setDate(1);this.setFullYear(b.getFullYear());this.setMonth(b.getMonth());this.setDate(b.getDate())};Date.prototype.print=function(l){var b=this.getMonth();var k=this.getDate();var n=this.getFullYear();var p=this.getWeekNumber();var q=this.getDay();var v={};var r=this.getHours();var c=(r>=12);var h=(c)?(r-12):r;var u=this.getDayOfYear();if(h==0){h=12}var e=this.getMinutes();var j=this.getSeconds();v["%a"]=Calendar._SDN[q];v["%A"]=Calendar._DN[q];v["%b"]=Calendar._SMN[b];v["%B"]=Calendar._MN[b];v["%C"]=1+Math.floor(n/100);v["%d"]=(k<10)?("0"+k):k;v["%e"]=k;v["%H"]=(r<10)?("0"+r):r;v["%I"]=(h<10)?("0"+h):h;v["%j"]=(u<100)?((u<10)?("00"+u):("0"+u)):u;v["%k"]=r;v["%l"]=h;v["%m"]=(b<9)?("0"+(1+b)):(1+b);v["%M"]=(e<10)?("0"+e):e;v["%n"]="\n";v["%p"]=c?"PM":"AM";v["%P"]=c?"pm":"am";v["%s"]=Math.floor(this.getTime()/1000);v["%S"]=(j<10)?("0"+j):j;v["%t"]="\t";v["%U"]=v["%W"]=v["%V"]=(p<10)?("0"+p):p;v["%u"]=q+1;v["%w"]=q;v["%y"]=(""+n).substr(2,2);v["%Y"]=n;v["%%"]="%";var t=/%./g;if(!Calendar.is_ie5&&!Calendar.is_khtml){return l.replace(t,function(a){return v[a]||a})}var o=l.match(t);for(var g=0;g<o.length;g++){var f=v[o[g]];if(f){t=new RegExp(o[g],"g");l=l.replace(t,f)}}return l};window._dynarch_popupCalendar=null;


/*===============================
/porto/media/system/js/calendar-setup.js
================================================================================*/;
Calendar.setup=function(g){function f(h,i){if(typeof g[h]=="undefined"){g[h]=i}}f("inputField",null);f("displayArea",null);f("button",null);f("eventName","click");f("ifFormat","%Y/%m/%d");f("daFormat","%Y/%m/%d");f("singleClick",true);f("disableFunc",null);f("dateStatusFunc",g.disableFunc);f("dateTooltipFunc",null);f("dateText",null);f("firstDay",null);f("align","Br");f("range",[1900,2999]);f("weekNumbers",true);f("flat",null);f("flatCallback",null);f("onSelect",null);f("onClose",null);f("onUpdate",null);f("date",null);f("showsTime",false);f("timeFormat","24");f("electric",true);f("step",2);f("position",null);f("cache",false);f("showOthers",false);f("multiple",null);var c=["inputField","displayArea","button"];for(var b in c){if(typeof g[c[b]]=="string"){g[c[b]]=document.getElementById(g[c[b]])}}if(!(g.flat||g.multiple||g.inputField||g.displayArea||g.button)){alert("Calendar.setup:\n  Nothing to setup (no fields found).  Please check your code");return false}function a(i){var h=i.params;var j=(i.dateClicked||h.electric);if(j&&h.inputField){h.inputField.value=i.date.print(h.ifFormat);if(typeof h.inputField.onchange=="function"){h.inputField.onchange()}}if(j&&h.displayArea){h.displayArea.innerHTML=i.date.print(h.daFormat)}if(j&&typeof h.onUpdate=="function"){h.onUpdate(i)}if(j&&h.flat){if(typeof h.flatCallback=="function"){h.flatCallback(i)}}if(j&&h.singleClick&&i.dateClicked){i.callCloseHandler()}}if(g.flat!=null){if(typeof g.flat=="string"){g.flat=document.getElementById(g.flat)}if(!g.flat){alert("Calendar.setup:\n  Flat specified but can't find parent.");return false}var e=new Calendar(g.firstDay,g.date,g.onSelect||a);e.setDateToolTipHandler(g.dateTooltipFunc);e.showsOtherMonths=g.showOthers;e.showsTime=g.showsTime;e.time24=(g.timeFormat=="24");e.params=g;e.weekNumbers=g.weekNumbers;e.setRange(g.range[0],g.range[1]);e.setDateStatusHandler(g.dateStatusFunc);e.getDateText=g.dateText;if(g.ifFormat){e.setDateFormat(g.ifFormat)}if(g.inputField&&typeof g.inputField.value=="string"){e.parseDate(g.inputField.value)}e.create(g.flat);e.show();return false}var d=g.button||g.displayArea||g.inputField;d["on"+g.eventName]=function(){var h=g.inputField||g.displayArea;var k=g.inputField?g.ifFormat:g.daFormat;var o=false;var m=window.calendar;if(h){g.date=Date.parseDate(h.value||h.innerHTML,k)}if(!(m&&g.cache)){window.calendar=m=new Calendar(g.firstDay,g.date,g.onSelect||a,g.onClose||function(i){i.hide()});m.setDateToolTipHandler(g.dateTooltipFunc);m.showsTime=g.showsTime;m.time24=(g.timeFormat=="24");m.weekNumbers=g.weekNumbers;o=true}else{if(g.date){m.setDate(g.date)}m.hide()}if(g.multiple){m.multiple={};for(var j=g.multiple.length;--j>=0;){var n=g.multiple[j];var l=n.print("%Y%m%d");m.multiple[l]=n}}m.showsOtherMonths=g.showOthers;m.yearStep=g.step;m.setRange(g.range[0],g.range[1]);m.params=g;m.setDateStatusHandler(g.dateStatusFunc);m.getDateText=g.dateText;m.setDateFormat(k);if(o){m.create()}m.refresh();if(!g.position){m.showAtElement(g.button||g.displayArea||g.inputField,g.align)}else{m.showAt(g.position[0],g.position[1])}return false};return e};


/*===============================
/porto/media/plg_captcha_recaptcha/js/recaptcha.min.js
================================================================================*/;
window.JoomlaInitReCaptcha2=function(){"use strict";var e=document.getElementsByClassName("g-recaptcha"),t,n;for(var r=0,i=e.length;r<i;r++)t=e[r],n=t.dataset?t.dataset:{sitekey:t.getAttribute("data-sitekey"),theme:t.getAttribute("data-theme"),size:t.getAttribute("data-size")},grecaptcha.render(t,n)};