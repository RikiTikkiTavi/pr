<?php
/**
 * @package     Joomla.Plugin
 * @subpackage  JBSmartportfolio.Like
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/**
 * Vote plugin.
 *
 * @since  1.5
 */
 



class PlgJBSmartportfolioFavourite extends JPlugin
{
	/**
	 * Load the language file on instantiation.
	 *
	 * @var    boolean
	 * @since  3.1
	 */
	protected $autoloadLanguage = true;

	/**
	 * Displays the voting area if in an article
	 *
	 * @param   string   $context  The context of the jbsmartportfolio being passed to the plugin
	 * @param   object   &$row     The article object
	 * @param   object   &$params  The article params
	 * @param   integer  $page     The 'page' number
	 *
	 * @return  mixed  html string containing code for the votes if in com_jbsmartportfolio else boolean false
	 *
	 * @since   1.6
	 */
	public function onAfterContentTitle($context, &$row, &$params, $page=0)
	{  
		$parts = explode(".", $context);

		if ($parts[0] != 'com_jbsmartportfolio')
		{
			return false;
		}
		$html = '';		
	   

		$doc = JFactory::getDocument();
		$doc->addStyleSheet(JURI::root().'plugins/jbsmartportfolio/favourite/assets/favourite.css');
		if (!empty($params) && $params->get('show_like_count', null))
		{
		
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		
	
		$query->select('rating_count')
		->from($db->quoteName('#__jbsmartportfolio_rating'))
		->where($db->quoteName('item_id') . ' = ' . (int) $row->jbsmartportfolio_item_id);
		
		// Set the query and load the result.
		 $db->setQuery($query);		 
		$rating = 0; 
	    $rating = $db->loadResult();	
	 	$view = JFactory::getApplication()->input->getString('view', '');
		$img = '';

		$starImageOn = JHtml::_('image', JURI::root().'plugins/jbsmartportfolio/favourite/assets/favourite_on.png', JText::_('PLG_LIKE_STAR_ACTIVE'), 'width="16"', true);
		$starImageOff = JHtml::_('image', JURI::root().'plugins/jbsmartportfolio/favourite/assets/favourite_off.png', JText::_('PLG_LIKE_STAR_INACTIVE'), 'width="16"', true);

		if($rating > 1) {
			$img .= $starImageOn;
		} else {
			$img .= $starImageOff;		
		}
		 
		if (($view == 'item' || $view == 'items' )&& $row->enabled == 1)
			{
				$uri = JUri::getInstance();
				$uri->setQuery($uri->getQuery() . '&like_count=0');
				// Generate voting form
				$html .= '<form method="post" action="' . htmlspecialchars($uri->toString()) . '" class="form-inline form-favourite">';
				$html .= '<span class="jbsmartportfolio_like">';
				$html .= '<input type="hidden" name="user_rating" value="5" />';
				$html .= '<button type="submit" name="submit_vote" class="btn-favourite">' .$img. '   ' .$rating.'</button>';
				$html .= '<input type="hidden" name="view" value="items" />';
				$html .= '<input type="hidden" name="task" value="likeMe" />';
				$html .= '<input type="hidden" name="like_count" value="0" />';
				$html .= '<input type="hidden" name="piid" value="'.$row->jbsmartportfolio_item_id.'" />';
				$html .= '<input type="hidden" name="url" value="' . htmlspecialchars($uri->toString()) . '" />';
				$html .= JHtml::_('form.token');
				$html .= '</span>';
				$html .= '</form>';
			}
		}
		return $html;
	}
}
