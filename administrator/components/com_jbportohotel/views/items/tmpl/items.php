<?php
/**
 * @package J2Store
 * @copyright Copyright (c)2014-17 Ramesh Elamathi / J2Store.org
 * @license GNU GPL v3 or later
 */
// No direct access to this file
defined('_JEXEC') or die;
JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.framework', true);

$app = JFactory::getApplication();
$db = JFactory::getDbo();
$function  = $app->input->getString('function', 'jSelectItem');
$field = $app->input->getString('field','jform_jbspitem_list');
$listOrder = $this->escape($this->state->get('list.ordering'));
$listDirn  = $this->escape($this->state->get('list.direction'));
?>
<style type="text/css">
/** sometime when we select  to hide toolbar when tmpl is opened  */
.navbar-fixed-top ,.header ,.subhead-collapse{
	display:none;
}
</style>

<form action="<?php echo JRoute::_('index.php?option=com_jbportohotel&view=items&task=setItems');?>" method="post" name="adminForm" id="productadminForm" class="form-inline">
	<h5><?php // echo JText::_('COM_J2STORE_PRODUCTS');?></h5>
	<div class="row-fluid">
		<table class="adminlist table table-striped ">
			<tr>
				<td>
					<div class="input-prepend">						 
						<?php // echo J2Html::text('search',htmlspecialchars($this->state->search),array('id'=>'search') );?>
						<?php  // echo J2Html::button('go',JText::_( 'J2STORE_FILTER_GO' ) ,array('class'=>'btn btn-success','onclick'=>'this.form.submit();'));?>
						<?php  // echo J2Html::button('reset',JText::_( 'J2STORE_FILTER_RESET' ),array('id'=>'reset-filter-search','class'=>'btn btn-inverse',"onclick"=>"jQuery('#search').attr('value','');this.form.submit();"));?>
					</div>
				</td>
				<td>
					<?php echo $this->pagination->getLimitBox(); ?>
				</td>
			<tr>
		</table>
		<table class="table table-striped table-bordered">
			<thead>
				<tr>
					<th>
						<input class="btn btn-success" id="setAllProductsBnt" type="button" value="<?php echo JText::_('J2STORE_SET_VALUES');?>"  style="display:none;"/>
						<br/>
						<?php echo JHtml::_('grid.checkall'); ?>
					</th>
					<th class="title">
						<?php echo JText::_('COM_JBPORTOHOTEL_ITEMS_TITLE_LABEL'); ?>
					</th>
				 
				</tr>
			</thead>
			<tfoot>
				<tr>
					<td colspan="3">
						<?php echo $this->pagination->getListFooter(); ?>
					</td>
				</tr>
			</tfoot>
			<tbody>
				<?php foreach($this->items as $key=>$item):?>
				<?php $canChange  = 1;?>
				<tr>
					<td>
						<input data-product-title="<?php echo $item->title;?>" id="cb<?php echo $item->jbportohotel_item_id;?>" type="checkbox" onclick="Joomla.isChecked(this.checked);" value="<?php echo $item->jbportohotel_item_id;?>" name="cid[]">
						<input type="hidden" name="tmp_title" value="<?php echo $item->jbportohotel_item_id; ?>" />
						<?php // echo J2html::hidden('tmp_title['.$item->jbportohotel_item_id.']', $item->title ,array('class'=>'tmp_title')); ?>
					</td>
					<td>
						<a href="javascript:if (window.parent) window.parent.<?php echo $db->escape($function);?>('<?php echo $item->jbportohotel_item_id; ?>','<?php echo $item->title;?>' ,'<?php echo $field;?>');">
							<?php echo $item->title; ?>
						</a>
					</td>
					 
				</tr>
				<?php endforeach;?>
			</tbody>
		</table>
		 
		
		
			<input type="hidden" value="com_jbportohotel" name="option">
	<input type="hidden" value="item" name="view">
	<input type="hidden" id="task" value="setItems" name="task">
	<input type="hidden" id="layout" value="items" name="layout">
	<input type="hidden" value="0" name="boxchecked">
	<input type="hidden" value="<?php echo $listOrder?>" name="filter_order">
	<input type="hidden" value="<?php echo $listDirn?>" name="filter_order_Dir">
	<input type="hidden" value="<?php echo $this->item->jbportohotel_item_id;?>" name="jbportohotel_item_id">
	<?php echo JHtml::_('form.token'); ?>
	</div>
</form>
<script>
var newArray =new Array();
var checkedValues;
var titles;

Joomla.submitform =function(){
	var pressbutton =  jQuery("#task").val();
	 submitform(pressbutton);
}
/**
 * Override Joomlaischecked
 */
Joomla.isChecked=function(a,d){
	if(typeof(d)==="undefined"){
		d=document.getElementById("portfolioadminForm")
	}
	if(a==true){
		d.boxchecked.value++

		(function($){
			//now show the
			$("#setAllProductsBnt").show();

		 		checkedValues =  $('input:checkbox:checked').map(function() {
			    	return this.value ;
				}).get();

					titles =$('input:checkbox:checked').map(function(){
				return $(this).data('title');
			}).get();

		})(jQuery);
		}else{
			d.boxchecked.value--

			(function($){
				$("#setAllProductsBnt").hide();
				})(j2store.jQuery);
	}

	var g=true,b,f;
	for(b=0,n=d.elements.length;b<n;b++){
		f=d.elements[b];
	if(f.type=="checkbox"){
		if(f.name!="checkall-toggle"&&f.checked==false){
				g=false;
				break
		}
		}
	}if(d.elements["checkall-toggle"]){
		d.elements["checkall-toggle"].checked= g
	}
};




(function($){
	$("input[name=checkall-toggle]").click(function(){
		$("#setAllProductsBnt").toggle(this.checked);
		checkedValues = $('input:checkbox:checked').map(function() {
	    	return this.value ;
		}).get();
		titles = $('.tmp_title').map(function(){
			return this.value;
		}).get();

	});
	//$("input[name=checkall-toggle]").trigger('change');
})(jQuery);

(function($){
	$("#setAllProductsBnt").click(function(){
		var form = $("#adminForm");
		var html ='';
		newArray = mergeArray(checkedValues , titles)
		$(newArray).each(function(index,value){
			if($('#jform_product_list' ,window.parent.document).find('#item-row-'+value.id ).length == 0){
				html ='<tr id="item-row-'+ value.id +'"><td><input type="hidden" name="items['+value.id +']" value='+value.id+' />'+value.title +'</td><td><button class="btn btn-danger" onclick="jQuery(this).closest(\'tr\').remove();"><i class="icon icon-trash"></button></td></tr>';
				$('#jform_product_list', window.parent.document).append(html);
				window.close();
			}
		});
		checkedValues.length='';

	});

 	function mergeArray(checkedValues , titles){

 		checkedValues = cleanArray(checkedValues);
	     for(var i = 0; i < checkedValues.length; i++){
		     newArray.push({'id':checkedValues[i],'title': titles[i]});
	        }
    	 return newArray;
	 };
	function cleanArray(actual){
	  var tmpArray = new Array();
	  for(var i = 0; i<actual.length; i++){
	      if (actual[i]){
	        tmpArray.push(actual[i]);
	    }
	  }
	  return tmpArray;
	}
})(jQuery);
</script>

