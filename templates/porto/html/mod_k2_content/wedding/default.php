<?php
/**
 * @version		2.6.x
 * @package		K2
 * @author		JoomlaWorks http://www.joomlaworks.net
 * @copyright	Copyright (c) 2006 - 2014 JoomlaWorks Ltd. All rights reserved.
 * @license		GNU/GPL license: http://www.gnu.org/copyleft/gpl.html
 */

// no direct access
defined('_JEXEC') or die;

$moduleIntro = $params->get ('module-intro');?>

<?php if($moduleIntro) : ?>
	<div class="module-intro"><?php echo $moduleIntro; ?></div>
<?php endif; ?>


<div id="k2ModuleBox<?php echo $module->id; ?>" class="k2ItemsBlock<?php if($params->get('moduleclass_sfx')) echo ' '.$params->get('moduleclass_sfx'); ?>">

	<?php if($params->get('itemPreText')): ?>
	<p class="modulePretext"><?php echo $params->get('itemPreText'); ?></p>
	<?php endif; ?>

	
	<?php if(count($items)): ?>
<ul id="blogLoadMoreWrapper" class="blog-list sort-destination p-none" data-total-pages="2">
	<?php foreach ($items as $key=>$item):	?>
		<li class="col-sm-4 col-md-4 p-md isotope-item">
			<div class="blog-item">
				<a href="<?php echo $item->link; ?>" class="text-decoration-none">
					<span class="thumb-info thumb-info-lighten mb-xlg">
						<span class="thumb-info-wrapper m-none">
							<?php if($params->get('itemImage') && isset($item->image)): ?>
								<img class="img-responsive"  src="<?php echo $item->image; ?>" alt="<?php echo K2HelperUtilities::cleanHtml($item->title); ?>"/>
							<?php endif; ?>
						</span>
					</span>
					
					<span class="blog-item-content">
						<span class="category text-uppercase font-weight-semibold">
							<?php if($params->get('itemCategory')): ?>
								<?php echo $item->categoryname; ?>
							<?php endif; ?>
						</span>
							<?php if($params->get('itemTitle')): ?>
								<h3 class="text-capitalize mb-xlg"><?php echo $item->title; ?></h3>
							 <?php endif; ?>
							
								<p class="mb-xlg"><?php echo $item->introtext; ?></p>
									<span class="author text-color-secondary pt-xs pb-xs">
									By: <?php echo $item->author; ?>
										<span class="likes custom-default-text-color pull-right">
											<i class="fa fa-heart-o text-color-primary pr-xs"></i>
											<?php echo $item->hits; ?>
										</span>
									</span>
						</span>
				</a>
			</div>
		</li>
		<?php endforeach; ?>
   <ul>
   <?php endif; ?>
	
	
	<div class="text-center">
		<?php if($params->get('itemCustomLink')): ?>
			<a class="btn btn-secondary btn-xs custom-border-radius font-size-md text-color-light text-uppercase outline-none p-md pl-xlg pr-xlg m-auto mb-xlg mt-xlg" href="<?php echo $params->get('itemCustomLinkURL'); ?>" title="<?php echo K2HelperUtilities::cleanHtml($itemCustomLinkTitle); ?>"><?php echo $itemCustomLinkTitle; ?> <em class="icon-line-arrow-right"></em></a>
		<?php endif; ?>
	</div>
		<?php if($params->get('feed')): ?>
	<div class="k2FeedIcon">
		<a href="<?php echo JRoute::_('index.php?option=com_k2&view=itemlist&format=feed&moduleID='.$module->id); ?>" title="<?php echo JText::_('K2_SUBSCRIBE_TO_THIS_RSS_FEED'); ?>">
			<span><?php echo JText::_('K2_SUBSCRIBE_TO_THIS_RSS_FEED'); ?></span>
		</a>
		<div class="clr"></div>
	</div>
	<?php endif; ?>

</div>
