<?php
/**
 * @package     JB Smart Portfolio
 *
 * @copyright   Copyright (C) 2016 Joomlabuff. All rights reserved.
 * @license     GNU General Public License version 2 or later.
 */

defined('_JEXEC') or die();
?>


<script type="text/javascript">
jQuery('.jbsmartportfolio-list-items .jb-smartportfolio-item .owl-carousel').owlCarousel({"items": 1, "margin": 10, "animateOut": "fadeOut", "autoplay": true, "autoplayTimeout": 3000});
</script>
