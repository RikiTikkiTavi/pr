<?php
/**
 * @version    CVS: 1.0.0
 * @package    com_jbeasyformmaker
 * @author     Priya Bose <support@joomlabuff.com>
 * @copyright  2016 www.joomlabuff.com
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access to this file
defined ( '_JEXEC' ) or die ();
require_once JPATH_ADMINISTRATOR . '/components/com_jbeasyformmaker/helpers/helper.php';
class JFormFieldJBTags extends JFormField {
	protected $type = 'JBTags';
	public function getInput() {
		$app = JFactory::getApplication();
		$form_id = $app->input->getInt('id');
		if($form_id){
			
			$db = JFactory::getDbo ();
			$query = $db->getQuery ( true );
			$query->select ( '*' );
			$query->from ( '#__jbeasyformmaker_cforms' );
			$query->where ( 'jbeasyformmaker_cform_id=' . $form_id);
			$db->setQuery ( $query );
			$item = $db->loadObject ();			
			$tags = JBHelper::Shortcodenames($item);
			$html = '';
			
			
			 
			foreach ( $tags as $key => $value) {
		 	 
				$key = trim($key);
				$html .= '<a onclick="insertEmailTag(\'#email_message\',this);return false;" class="btn btn-default"   data-shortcode-key="['.$key.']" >' . $value . '</a>';
				$html .= '&nbsp';
			}
			
			$html .= '<script type="text/javascript"> function insertEmailTag(id,html){ 	  var shortcode = jQuery(html).data("shortcode-key"); jInsertEditorText(shortcode,id);  var areaValue = jQuery(id).val();  jQuery(id).val(areaValue + shortcode); };</script>';
			return $html;
		}
		
	}
	public function getLabel() {
	}
}