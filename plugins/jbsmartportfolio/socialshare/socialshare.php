<?php
/**
 * @package		Social Buttons
 * @subpackage	plg_social_buttons
 * @copyright	Copyright (C) 2013 Elite Developers All rights reserved.
 * @license		GNU/GPL v3 http://www.gnu.org/licenses/gpl.html
 */

defined('_JEXEC') or die( 'Restricted access' );
//	ECHO 'test'; exit;
class plgJBSmartportfolioSocialShare extends JPlugin {
	
	public function onAfterContentDisplay($context, &$row, &$params, $page = 0) {
		
		return $this->getSocialShare($context, $row, $params, $page = 0);
		
		if($params->get('show_social_share',0)){
		$app = JFactory::getApplication();
		$active = $app -> getMenu() -> getActive();
		$show = $this->params->get( 'show' );
		if ( $show ) {
			if ( !is_array( $show ) ) {
				$shows[] = $show ;
			} else {
				$shows = $show ;
			}
			
			foreach ( $shows as $va ) {
				if ( $va == 'other' ) {
					if ( ( $active->component != 'com_jbsmartportfolio' ) || ( $context != 'com_jbsmartportfolio.item' ) ) {
						return ;
					}
				} else {
					if ( ( JRequest :: getVar( 'view' ) ) == $va ) {
						return ;
					}
					if ( $va == 'frontpage' ) {
						$menu = $app->getMenu();
						if ($active == $menu->getDefault()) {
							return ;
						}
					}
				}
			}
		}
		if ( $context != 'mod_custom.content' ) {
			$exclude_cat = $this->params->get( 'exclude_cat' , 0 );
			if ( !empty( $exclude_cat ) ) {
				if ( strlen( array_search( $row->catid , $exclude_cat ) ) ) { 
					return ; 
				}
			}
			$exclude_art = $this->params->get( 'exclude_art' , '' );
			$itemsArray = explode( "," , $exclude_art );
			if( !empty( $exclude_art ) ) { 
				if ( strlen( array_search( $row->jbsmartportfolio_item_id , $itemsArray ) ) ) {
					return ; 
				}
			}
			require_once JPATH_BASE . '/components/com_jbsmartportfolio/router.php' ;
			$Itemid = JRequest::getVar( "Itemid" , "1" );
			if ( $row->jbsmartportfolio_item_id ) {
				$link = JURI::getInstance();
				$root = $link->getScheme() . "://" . $link->getHost();  
				if ( $active->component ) {
					if ( $active->component == 'com_jbsmartportfolio' ) {
						if ( $row->alias  ) {
							$link = JRoute::_('index.php?option=com_jbsmartportfolio&view=item&id='.$row->jbsmartportfolio_item_id.':'.$row->alias . $Itemid);
							
							//$link = JRoute::_( ContentHelperRoute::getArticleRoute( $row->slug , $row->catslug ) , false );
							
							
						} 
					}
				}
				$link = $root . $link ;
			} else {
				$jURI = JURI::getInstance();
				$link = $jURI->toString();
			}
			$facebook_width = $this->params->get( 'facebook_width' );

			$twitter_width = $this->params->get( 'twitter_width' );
			
			$googleplus_width = $this->params->get( 'googleplus_width' ); 
			
			$linkedin_width = $this->params->get( 'linkedin_width' );
			
			$html = '' ;
			$html .= '<div style="clear:both;"></div>' ;
			$html .= '<div class="socialbuttons" style="padding-top: 5px;padding-bottom:5px; overflow: hidden; float: ' . $this->params->get( 'align' , 'left' ) . ';">' ;
			$document = JFactory::getDocument();
		    $config = JFactory::getConfig();
		    $pattern = "/<img[^>]*src\=['\"]?(([^>]*)(jpg|gif|png|jpeg))['\"]?/" ;
			preg_match( $pattern , $row->description , $matches );
			if ( !empty( $matches ) ) {
				$document->addCustomTag( '<meta property="og:image" content="' . JURI::root() . '' . $matches[1] . '"/>' );
			}
			
			
			// htmlspecialchars($uri->toString());
			if ( $this->params->get( 'facebook' ) == 1 ) { 
				$sitename = $config->get( 'sitename' );
				$document->addCustomTag( '<meta property="og:site_name" content="' . $sitename . '"/>' );
				$document->addCustomTag( '<meta property="og:title" content="' . $row->title . '"/>' );
				$document->addCustomTag( '<meta property="og:type" content="item"/>' );
				$document->addCustomTag( '<meta property="og:url" content="' . $link . '"/>' );
				$html .='<a href="http://www.facebook.com/sharer.php?'.htmlspecialchars($uri->toString()).'" target="_blank">';
				$html .='<img src="https://simplesharebuttons.com/images/somacro/facebook.png" alt="Facebook" />';
				$html .='</a>';
    
			/*	$html .= '<div style="width: ' . $facebook_width . 'px !important; height: 20px; float: left; border: none;">' ;
				$html .= '<iframe src="https://www.facebook.com/plugins/like.php?locale=' . $this->params->get( 'facebook_language' ) . '&href=' . rawurlencode( $link ) . '&amp;layout=button_count&amp;show_faces=true&amp;action=' . $this->params->get( 'facebook_action' ) . '&amp;colorscheme=' . $this->params->get( 'facebook_color' ) . '&amp;font=' . $this->params->get( 'facebook_font' ) . '&amp;height=20" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width: ' . $this->params->get( 'facebook_width' ) . 'px; height :20px;" allowTransparency="true"></iframe>' ;
				$html .= '</div>' ;*/
			} else { 
				$html .= '' ; 
			}
			if ( $this->params->get( 'twitter' ) == 1 ) {
				$html .= '<div style="width: ' . $twitter_width . 'px !important; height: 20px; float: left; margin-left: 10px; border: none;">' ;
				$html .= '<a rel="nofollow" href="http://twitter.com/share" class="twitter-share-button" data-url="' . $link . '" data-count="horizontal" data-lang="en">Twitter</a><script src="https://platform.twitter.com/widgets.js" type="text/javascript"></script>' ; 
				$html .= '</div>' ;
			} else { 
				$html .= '' ; 
			}			
			if ( $this->params->get( 'google' ) == 1 ) {
				$doc = JFactory::getDocument();
				$document->addCustomTag( '<script type="text/javascript" src="https://apis.google.com/js/plusone.js">{lang: \'' . $this->params->get( 'googleplus_language' ) . '\'}</script>');
				$html .= '<div style="width: ' . $googleplus_width . 'px !important; height: 20px; float: left; margin-left: 10px; border: none;">' ;
				$html .= '<g:plusone size="medium"></g:plusone>' ;
				$html .= '</div>';
			} else {
				$html .= '' ;
			}
			if ( $this->params->get( 'linkedin' ) == 1 ) {
				$html .= '<div style="width: ' . $linkedin_width . 'px !important; height: 20px; float: left; margin-left: 10px; border: none;">' ;
				$html .= '<script type="text/javascript" src="https://platform.linkedin.com/in.js"></script><script type="IN/share" data-url="' . $link . '" data-counter="right"></script>' ; 
				$html .= '</div>' ;
			} else {
				$html .= '' ;
			}
			$html .= '</div>' ;
			$html .= '<div style="clear:both;"></div>' ;
			
			return $html;
		  
	}
}
	}

	public function onContentAfterTitle ( $context , &$row , &$params , $page = 0 ) {
		/*
	 
			$html = $this->getSocialShare( $context , $row , $params , $page = 0);
             $position = $this->params->get( 'position' , 'above' ) ;
			
			
			 if ( $this->params->get( 'show_front' ) == 1 ) {
				if ( $position == 'above' ) {
					$row->description = $html . $row->description ;
					$row->introtext = $html . $row->introtext ;
				} else {
					$row->description .= $html ;
					$row->introtext .= $html ;
				}
			} else {
				if ( $position == 'above' ) {
					$row->description = $html . $row->description ;
				} else {
					$row->description .= $html ;
				}
			}  */
	} 
		
		
		
		public function getSocialShare($context, &$row, &$params, $page = 0){
					$document = JFactory::getDocument();
			//$document>add
			$image_path = JUri::root().'plugins/jbsmartportfolio/socialshare/assets/images/';
	if($params->get('show_social_share',0)){
		$app = JFactory::getApplication();
		$active = $app -> getMenu() -> getActive();
		$show = $this->params->get( 'show' );
		if ( $show ) {
			if ( !is_array( $show ) ) {
				$shows[] = $show ;
			} else {
				$shows = $show ;
			}
			
			foreach ( $shows as $va ) {
				if ( $va == 'other' ) {
					if ( ( $active->component != 'com_jbsmartportfolio' ) || ( $context != 'com_jbsmartportfolio.item' ) ) {
						return ;
					}
				} else {
					if ( ( JRequest :: getVar( 'view' ) ) == $va ) {
						return ;
					}
					if ( $va == 'frontpage' ) {
						$menu = $app->getMenu();
						if ($active == $menu->getDefault()) {
							return ;
						}
					}
				}
			}
		}
		//if ( $context != 'mod_custom.content' ) {
			/*$exclude_cat = $this->params->get( 'exclude_cat' , 0 );
			if ( !empty( $exclude_cat ) ) {
				if ( strlen( array_search( $row->catid , $exclude_cat ) ) ) { 
					return ; 
				}
			}
			$exclude_art = $this->params->get( 'exclude_art' , '' );
			$itemsArray = explode( "," , $exclude_art );
			if( !empty( $exclude_art ) ) { 
				if ( strlen( array_search( $row->jbsmartportfolio_item_id , $itemsArray ) ) ) {
					return ; 
				}
			}
			
			*/
			require_once JPATH_BASE . '/components/com_jbsmartportfolio/router.php' ;
			$Itemid = JRequest::getVar( "Itemid" , "1" );
			if ( $row->jbsmartportfolio_item_id ) {
				$link = JURI::getInstance();
				$root = $link->getScheme() . "://" . $link->getHost();  
				if ( $active->component ) {
					if ( $active->component == 'com_jbsmartportfolio' ) {
						if ( $row->alias  ) {
							$link = JRoute::_('index.php?option=com_jbsmartportfolio&view=item&id='.$row->jbsmartportfolio_item_id.':'.$row->alias . $Itemid);
							
							//$link = JRoute::_( ContentHelperRoute::getArticleRoute( $row->slug , $row->catslug ) , false );
							
							
						} 
					}
				}
				$link = $root . $link ;
			} else {
				$jURI = JURI::getInstance();
				$link = $jURI->toString();
			}
			
			$facebook_width = $this->params->get( 'facebook_width' );

			$twitter_width = $this->params->get( 'twitter_width' );
			
			$googleplus_width = $this->params->get( 'googleplus_width' ); 
			
			$linkedin_width = $this->params->get( 'linkedin_width' );
			 
			$html = '' ;
			$html .='<style type="text/css">
 
#share-buttons img {
width: 35px;
padding: 5px;
border: 0;
box-shadow: 0;
display: inline;
}
 .socialbuttons img {
    border-radius: 4px;
    color: #fff;
    display: inline-block;
    font-size: 14px;
    height: 2.2857em;
    margin: 0.2857em 0.5714em 0.2857em 0;
    *position: relative;
    text-align: center;
    text-decoration: none;
    text-indent: -9999em;
    transform: translate3d(0px, 0px, 0px);
    transition: all 0.25s ease 0s;
    vertical-align: middle;
    width: 2.2857em;
}
</style>';
			$html .= '<div class="jbsmartportfolio socialbuttons">' ;
			$document = JFactory::getDocument();
		    $config = JFactory::getConfig();
		    $pattern = "/<img[^>]*src\=['\"]?(([^>]*)(jpg|gif|png|jpeg))['\"]?/" ;
			preg_match( $pattern , $row->description , $matches );
			if ( !empty( $matches ) ) {
				$document->addCustomTag( '<meta property="og:image" content="' . JURI::root() . '' . $matches[1] . '"/>' );
			}
			if ( $this->params->get( 'facebook' ) == 1 ) { 
				$sitename = $config->get( 'sitename' );
				$document->addCustomTag( '<meta property="og:site_name" content="' . $sitename . '"/>' );
				$document->addCustomTag( '<meta property="og:title" content="' . $row->title . '"/>' );
				$document->addCustomTag( '<meta property="og:type" content="item"/>' );
				$document->addCustomTag( '<meta property="og:url" content="' . $link . '"/>' );
			
			 
				$fb_image = $image_path.'/facebook.png';
				$html .='<a href="http://www.facebook.com/sharer.php?u='.$link.'" target="_blank">';
				$html .='<img src="'.$fb_image.'" alt="Facebook" />';
				$html .='</a>';
				
				
			} else { 
				$html .= '' ; 
			}
			if ( $this->params->get( 'twitter' ) == 1 ) {
		 
				
				$html .='<a href="https://twitter.com/share?url='.$link.'&amp;text='.$row->title.'&amp;hashtags='.$row->title.'" target="_blank">';
				$html .='<img src="'.$image_path.'/twitter.png" alt="Twitter" />';
				$html .='</a>';
			} else { 
				$html .= '' ; 
			}
			
			if ( $this->params->get( 'linkedin' ) == 1 ) {
			 
				$lin_image = $image_path.'/linkedin.png';
				$html .='<a href="http://www.linkedin.com/shareArticle?mini=true&amp;url='.$link.'" target="_blank">';
				//$html .='<img src="https://simplesharebuttons.com/images/somacro/linkedin.png" alt="LinkedIn" />';
				$html .='<img src="'.$lin_image.'" alt="LinkedIn" />';
				$html .='</a>';
			} else {
				$html .= '' ;
			}
						
			if ( $this->params->get( 'google' ) == 1 ) {
				$doc = JFactory::getDocument();
				$document->addCustomTag( '<script type="text/javascript" src="https://apis.google.com/js/plusone.js">{lang: \'' . $this->params->get( 'googleplus_language' ) . '\'}</script>');
			 
				$gg_image = $image_path.'/google-plus.png';
			   $html .='<a href="https://plus.google.com/share?url='.$link.'" target="_blank">';
			  // $html .='<img src="https://simplesharebuttons.com/images/somacro/google.png" alt="Google" />';
			   $html .='<img src="'.$gg_image.'" alt="Google" />';
			   $html .='</a>';
			} else {
				$html .= '' ;
			}
			
			//if ( $this->params->get( 'email' ) == 1 ) {
			 
				$e_image = $image_path.'/email.png';
				$html .='<a href="mailto:?Subject='.$row->title.'&amp;Body=I%20saw%20this%20and%20thought%20of%20you!%20 '.$link.'" target="_blank">';
				//$html .='<img src="https://simplesharebuttons.com/images/somacro/linkedin.png" alt="LinkedIn" />';
				$html .='<img src="'.$e_image.'" alt="LinkedIn" />';
				$html .='</a>';
			//} else {
			//	$html .= '' ;
			//}
			
			
			$html .= '</div>' ;
			$html .= '<div style="clear:both;"></div>' ;
			
			return $html;
		 
	}
}
}


?>
