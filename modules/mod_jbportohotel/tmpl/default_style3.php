<?php
/**
 * @package     JB Porto Hotel 
 * @subpackage  mod_jbportohotel
 *
 * @copyright   Copyright (C) 2010 - 2015 Joombuff. All rights reserved.
 * @license     GNU General Public License version 2 or later.
 */
defined ( '_JEXEC' ) or die ();
jimport ( 'joomla.filesystem.file' );
//$doc = JFactory::getDocument (); 
//$doc->addStylesheet ( JURI::root ( true ) . '/modules/mod_jbportohotel/theme-elements.css' );
// Params
$cparams = JComponentHelper::getParams ( 'com_jbportohotel' );
?>

<?php  $slider =  $params->get('enable_slider',false); 
 // owl-carousel owl-theme ?>
 
<div  class="jb-smartportfolio jb-portohotel-view-items <?php echo $moduleclass_sfx; ?>">

	<div class="image-gallery sort-destination full-width1 mb-none1 owl-carousel" id="portfolio-style3-<?php echo $module->id;?>">
					
					<?php 
									foreach ( $items as $item ) :
								  $item->url = JRoute::_('index.php?option=com_jbportohotel&view=item&id='.$item->jbportohotel_item_id.':'.$item->alias. ModjbportohotelHelper::getItemid());
									$item->category_title = ModjbportohotelHelper::getCategoryTitle($item->category_id);
									//	print_r($item);
									
									?>
								  
								  
					  <div>
					<div class="isotope-item">
						<div class="image-gallery-item mb-none">
							<a href="<?php echo $item->url;?>">
								<span class="thumb-info thumb-info-centered-info thumb-info-no-borders">
									<span class="thumb-info-wrapper">
											<img src="<?php echo JUri::root().'/'.$item->image;?>" class="img-responsive" alt="" />
										<span class="thumb-info-title">
											<span class="thumb-info-inner"><?php echo $item->title;?></span>
											<span class="thumb-info-type"><?php  echo $item->category_title; ?></span>
										</span>
										<span class="thumb-info-action">
											<span class="thumb-info-action-icon"><i class="fa fa-link"></i></span>
										</span>
									</span>
								</span>
							</a>
						</div>
					</div>  
						</div>
						<?php endforeach;?>
 </div>

 
 
</div>
<script type="text/javascript">
var $ = jQuery.noConflict();
 
        jQuery("#portfolio-style3-<?php echo $module->id;?>").owlCarousel({
            items:<?php echo $no_of_items;?>,
            nav:<?php echo $slider_navigation;?>,
            dots :<?php echo $slider_pagination;?>,
            autoplay : <?php echo $autoplay;?>
            
          });
 </script>