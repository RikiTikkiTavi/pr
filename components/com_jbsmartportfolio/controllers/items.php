<?php
/**
 * @package     JB Smart Portfolio
 *
 * @copyright   Copyright (C) 2016 Joomlabuff. All rights reserved.
 * @license     GNU General Public License version 2 or later.
 */
defined ( '_JEXEC' ) or die ();
class JbsmartportfolioControllerItems extends F0FController {
	function likeMe() {
		// Check for request forgeries.
		JSession::checkToken () or jexit ( JText::_ ( 'JINVALID_TOKEN' ) );	
		$app = JFactory::getApplication ();		
		$count = $app->input->getInt ( 'user_rating' );
		$pitemId = $app->input->getInt ( 'id' );
		$url = $app->input->getString ( 'url', '' );
		$id = $app->input->getInt ( 'id', 0 );		
		$model = F0FModel::getTmpInstance ( 'Items', 'JbsmartportfolioModel' );
		
		if ($model->storeVote ( $id, $count )) {
			$this->setRedirect ( $url, JText::_ ( 'COM_JBSMARTPORTFOLIO_ITEM_VOTE_SUCCESS' ) );
		} else {
			$this->setRedirect ( $url, JText::_ ( 'COM_JBSMARTPORTFOLIO_ITEM_VOTE_FAILURE' ) );
		}
	}
	public function onBeforeBrowse() {
		$app = JFactory::getApplication ();
		$params = $app->getParams ();
		$menu = JFactory::getApplication ()->getMenu ();
		$active = $menu->getActive ();
		$menu_params = $menu->getParams ( $active->id );
		$this->getThisModel ()->limit ( $params->get ( 'limit', 12 ) );
		$this->getThisModel ()->limitstart ( $this->input->getInt ( 'limitstart', 0 ) );
		return true;
	}
}
