<?php
/**
 * @package     ${NAMESPACE}
 * @subpackage
 *
 * @copyright   A copyright
 * @license     A "Slug" license name e.g. GPL2
 */

defined ('_JEXEC') or die;

class PlgContentExtrafields extends JPlugin
{
	/**
	 * Load the language file on instantiation.
	 * Note this is only available in Joomla 3.1 and higher.
	 * If you want to support 3.0 series you must override the constructor
	 *
	 * @var boolean
	 * @since <your version>
	 */
	protected $autoloadLanguage = true;

	/**
	 * Prepare form and add my field.
	 *
	 * @param   JForm  $form  The form to be altered.
	 * @param   mixed  $data  The associated data for the form.
	 *
	 * @return  boolean
	 *
	 * @since   <your version>
	 */
	function onContentPrepareForm($form, $data)
	{
		$app    = JFactory::getApplication();
		$option = $app->input->get('option');

		switch($option)
		{
			case 'com_content' :
				if ($app->isAdmin())
				{
					JForm::addFormPath(__DIR__ . '/extrafields');
					$form->loadFile('extrafields', false);
				}

				return true;
		}

		return true;
	}
}
?>