<?php
/**
 * @package     JB Smart Portfolio
 *
 * @copyright   Copyright (C) 2016 Joomlabuff. All rights reserved.
 * @license     GNU General Public License version 2 or later.
 */

defined('_JEXEC') or die();


require_once JPATH_COMPONENT . '/helpers/helper.php';
jimport( 'joomla.filesystem.file' );
JHtml::_('jquery.framework');


$columns = $this->params->get('columns', 3);
$this->col ='col-md-6';
if($columns == 1){
	$this->col ='col-md-12';
}elseif($columns == 2){
	$this->col ='col-md-6';
}elseif($columns == 3){
	$this->col ='col-md-4';
}elseif($columns == 4){
	$this->col ='col-md-3';
}elseif($columns == 5){
	$this->col ='col-md-1-5';
}elseif($columns == 6){
	$this->col ='col-md-2';
}


$doc = JFactory::getDocument();


//Add js and css files


$doc->addScript( JURI::root(true) . '/components/com_jbsmartportfolio/assets/owl.carousel/owl.carousel.min.js' );
$doc->addStylesheet( JURI::root(true) . '/components/com_jbsmartportfolio/assets/owl.carousel/assets/owl.carousel.min.css' );
$doc->addStylesheet( JURI::root(true) . '/components/com_jbsmartportfolio/assets/owl.carousel/assets/owl.theme.default.min.css' );
$doc->addStylesheet( JURI::root(true) . '/components/com_jbsmartportfolio/assets/css/featherlight.min.css' );
$doc->addStylesheet( JURI::root(true) . '/components/com_jbsmartportfolio/assets/css/jbsmartportfolio.css' );
$doc->addScript( JURI::root(true) . '/components/com_jbsmartportfolio/assets/js/jquery.shuffle.modernizr.min.js' );
$doc->addScript(JURI::root(true) . '/components/com_jbsmartportfolio/assets/isotope/jquery.isotope.min.js');
$doc->addScript( JURI::root(true) . '/components/com_jbsmartportfolio/assets/js/featherlight.min.js' );
$doc->addScript( JURI::root(true) . '/components/com_jbsmartportfolio/assets/js/jbsmartportfolio.js' );


 
//Params
$params 	= JComponentHelper::getParams('com_jbsmartportfolio'); 



$menu 	= JFactory::getApplication()->getMenu();
$this->itemId = '';
if(is_object($menu->getActive())) {
	$active = $menu->getActive();
	$this->itemId = '&Itemid=' . $active->id;
}

$this->layout_type = $this->params->get('layout_type', 'mansory');