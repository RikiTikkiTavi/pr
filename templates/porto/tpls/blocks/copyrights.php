<?php
/**
 * @package   T3 Blank
 * @copyright Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license   GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
?>

<!-- FOOTER -->
<footer id="t3-footer" class="wrap">
	
	

<div class="jb-copyright">
		

<div class="container">
			<div class="row-remove">
				
				
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 hidden-xs center">
					<div class="<?php echo $this->getParam('t3-rmvlogo', 1) ? 'pull' : 'col' ?> copyright <?php $this->_c('footer') ?>">
						<jdoc:include type="modules" name="<?php $this->_p('footer') ?>" />
					</div>
				</div>
				
				
			</div>
	</div>
</div>
</footer>

<!-- //FOOTER -->
