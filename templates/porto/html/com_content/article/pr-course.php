<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_content
 *
 * @copyright   Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined( '_JEXEC' ) or die;

JHtml::addIncludePath( JPATH_COMPONENT . '/helpers' );
JLoader::register( 'TagsHelperRoute', JPATH_BASE . '/components/com_tags/helpers/route.php' );
// Create shortcuts to some parameters.
$params  = $this->item->params;
$item    = $this->item;
$images  = json_decode( $this->item->images );
$urls    = json_decode( $this->item->urls );
$attribs = json_decode( $this->item->attribs );
$canEdit = $params->get( 'access-edit' );
$user    = JFactory::getUser();

JHtml::_( 'behavior.caption' );

function get_articles_by_category( $cat_id ) {
	$db    =& JFactory::getDBO();
	$query = $db->getQuery( true );
	$query = 'SELECT * FROM `#__content` WHERE `catid`=' . $cat_id . ' AND `state`>0';
	$db->setQuery( $query );
	$res = $db->loadObjectList();

	return $res;
}

$assignedCategory  = $attribs->course_assignedCategory;
$this->courseItems = get_articles_by_category( $assignedCategory );
function display_locks( $locks ) {
	for ( $l = 0; $l < $locks; $l ++ )
	{
		echo "<i class=\"fa fa-lock\" aria-hidden=\"true\"></i>";
	}
	for ( $o_l = 0; $o_l < 5 - $locks; $o_l ++ )
	{
		echo "<i class=\"fa fa-lock lock-light\" aria-hidden=\"true\"></i>";
	}
}

$document = &JFactory::getDocument();
$renderer = $document->loadRenderer('module');

$Module = &JModuleHelper::getModule('mod_unite_revolution2');

$Params = "sliderid=$attribs->sliderid";    //This will only required when you pass module params through code
$Module->params = $Params;

$Module_breadcrumbs = &JModuleHelper::getModule('mod_breadcrumbs');

?>

<div class="wrap jb-slideshow">
    <?=$renderer->render($Module);?>
</div>

<div class="wrap t3-sl-1 jb-breadcrumb page-header masthead">
    <div class="container">
        <?=$renderer->render($Module_breadcrumbs);?>
    </div>
</div>

<div class="course-blocks">
    <div class="container">

        <!--Line0 Menu-->
        <div id="pr-course-line0section" class="row">
            <div class="col-md-12">
                <ul class="nav nav-pills">
                    <li class=""><a data-scrol href="#pr-course-line1section">Об авторе</a></li>
                    <li class=""><a data-scrol href="#pr-course-line3section">О курсе</a></li>
                    <li class=""><a data-scrol href="#pr-course-line4section">Содержание курса</a></li>
                    <li class=""><a data-scrol href="#pr-course-line5section">Бонус</a></li>
                    <li class=""><a data-scrol href="#pr-course-line5section">Гарантии</a></li>
                    <li class=""><a data-scrol href="#pr-course-line55section">Отзывы</a></li>
                    <li class=""><a data-scrol href="#pr-course-block-Questions">Частые вопросы</a></li>
                </ul>
            </div>
        </div>

        <script src="/templates/porto/js/smooth_scroll.min.js" type="text/javascript"></script>
        <script>
            var scroll = new SmoothScroll('a[href*="#"]');
        </script>

        <!--Line1 1 HEADING SECTION-->
        <div id="pr-course-line1section" class="row">
            <div class="col-md-12">
                <h2><?= $attribs->course_line1_heading1 ?></h2>
            </div>
        </div>

        <!--Line2 1 VIDEO-->
        <div id="pr-course-line2section" class="row">
            <div class="col-md-12">
                <iframe width="100%" height="400" src="<?= $attribs->course_line2_video ?>" frameborder="0"
                        allow="autoplay; encrypted-media" allowfullscreen></iframe>
            </div>
        </div>

        <!--Line3 4 sections-->
        <div id="pr-course-line3section" class="row">
            <div class="col-md-3 text-center">
                <i class="fa fa-5x <?= $attribs->course_line3_icon1 ?>" aria-hidden="true"></i>
                <br>
				<?= $attribs->course_line3_text1 ?>
            </div>
            <div class="col-md-3 text-center">
                <i class="fa fa-5x <?= $attribs->course_line3_icon2 ?>" aria-hidden="true"></i>
                <br>
				<?= $attribs->course_line3_text2 ?>
            </div>
            <div class="col-md-3 text-center">
                <i class="fa fa-5x <?= $attribs->course_line3_icon3 ?>" aria-hidden="true"></i>
                <br>
				<?= $attribs->course_line3_text3 ?>
            </div>
            <div class="col-md-3 text-center">
                <i class="fa fa-5x <?= $attribs->course_line3_icon4 ?>" aria-hidden="true"></i>
                <br>
				<?= $attribs->course_line3_text4 ?>
            </div>
        </div>

        <!--Line4 1 section about-->
        <div id="pr-course-line4section">
			<?php $foo = 0;
			if ( isset( $attribs->course_line4_block ) )
			{
				foreach ( $attribs->course_line4_block as $line4_block )
				{
					?>
                    <div class="row course_line4_block-row">
                        <div class="pr-course-block"
                             style="background-color: <?= $line4_block->course_line4_block_bgcolor ?>;">
							<?php if ( $foo == 0 ) { ?>
                                <h3 class="text-center"><?= $line4_block->course_line4_block_heading ?></h3>
							<?php }
							$foo += 1; ?>
							<?php if ( $line4_block->course_line4_block_layout == "left" ): ?>
                                <div class="col-md-4">
                                    <img class="img-responsive center-block"
                                         src="/<?= $line4_block->course_line4_block_image ?>">
                                </div>
							<?php endif; ?>
                            <div class="col-md-8">
                                <div class="row">
                                    <div class="col-md-2 col-xs-2 vertical-align">
                                        <i class="fa fa-4x <?= $line4_block->course_line4_block_icons ?>"
                                           aria-hidden="true"></i>
                                    </div>
                                    <div class="col-md-10 col-xs-10">
                                        <p class="pr-course-block-answer">
											<?= $line4_block->course_line4_block_answer1 ?>
                                        </p>
                                        <p class="pr-course-block-question"
                                           style="color: <?= $line4_block->course_line4_block_questioncolor ?>;">
											<?= $line4_block->course_line4_block_question1 ?>
                                        </p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-2 col-xs-2 vertical-align">
                                        <i class="fa fa-4x <?= $line4_block->course_line4_block_icons ?>"
                                           aria-hidden="true"></i>
                                    </div>
                                    <div class="col-md-10 col-xs-10">
                                        <p class="pr-course-block-answer">
											<?= $line4_block->course_line4_block_answer2 ?>
                                        </p>
                                        <p class="pr-course-block-question"
                                           style="color: <?= $line4_block->course_line4_block_questioncolor ?>;">
											<?= $line4_block->course_line4_block_question2 ?>
                                        </p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-2 col-xs-2 vertical-align">
                                        <i class="fa fa-4x <?= $line4_block->course_line4_block_icons ?>"
                                           aria-hidden="true"></i>
                                    </div>
                                    <div class="col-md-10 col-xs-10">
                                        <p class="pr-course-block-answer">
											<?= $line4_block->course_line4_block_answer3 ?>
                                        </p>
                                        <p class="pr-course-block-question"
                                           style="color: <?= $line4_block->course_line4_block_questioncolor ?>;">
											<?= $line4_block->course_line4_block_question3 ?>
                                        </p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-2 col-xs-2 vertical-align">
                                        <i class="fa fa-4x <?= $line4_block->course_line4_block_icons ?>"
                                           aria-hidden="true"></i>
                                    </div>
                                    <div class="col-md-10 col-xs-10">
                                        <p class="pr-course-block-answer">
											<?= $line4_block->course_line4_block_answer4 ?>
                                        </p>
                                        <p class="pr-course-block-question"
                                           style="color: <?= $line4_block->course_line4_block_questioncolor ?>;">
											<?= $line4_block->course_line4_block_question4 ?>
                                        </p>
                                    </div>
                                </div>
                            </div>
							<?php if ( $line4_block->course_line4_block_layout == "right" ): ?>
                                <div class="col-md-4">
                                    <img class="img-responsive center-block"
                                         src="/<?= $line4_block->course_line4_block_image ?>">
                                </div>
							<?php endif; ?>
                        </div>
                    </div>
					<?php
				}
			}
			?>
        </div>
    </div>
    <div class="container-fluid">

        <!--Line 5 GET THE COURSE BUTTON-->
        <div id="pr-course-line5section" class="row course_line5-row">
            <div class="pr-course-line5-getTheCourseButtonBlock">
                <div class="col-md-12 text-center">
                    <a class="btn btn-success btn-lg"
                       href="<?= $attribs->course_line5_link ?>"><?= $attribs->course_line5_text ?></a>
                </div>
            </div>
        </div>

    </div>

	<?php if ( $attribs->course_line55_display == "1" ): ?>
        <div class="container">
            <!--Line 5.5 video review-->
            <div id="pr-course-line55section" class="row course_line55-row">
                <div class="col-md-12">
                    <h3><?= $attribs->course_line55_heading ?></h3>
                </div>
				<?php
				if ( isset( $attribs->course_line55_block ) )
				{
					$review_quantity = count( (array)$attribs->course_line55_block );
					if ($review_quantity==0) $review_quantity++;
					$col_width       = 12 / $review_quantity;
					foreach ( $attribs->course_line55_block as $line55_block )
					{
						?>
                        <div class="col-md-<?=$col_width?>">
							<?= $line55_block->course_line55_block_video ?>
                        </div>
						<?php
					}
				} ?>
            </div>
        </div>
	<?php endif; ?>

</div>


<div class="row category-articles pr-course-category-articles">
    <div class="container-fluid">

		<?php foreach ( $this->courseItems as $key => &$courseItem ) :


			$this->courseItem = &$courseItem;

			$attribs = json_decode( $courseItem->attribs );

			$article_icon  = $attribs->articleShortStory_icon;
			$article_locks = $attribs->articleShortStory_locks;
			$article_group = $attribs->articleShortStory_group;

			$images = json_decode( $item->images );
			$link   = JRoute::_( ContentHelperRoute::getCategoryRoute( $this->item->catslug ) ) . '/' . $courseItem->id;

			?>


            <div class="row category-article-row">
                <div class="container vertical-align">
                    <div class="col-md-4 col-sm-4 pr-news-card-col">
                        <a class="pr-news-card-link" href="<?= $link ?>">
                            <div class="pr-news-card"
                                 style="background-image: url('/<?= $images->image_intro ?>');">
                                <div class="pr-news-card-top">
                                </div>
                                <div class="pr-news-card-bottom">
                                    <div class="row">
                                        <div class="col-sm-4 col-xs-4 pr-news-card-icon">
                                            <i class="fa fa-2x <?= $article_icon ?>" aria-hidden="true"></i>
                                        </div>
                                        <div class="col-md-8 col-xs-8 pr-news-card-description">
                                            <div class="row pr-news-card-heading"><?= $courseItem->title; ?></div>
                                            <div class="row pr-news-card-params">
                                                <div class="pr-news-card-params-lock">
													<?php display_locks( $article_locks ) ?>
                                                </div>
                                                <div class="pr-news-card-params-group">
                                                    <i class="fa fa-users"
                                                       aria-hidden="true"></i> <?= $article_group; ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-8 col-sm-8 col-xs-8">
                        <div class="pr-news-description ">
                            <h4><?= $courseItem->title; ?></h4>
                            <p>
								<?= $courseItem->introtext; ?>
                            </p>
                        </div>
                    </div>
                </div>
            </div>


		<?php endforeach; ?>

    </div><!-- /post-wrap -->
</div>

<div class="course-blocks">

    <!--Line LATEST (Questions<->Answers) -->
	<?php $attribs = json_decode( $this->item->attribs ); ?>

    <div id="pr-course-block-Questions" class="container pr-course-block-Questions">
        <div class="row">
            <h3 class="text-center">Ответы на частые вопросы про "<?= $attribs->course_line8_heading ?>"</h3>
            <br>
        </div>
		<?php
		if ( isset( $attribs->course_line8_block ) )
		{
			foreach ( $attribs->course_line8_block as $line8_block )
			{ ?>
                <div class="row pr-course-block-Questions-Question-row">
                    <div class="col-md-1 col-sm-1"><i class="fa fa-4x fa-comments-o" aria-hidden="true"></i></div>
                    <div class="col-md-11 col-sm-11">
                        <h4><?= $line8_block->course_line_block_often_questions_question ?></h4> <!--heading-->
						<?= $line8_block->course_line_block_often_questions_answer ?> <!--editor-->
                    </div>
                </div>

			<?php }
		} ?>
    </div>

</div>