<?php
/**
 * @version    CVS: 1.0.0
 * @package    com_jbeasyformmaker
 * @author     Priya Bose <support@joomlabuff.com>
 * @copyright  2016 www.joomlabuff.com
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access to this file
defined ( '_JEXEC' ) or die ();

/* class JFormFieldFieldtypes extends JFormField */
class JFormFieldFormmodules extends JFormField {
	protected $type = 'formmodules';
	public function getInput() {
		$html = '';
		
		$shortcodes = $this->shortcodes ();
		foreach ( $shortcodes as $key => $shortcode ) {
			$html .= '<a onclick="jQuery(\'#sshortcode\').val(jQuery(this).data(\'shortcode-key\'));ShowFileds(jQuery(this).data(\'shortcode-key\'));" class="btn btn-default" data-toggle="modal" data-target="#myModal" data-shortcode-key="' . $key . '" >' . $shortcode . '</a>';
			$html .= '&nbsp';
		}
		
		return $html;
	}
	public function getLabel() {
		return '';
	}
	public function shortcodes() {
		return array (
				'text' => JText::_ ( 'FIELD_TEXT' ),
				'textarea' => JText::_ ( 'FIELD_TEXTAREA' ),
				'email' => JText::_ ( 'FIELD_EMAIL' ),
				'phone' => JText::_ ( 'FIELD_PHONE' ),
				'checkbox' => JText::_ ( 'FIELD_CHECKBOX' ),
				'list' => JText::_ ( 'FIELD_LIST' ),
				'radio' => JText::_ ( 'FIELD_RADIO' ),
/* 				'count' => JText::_ ( 'FIELD_COUNT' ), */
				'date' => JText::_ ( 'FIELD_DATE' ),
				'number' => JText::_ ( 'FIELD_NUMBER' ),				
				/* 'acceptence' => JText::_ ( 'FIELD_ACCEPTENCE' ), */				 
				'submit' => JText::_ ( 'FIELD_SUBMIT' ),
				/* 'response' => JText::_ ( 'FIELD_RESPONSE' )  */
		);
	}
}