<?php
/**
 * @version    CVS: 1.0.0
 * @package    com_jbeasyformmaker
 * @author     Priya Bose <support@joomlabuff.com>
 * @copyright  2016 www.joomlabuff.com
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
 
defined('_JEXEC') or die; 


class JBEasyFormmakerDispatcher  extends F0FDispatcher
{
	public $defaultView = 'cforms';
	
	 
	public function onBeforeDispatch() {
		
		
		return  parent::onBeforeDispatch();
	}
}
