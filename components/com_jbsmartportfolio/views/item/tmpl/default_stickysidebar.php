<div class="container">

					<div class="row">
						<div class="col-md-7">

							<div class="row">
							<?php $images = JbsmartportfolioHelper::getItemImages($this->item);?>	
 				 				<?php if(!empty($images)):?>
								<div class="lightbox" data-plugin-options='{"delegate": "a.lightbox-portfolio", "type": "image", "gallery": {"enabled": true}}'>
									<ul class="portfolio-list">
										 <?php foreach($images as $pimg):?>	
										<li class="col-md-12">
											<div class="portfolio-item">
												<span class="thumb-info thumb-info-centered-icons thumb-info-no-borders">
													<span class="thumb-info-wrapper">
														<img src="<?php echo $pimg;?>" class="img-responsive" alt="">
														<a href="<?php echo $pimg;?>" class="lightbox-portfolio">
															<span class="thumb-info-action">
																<span class="thumb-info-action-icon thumb-info-action-icon-light"><i class="fa fa-search-plus"></i></span>
															</span>
														</a>
													</span>
												</span>
											</div>
										</li>
										<?php endforeach;?>
										 
									</ul>
								</div>	
								<?php endif;?>
							</div>

						</div>

						<div class="col-md-5">
							<aside class="sidebar" id="sidebar" data-plugin-sticky data-plugin-options='{"minWidth": 991, "contentSelector": ".container", "padding": {"top": 110}}'>
								<div class="inside">
								<div class="portfolio-info pull-none">
									<?php echo $this->loadTemplate('niconlist');?>
								</div>

								<h5 class="mt-lg mb-sm"><?php echo JText::_('JBSMARTPORTFOLO_PROJECT_DESCRIPTION');?></h5>

								<p class="mt-none mb-xlg">
									<?php echo $this->item->description; ?>
								</p>
								<?php if($this->item->url):?>
								<a href="<?php echo $this->item->url;?>" class="btn btn-primary btn-icon"> <i class="fa fa-external-link"></i>
									<?php echo JText::_('JBSMARTPORTFOLIO_LIVE_PREVIEW');?>
								</a>
								<?php endif;?>
								<span class="arrow hlb appear-animation" data-appear-animation="rotateInUpLeft" data-appear-animation-delay="800"></span>
						
								<ul class="portfolio-details">
									<?php if($this->params->get('show_skills',0)):?>
									<li>
										<?php echo $this->loadTemplate('skills');?>
									</li>
									<?php endif;?>										
								</ul>
								
								<?php if($this->params->get('show_clientinfo',0) && $this->item->client):?>
									<ul class="portfolio-details mt-none mb-xl">
										<li>
											<?php echo $this->loadTemplate('client');?>				
										</li>
									</ul>
								<?php endif;?>	
								
								<?php if($this->params->get('show_social_share',0)):?>
									<h5 class="m-t-md"><?php echo JText::_('JBSMARTPORTFOLIO_ITEM_SHARE')?></h5>
									<?php echo $this->item->aftercontentDisplay; ?>		
								<?php endif; ?>
								</div>
							</aside>
						</div>
					</div>

				</div>

			</div>
		
		 