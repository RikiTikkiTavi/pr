<?php
/**
 * @package     JB Porto Hotel 
 *
 * @copyright   Copyright (C) 2016 Joomlabuff. All rights reserved.
 * @license     GNU General Public License version 2 or later.
 */

defined('_JEXEC') or die();

require_once JPATH_COMPONENT . '/helpers/helper.php';
jimport( 'joomla.filesystem.file' );
$layout_type = $this->params->get('layout_type', 'old_default');


//Load the method jquery script.
JHtml::_('jquery.framework');

//Params
$params 	= JComponentHelper::getParams('com_jbportohotel');
$square 	= strtolower( $params->get('square', '600x600') );
$rectangle 	= strtolower( $params->get('rectangle', '600x400') );
$tower 		= strtolower( $params->get('tower', '600x800') );

//Add js and css files
$doc = JFactory::getDocument();
$doc->addStylesheet( JURI::root(true) . '/components/com_jbportohotel/assets/css/featherlight.min.css' );
$doc->addStylesheet( JURI::root(true) . '/components/com_jbportohotel/assets/css/jbportohotel.css' );
$doc->addScript( JURI::root(true) . '/components/com_jbportohotel/assets/js/jquery.shuffle.modernizr.min.js' );
$doc->addScript( JURI::root(true) . '/components/com_jbportohotel/assets/js/featherlight.min.js' );
$doc->addScript( JURI::root(true) . '/components/com_jbportohotel/assets/js/jbportohotel.js' );

$menu 	= JFactory::getApplication()->getMenu();
$itemId = '';
if(is_object($menu->getActive())) {
	$active = $menu->getActive();
	$itemId = '&Itemid=' . $active->id;
}

if( $this->params->get('show_page_heading') && $this->params->get( 'page_heading' ) ) {
	echo "<h1 class='page-header'>" . $this->params->get( 'page_heading' ) . "</h1>";
}

$i = 0;
//Sizes
$sizes = array(
	$rectangle,
	$tower,
	$square,

	$tower,
	$rectangle,
	$square,

	$square,
	$rectangle,
	$tower,

	$square,
	$tower,
	$rectangle
	);

?>

<div id="jb-smartportfolio" class="jb-smartportfolio jb-portohotel-view-items layout-<?php echo str_replace('_', '-', $layout_type); ?>">


	<?php if($this->params->get('show_filter', 1)) { ?>
		<div class="jb-portohotel-filter">
			<ul>
				<li class="active" data-group="all"><a href="#"><?php echo JText::_('COM_JBPORTOHOTEL_SHOW_ALL'); ?></a></li>
				<?php
					$filters = jbportohotelHelper::getTagList( $this->items );
					foreach ($filters as $filter) {
						?>
							<li data-group="<?php echo $filter->alias; ?>"><a href="#"><?php echo $filter->title; ?></a></li>
						<?php
					}	
				?>
			</ul>
		</div>
	<?php } ?>

	<?php
		//Videos
		foreach ($this->items as $key => $this->item) {

			if($this->item->video) {
				$video = parse_url($this->item->video);

				switch($video['host']) {
					case 'youtu.be':
					$video_id 	= trim($video['path'],'/');
					$video_src 	= '//www.youtube.com/embed/' . $video_id;
					break;

					case 'www.youtube.com':
					case 'youtube.com':
					parse_str($video['query'], $query);
					$video_id 	= $query['v'];
					$video_src 	= '//www.youtube.com/embed/' . $video_id;
					break;

					case 'vimeo.com':
					case 'www.vimeo.com':
					$video_id 	= trim($video['path'],'/');
					$video_src 	= "//player.vimeo.com/video/" . $video_id;
				}

				echo '<iframe class="jb-portohotel-lightbox" src="'. $video_src .'" width="500" height="281" id="jb-portohotel-video'.$this->item->jbportohotel_item_id.'" style="border:none;" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>';
			}
		}
	?>

	<div class="jb-portohotel-items jb-portohotel-columns-<?php echo $this->params->get('columns', 3); ?>">
		<?php foreach ($this->items as $this->item) { ?>
			
			<?php
			$tags = jbportohotelHelper::getTags( $this->item->jbportohotel_tag_id );
			$newtags = array();
			$filter = '';
			$groups = array();
			foreach ($tags as $tag) {
				$newtags[] 	 = $tag->title;
				$filter 	.= ' ' . $tag->alias;
				$groups[] 	.= '"' . $tag->alias . '"';
			}

			$groups = implode(',', $groups);

			?>

			<div class="jb-portohotel-item" data-groups='[<?php echo $groups; ?>]'>
				<?php $this->item->url = JRoute::_('index.php?option=com_jbportohotel&view=item&id='.$this->item->jbportohotel_item_id.':'.$this->item->alias . $itemId); ?>
				
				<div class="jb-portohotel-overlay-wrapper clearfix">
					
					<?php if($this->item->video) { ?>
						<span class="jb-portohotel-icon-video"></span>
					<?php } ?>

					<?php if($this->params->get('thumbnail_type', 'masonry') == 'masonry') { ?>
						<img class="jb-portohotel-img" src="<?php echo JURI::base(true) . '/images/jbportohotel/' . $this->item->alias . '/' . JFile::stripExt(JFile::getName($this->item->image)) . '_' . $sizes[$i] . '.' . JFile::getExt($this->item->image); ?>" alt="<?php echo $this->item->title; ?>">
					<?php } else if($this->params->get('thumbnail_type', 'masonry') == 'rectangular') { ?>
						<img class="jb-portohotel-img" src="<?php echo JURI::base(true) . '/images/jbportohotel/' . $this->item->alias . '/' . JFile::stripExt(JFile::getName($this->item->image)) . '_'. $rectangle .'.' . JFile::getExt($this->item->image); ?>" alt="<?php echo $this->item->title; ?>">
					<?php } else { ?>
						<img class="jb-portohotel-img" src="<?php echo JURI::base(true) . '/images/jbportohotel/' . $this->item->alias . '/' . JFile::stripExt(JFile::getName($this->item->image)) . '_'. $square .'.' . JFile::getExt($this->item->image); ?>" alt="<?php echo $this->item->title; ?>">
					<?php } ?>

					<div class="jb-portohotel-overlay">
						<div class="sp-vertical-middle">
							<div>
								<div class="jb-portohotel-btns">
									<?php if( $this->item->video ) { ?>
										<a class="btn-zoom" href="#" data-featherlight="#jb-portohotel-video<?php echo $this->item->jbportohotel_item_id; ?>"><?php echo JText::_('COM_JBPORTOHOTEL_WATCH'); ?></a>
									<?php } else { ?>
										<a class="btn-zoom" href="<?php echo JURI::base(true) . '/images/jbportohotel/' . $this->item->alias . '/' . JFile::stripExt(JFile::getName($this->item->image)) . '_'. $rectangle .'.' . JFile::getExt($this->item->image); ?>" data-featherlight="image"><?php echo JText::_('COM_JBPORTOHOTEL_ZOOM'); ?></a>
									<?php } ?>
									<a class="btn-view" href="<?php echo $this->item->url; ?>"><?php echo JText::_('COM_JBPORTOHOTEL_VIEW'); ?></a>
								</div>
								<?php if($layout_type!='old_default') { ?>
								<h3 class="jb-portohotel-title">
									<a href="<?php echo $this->item->url; ?>">
										<?php echo $this->item->title; ?>
									</a>
								</h3>
								<div class="jb-portohotel-tags">
									<?php echo implode(', ', $newtags); ?>
								</div>
								<?php } ?>
							</div>
						</div>
					</div>
				</div>
				
				<?php if($layout_type=='old_default') { ?>
					<div class="jb-portohotel-info">
						<h3 class="jb-portohotel-title">
							<a href="<?php echo $this->item->url; ?>">
								<?php echo $this->item->title; ?>
							</a>
						</h3>
						<div class="jb-portohotel-tags">
							<?php echo implode(', ', $newtags); ?>
						</div>
					</div>
				<?php } ?>

			</div>
			
			<?php
			$i++;
			if($i==11) {
				$i = 0;
			}
			?>

		<?php } ?>
	</div>

	<?php if ($this->pagination->get('pages.total') >1) { ?>
	<div class="pagination">
		<?php echo $this->pagination->getPagesLinks(); ?>
	</div>
	<?php } ?>
</div>




