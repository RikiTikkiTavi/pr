<?php 

?> 
<div class="row">
	<div class="col-md-12"> 
		 <?php if($this->item->item_type =='slideshow'):?>
			 <?php $images = JbsmartportfolioHelper::getItemImages($this->item);?>
			<div class="owl-carousel owl-theme full-width owl-loaded owl-drag owl-carousel-init m-none mb-lg" data-plugin-options='{"items": 1, "loop": true, "nav": true, "dots": false, "animateOut": "fadeOut"}'>
			<?php foreach($images as $img):?>
				<div>
				<img class="jb-smartportfolio-img img-responsive" src="<?php echo $img;?>" alt="<?php echo $item->title; ?>" />
				</div>
			<?php endforeach;?>
			</span>
		 <?php elseif($this->item->video): ?>
			 <div class="jb-smartportfolio-embed">
				<iframe src="<?php echo $this->video_src; ?>" frameborder="0" 	webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>
			</div>	
		<?php else:?>
			<span class="img-thumbnail">
				<img class="jb-smartportfolio-img img-responsive" src="<?php echo $this->item->image; ?>" alt="<?php echo $this->item->title; ?>" />
			</span>			
		<?php endif;?> 
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="portfolio-info pull-left">
			<div class="row">
				<div class="col-md-12 center">
					<ul class="item-fav-icons-list">
						<?php if($this->params->get('show_like_count',0)):?>
						<li>
							<?php echo $this->item->like_count_html;?>
							<!-- <a href="#" data-tooltip data-original-title="Like"><i class="fa fa-heart"></i></a> -->
						</li>
						<?php endif;?>
						<?php if($this->params->get('show_created_date',0)):?>
						<li><i class="fa fa-calendar"></i>
							<?php echo JHtml::_('date', $this->item->created_on, JText::_('DATE_FORMAT_LC3')); ?>
						</li>
						 <?php endif;?>
						
						<?php if($this->params->get('show_tags',0) && !empty($this->newtags)):?>
						<li><i class="fa fa-tags"></i>
							<?php echo implode(', ', $this->newtags); ?>
						</li>
						<?php endif;?>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="row">	
	<div class="col-md-7">
		
		<h5 class="mt-lg mb-sm"><?php echo JText::_('JBSMART_PORTFOLO_PROJECT_DESCRIPTION');?></h5>
		
		<p class="mt-none mb-xlg"><?php echo $this->item->description; ?></p>
		<?php if($this->item->url):?>
		<a href="<?php echo $this->item->url;?>" class="btn btn-primary btn-icon">
			<i class="fa fa-external-link"></i>
		 	<?php echo JText::_('JBSMARTPORTFOLIO_LIVE_PREVIEW');?>
		 </a>
		 <?php endif;?>
		 <span class="arrow hlb appear-animation" data-appear-animation="rotateInUpLeft" data-appear-animation-delay="800"></span>
		
		 <?php if(!empty($this->item->jbsmartportfolio_skill_id)):?>
		 <?php $skills  = JbsmartportfolioHelper::getSkills($this->item->jbsmartportfolio_skill_id);?>
		  <ul class="portfolio-details">
			<li>
				<h5 class="mt-sm mb-xs"><?php echo JText::_('JBSMARTPORTFOLIO_SKILLS');?></h5>
				<ul class="list list-inline list-icons">								
					<?php foreach($skills as $skill):?>
					<li><i class="fa fa-check-circle"></i><?php echo $skill->title;?></li>
					<?php endforeach;?>														 
				</ul>
			</li>				
		</ul>
		<?php endif;?>	
		 
	</div>
	<div class="col-md-5">		
		<?php if($this->params->get('show_clientinfo',0) && $this->item->client):?>		
		<ul class="portfolio-details mt-none mb-xl">
			<li>
		
		<h5 class="mt-sm mb-xs"><?php echo JText::_('JBSMARTPORTFOLIO_CLIENT');?></h5>
				<?php if($this->item->client !==''):?>
				<p>
					<?php echo $this->item->client;?>
						<a href="<?php echo $this->item->client_url;?>" target="_blank">
							<i class="fa fa-external-link"></i>
							<?php echo $this->item->client_url;?>
						</a>
				</p>
				<?php endif;?>
 		
 			</li>
 		</ul>
 		<?php endif;?>
 		
 		<?php if($this->params->get('show_testimonial',0)):?>
		<div class="testimonial testimonial-style-4">
			<?php if($this->item->author_quote !==''):?>
			<blockquote>
				<p><?php echo $this->item->author_quote;?></p>
			</blockquote>
			<?php endif;?>
			<div class="testimonial-arrow-down"></div>
			<div class="testimonial-author">
				<?php if($this->item->author_image !==''):?>
				<div class="testimonial-author-thumbnail">
					<img alt="" class="img-responsive img-circle" src="<?php echo $this->item->author_image;?>" />
				</div>
				<?php endif;?>
				
				<p>
					<?php if($this->item->author_name !==''):?>
					<strong><?php echo $this->item->author_name;?></strong> 
					<?php endif;?>
					<span><?php echo ($this->item->author_role) ? $this->item->author_role :'' ;?> - <?php echo ($this->item->author_company) ? $this->item->author_company : "";?> </span>
				</p>
			</div>
		</div>
		<?php endif;?>
	 </div>	 
</div>
<?php echo $this->loadTemplate('relateditems');?>		