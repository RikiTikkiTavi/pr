<?php
/**
 * @package     JB Porto Hotel 
 *
 * @copyright   Copyright (C) 2016 Joomlabuff. All rights reserved.
 * @license     GNU General Public License version 2 or later.
 */

defined('_JEXEC') or die();

class JbportohotelToolbar extends F0FToolbar{

	function onBrowse(){
		JToolBarHelper::preferences('com_jbportohotel');

		$categories_view = 'index.php?option=com_categories&view=categories&extension=com_jbportohotel';
		JHtmlSidebar::addEntry(JText::_('COM_JBPORTOHOTEL_CATEGORIES'), $categories_view, 'categories');

		parent::onBrowse();
	}
}