<?php
/**
 * @package     JB Smart Portfolio
 *
 * @copyright   Copyright (C) 2016 Joomlabuff. All rights reserved.
 * @license     GNU General Public License version 2 or later.
 */

defined('_JEXEC') or die();

JLoader::register('JHtmlString', JPATH_LIBRARIES.'/joomla/html/html/string.php');

class JbsmartportfolioHelper {

	public static function generateMeta($item) {
		$document = JFactory::getDocument();
		$app = JFactory::getApplication();
		$menus = $app->getMenu();
		$menu = $menus->getActive();
		$title = null;

		$document->setTitle($item->title);
		$document->addCustomTag('<meta content="website" property="og:type"/>');
		$document->addCustomTag('<meta content="'.JURI::current().'" property="og:url" />');
		$document->setDescription( JHtml::_('string.truncate', $item->description, 155, false, false ) );
		$document->addCustomTag('<meta content="'. $item->title .'" property="og:title" />');
		$document->addCustomTag('<meta content="'. JURI::root().$item->image.'" property="og:image" />');
		$document->addCustomTag('<meta content="'. JHtml::_('string.truncate', $item->description, 155, false, false ) .'" property="og:description" />');
	
		return true;
	}
	
/* 	
	public static function getItemRating($id) {
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
	
		$query->select('*')
		->from($db->quoteName('#__jbsmartportfolio_rating'))
		->where($db->quoteName('item_id') . ' = ' . (int) $id);
		
		// Set the query and load the result.
		$db->setQuery($query);		 
		return $rating = $db->loadObject();	
	}
	 */

	public static function getTags($ids) {
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);

		if(!is_array($ids)) {
			$ids = (array) json_decode($ids);
		}

		$ids = implode(',', $ids);	
		

		$query->select($db->quoteName(array('jbsmartportfolio_tag_id', 'title', 'alias')));
		$query->from($db->quoteName('#__jbsmartportfolio_tags'));
		$query->where($db->quoteName('jbsmartportfolio_tag_id')." IN (" .$ids . ")");
	 	$query->where('language in (' . $db->quote(JFactory::getLanguage()->getTag()) . ',' . $db->quote('*') . ')');
		
		$db->setQuery($query);

		return $db->loadObjectList();
	}


	public static function getTagList($items) {
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);

		$tags = array();

		foreach ($items as $item) {
			$itemtags = json_decode( $item->jbsmartportfolio_tag_id );
			foreach ($itemtags as $itemtag) {
				$tags[] = $itemtag;
			}
		}

		$json = json_encode(array_unique($tags));

		$result = self::getTags( $json );

		return $result;
	}
	
	/**
	 * Method to get category title
	 * @param int $id
	 */
	public static function getCategoryTitle($id) {
		$db = JFactory::getDbo ();
		$query = $db->getQuery ( true );
		$query->select ( 'title' )->from ( '#__categories' )->where ( 'id=' . $db->q ( $id ) );
		$query->where ( 'extension ="com_jbsmartportfolio"' );
		$db->setQuery ( $query );
		return $category_title = $db->loadResult ();
	}
	
	
	public static function getItemImages($item) {
		 $images = array();	 
		
		 $image_path = JURI::root();
		 
		 if($item->image !==''){
		 	$images[] = $image_path.'/'.$item->image;
		 }
		 
		 
		 if($item->alternative_img_1 !=='' ){
		 	$images[] = $image_path.'/'.$item->alternative_img_1;
		 }
		 	
		 /* if($item->alternative_img_1 !=='' && JFile::exists($image_path.'/'.$item->alternative_img_2) ){
		  $images[] = $item->alternative_img_1;
		  } */
		 
		 
		 if($item->alternative_img_2 !=='' ){
		 	$images[] =$image_path.'/'.$item->alternative_img_2;
		 }
		 
		 
		 if($item->alternative_img_3 !=='' ){
		 	$images[] = $image_path.'/'.$item->alternative_img_3;
		 }
		 
		 
		 if($item->alternative_img_4 !==''  ){
		 	$images[] = $image_path.'/'.$item->alternative_img_4;
		 }
		 
		 if($item->alternative_img_5 !==''  ){
		 	$images[] = $image_path.'/'.$item->alternative_img_5;
		 }
		 	
		 if($item->alternative_img_6!==''  ){
		 	$images[] = $image_path.'/'.$item->alternative_img_6;
		 }
		 	
		 if($item->alternative_img_7 !==''  ){
		 	$images[] = $image_path.'/'.$item->alternative_img_7;
		 }
		 	
		 if($item->alternative_img_8 !==''  ){
		 	$images[] = $image_path.'/'.$item->alternative_img_8;
		 } 
		 
		return  $images;
		
	}
	
	public static function getRelatedItems($item){ 
		
		if(empty($item->related_items)) return;
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$related_items_ids = array();
	 	if(!is_array($item->related_items)) {
			$related_items_ids = (array) json_decode($item->related_items);
	 	}
		 
		$related_items_ids = implode(',', $item->related_items);	 
		$query->select('*');
		$query->from($db->quoteName('#__jbsmartportfolio_items'));
		$query->where($db->quoteName('jbsmartportfolio_item_id')." IN (" .$related_items_ids . ")");	 
		$db->setQuery($query);
		
		return $db->loadObjectList();
	}
	
	
	
	
	public static function getSkills($ids) {
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
	
		if(!is_array($ids)) {
			$ids = (array) json_decode($ids);
		}
	
		$ids = implode(',', $ids);

		$query->select($db->quoteName(array('jbsmartportfolio_skill_id', 'title')));
		$query->from($db->quoteName('#__jbsmartportfolio_skills'));
		$query->where($db->quoteName('jbsmartportfolio_skill_id')." IN (" .$ids . ")");
		
	
		$db->setQuery($query);
	
		return $db->loadObjectList();
	}
	
	public static function getItemSkills($items){
		
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		
		$tags = array();
		//print_r($item->jbsmartportfolio_skill_id); exit;
		foreach ($items as $item) {
			$itemtags = json_decode( $item->jbsmartportfolio_skill_id );
			
			//print_r($itemtags); exit;
			foreach ($itemtags as $itemtag) {
				$tags[] = $itemtag;
			}
		}
		
		$json = json_encode(array_unique($tags));
		
		$result = self::getSkills( $json );
		
		return $result;
		
	} 
}
