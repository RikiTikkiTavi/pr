<?php
/**
 * @package   T3 Blank
 * @copyright Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license   GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
?>

<!-- FOOTER -->
<footer id="t3-footer" class="wrap shop-footer fixed">
	
	

	<div class="jb-copyright">
		

<div class="container">
			<div class="row-remove">
				
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
					<?php if ($this->countModules('footer-social')) : ?>
						<div class="jb-footer-menu pull-left <?php $this->_c('footer-social') ?>">
							<jdoc:include type="modules" name="<?php $this->_p('footer-social') ?>" style="raw" />
						</div>
					<?php endif ?>
				</div>
				
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 hidden-xs">
					<div class="<?php echo $this->getParam('t3-rmvlogo', 1) ? 'pull' : 'col' ?> copyright <?php $this->_c('footer') ?>">
						<jdoc:include type="modules" name="<?php $this->_p('footer') ?>" />
					</div>
				</div>
				
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 hidden-xs">
					<?php if ($this->countModules('footer-menu')) : ?>
						<div class="jb-footer-menu <?php $this->_c('footer-menu') ?>">
							<jdoc:include type="modules" name="<?php $this->_p('footer-menu') ?>" style="raw" />
						</div>
					<?php endif ?>
				</div>
			</div>
	</div>
</div>
</footer>

<!-- //FOOTER -->
