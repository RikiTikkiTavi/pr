<?php

$verticalseparator = " vertical-separator";
$ItemidStr = '';
$Itemid = shopFunctionsF::getLastVisitedItemId();
if(!empty($Itemid)){
	$ItemidStr = '&Itemid='.$Itemid;
}


// Rating
$ratingModel = VmModel::getModel('ratings');


$showRating =1;
?>




<div class="vmgroup<?php echo $params->get ('moduleclass_sfx') ?>">
	<?php // Calculating Products Per Row
	$cellwidth 				= 'width-percent floatleft width'.floor ( 100 / $products_per_row );
	$clear 					= '';
	$BrowseTotalProducts 	= count($products);
	$col 					= 1;
	$nb 					= 1;
	$row 					= 1;	
	
	if($products_per_row == 3) {
		$cellwidth = "col-xs-12 col-sm-4 col-md-4 col-lg-4";		
	}
	if($products_per_row == 2 || $products_per_row == 4) {
		$cellwidth = "col-xs-12 col-sm-6 col-md-".(12/$products_per_row)." col-lg-".(12/$products_per_row);	
	}
	if($products_per_row == 6 || $products_per_row == 12) {
		$cellwidth = "col-xs-12 col-sm-6 col-md-4 col-lg-".(12/$products_per_row);	
	} ?>
	
	<div class="list-product listing-view vm_grid_view">
		<?php foreach ( $products as $product ) {
			//Quick View Link
			$quickview = $product->link.'&amp;tmpl=component';
			
			// Show the vertical seperator
		 
				$show_vertical_separator = $verticalseparator;
		 
			?>
			<?php 
				// Show Label Sale Or New								
				$isSaleLabel = (!empty($product->prices['discountAmount'])) ? 1 : 0;

				$pid = $product->virtuemart_product_id;
				//$isNewLabel = in_array($pid, $newIds);
			?>
			<?php // Show Products ?>
			<?php if ($col == 1 ) { ?>
				<div class="row product-row">
			<?php } ?>
			
			<div class="product-i <?php echo $cellwidth . $show_vertical_separator ?>">
				<div class="product-inner">					
					<div class="item-i">
								
						<!-- Product Image -->							
						<div class="vm-product-media-container">
							<!-- Check Product Label -->
								
							<!-- Image Block -->															
							<div class="image-block">									
								<?php
									$image = $product->images[0]->displayMediaThumb('class="browseProductImage img-responsive"', false);
									if(!empty($product->images[1])){
										$image2 = $product->images[1]->displayMediaThumb('class="browseProductImage img-responsive"', false);
										echo JHTML::_('link', $product->link.$ItemidStr,'<div class="pro-image first-image">'.$image.'</div><div class="pro-image second-image">'.$image2.'</div>',array('class'=>"double-image img-responsive",'title'=>$product->product_name));
									} else {								
										echo JHTML::_('link', $product->link.$ItemidStr,'<div class="pro-image">'.$image.'</div>',array('class'=>"single-image",'title'=>$product->product_name));
									}
								?>	
								 
								
								
							</div>
						</div>
						<!-- End Product Image -->
						
						<div class="text-block">
							<div class="text-block-inner">
							
							
								<div class="box-review">
											<div class="vm-product-rating-container">
												<?php
												$maxrating = VmConfig::get('vm_maximum_rating_scale',5);
												$rating = $ratingModel->getRatingByProduct($product->virtuemart_product_id);
												$reviews = $ratingModel->getReviewsByProduct($product->virtuemart_product_id);
												if(empty($rating->rating)) { ?>						
													<div class="ratingbox dummy" title="<?php echo vmText::_('COM_VIRTUEMART_UNRATED'); ?>" >
													</div>
												<?php } else {						
													$ratingwidth = $rating->rating * 14; ?>
													<div title=" <?php echo (vmText::_("COM_VIRTUEMART_RATING_TITLE") . round($rating->rating) . '/' . $maxrating) ?>" class="ratingbox" >
													  <div class="stars-orange" style="width:<?php echo $ratingwidth.'px'; ?>"></div>
													</div>
												<?php } ?> 
												<?php if(!empty($reviews)) {					
													$count_review = 0;
													foreach($reviews as $k=>$review) {
														$count_review ++;
													}										
												?>
												<?php } ?>								
											</div>
										</div>
										
										<!-- Product Title -->
								<h2 class="product-title"><?php echo JHtml::link ($product->link.$ItemidStr, $product->product_name); ?></h2>
							
								 
								<div class="price-rating">
									<div class="vm3pr-<?php echo $rowsHeight[$row]['price'] ?>">
										<?php echo shopFunctionsF::renderVmSubLayout('prices',array('product'=>$product,'currency'=>$currency)); ?>
										<div class="clear"></div>
									</div> 
								</div>
								<!-- Product Stock -->
								<div class="product-stock">
									<?php   if ( VmConfig::get ('display_stock', 1)):?>
								 	<p class="in-stock">
										<?php echo "<span>".vmText::_('COM_VIRTUEMART_AVAILABILITY')."</span>"; ?>
										<?php if($product->product_in_stock > 0):?>
											<?php echo vmText::_('COM_VIRTUEMART_PRODUCT_FORM_IN_STOCK');?>
										<?php else:?>
											<?php echo vmText::_('COM_VIRTUEMART_STOCK_LEVEL_OUT'); ?>
										<?php endif;?>	
									</p>				
									<?php endif;?>
								</div>	
								<!-- Product Short Description -->
								<div class="product_s_desc vm-product-descr-container-<?php echo $rowsHeight[$row]['product_s_desc'] ?>">
									<?php if(!empty($rowsHeight[$row]['product_s_desc'])): ?>						
										<?php // Product Short Description  
											echo $product->product_s_desc;
										 ?> 			
									<?php endif; ?>
								</div>
								
								  
								
								<!-- Product Actions -->					
								<div class="actions">
									<div class="btn-actions">

										<!-- Wishlist -->
										<?php
											$detail_class = ' details-class';
											if(is_dir(JPATH_BASE."/components/com_wishlist/")) {
											$app = JFactory::getApplication();
											$detail_class ='';
										?>
											<div class="btn-group btn-wishlist">							
												<?php require(JPATH_BASE . "/templates/".$app->getTemplate()."/html/wishlist.php"); ?>
											</div>
										<?php } ?>
								
										<!-- Add to cart -->
										<?php
											echo shopFunctionsF::renderVmSubLayout('addtocart',array('product'=>$product)); 
										?>
										
									</div>
								</div>
							</div>
						</div>
					</div>					
				</div>
			</div>

			<?php
			$nb ++;
			// Do we need to close the current row now?
			if ($col == $products_per_row || $nb>$BrowseTotalProducts) { ?>			
				</div>
				<?php
				$col = 1;
				$row++;
			} else {
			  $col ++;
			}
		} ?>
		<?php if ($col != 1) { ?>			
			</div>
		<?php } ?>
	</div>
</div>
