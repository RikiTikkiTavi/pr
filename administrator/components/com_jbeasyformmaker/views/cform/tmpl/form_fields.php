<?php
/**
 * @copyright	Copyright (c) 2016 Formbuilder. All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 */
// no direct access
defined ( '_JEXEC' ) or die ();
?>

<div class="modal fade bs-example-modal-sm" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
								
				<h4 class="modal-title" id="myModalLabel">
					<?php echo JText::_('COM_JBEASYFORMMAKER_TITLE_CFORMS_EDIT');?>
				</h4>
			</div>
			
			<div class="modal-body">
				<form class="shortcode-form" action="index.php"  method="POST">	
					<div class="row-fluid">
						<div class="span6">		
						
							<!-- Field submit -->
							
							<div class="control-group field-submit" style="display:none;">
								<label class="control-label"><?php echo  JText::_ ( 'FIELD_LABEL_LABEL' ) ;?></label>
								<div class="controls">
									<input type="text" class="sfield" name="value" />
								</div>
							</div>
							
							<div class="control-group field-submit" style="display:none;">
									<label class="control-label"><?php echo JText::_ ( 'FIELD_ID_ATTRIBUTE_LABEL' );?></label>
									<div class="controls">
										<input type="text" class="sfield" name="id" />
									</div>
								</div> 
								
						 	<div class="control-group field-submit" style="display:none;">
									<label class="control-label"><?php echo JText::_ ( 'FIELD_FIELD_CLASS_LABEL' ) ;?></label>
									<div class="controls">
										<input type="text" class="sfield" name="class" />
									</div>
								</div>
								
								
								<!-- - ---------------->
							
						
							<div class="control-group field-general" style="display:none;">
								<label class="control-label"><?php echo  JText::_ ( 'FIELD_LABEL_LABEL' ) ;?></label>
								<div class="controls">
									<input type="text" class="sfield" name="label" />
								</div>
							</div>
						
							<div class="control-group field-general" style="display:none;">
								<label class="control-label"><?php echo  JText::_ ( 'FIELD_NAME_LABEL' ) ;?></label>
								<div class="controls">
									<input type="text" class="sfield" name="name" />
								</div>
							</div>
							
							<div class="control-group field-textarea" style="display:none;">
								<label class="control-label"><?php echo  JText::_ ( 'FIELD_NAME_TEXTAREA_ROWS' ) ;?></label>
								<div class="controls">
									<input type="text" class="sfield" name="rows" />
								</div>
							</div>
							
							<div class="control-group field-textarea" style="display:none;">
								<label class="control-label"><?php echo  JText::_ ( 'FIELD_NAME_TEXTAREA_COLS' ) ;?></label>
								<div class="controls">
									<input type="text" class="sfield" name="cols" />
								</div>
							</div>
							
							<div class="control-group field-general" style="display:none;">
								<label class="control-label"><?php echo JText::_ ( 'FIELD_DEFAULT_PLACEHOLDER_LABEL' );?></label>
								<div class="controls">
									<input type="text" class="sfield" name="placeholder" />
								</div>
							</div>
							
						
							<div class="control-group field-general" style="display:none;">
								<label class="control-label"><?php echo JText::_ ( 'FIELD_DEFAULT_VALUE_LABEL' );?></label>
								<div class="controls">
									<input type="text" class="sfield" name="value" />
								</div>
							</div>
	
							<div class="control-group field-general" style="display:none;">
								<label class="control-label"><?php echo JText::_ ( 'FIELD_FIELD_REQUIRED_LABEL' ) ;?></label>
								<div class="controls">
									<input type="checkbox" class="sfield" name="required" />
								</div>
							</div> 
	 
						</div>
						<div class="span6">
								<div class="control-group field-general " style="display:none;">
									<label class="control-label"><?php echo JText::_ ( 'FIELD_FIELD_CLASS_LABEL' ) ;?></label>
									<div class="controls">
										<input type="text" class="sfield" name="class" />
									</div>
								</div>
		
								<div class="control-group field-general" style="display:none;">
									<label class="control-label"><?php echo JText::_ ( 'FIELD_ID_ATTRIBUTE_LABEL' );?></label>
									<div class="controls">
										<input type="text" class="sfield" name="id" />
									</div>
								</div> 
								
								<div class="control-group field-number" style="display:none;">					
									<label class="control-label"><?php echo JText::_ ( 'FIELD_MIN_NUMBER' ) ;?></label>
									<div class="controls">										 
										<input type="number" class="sfield" min="1" value="1" name="min_num" />							
									</div>
								</div>
								
								<div class="control-group field-number" style="display:none;">					
									<label class="control-label"><?php echo JText::_ ( 'FIELD_MAX_NUMBER' ) ;?></label>
									<div class="controls">										 
										<input type="number" class="sfield" min="1" name="max_num" />							
									</div>
								</div> 
								
								<!-- list related fields -->
								<div class="control-group field-list" style="display:none;">					
									<label class="control-label"><?php echo JText::_ ( 'FIELD_FIELD_OPTIONS_LABEL' ) ;?></label>
									<div class="controls">
										<pre>option_value1:Option label1,option_value2:Option label2</pre>
										<textarea class="sfield" name="option"></textarea>							
									</div>
								</div> 
								<div class="control-group field-anchor" style="display:none;">
									<label class="control-label"><?php echo JText::_ ( 'FIELD_FIELD_INSERT_BLANK_OPTION_LABEL' ) ;?></label>
									<div class="controls">
										<input type="checkbox" class="sfield" name="insert_blank" />
									</div>
								</div> 
	 
		
							<div class="control-group field-date" style="display:none;">
								<label class="control-label"><?php echo JText::_ ( 'FIELD_FIELD_DATE_SELECTION_LABEL' ) ;?></label>
								<div class="controls">
									<input type="text" class="sfield" name="date_format" />
								</div>
							</div> 
						
						</div>						
					</div> 						
				</form>
			</div>
			<div class="modal-footer">	
				<div class="insert-box">
					 <textarea  readonly="readonly"	id="stag" class="input-xxlarge" name="text"></textarea> 
					 <input type="hidden" name="sshortcode" value="" id="sshortcode" />
					<input type="button" class="btn btn-reset" id="resetInputs" value="<?php echo JText::_('FORMBUILDER_INPUTS_RESET');?>" />
					 <input type="button" data-dismiss="modal" aria-label="Close" onclick="insertShortcode();" value="<?php echo JText::_('FORMBUILDER_INSERT_TAG');?>" 	class="btn btn-primary insert-tag" />
				</div>
				<button type="button" class="btn btn-default" data-dismiss="modal"><?php echo JText::_('JCANCEL');?></button>			 
			</div>
		</div>
	</div>
</div>
