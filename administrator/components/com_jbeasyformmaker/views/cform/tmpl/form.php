<?php
/**
 * @version    CVS: 1.0.0
 * @package    com_jbeasyformmaker
 * @author     Priya Bose <support@joomlabuff.com>
 * @copyright  2016 www.joomlabuff.com
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access to this file
defined ( '_JEXEC' ) or die ();
// no direct access
defined ( '_JEXEC' ) or die ();
JHtml::_('behavior.framework', true);
JHtml::_('behavior.multiselect');
JHtml::_('behavior.modal'); 
JHtml::_('formbehavior.chosen', '.chosenselect');
?>
<style type="text/css">
div.modal{
	position:relative !important;
} 
</style>
 

<div class="formbuilder-settings">
<form action="<?php echo JRoute::_('index.php'); ?>" method="post" name="adminForm" id="adminForm" class="form-horizontal form-validate">
	 <input type="hidden" name="option" value="com_jbeasyformmaker" />
	 <input type="hidden" name="view" value="cforms" />
	 <input type="hidden" name="task" value="" id="task" />
	 <input type="hidden" name="jbeasyformmaker_cform_id" value="<?php echo $this->item->jbeasyformmaker_cform_id;?>" /> 
	<?php echo JHtml::_('form.token'); ?>  
	<?php
        $fieldsets = $this->form->getFieldsets();
        $shortcode = $this->form->getValue('text');
        $tab_count = 0;
        foreach ($fieldsets as $key => $attr){

	            if ( $tab_count == 0 )
	            {
	                echo JHtml::_('bootstrap.startTabSet', 'configuration', array('active' => $attr->name));
	            }
	            echo JHtml::_('bootstrap.addTab', 'configuration', $attr->name, JText::_($attr->label, true));
	            ?>
	   
	            <div class="row-fluid">
	                <div class="span12">
	                    <?php
	                    $layout = '';
	                    $style = '';
	                    $fields = $this->form->getFieldset($attr->name);
	                    foreach ($fields as $key => $field)
	                    {
 
	                    ?>
	                        <div class="control-group <?php echo $layout; ?>" <?php echo $style; ?>>
	                            <div class="control-label"><?php echo $field->label; ?></div>
 
	                           <div class="controls"><?php echo $field->input; ?>
	                            	<br />
	                            	<small class="muted"><?php echo JText::_($field->description); ?></small>	 
	                            </div>
	                        </div>
	                    <?php }   ?>
	                </div>
	            </div>
	       
	            <?php
	            echo JHtml::_('bootstrap.endTab');
	            $tab_count++;

        }
        ?>
 </form> 
</div> 
<script type="text/javascript">
 /* Override joomla.javascript, as form-validation not work with ToolBar */
 Joomla.submitbutton =function(pressbutton){
	 
    if (pressbutton == 'cancel') {
        submitform(pressbutton);
    }else if(pressbutton =='apply' || pressbutton =='save' ){
        //to add selected attribute
    	jQuery('#adminForm').find('.selected').find('option').each(function(i,e){
        	jQuery(e).prop('selected',true);
    	});
        submitform(pressbutton);
    }
    
}
</script>

<?php echo $this->loadTemplate('fields');?>
		
<script type="text/javascript">

var start_scode= "[ " + jQuery('#sshortcode').val();
var change_temp ='';
var end_scode	= "]";
var new_ss = '';


jQuery('#resetInputs').on('click', function() {
	change_temp ='';  
	jQuery('.sfield').attr('value','');
	jQuery('.sfield').on('change',function(ind,el){	
		jQuery(el).attr('value','');
	})
  jQuery('#stag').val('');
	
});


function ShowFileds(shortcode){ 

	 
	 jQuery('.field-general').show();
	 jQuery('.field-list').hide();
	 jQuery('.field-radio').hide();
	 jQuery('.field-textarea').hide();
	 if (shortcode.toLowerCase().indexOf("list") >= 0 ||  shortcode.toLowerCase().indexOf("radio") >= 0 || shortcode.toLowerCase().indexOf("checkbox") >= 0 ){
		 jQuery('.field-list').show();
		 jQuery('.field-radio').show();
	 }
	 if (shortcode.toLowerCase().indexOf("textarea") >= 0){
		 jQuery('.field-textarea').show();
	}

	if (shortcode.toLowerCase().indexOf("number") >= 0){
		 jQuery('.field-number').show();
	}

	if(shortcode.toLowerCase().indexOf("submit") >=0){
		 
		 jQuery('.field-submit').show();
		 jQuery('.field-general').hide();
	}
	 	
}

function doinsertShortcode(thisElement){			
	jQuery('#sshortcode').val(jQuery(thisElement).data('shortcode-key'));
	change_temp ='';  
}

function insertShortcode(){
	 jInsertEditorText(jQuery('#stag').val(),'body');
}

	jQuery(document).ready(function($){ 
						var temp ='';
			$('.sfield').on('change',function(ind,el){				

				if(temp.indexOf(this.name) >= 0){
					change_temp += '| ' + this.name +'="'+ this.value +'"  ' ; 
					
				}else{
					change_temp += '| ' + this.name +'="'+ this.value +'"  ' ;
				}
				if(this.name === 'name'){
					change_temp += '| tagname="'+ this.value +'"  ' ; 
				}
				 
				temp ='{'+jQuery('#sshortcode').val()+'jbeasyformmaker} |type="' +jQuery('#sshortcode').val()+ '"  ' + change_temp +'{/jbeasyformmaker}';


				jQuery('#stag').attr('value',temp);
			}); 
	 
	  });
	</script>