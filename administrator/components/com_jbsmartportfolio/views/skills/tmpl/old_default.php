<?php
/**
 * @package     JB Smart Portfolio
 *
 * @copyright   Copyright (C) 2016 Joomlabuff. All rights reserved.
 * @license     GNU General Public License version 2 or later.
 */

defined('_JEXEC') or die();

// Load the method jquery script.
JHtml::_('jquery.framework');

$doc = JFactory::getDocument();
//$doc->addStylesheet( JURI::base(true) . '/components/com_jbsmartportfolio/assets/css/jbsmartportfolio.css' );

echo $this->getRenderedForm();