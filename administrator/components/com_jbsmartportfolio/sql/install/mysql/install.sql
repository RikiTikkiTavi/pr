 CREATE TABLE IF NOT EXISTS `#__jbsmartportfolio_items` (
  `jbsmartportfolio_item_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `alias` varchar(55) NOT NULL,
  `item_type` varchar(255) NOT NULL,
  `category_id` int(11) NOT NULL,
  `image` text NOT NULL,
  `alternative_img_1` text NOT NULL,
  `alternative_img_2` text NOT NULL,
  `alternative_img_3` text NOT NULL,
  `alternative_img_4` text NOT NULL,
  `video` text NOT NULL,
  `description` mediumtext,
  `jbsmartportfolio_tag_id` text NOT NULL,
  `jbsmartportfolio_skill_id` text NOT NULL,
  `related_items` text NOT NULL,
  `url` text NOT NULL,
  `location` text NOT NULL,
  `client` varchar(255) NOT NULL,
  `client_url` text NOT NULL,
  `author_name` varchar(255) NOT NULL,
  `author_image` text NOT NULL,
  `author_role` varchar(255) NOT NULL,
  `author_quote` text NOT NULL,
  `author_company` varchar(255) NOT NULL,
  `social_share` int(11) NOT NULL,
  `like_count` int(11) NOT NULL,
  `enabled` tinyint(3) NOT NULL DEFAULT '1',
  `language` varchar(255) NOT NULL DEFAULT '*',
  `access` int(5) NOT NULL DEFAULT '1',
  `ordering` int(10) NOT NULL DEFAULT '0',
  `created_by` bigint(20) NOT NULL DEFAULT '0',
  `created_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` bigint(20) NOT NULL DEFAULT '0',
  `modified_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `locked_by` bigint(20) NOT NULL DEFAULT '0',
  `locked_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`jbsmartportfolio_item_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `#__jbsmartportfolio_tags` (
  `jbsmartportfolio_tag_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `alias` varchar(55) NOT NULL,
  `language` varchar(255) NOT NULL DEFAULT '*',
  PRIMARY KEY (`jbsmartportfolio_tag_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `#__jbsmartportfolio_rating` (
  `item_id` int(11) NOT NULL DEFAULT '0',
  `rating_sum` int(10) unsigned NOT NULL DEFAULT '0',
  `rating_count` int(10) unsigned NOT NULL DEFAULT '0',
  `lastip` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`item_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `#__jbsmartportfolio_skills` (
  `jbsmartportfolio_skill_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `enabled` tinyint(3) NOT NULL DEFAULT '1',
  `ordering` int(10) NOT NULL DEFAULT '0',
  `created_by` bigint(20) NOT NULL DEFAULT '0',
  `created_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` bigint(20) NOT NULL DEFAULT '0',
  `modified_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `locked_by` bigint(20) NOT NULL DEFAULT '0',
  `locked_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`jbsmartportfolio_skill_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;
