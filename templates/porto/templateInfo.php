<?php
/** 
 *------------------------------------------------------------------------------
 * @package       T3 Framework for Joomla!
 *------------------------------------------------------------------------------
 * @copyright     Copyright (C) 2004-2013 JoomlArt.com. All Rights Reserved.
 * @license       GNU General Public License version 2 or later; see LICENSE.txt
 * @authors       JoomlArt, JoomlaBamboo, (contribute to this project at github 
 *                & Google group to become co-author)
 * @Google group: https://groups.google.com/forum/#!forum/t3fw
 * @Link:         http://t3-framework.org 
 *------------------------------------------------------------------------------
 */

 
// no direct access
defined('_JEXEC') or die;
?>

<div class="span4 col-md-4">
	<div class="tpl-preview">
		<img src="<?php echo T3_TEMPLATE_URL ?>/template_preview.png" alt="Template Preview"/>
	</div>
</div>
<div class="span8 col-md-8">
	<div class="t3-admin-overview-header">
		<h2>
			<?php echo JText::_('JB Porto') ?>
			<small><?php echo JText::_('The best business and virtuemart Joomla template ') ?></small>
		</h2>
		<p>Porto is an Responsive Joomla + eCommerce Theme that is extremely customizable, easy to use and fully responsive. Suitable for every type of business, portfolio, blog and ecommerce sites. Great as a starting point for your custom projects. Porto includes 30 homepage layouts and skins and it has huge variation to be suitable for any purpose. More amazing features are coming soon!</p>
	</div>
	
</div>
