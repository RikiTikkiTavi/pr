<?php 
// No direct access to this file
defined('_JEXEC') or die;

class JbportohotelControllerItems extends F0FController
{


function setItems(){ 

		//get variant id
		$model =  F0FModel::getTmpInstance('Items' ,'jbportohotelModel');

		$filter_category = $this->input->getInt('filter_category');

		$limit = $this->input->getInt('limit',0);
		$limitstart = $this->input->getInt('limitstart',0);

		$model->setState('filter_category',$filter_category);
		$model->setState('limit',$limit);
		$model->setState('limitstart',$limitstart);
		$model->setState('enabled',1);
		$model->setState('visible', 1);

		$items = $model->getItemList();
		$catarray = array();
	 	
		//print_r($items); exit;
	 
		$categories = JHtmlCategory::options('com_jbportohotel');
		
		 $layout = $this->input->getString('layout');
		 
		
		$view = $this->getThisView();
		$view->setModel($model, true);
		$view->set('state',$model->getState());
		$view->set('pagination',$model->getPagiantion());
		$view->set('total',$model->getTotal());
		$view->set('items',$items);
		$view->set('categories',$categories);
		$view->setLayout('items');
		$view->display();
	}
}