<?php
/**
 * @package     JB Porto Hotel 
 *
 * @copyright   Copyright (C) 2016 Joomlabuff. All rights reserved.
 * @license     GNU General Public License version 2 or later.
 */

defined('_JEXEC') or die();
 
require_once JPATH_COMPONENT . '/helpers/helper.php';
jbportohotelHelper::generateMeta($this->item);
$app = JFactory::getApplication(); 
JPluginHelper::importPlugin('jbportohotel');
$jitem  = $app->triggerEvent('onContentBeforeDisplay', array('com_jbportohotel.item', &$this->item, &$params, 1));
$menu 	= JFactory::getApplication()->getMenu();		
$itemId = '';
if(is_object($menu->getActive())) {
	$active = $menu->getActive();
   $itemId = $active->id;
}
 
$link = JRoute::_("index.php?Itemid={$itemId}");	
 
$doc = JFactory::getDocument();
$doc->addStylesheet( JURI::root(true) . '/components/com_jbportohotel/assets/css/jbportohotel.css' );

 

$doc->addScript( JURI::root(true) . '/components/com_jbportohotel/assets/owl.carousel/owl.carousel.min.js' );
$doc->addStylesheet( JURI::root(true) . '/components/com_jbportohotel/assets/owl.carousel/assets/owl.carousel.min.css' );
$doc->addStylesheet( JURI::root(true) . '/components/com_jbportohotel/assets/owl.carousel/assets/owl.theme.default.min.css' );

/* $tags = jbportohotelHelper::getTags( (array) $this->item->jbportohotel_tag_id );
$newtags = array();
foreach ($tags as $tag) {
	$newtags[] 	 = '<a href="#">'.$tag->title.'</a>';
}  */
?>
<div class="container">
	<div class="row">
		<div class="col-md-12 col-sm-12">
		<div class="item-detail-inner pb-xlg">
			<div class="item-header">
				<h3 class="mt-xlg mb-none pb-none text-uppercase"><?php echo $this->item->title;?></h3>
				<div class="divider divider-primary divider-small mb-xl mt-none">
					<hr class="mt-sm">
				</div>
			</div>		
			<div class="row">
				<div class="col-md-7 col-sm-7">
				
					<div class="item-description">
						<?php echo $this->item->description;?>
					</div>
				<div class="room-suite-info">
					<ul>
						 <li><label><?php echo JText::_('JBPORTO_BEDS');?></label> <b><?php echo $this->item->beds;?></b></li>
						 <li><label><?php echo JText::_('JBPORTO_OCCUPANCY');?></label> <b><?php echo $this->item->occupancy;?> Persons</b></li>
						 <li><label><?php echo JText::_('JBPORTO_SIZE');?></label> <b><?php echo $this->item->size;?> sqm.</b></li>
						 <li><label><?php echo JText::_('JBPORTO_VIEW');?></label> <b><?php echo $this->item->hview;?></b></li>
						 <li>
						 	<label><?php echo JText::_('JBPORTO_RATES_ROOM');?></label>
						 	<strong><?php echo $this->item->currency;?> <?php echo $this->item->rates;?></strong>
						 </li>
						 <li>
						 	 
							<a  href="<?php echo $this->item->book_now;?>" title="" class="room-suite-info-book btn btn-primary">
								<?php echo JText::_('JBPORTO_BOOK_NOW');?>
							</a>
						</li>
					 </ul>
				</div>
				</div>
				<div class="col-md-5 col-sm-5">	
				<?php $images = JbportohotelHelper::getItemImages($this->item);	?>													
					<span class="owl-carousel owl-theme nav-inside m-none" data-plugin-options='{"items": 1, "margin": 10, "animateOut": "fadeOut", "autoplay": true, "autoplayTimeout": 3000}'>
						<?php foreach($images as $img):?>
							<span>
								<img class="jb-portohotel-img img-responsive" src="<?php echo $img;?>" alt="<?php echo $item->title; ?>" />															
							</span>
						<?php endforeach;?>												 
					</span>			
				</div>
			</div> 
		</div>
		</div>			
	</div>
</div>
 
<script type="text/javascript">
jQuery('.owl-carousel').owlCarousel({"items": 1, "margin": 10, "animateOut": "fadeOut", "autoplay": true, "autoplayTimeout": 3000});
</script>
