<?php

?>
<?php
/**
*	@version	$Id: default_ajax.php 4 2014-10-15 08:40:47Z linhnt $
*	@package	K2 Content as Timeline (mod_k2latest_timeline)
*	@copyright	Copyright (C) 2009 - 2014 Omegatheme. All rights reserved.
*	@license	GNU/GPL version 2, or later
*	@website:	http://www.omegatheme.com
*	Support Forum - http://www.omegatheme.com/forum/
*/

defined( '_JEXEC' ) or die( 'Restricted access' );

$nomargin =  ($this->params->get('nomargin',0)) ? 'm-none p-none' : '';

$div_layout_class="portfolio-item";
if($nomargin){
	$div_layout_class ='portfolio-item m-none';
}

$span_layout_class = 'thumb-info thumb-info-centered-info';
if($this->params->get('layout_type','mansory') == 'mansory'){
	$span_layout_class = 'thumb-info thumb-info-centered-info thumb-info-no-borders';
	if($this->params->get('nomargin',0) ==1){
		$span_layout_class = 'thumb-info thumb-info-centered-info thumb-info-no-borders m-none';

	}
}elseif($this->params->get('layout_type','mansory') == 'grid'){
	//thumb-info thumb-info-lighten thumb-info-no-zoom
	$span_layout_class ='thumb-info thumb-info-lighten thumb-info-no-zoom';
		
	if($this->params->get('nomargin',0) ==1){
		//thumb-info thumb-info-centered-info thumb-info-no-borders m-none
		$span_layout_class = 'thumb-info thumb-info-centered-info thumb-info-no-borders m-none';
	}
}
?>
<?php if (count($this->items)) : ?>
 <div class="sort-destination-loader1 sort-destination-loader-showing">
	<div class="portfolio-list sort-destination" data-sort-id="portfolio">
<?php foreach ($this->items as $idx => $item) : 
$item->category_title = JbsmartportfolioHelper::getCategoryTitle($item->category_id);
$tags = JbsmartportfolioHelper::getTags ( $item->jbsmartportfolio_tag_id );
$newtags = array ();
$filter = '';
$groups = array ();
foreach ( $tags as $tag ) {
	$newtags [] = $tag->title;
	$filter .= ' ' . $tag->alias;
	$groups [] .= '"' . $tag->alias . '"';
}
$groups = implode ( ',', $groups );
$item->url = JRoute::_ ( 'index.php?option=com_jbsmartportfolio&view=item&id=' . $item->jbsmartportfolio_item_id . ':' . $item->alias . $this->itemId );


?>
    <?php if ($idx %2 == 0): ?>
        <div class="clearfix"></div>
    <?php endif; ?>
   <article class="<?php echo $this->col;?> jb-smartportfolio-item isotope-item  <?php echo $nomargin;?>   timeline-item<?php echo $idx == 0 ? ' first' : ''; ?><?php echo ($idx %2 == 0) ? ' even' : ' odd'; ?>" data-groups='[<?php echo $groups; ?>]'>
   <!-- 
   	<article class="post timeline-item<?php echo $idx == 0 ? ' first' : ''; ?><?php echo ($idx %2 == 0) ? ' even' : ' odd'; ?>" itemscope itemtype="http://schema.org/Article">
   	-->
        <div class="post-inner">
            
            <time class="item-time label label-info" datetime="<?php echo JHtml::_('date',$item->created_on, 'c'); ?>"><?php echo JHtml::_('date',$item->created_on, JText::_('DATE_FORMAT_LC3')) ;?>
            </time>
            
           <a href="<?php echo $item->url;?>">
						<span class="thumb-info thumb-info-no-zoom thumb-info-lighten">
							<span class="thumb-info-wrapper">
								<img class="jb-smartportfolio-img img-responsive" src="<?php echo JURI::root() . $item->image;?>" alt="<?php echo $item->title; ?>" />
								<span class="thumb-info-action">
									<span class="thumb-info-action-icon"><i class="fa fa-link"></i></span>
								</span>
							</span>
						</span>
			</a> 
             <div class="item-intro">
                <?php echo $item->description; ?>
               </div>
            
            
               
            
            <?php if($this->params->get('show_tags',0) && !empty($newtags)):?>
            	<ul>
			 		<li>
			 			<i class="fa fa-tags"></i>
							<?php echo implode(', ', $newtags); ?>
					</li>
			</ul>
			 <?php endif;?>

                
              

            <div class="clearfix"></div>
        </div>
    </article>
 
<?php endforeach; ?>
   </div>
    </div>
<?php endif; ?>