/** 
 *------------------------------------------------------------------------------
 * @package       T3 Framework for Joomla!
 *------------------------------------------------------------------------------
 * @copyright     Copyright (C) 2004-2013 JoomlArt.com. All Rights Reserved.
 * @license       GNU General Public License version 2 or later; see LICENSE.txt
 * @authors       JoomlArt, JoomlaBamboo, (contribute to this project at github 
 *                & Google group to become co-author)
 * @Google group: https://groups.google.com/forum/#!forum/t3fw
 * @Link:         http://t3-framework.org 
 *------------------------------------------------------------------------------
 */


jQuery(document).ready(function($){

    var headerwrap = $('#header');

    $(window).scroll(function(){

        var scroll = $(this).scrollTop();
        var topDist = 30;

        if( scroll > topDist ) {
            headerwrap.addClass('sticky');
        }
        else {
            headerwrap.removeClass('sticky');
        }
    });
    
    
    
    
    
$('a[data-gal]').each(function() { jQuery(this).attr('rel', jQuery(this).data('gal')); });     
$("a[data-gal^='prettyPhoto']").prettyPhoto({animationSpeed:'slow',slideshow:false,overlay_gallery: false,theme:'light_square',social_tools:false,deeplinking:false});


// Back to top
	
	$('#back-to-top').on('click', function(){
		$("html, body").animate({scrollTop: 0}, 500);
		return false;
	});

});

