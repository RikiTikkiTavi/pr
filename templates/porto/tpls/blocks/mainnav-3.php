<?php
/**
 * @package   T3 Blank
 * @copyright Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license   GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
?>

<!-- MAIN NAVIGATION -->
<div class="header-container header-nav jb-style-5"  data-spy="affix" data-offset-top="110">
	
<!-- MAIN NAVIGATION -->
					<nav id="t3-mainnav" class="jb-navigation">
					<div class="container">
			
					<!-- Right side block starts -->
					<div class="header-right-block">
						<div class="t3-nav-btn">
							<!-- OFFCANVAS -->
							<?php if ($this->countModules('off-canvas')) : ?>
								<div class="pull-left">
								<?php if ($this->getParam('addon_offcanvas_enable')) : ?>
									<?php $this->loadBlock ('off-canvas') ?>
								<?php endif ?>
								</div>
							<?php endif ?>
							<!-- //OFFCANVAS -->
				
							<!-- Brand and toggle get grouped for better mobile display -->
							<div class="navbar-header hidden-xs">
							
								<?php if ($this->getParam('navigation_collapse_enable', 1) && $this->getParam('responsive', 1)) : ?>
									<?php $this->addScript(T3_URL.'/js/nav-collapse.js'); ?>
									<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".t3-navbar-collapse">
										<i class="fa fa-bars"></i>
									</button>
								<?php endif ?>
							</div>
							<!-- //Brand and toggle get grouped for better mobile display -->
						</div>
					</div>
					<!-- //Right side block ends -->
					
				
				<div class="navbar navbar-default t3-mainnav hidden-sm hidden-xs hidden-md">
			
					<?php if ($this->getParam('navigation_collapse_enable')) : ?>
						<div class="t3-navbar-collapse navbar-collapse collapse"></div>
					<?php endif ?>
			
					<div class="t3-navbar  navbar-collapse collapse">
						<jdoc:include type="<?php echo $this->getParam('navigation_type', 'megamenu') ?>" name="<?php echo $this->getParam('mm_type', 'mainmenu') ?>" />
					</div>
			
				</div>
				</div>
			</nav>
			<!-- //MAIN NAVIGATION --></div>
<!-- //MAIN NAVIGATION -->
