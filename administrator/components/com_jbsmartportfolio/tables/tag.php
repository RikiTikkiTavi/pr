<?php
/**
 * @package     JB Smart Portfolio
 *
 * @copyright   Copyright (C) 2016 Joomlabuff. All rights reserved.
 * @license     GNU General Public License version 2 or later.
 */

defined('_JEXEC') or die();

class JbsmartportfolioTableTag extends F0FTable
{

	public function check() {

		$result = true;

		//Alias
		if(empty($this->alias)) {
			$this->alias = JFilterOutput::stringURLSafe($this->title);
		} else {
			$this->alias =JFilterOutput::stringURLSafe($this->alias);
		}

		$existingAlias = F0FModel::getTmpInstance('Tags','JbsmartportfolioModel')
			->alias($this->alias)
			->getList(true);

		if(!empty($existingAlias)) {
			$count = 0;
			$k = $this->getKeyName();
			foreach($existingAlias as $item) {
				if($item->$k != $this->$k) $count++;
			}
			if($count) {
				$this->setError(JText::_('COM_JBSMARTPORTFOLIO_ALIAS_ERR_SLUGUNIQUE'));
				$result = false;
			}
		}

		return $result;
	}

}