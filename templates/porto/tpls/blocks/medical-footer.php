<?php
/**
 * @package   T3 Blank
 * @copyright Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license   GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
?>


<!-- FOOTER -->
<footer id="t3-footer" class="wrap medical-footer">

	<?php if ($this->checkSpotlight('footnav', 'footer-1, footer-2, footer-3, footer-4, footer-5, footer-6')) : ?>
		<!-- FOOT NAVIGATION -->
		<div class="container">
			<div class="test">
				
							<?php $this->spotlight('footnav', 'footer-1, footer-2, footer-3, footer-4, footer-5, footer-6') ?>
			</div>
		</div>
		<!-- //FOOT NAVIGATION -->
	<?php endif ?>

	<div class="jb-copyright pt-md pb-md center">
		<div class="container">
			<div class="row">
				<!-- //COPYRIGHTS -->
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="<?php echo $this->getParam('t3-rmvlogo', 1) ? 'col-md-12' : 'col-md-12' ?> copyright <?php $this->_c('footer') ?>">
						<jdoc:include type="modules" name="<?php $this->_p('footer') ?>" />
					</div>
				</div>
			</div>
		</div>
	</div>

</footer>
<!-- //FOOTER -->
