CREATE TABLE IF NOT EXISTS `#__jbeasyformmaker_cforms` (
  `jbeasyformmaker_cform_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL DEFAULT '',
  `slug` varchar(255) NOT NULL DEFAULT '',
  `form_type` varchar(255) NOT NULL,
  `show_heading` int(11) NOT NULL,
  `heading` varchar(255) NOT NULL,
  `show_description` int(11) NOT NULL,
  `show_captcha` int(11) NOT NULL,
  `description` varchar(255) NOT NULL,
  `form_body` text NOT NULL,
  `to_name` text NOT NULL,
  `to_email` text NOT NULL,
  `to_bcc_email` text NOT NULL,
  `to_cc_email` text NOT NULL,
  `email_message` text NOT NULL,
  `notify_success` varchar(255) NOT NULL,
  `notify_error` varchar(255) NOT NULL,
  `params` mediumtext,
  `pages` int(11) NOT NULL,
  `enabled` tinyint(3) NOT NULL DEFAULT '1',
  `ordering` bigint(20) unsigned NOT NULL DEFAULT '0',
  `created_by` bigint(20) NOT NULL DEFAULT '0',
  `created_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` bigint(20) NOT NULL DEFAULT '0',
  `modified_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`jbeasyformmaker_cform_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;
 

 
 
