<?php
// no direct access
defined( '_JEXEC' ) or die;

class plgContentExtrafields extends JPlugin
{
	/**
	 * Load the language file on instantiation. Note this is only available in Joomla 3.1 and higher.
	 * If you want to support 3.0 series you must override the constructor
	 *
	 * @var    boolean
	 * @since  3.1
	 */
	protected $autoloadLanguage = true;

	/**
	 * Plugin method with the same name as the event will be called automatically.
	 */
	function onContentPrepareForm($form, $data)
	{
		$app    = JFactory::getApplication();
		$option = $app->input->get('option');

		switch($option)
		{
			case 'com_content' :
				if ($app->isAdmin())
				{
					JForm::addFormPath(__DIR__ . '/extrafields');
					$form->loadFile('extrafields', false);
				}
				return true;
		}

		return true;
	}
}
?>