<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_content
 *
 * @copyright   Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined( '_JEXEC' ) or die;

JHtml::addIncludePath( JPATH_COMPONENT . '/helpers' );
JLoader::register( 'TagsHelperRoute', JPATH_BASE . '/components/com_tags/helpers/route.php' );
// Create shortcuts to some parameters.
$params  = $this->item->params;
$item    = $this->item;
$images  = json_decode( $this->item->images );
$urls    = json_decode( $this->item->urls );
$canEdit = $params->get( 'access-edit' );
$user    = JFactory::getUser();
$info    = $params->get( 'info_block_position', 0 );
$date    = JFactory::getDate( $item->publishDate )->format( "y.m.d" );

// Check if associations are implemented. If they are, define the parameter.
$assocParam = ( JLanguageAssociations::isEnabled() && $params->get( 'show_associations' ) );
JHtml::_( 'behavior.caption' );

foreach ( $item->jcfields as $jcfield )
{
	$item->jcFields[$jcfield->name] = $jcfield;
}

/*Custom fields*/
$article_lecture_preview      = $item->jcFields['articlelecturepreview']->value;
$article_lecture_content      = $item->jcFields['articlelecturecontent']->value;
$article_watch_text1          = $item->jcFields['articlewatchtext1']->value;
$article_watch_image          = $item->jcFields['articlewatchimage']->rawvalue;
$article_watch_price          = $item->jcFields['articlewatchprice']->value;
$article_watch_text2          = $item->jcFields['articlewatchtext2']->value;
$article_watch_preview        = $item->jcFields['articlewatchpreview']->value;
$article_download_image       = $item->jcFields['articledownloadimage']->rawvalue;
$article_download_price       = $item->jcFields['articledownloadprice']->value;
$article_download_button_link = $item->jcFields['articledownloadbuttonlink']->value;
$article_download_button      = $item->jcFields['articledownloadbutton']->value;

$document = &JFactory::getDocument();
$renderer = $document->loadRenderer('module');
$Module_breadcrumbs = &JModuleHelper::getModule('mod_breadcrumbs');
?>

<div class="wrap t3-sl-1 jb-breadcrumb page-header masthead article-breadcrumbs">
    <div class="container">
		<?=$renderer->render($Module_breadcrumbs);?>
    </div>
</div>

<div class="container-fluid article-full">

    <!--Preview-->
    <div class="row article-full-preview">
        <div class="container">
            <div class="col-md-5">
				<?= $article_lecture_preview ?>
            </div>
            <div class="col-md-7">
                <h3 class="article-lecture-content-heading text-center article-full-section-heading">Содержание
                    "<?= $item->title; ?>"</h3>
				<?= $article_lecture_content ?>
            </div>
        </div>
    </div>

    <!--Watch-->
    <div class="row article-full-watch">
        <h3 class="text-center article-full-section-heading">Смотреть</h3>
        <div class="container vertical-align">
            <div class="col-md-7">
                <div class="vertical-align">
					<?= $article_watch_preview ?>
                </div>
            </div>
            <div class="col-md-5">
                <h4><?= $article_watch_text1 ?></h4>
                <br>
                <img class="img-responsive" src="/<?= $article_watch_image ?>">
                <br>
                <h4 class="text-center"><b>Цена - <?= $article_watch_price ?></b></h4>
                <br>
                <h4><b><?= $article_watch_text2 ?></b></h4>
            </div>
        </div>
    </div>

    <!--Download-->
    <div class="row article-full-download">
        <h3 class="text-center article-full-section-heading">Скачать</h3>
        <div class="container vertical-align">
            <div class="col-md-4 text-center">
                <img class="img-responsive text-center center-block" src="/<?= $article_download_image ?>">
                <br><br>
                <h4 class="text-center">Цена - <?= $article_download_price ?></h4>
                <br><br>
                <a class="btn btn btn-success btn-lg article-full-download-order-button"
                   style="text-transform: uppercase"
                   href="<?= $article_download_button_link ?>"><i class="fa fa-shopping-cart" aria-hidden="true"></i>
                    <b><?= $article_download_button ?></b></a>
            </div>
            <div class="col-md-8">
                <!--PRTODO Create module "Available payments"-->
                <jdoc:include type="modules" name="article_full_download_right" style="T3Xhtml"/>
                <div class="pay_box">
                    <h4 class="text-center">Доступные способы оплаты</h4>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-8 col-xs-8 col-sm-8">
                                    <span>Картой VISA, MasterCard</span>
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-4">
                                    <img class="img-responsive"
                                         src="http://remontkv.pro/gkl/newkurs/img/payment/pay1.png"
                                         alt="">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-8 col-xs-8 col-sm-8">
                                    <span>Платежная система Webmoney</span>
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-4">
                                    <img class="img-responsive"
                                         src="http://remontkv.pro/gkl/newkurs/img/payment/pay2.png"
                                         alt="">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-8 col-xs-8 col-sm-8">
                                    <span>Платежная система Яндекс.Деньги </span>
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-4">
                                    <img class="img-responsive"
                                         src="http://remontkv.pro/gkl/newkurs/img/payment/pay3.png"
                                         alt="">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-8 col-xs-8 col-sm-8">
                                    <span>Платежная система QIWI </span>
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-4">
                                    <img class="img-responsive"
                                         src="http://remontkv.pro/gkl/newkurs/img/payment/pay4.png"
                                         alt="">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-8 col-xs-8 col-sm-8">
                                    <span>Оплата через салоны связи:</span>
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-4">
                                    <img class="img-responsive"
                                         src="http://remontkv.pro/gkl/newkurs/img/payment/pay5.png"
                                         alt="">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-8 col-xs-8 col-sm-8">
                                    <span>Платежная система Paypal </span>
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-4">
                                    <img class="img-responsive"
                                         src="http://remontkv.pro/gkl/newkurs/img/payment/pay6.png"
                                         alt="">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-8 col-xs-8 col-sm-8">
                                    <span>Безнал. расчёт для ИП и юр. лиц </span>
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-4">
                                    <img class="img-responsive"
                                         src="http://remontkv.pro/gkl/newkurs/img/payment/pay7.png"
                                         alt="">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-8 col-xs-8 col-sm-8">
                                    <span>По квитанции через любой банк </span>
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-4">
                                    <img class="img-responsive"
                                         src="http://remontkv.pro/gkl/newkurs/img/payment/pay8.png"
                                         alt="">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-8 col-xs-8 col-sm-8">
                                    <span>Перевод на карту ВТБ24</span>
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-4">
                                    <img class="img-responsive"
                                         src="http://remontkv.pro/gkl/newkurs/img/payment/pay9.png"
                                         alt="">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-8 col-xs-8 col-sm-8">
                                    <span>Перевод на карту Сбербанка </span>
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-4">
                                    <img class="img-responsive"
                                         src="http://remontkv.pro/gkl/newkurs/img/payment/pay10.png"
                                         alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-xs-6 col-sm-6">
                            <span>Денежные переводы через системы: </span>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <img class="img-responsive"
                                 src="http://remontkv.pro/gkl/newkurs/img/payment/pay_line.png"
                                 alt="">
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
</div>