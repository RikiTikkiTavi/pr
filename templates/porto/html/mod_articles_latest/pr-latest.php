<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_articles_latest
 *
 * @copyright   Copyright (C) 2005 - 2018 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined( '_JEXEC' ) or die;

JHtml::addIncludePath( JPATH_COMPONENT . '/helpers' );

JHtml::_( 'behavior.caption' );

jimport( 'joomla.application.module.helper' );

?>


<div class="row category-article-row">
    <div class="container vertical-align">
		<?php foreach ( $list as $courseItem )
		{


			$article_icon  = json_decode($courseItem->attribs)->articleShortStory_icon;
			$article_locks = json_decode($courseItem->attribs)->articleShortStory_locks;
			$article_group = json_decode($courseItem ->attribs)->articleShortStory_group;

			$images = json_decode( $courseItem->images );
			$link   = JRoute::_( ContentHelperRoute::getCategoryRoute( $courseItem->catslug ) ) . '/' . $courseItem->id;
			?>
            <div class="col-md-4 col-sm-4 pr-news-card-col">
                <a class="pr-news-card-link" href="<?= $link ?>">
                    <div class="pr-news-card"
                         style="background-image: url('/<?= $images->image_intro ?>');">
                        <div class="pr-news-card-top">
                        </div>
                        <div class="pr-news-card-bottom">
                            <div class="row">
                                <div class="col-sm-4 col-xs-4 pr-news-card-icon">
                                    <i class="fa fa-2x <?= $article_icon ?>" aria-hidden="true"></i>
                                </div>
                                <div class="col-md-8 col-xs-8 pr-news-card-description">
                                    <div class="row pr-news-card-heading"><?= $courseItem->title; ?></div>
                                    <div class="row pr-news-card-params">
                                        <div class="pr-news-card-params-lock">
											<?php display_locks( $article_locks ) ?>
                                        </div>
                                        <div class="pr-news-card-params-group">
                                            <i class="fa fa-users"
                                               aria-hidden="true"></i> <?= $article_group; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
		<?php } ?>
    </div>
</div>

