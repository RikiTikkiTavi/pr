<?php
/**
 * @package   T3 Blank
 * @copyright Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license   GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
?>


<!-- FOOTER -->
<footer id="t3-footer" class="wrap t3-footer">

	<?php if ($this->checkSpotlight('footnav', 'footer-1, footer-2, footer-3, footer-4, footer-5, footer-6')) : ?>
		<!-- FOOT NAVIGATION -->
		<div class="container">
			<div class="test">
				<?php $this->spotlight('footnav', 'footer-1, footer-2, footer-3, footer-4, footer-5, footer-6') ?>
			</div>
		</div>
		<!-- //FOOT NAVIGATION -->
	<?php endif ?>

	<div class="t3-copyright">
		<div class="container">
			<div class="row">
				<!-- //COPYRIGHTS -->
				<?php if ($this->getParam('t3-rmvlogo', 1)): ?>
						<div class="col-md-1 poweredby text-hide">
							<a class="t3-logo t3-logo-color" href="http://t3-framework.org" title="<?php echo JText::_('T3_POWER_BY_TEXT') ?>"
							   target="_blank" <?php echo method_exists('T3', 'isHome') && T3::isHome() ? '' : 'rel="nofollow"' ?>><?php echo JText::_('T3_POWER_BY_HTML') ?></a>
						</div>
					<?php endif; ?>
				<div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">
					<div class="<?php echo $this->getParam('t3-rmvlogo', 1) ? 'col-md-7' : 'col-md-12' ?> copyright <?php $this->_c('footer') ?>">
						<jdoc:include type="modules" name="<?php $this->_p('footer') ?>" />
					</div>
				</div>
				<!-- //FOOTER MENU -->
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
					<?php if ($this->countModules('footer-menu')) : ?>
						<div class="jb-footer-menu <?php $this->_c('footer-menu') ?>">
							<jdoc:include type="modules" name="<?php $this->_p('footer-menu') ?>" style="raw" />
						</div>
						<!-- //FOOTER MENU -->
					<?php endif ?>
				</div>
			</div>
		</div>
	</div>

</footer>
<!-- //FOOTER -->
