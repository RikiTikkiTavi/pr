<?php
/**
 * Flex 1.0 @package SP Page Builder
 * Template Name - Flex
 * @author Aplikko http://www.aplikko.com
 * @copyright Copyright (c) 2015 Aplikko
 * @license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 or later
*/
// no direct access
defined('_JEXEC') or die;

AddonParser::addAddon('sp_portotriplecarousel','sp_portotriplecarousel_addon');
AddonParser::addAddon('sp_portotriplecarousel_header','sp_portotriplecarousel_header_addon');
AddonParser::addAddon('sp_portotriplecarousel_body','sp_portotriplecarousel_body_addon');
AddonParser::addAddon('sp_portotriplecarousel_footer','sp_portotriplecarousel_footer_addon');

 global $spbTCH;
 global $spbTCB;
 global $spbTCF;
function sp_portotriplecarousel_addon($atts, $content){
	
	global $sppbportotriplecarouselParam;	
	global $spbTCH ;
	global $spbTCB ;
	global $spbTCF ;
	$app = JFactory::getApplication();
	$doc = JFactory::getDocument(); 
	$media_path = JURI::base(true) . '/templates/'.$app->getTemplate().'/sppagebuilder/addons/portotriplecarousel/assets'; 
	
	
	$doc->addStylesheet( $media_path.'/assets/owl.carousel.min.css');
	$doc->addStylesheet(  $media_path.'/assets/owl.theme.default.min.css');
	$doc->addScript(  $media_path.'/owl.carousel.min.js');
	$doc->addScript(  $media_path.'/script.js');
	
	
		//print_r($content);
	extract(spAddonAtts(array(
		"count" =>'',
		
		"loop" =>'',
		"nav" =>'',
		"dots" =>'',
		"autoplay"=>'',
		"autoplay_interval"=>'',
		"arrows"=>'',
		"class"=>'',
		"dynamicid" =>'',
		 	
		), $atts));


		$sppbportotriplecarouselParam['class'] = $class;
	$autoplay = ($autoplay) ?  'true' : 'false';
	$count = ($count) ? $count : 6;
	$loop = ($loop) ? 'true' : 'false';
	$nav = ($nav) ? 'true' : 'false';
	$dots = ($dots) ? 'true' : 'false';
		
		//print_r($content); exit;
	 $output ='';
	// $output .='<div class="row center">';
	 AddonParser::spDoAddon($content);
	$class  = ($class) ? 'owl-carousel owl-theme '. $class :  'owl-carousel owlc-'.$dynamicid.' owl-theme ';
	$output .='<div class="carousel-areas mt-xl portotriplecarousel">';
  	 
  	 $output .='<div class="owl-carousel owl-theme m-none" data-plugin-options=" ">'; 
  	foreach($spbTCH as $key => $value){ 
		$output .='<div>'; 
		$output .='<img class="img-responsive" alt=" "  src="'.$value['image'] .'"  >'; 
	 $output .='</div>';
	}
	$output .='</div>'; 
	
	
	 $output .='<div class="owl-carousel owl-theme m-none" data-plugin-options=" ">'; 
  	foreach($spbTCB as $key => $value){ 
		$output .='<div>'; 
		$output .='<img class="img-responsive" alt=" " src="'.$value['image'] .'"  >'; 
	 $output .='</div>';
	}
	$output .='</div>'; 
	
	
	 $output .='<div class="owl-carousel owl-theme m-none" data-plugin-options=" ">'; 
  	foreach($spbTCF as $key => $value){ 
		$output .='<div>'; 
		$output .='<img class="img-responsive" alt=" " src="'.$value['image'] .'"  >'; 
	 $output .='</div>';
	}
	$output .='</div>'; 
	
	 $output .='</div>';
	/* $output .='</div>';*/
	 
	 
		return $output;		
}	


function sp_portotriplecarousel_body_addon( $atts ){
	
//global $sppbportoconcept_itemParam;
	global $spbTCB;
	 $json ='{"autoHeight": true, "items": 1, "margin": 10, "nav": true, "dots": false, "stagePadding": 0}';
	extract(spAddonAtts(array(
		"title" =>'',
		"url" =>'',
		"image" =>'',		
		), $atts));
	$title =($title) ? $title : '';
	$output =''; 
	
	$output .='<div>';
	$output .='<a href="'.$url.'">';
	$output .='<img class="img-responsive"  src="'.$image .'" alt="'.$title.'">';
	$output .='</a>';
	$output .='</div>';  
	
	$spbTCB[]=array('image' => $image);

 
}

 
function sp_portotriplecarousel_footer_addon( $atts ){
	global $spbTCF;
	//global $sppbportoconcept_itemParam;
	
	 $json ='{"autoHeight": true, "items": 1, "margin": 10, "nav": true, "dots": false, "stagePadding": 0}';
	extract(spAddonAtts(array(
		"title" =>'',
		"url" =>'',
		"image" =>'',		
		), $atts));
	 
	 $title =($title) ? $title : '';
	$output =''; 
	
	$output .='<div>'; 
	$output .='<img class="img-responsive"  src="'.$image .'" alt="'.$title.'">'; 
	$output .='</div>';  
	$spbTCF[]=array('image' => $image);
	 
}

function sp_portotriplecarousel_header_addon( $atts ){
	global $spbTCH;
	//global $sppbportoconcept_itemParam;
	
	 $json ='{"autoHeight": true, "items": 1, "margin": 10, "nav": true, "dots": false, "stagePadding": 0}';
	extract(spAddonAtts(array(
		"title" =>'',
		"url" =>'',
		"image" =>'',		
		), $atts));
	$title =($title) ? $title : '';	
	$output =''; 	
	$output .='<div>';	 
	$output .='<img class="img-responsive"  src="'.$image .'" alt="'.$title.'">';	 
	$output .='</div>';  
	 
	$spbTCH[]=array('image' => $image);
  
}
