<?php
/**
 * @version    CVS: 1.0.0
 * @package    com_jbeasyformmaker
 * @author     Priya Bose <support@joomlabuff.com>
 * @copyright  2016 www.joomlabuff.com
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined ( '_JEXEC' ) or die ( 'Restricted Access' );

// load tooltip behavior
JHtml::_ ( 'bootstrap.tooltip' );
JHtml::_ ( 'behavior.multiselect' );
JHtml::_ ( 'formbehavior.chosen', 'select' );
$sidebar = JHtmlSidebar::render ();
?> 
<form action="<?php echo JRoute::_('index.php?option=com_jbeasyformmaker&view=cpanel'); ?>" method="post" name="adminForm" id="adminForm">	
	<?php if(!empty( $sidebar)): ?>
	<div id="j-sidebar-container" class="span2">
		<?php echo $sidebar; ?>
	</div>
	<div id="j-main-container" class="span10">  
	 <?php else : ?>
		<div id="j-main-container">
		<?php endif;?>
			<div id="bl" class="row-fluid ">
				<div class="span6 span6-offset">
					<div class="row-fluid">
						<div class="span4 span8-offset">
							<div class="card red summary-inline">
								<div class="card-body">
									<i class="icon fa fa-file-text-o fa-2x"></i>
								</div>
								<div class="content">
									<div class="sub-title">
										<a href="<?php echo JRoute::_('index.php?option=com_jbeasyformmaker&view=cforms');?>">
											<?php echo JText::_('COM_JBEASYFORMMAKER_TITLE_CFORMS');?>
										 </a>
									</div>
								</div>
							</div>
						</div>						 
					</div>
				</div> 
			</div> 
		</div> 
	</div>
	</div>
</form>


