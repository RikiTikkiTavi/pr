<?php
/**
 * @package     JB Smart Portfolio
 * @subpackage  mod_jbsmartportfolio
 *
 * @copyright   Copyright (C) 2010 - 2015 Joombuff. All rights reserved.
 * @license     GNU General Public License version 2 or later.
 */
defined ( '_JEXEC' ) or die ();
jimport ( 'joomla.filesystem.file' );
JHTML::_('behavior.modal');
 

$slider_navigation = ($slider_navigation ==1) ? 'true' : 'false';
$slider_pagination = ($slider_pagination ==1) ? 'true' : 'false';
$autoplay =( $autoplay ==1) ? 'true' : 'false';
$autoplay =( $loop ==1) ? 'true' : 'false';

$cparams = JComponentHelper::getParams ( 'com_jbsmartportfolio' );
?>
<style>
.owl-carousel .modal{
	display:block !important;
 	position:unset !important;
}
</style>

 
<div   class="jb-smartportfolio jb-smartportfolio-view-items <?php echo $moduleclass_sfx; ?>">
	<div class="owl-carousel owl-theme" id="portfolio-style2-<?php echo $module->id;?>">
								
								<?php 
									foreach ( $items as $item ) :
									$item->category_title = ModJbsmartportfolioHelper::getCategoryTitle($item->category_id);
									  $item->url = JRoute::_('index.php?option=com_jbsmartportfolio&view=item&id='.$item->jbsmartportfolio_item_id.':'.$item->alias. ModJbsmartportfolioHelper::getItemid().'&tmpl=component'); ?>
									
							

								<div class="portfolio-item">
								  <a class="modal"  rel="{handler: 'iframe', size: {x: 1000, y: 600}}" href="<?php echo $item->url;?>"> 
										<span class="thumb-info thumb-info-lighten">
											<span class="thumb-info-wrapper">
												<img src="<?php echo JUri::root().'/'.$item->image;?>" class="img-responsive" alt="">
												<span class="thumb-info-title">
													<span class="thumb-info-inner"><?php echo $item->title;?></span>
													<span class="thumb-info-type"><?php echo $item->category_title;?></span>
												</span>
												<span class="thumb-info-action">
													<span class="thumb-info-action-icon"><i class="fa fa-plus-square"></i></span>
												</span>
											</span>
										</span>
									</a>								
								 
								</div>
							 
								
								<?php endforeach;?>
								 
							</div>
</div>
<script type="text/javascript">
  jQuery("#portfolio-style2-<?php echo $module->id;?>").owlCarousel({        
        items:<?php echo $no_of_items;?>,
        mouseDrag:true,       
		startPosition:0,
		loop:<?php echo $loop;?>,
        nav:<?php echo $slider_navigation;?>,
        dots :<?php echo $slider_pagination;?>,
        autoplay :<?php echo $autoplay;?>,
        margin:20,
        navText: ["",""], 
        responsiveClass:true,
		responsive:{
			0:{
				items:1,				 
			},
			600:{
				items:1,
			 
			},
			700:{
				items:2,
			 
			},
			1000:{
				items:<?php echo $no_of_items;?>,			
			}
    } 
        
      });
 
</script>
