<?php
// No direct access to this file
defined('_JEXEC') or die;
class JFormFieldCategorytext extends F0FFormFieldText
{
	/**
	 * The field type.
	 *
	 * @var		string
	 */
	protected $type = 'Categorytext';

	public function getRepeatable()
	{
		
		$db = JFactory::getDbo ();
		$query = $db->getQuery ( true );
	 	$query->select ( 'title' )->from ( '#__categories' )->where ( 'id=' . $db->q ( $this->value ) );
		$query->where ( 'extension ="com_jbsmartportfolio"' );
		$db->setQuery ( $query );
		$html = $category_title = $db->loadResult ();  	 
		return $html;
	}
 
}
