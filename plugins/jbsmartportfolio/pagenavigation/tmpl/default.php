<?php
/**
 * @package     Joomla.Plugin
 * @subpackage  JBSmartportfolio.pagenavigation
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;
$lang = JFactory::getLanguage(); ?> 
<?php if ($row->prev) :
	$direction = $lang->isRtl() ? 'right' : 'left'; ?>
		<a href="<?php echo $row->prev; ?>" rel="prev" class="portfolio-nav-prev" data-tooltip data-original-title="Previous">
				   <i class="fa fa-chevron-<?php echo $direction;?>"></i> 
		</a>
<?php endif; ?>
<?php if ($row->next) :
	$direction = $lang->isRtl() ? 'left' : 'right'; ?>
		<a href="<?php echo $row->next; ?>" rel="next" class="portfolio-nav-next" data-tooltip data-original-title="Next">
			   <i class="fa fa-chevron-<?php echo $direction;?> "></i> 			   
		</a>
<?php endif; ?>
					
					
