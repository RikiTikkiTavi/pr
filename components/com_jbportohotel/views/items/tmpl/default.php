
<?php
/**
 * @package     JB Porto Hotel 
 *
 * @copyright   Copyright (C) 2016 Joomlabuff. All rights reserved.
 * @license     GNU General Public License version 2 or later.
 */

defined('_JEXEC') or die();

jimport( 'joomla.filesystem.file' );
$layout_type = $this->params->get('layout_type', 'default');
$doc = JFactory::getDocument();
JHtml::_('jquery.framework');
//Add js and css files

$doc->addScript( JURI::root(true) . '/components/com_jbportohotel/assets/owl.carousel/owl.carousel.min.js' );
$doc->addStylesheet( JURI::root(true) . '/components/com_jbportohotel/assets/owl.carousel/assets/owl.carousel.min.css' );
$doc->addStylesheet( JURI::root(true) . '/components/com_jbportohotel/assets/owl.carousel/assets/owl.theme.default.min.css' );



$doc->addStylesheet( JURI::root(true) . '/components/com_jbportohotel/assets/css/featherlight.min.css' );
$doc->addStylesheet( JURI::root(true) . '/components/com_jbportohotel/assets/css/jbportohotel.css' );
$doc->addScript( JURI::root(true) . '/components/com_jbportohotel/assets/js/jquery.shuffle.modernizr.min.js' );
$doc->addScript( JURI::root(true) . '/components/com_jbportohotel/assets/js/featherlight.min.js' );
$doc->addScript( JURI::root(true) . '/components/com_jbportohotel/assets/js/jbportohotel.js' );



$columns = $this->params->get('columns', 3);

$col ='col-md-6';
if($columns == 2){
	$col ='col-md-6';
}elseif($columns == 3){
	$col ='col-md-4';
}elseif($columns == 4){
	$col ='col-md-3';
}elseif($columns == 5){
	$col ='col-md-1-5';
}elseif($columns == 6){
	$col ='col-md-2';
}

//Load the method jquery script.
JHtml::_('jquery.framework');

//Params
$params 	= JComponentHelper::getParams('com_jbportohotel');
 



$menu 	= JFactory::getApplication()->getMenu();
$itemId = '';
if(is_object($menu->getActive())) {
	$active = $menu->getActive();
	$itemId = '&Itemid=' . $active->id;
}

 $layout_type = $this->params->get('layout_type', 'mansory');
 
?>

<style>

.col-md-1-5 {
    float: left;
    width: 20%;
}
.col-xs-1-5, .col-sm-1-5, .col-md-1-5, .col-lg-1-5, .col-xs-2-5, .col-sm-2-5, .col-md-2-5, .col-lg-2-5, .col-xs-3-5, .col-sm-3-5, .col-md-3-5, .col-lg-3-5, .col-xs-4-5, .col-sm-4-5, .col-md-4-5, .col-lg-4-5 {
    min-height: 1px;
    padding-left: 15px;
    padding-right: 15px;
    position: relative;
}
</style>
 <div class="container">
		<?php if( $this->params->get('show_page_heading') && $this->params->get( 'page_heading' ) ):?>
		<h1><?php echo $this->params->get( 'page_heading');?></h1>
	<?php endif;?>

	<?php if($this->params->get('show_filter', 1)): ?>
	<div class="row">
		<div class="col-md-12">	
			<div class="jb-portohotel-filter">
				<ul  class="nav nav-pills sort-source" data-sort-id="portfolio" data-option-key="filter" data-plugin-options='{"layoutMode": "masonry", "filter": "*"}'>
					<li data-option-value="*" class="active" data-group="all"><a href="#"><?php echo JText::_('COM_JBPORTOHOTEL_SHOW_ALL'); ?></a></li>
					<?php $filters = jbportohotelHelper::getTagList( $this->items ); ?>
					
					<?php   foreach ($filters as $filter): ?>
					<li  data-option-value=".<?php echo $filter->alias; ?>" data-group="<?php echo $filter->alias; ?>">
						<a href="#"><?php echo $filter->title; ?></a>
					</li>
					<?php  endforeach ?>
				</ul>
			</div>
		</div>
	</div>
	<?php endif; ?>
	<hr>
	<div class="row">
		<div class="sort-destination-loader1 sort-destination-loader-showing">
			<ul class="portfolio-list sort-destination jb-portohotel-items" data-sort-id="portfolio">
				<?php  foreach($this->items as $item):?>
				<?php					
				$tags = jbportohotelHelper::getTags( $item->jbportohotel_tag_id );
				$newtags = array();
				$filter = '';
				$groups = array();
				foreach ($tags as $tag) {
					$newtags[] 	 = $tag->title;
					$filter 	.= ' ' . $tag->alias;
					$groups[] 	.= '"' . $tag->alias . '"';
				}
				
				
				$groups = implode(',', $groups);
						
									
							//print_r($images);
								
								  
					?>
								<?php $item->url = JRoute::_('index.php?option=com_jbportohotel&view=item&id='.$item->jbportohotel_item_id.':'.$item->alias . $itemId); ?>
									<li class="<?php echo $col;?> jb-portohotel-item isotope-item" data-groups='[<?php echo $groups; ?>]'>
										<div class="portfolio-item"> 
											<div class="porto-image-frame  m-b-none">
												<span class="thumb-info thumb-info-centered-info thumb-info-no-borders">
												 	<a target="_self"  title="<?php echo $item->title;?>"  href="<?php echo $item->url;?>">
														<span class="thumb-info-wrapper">												
															<img class="jb-portohotel-img img-responsive" src="<?php echo JURI::root() . $item->image;?>" alt="<?php echo $item->title; ?>">
														<span class="thumb-info-title">
															<span class="thumb-info-inner">
																<?php echo JText::_('JBPORTO_VIEW_DETAILS');?>
																<em></em>
															</span>
														</span>
														</span>
													</a>
												</span>
										    </div>
											<h5><?php echo $item->title;?></h5>
											
											<div class="room-suite-info">
												<ul>
													<li><label><?php echo JText::_('JBPORTO_BEDS');?></label> <b><?php echo $item->beds;?></b></li>
													<li><label><?php echo JText::_('JBPORTO_OCCUPANCY');?></label> <b><?php echo $item->occupancy;?> Persons</b></li>
													<li><label><?php echo JText::_('JBPORTO_SIZE');?></label> <b><?php echo $item->size;?> sqm.</b></li>
													<li><label><?php echo JText::_('JBPORTO_VIEW');?></label> <b><?php echo $item->hview;?></b></li>
													<li><label><?php echo JText::_('JBPORTO_RATES_ROOM');?></label> <strong><?php echo $item->currency;?> <?php echo $item->rates;?></strong></li>
													<li><a href="<?php echo $item->url;?>" title="" class="room-suite-info-detail">
															<?php echo JText::_('JBPORTO_VIEW_DETAILS');?> <i class="fa fa-long-arrow-right"></i>
														</a>
													<a href="<?php echo $item->book_now;?>" title="" class="room-suite-info-book"><?php echo JText::_('JBPORTO_BOOK_NOW');?></a></li>
												</ul>
											</div> 
												 
										 
										</div>
									</li>
								<?php endforeach;?>							 
							</ul>
							<?php if ($this->pagination->get('pages.total') >1) { ?>
							<div class="pagination">
								<?php echo $this->pagination->getPagesLinks(); ?>
							</div>
							<?php } ?>
					</div>
				</div>
			</div>
		</div>
<script>
jQuery('.owl-carousel').owlCarousel({"items": 1, "margin": 10, "animateOut": "fadeOut", "autoplay": true, "autoplayTimeout": 3000});
</script>
