<?php
/**
 * @package     Joomla.Plugin
 * @subpackage  Content.pagenavigation
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined ( '_JEXEC' ) or die ();

/**
 * Pagenavigation plugin class.
 *
 * @since 1.5
 */
class PlgJbsmartportfolioPagenavigation extends JPlugin {
	
	
	
	protected static $lookup = array ();
	/**
	 * If in the article view and the parameter is enabled shows the page navigation
	 *
	 * @param string $context
	 *        	The context of the content being passed to the plugin
	 * @param
	 *        	object &$row The article object
	 * @param
	 *        	mixed &$params The article params
	 * @param integer $page
	 *        	The 'page' number
	 *        	
	 * @return mixed void or true
	 *        
	 * @since 1.6
	 */
	public function onContentBeforeDisplay($context, &$row, &$params, $page = 0) {
		$app = JFactory::getApplication ();
		$view = $app->input->get ( 'view' );
		
		$params = $app->getMenu ()->getActive ()->params; // get the active item
		$categoryId = $params->get ( 'category_id' );
		$menu = JFactory::getApplication ()->getMenu ();
		$itemId = '';
		if (is_object ( $menu->getActive () )) {
			$active = $menu->getActive ();
			$itemId = '&Itemid=' . $active->id;
		}
		$db = JFactory::getDbo ();
		$user = JFactory::getUser ();
		$lang = JFactory::getLanguage ();
		$nullDate = $db->getNullDate ();
		
		$date = JFactory::getDate ();
		$now = $date->toSql ();
		
		$uid = $row->jbsmartportfolio_item_id;
		$option = 'com_jbsmartportfolio';
		$canPublish = $user->authorise ( 'core.edit.state', $option . '.item.' . $row->jbsmartportfolio_item_id );
		
		require_once (JPATH_COMPONENT . '/models/items.php');
		// $list = F0FModel::getTmpInstance('Items')->getItemList('id');
		
		$list = $this->getNavItems ();
		// print_r($tlist); exit;
		// $list = $db->loadObjectList('id');
		
		// This check needed if incorrect Itemid is given resulting in an incorrect result.
		if (! is_array ( $list )) {
			$list = array ();
		}
		
		reset ( $list );
		
		// Location of current content item in array list.
		$location = array_search ( $uid, array_keys ( $list ) );
		
		$rows = array_values ( $list );
		
		// print_r($rows);
		
		$row->prev = null;
		$row->next = null;
		
		if ($location - 1 >= 0) {
			// The previous content item cannot be in the array position -1.
			$row->prev = $rows [$location - 1];
		}
		
		if (($location + 1) < count ( $rows )) {
			// The next content item cannot be in an array position greater than the number of array postions.
			$row->next = $rows [$location + 1];
		}
		
		if ($row->prev) {
			$row->prev_label = ($this->params->get ( 'display', 0 ) == 0) ? JText::_ ( 'JPREV' ) : $row->prev->title;
			$row->prev = JRoute::_ ( $this->getItemRoute ( $row->prev->jbsmartportfolio_item_id, $row->prev->alias, $itemId ) );
		} else {
			$row->prev_label = '';
			$row->prev = '';
		}
		
		if ($row->next) {
			$row->next_label = ($this->params->get ( 'display', 0 ) == 0) ? JText::_ ( 'JNEXT' ) : $row->next->title;
			$row->next = JRoute::_ ( $this->getItemRoute ( $row->next->jbsmartportfolio_item_id, $row->next->alias, $itemId ) );
		} else {
			$row->next_label = '';
			$row->next = '';
		}
		
		// Output.
		if ($row->prev || $row->next) {
			// Get the path for the layout file
			$path = JPluginHelper::getLayoutPath ( 'jbsmartportfolio', 'pagenavigation' );
			
			// Render the pagenav
			ob_start ();
			include $path;
			$row->pagination = ob_get_clean ();
			
			$row->paginationposition = $this->params->get ( 'position', 1 );
			
			// This will default to the 1.5 and 1.6-1.7 behavior.
			$row->paginationrelative = $this->params->get ( 'relative', 0 );
		}
		/* } */
		
		return;
	}
	public function getNavItems() {
		$model = F0FModel::getTmpInstance ( 'Items' );
		$db = JFactory::getDbo ();
		$query = $model->buildQuery ();
		
		$db->setQuery ( $query );
		return $db->loadObjectList ( 'jbsmartportfolio_item_id' );
	}
	protected static function _findItem($needles = null) {
		$app = JFactory::getApplication ();
		$menus = $app->getMenu ( 'site' );
		$language = isset ( $needles ['language'] ) ? $needles ['language'] : '*';
		
		// Prepare the reverse lookup array.
		if (! isset ( self::$lookup [$language] )) {
			self::$lookup [$language] = array ();
			
			$component = JComponentHelper::getComponent ( 'com_jbsmartportfolio' );
			
			$attributes = array (
					'component_id' 
			);
			$values = array (
					$component->id 
			);
			
			if ($language != '*') {
				$attributes [] = 'language';
				$values [] = array (
						$needles ['language'],
						'*' 
				);
			}
			
			$items = $menus->getItems ( $attributes, $values );
			
			foreach ( $items as $item ) {
				if (isset ( $item->query ) && isset ( $item->query ['view'] )) {
					$view = $item->query ['view'];
					
					if (! isset ( self::$lookup [$language] [$view] )) {
						self::$lookup [$language] [$view] = array ();
					}
					
					if (isset ( $item->query ['id'] )) {
						/**
						 * Here it will become a bit tricky
						 * language != * can override existing entries
						 * language == * cannot override existing entries
						 */
						if (! isset ( self::$lookup [$language] [$view] [$item->query ['id']] ) || $item->language != '*') {
							self::$lookup [$language] [$view] [$item->query ['id']] = $item->id;
						}
					}
				}
			}
		}
		
		if ($needles) {
			foreach ( $needles as $view => $ids ) {
				if (isset ( self::$lookup [$language] [$view] )) {
					foreach ( $ids as $id ) {
						if (isset ( self::$lookup [$language] [$view] [( int ) $id] )) {
							return self::$lookup [$language] [$view] [( int ) $id];
						}
					}
				}
			}
		}
		
		// Check if the active menuitem matches the requested language
		$active = $menus->getActive ();
		
		if ($active && $active->component == 'com_content' && ($language == '*' || in_array ( $active->language, array (
				'*',
				$language 
		) ) || ! JLanguageMultilang::isEnabled ())) {
			return $active->id;
		}
		
		// If not found, return language specific home link
		$default = $menus->getDefault ( $language );
		
		return ! empty ( $default->id ) ? $default->id : null;
	}
	
	/**
	 * Translate an order code to a field for primary ordering.
	 *
	 * @param string $orderDate
	 *        	The ordering code.
	 *        	
	 * @return string The SQL field(s) to order by.
	 *        
	 * @since 3.3
	 */
	private static function getQueryDate($orderDate) {
		$db = JFactory::getDbo ();
		
		switch ($orderDate) {
			// Use created if modified is not set
			case 'modified' :
				$queryDate = ' CASE WHEN a.modified = ' . $db->quote ( $db->getNullDate () ) . ' THEN a.created ELSE a.modified END';
				break;
			
			// Use created if publish_up is not set
			case 'published' :
				$queryDate = ' CASE WHEN a.publish_up = ' . $db->quote ( $db->getNullDate () ) . ' THEN a.created ELSE a.publish_up END ';
				break;
			
			// Use created as default
			case 'created' :
			default :
				$queryDate = ' a.created ';
				break;
		}
		
		return $queryDate;
	}
	public static function getItemRoute($id, $alias, $itemId) {
		$needles = array (
				'item' => array (
						( int ) $id 
				) 
		);
		
		// Create the link
		// $link = 'index.php?option=com_jbsmartportfolio&view=item&id=' . $id;
		// $link .= '&catid=' . $catid;
		
		$link = 'index.php?option=com_jbsmartportfolio&view=item&id=' . $id . ':' . $alias . $itemId;
		/*
		 * if ((int) $catid > 1)
		 * {
		 * $categories = JCategories::getInstance('Jbsmartportfolio');
		 *
		 * $category = $categories->get((int) $catid);
		 *
		 * if ($category)
		 * {
		 * $needles['category'] = array_reverse($category->getPath());
		 * $needles['categories'] = $needles['category'];
		 *
		 * }
		 * }
		 *
		 * if ($language && $language != "*" && JLanguageMultilang::isEnabled())
		 * {
		 * $link .= '&lang=' . $language;
		 * $needles['language'] = $language;
		 * }
		 *
		 * if ($item = self::_findItem($needles))
		 * {
		 * $link .= '&Itemid=' . $item;
		 * }
		 */
		return $link;
	}
}
