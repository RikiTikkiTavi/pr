<?php
/**
 * @package     JB Smart Portfolio
 *
 * @copyright   Copyright (C) 2016 Joomlabuff. All rights reserved.
 * @license     GNU General Public License version 2 or later.
 */

defined('_JEXEC') or die;

/**
 * This class is required since Joomla will look for a file in helpers/ats.php with a class and method named
 * AtsHelper::addSubmenu
 */

class JbsmartportfolioHelper extends JHelperContent{

    public static function addSubmenu($vName){
    	if ($view = 'categories') {
    		$categories_view = 'index.php?option=com_categories&view=categories&extension=com_jbsmartportfolio';
    		$items_view = 'index.php?option=com_jbsmartportfolio&view=items';
    		$tegs_view = 'index.php?option=com_jbsmartportfolio&view=tags';

        	JHtmlSidebar::addEntry(JText::_('COM_JBSMARTPORTFOLIO_CATEGORIES'), $categories_view, $vName == 'categories');
        	JHtmlSidebar::addEntry(JText::_('COM_JBSMARTPORTFOLIO_TITLE_ITEMS'), $items_view, $vName == 'items');
        	JHtmlSidebar::addEntry(JText::_('COM_JBSMARTPORTFOLIO_TITLE_TAGS'), $tegs_view, $vName == 'tags');
    	}

    	
    }
    
    public static function popup( $url, $text, $options = array() )
    {
    
    	$params = JComponentHelper::getParams('com_jbsmartportfolio');
    	$html = "";
    
    	if(!empty($options['onclose']))
    	{
    		//	JHTML::_('behavior.modal', 'a.modal', array('onClose'=> $options['onclose']) );
    	}
    	else
    	{
    		if (!empty($options['update']))
    		{
    			//		    JHTML::_('behavior.modal', 'a.modal', array('onClose'=>'\function(){j2storeUpdate();}') );
    		}
    		else
    		{
    			//	    JHTML::_('behavior.modal', 'a.modal');
    		}
    	}
    
    	// set the $handler_string based on the user's browser
    	if(!empty($options['onclose'])) {
    		$handler_string = "{handler:'iframe',size:{x: window.innerWidth-80, y: window.innerHeight-80}, onClose: function(){}}";
    	} else {
    		$handler_string = "{handler:'iframe',size:{x: window.innerWidth-80, y: window.innerHeight-80}}";
    	}
    
    
    	if ( self::getBrowser() == 'ie')
    	{
    		// if IE, use
    		if(!empty($options['onclose'])) {
    			$handler_string = "{handler:'iframe',size:{x:document.documentElement.­clientWidth-80, y: document.documentElement.­clientHeight-80} onClose: function(){}}";
    		} else {
    			$handler_string = "{handler:'iframe',size:{x:document.documentElement.­clientWidth-80, y: document.documentElement.­clientHeight-80}}";
    		}
    	}
    
    	$handler = (!empty($options['img']))
    	? "{handler:'image'}"
    			: $handler_string;
    
    	$lightbox_width = $params->get('lightbox_width');
    	if(empty($options['width']) && !empty($lightbox_width))
    		$options['width'] = $lightbox_width;
    
    	if(!empty($options['width']))
    	{
    		if (empty($options['height']))
    			$options['height'] = 480;
    
    		$handler = "{handler: 'iframe', size: {x: ".$options['width'].", y: ".$options['height']. "}}";
    	}
    
    	$class = (!empty($options['class'])) ? $options['class'] : '';
    
    	$html	= "<a style='display:inline;position:relative;' class=\"modal\" href=\"$url\" rel=\"$handler\" >\n";
    	$html 	.= "<span class=\"".$class."\" >\n";
    	$html   .= "$text\n";
    	$html 	.= "</span>\n";
    	$html	.= "</a>\n";
    	return $html;
    }
    
    public static function getBrowser() {
    	if(preg_match('/(?i)msie [2-9]/',$_SERVER['HTTP_USER_AGENT']))
    	{
    		return 'ie';
    	}else {
    		return 'good';
    	}
    }
    
    
}