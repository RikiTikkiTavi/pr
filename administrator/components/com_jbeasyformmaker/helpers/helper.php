<?php
/**
 * @version    CVS: 1.0.0
 * @package    com_jbeasyformmaker
 * @author     Priya Bose <support@joomlabuff.com>
 * @copyright  2016 www.joomlabuff.com
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
defined ( '_JEXEC' ) or die ();

JLoader::register ( 'JHtmlString', JPATH_LIBRARIES . '/joomla/html/html/string.php' );
class JBHelper {
	public static function generateMeta($item) {
		$document = JFactory::getDocument ();
		$app = JFactory::getApplication ();
		$menus = $app->getMenu ();
		$menu = $menus->getActive ();
		$title = null;
		
		$document->setTitle ( $item->title );
		$document->addCustomTag ( '<meta content="website" property="og:type"/>' );
		$document->addCustomTag ( '<meta content="' . JURI::current () . '" property="og:url" />' );
		$document->setDescription ( JHtml::_ ( 'string.truncate', $item->description, 155, false, false ) );
		$document->addCustomTag ( '<meta content="' . $item->title . '" property="og:title" />' );
		$document->addCustomTag ( '<meta content="' . JURI::root () . $item->image . '" property="og:image" />' );
		$document->addCustomTag ( '<meta content="' . JHtml::_ ( 'string.truncate', $item->description, 155, false, false ) . '" property="og:description" />' );
		
		return true;
	}
	public static function parseShortCodes($item) {
		$regex = '/{.*?jbeasyformmaker}(.*?){\/jbeasyformmaker}/';
		preg_match_all ( $regex, $item->form_body, $newmatches, PREG_SET_ORDER );
		
		return $newmatches;
	}
	
	public static function Shortcodenames($item){		
		$newmatches = self::parseShortCodes($item); 
		
		$newTags = array();
		if (isset ( $newmatches [0] ) && count ( $newmatches [0] )) {
			foreach ( $newmatches as $newmatch ) {
		
				if (empty ( $newmatch [1] )) {
					break;
				}
		
				foreach ( $newmatch as $tthtml ) {
						
					$regex = '/{.*?jbeasyformmaker}(.*?){\/jbeasyformmaker}/';
					preg_match_all ( $regex, $item->form_body, $newmatches1, PREG_SET_ORDER );
						
					foreach ( $newmatches1 as $newmatch ) {
		
						if (empty ( $newmatch [1] )) {
							break;
						}
		
						$values = explode ( '|', $newmatch [1] );
						$values = array_filter ( $values ); 
						 
						
						
						
						// first value should always be the ID.
						if (isset ( $values [0] )) {
							$label = '';
							$attribute = '';
							$processed_body = '';
							foreach ( $values as $key => $fvalue ) {
								$attribute = $fvalue;								
								if (strpos ( $fvalue, 'tagname' ) !== false ) { 									 
									$labelv = str_replace ( 'tagname=', '', $fvalue );
									$label = str_replace ( '"', '', $labelv );
									$newTags[$label] = $label;									 
								}
							}
						}
					 
						
					}
				}
			}
		}
		
	 return $newTags;
	}
	
	/**
	 * Method to process shortcodes
	 * 
	 * @param object $item        	
	 */
	public static function Shortcode(&$item) {
		$newmatches = self::parseShortCodes ( $item );
		
		if (isset ( $newmatches [0] ) && count ( $newmatches [0] )) {
			foreach ( $newmatches as $newmatch ) {
				
				if (empty ( $newmatch [1] )) {
					break;
				}
				
				foreach ( $newmatch as $tthtml ) {
					
					$regex = '/{.*?jbeasyformmaker}(.*?){\/jbeasyformmaker}/';
					preg_match_all ( $regex, $item->form_body, $newmatches1, PREG_SET_ORDER );
					
					foreach ( $newmatches1 as $newmatch ) {
						
						if (empty ( $newmatch [1] )) {
							break;
						}
						
						$values = explode ( '|', $newmatch [1] );
						$values = array_filter ( $values );
						
						// first value should always be the ID.
						if (isset ( $values [0] )) {
							$label = '';
							$attribute = '';
							$processed_body = '';
							foreach ( $values as $key => $fvalue ) {
								$attribute = $fvalue;
								if (strpos ( $fvalue, 'label' ) !== false && ! preg_match ( '~\bsubmit\b~', $ftype ) && ! preg_match ( '~\bcaptcha\b~', $ftype )) {
									$label .= '<label class="form-label">';
									$labelv = str_replace ( 'label=', '', $fvalue );
									$label .= str_replace ( '"', '', $labelv );
									$label .= '</label>';
								}
								
								if (strpos ( $fvalue, 'type' ) !== false) {
									$tlabelv = str_replace ( 'type=', '', $fvalue );
									$ftype = str_replace ( '"', '', $tlabelv );
									if (preg_match ( '~\btext\b~', $ftype )) {
										$processed_body .= '<div class="controls">';
										$processed_body .= '<input ';
										$processed_body .= implode ( ' ', $values );
										$processed_body .= ' />';
										$processed_body .= '</div>';
									}
									
									if (preg_match ( '~\btextarea\b~', $ftype )) {
										$processed_body .= '<div class="controls">';
										$processed_body .= '<textarea ';
										$processed_body .= implode ( ' ', $values );
										$processed_body .= '  ></textarea>';
										$processed_body .= '</div>';
									}
									
									if (preg_match ( '~\bdate\b~', $ftype )) {
										$new_array = array ();
										foreach ( $values as $keys => $vs ) {
											$vs = str_replace ( '"', '', $vs );
											$vs = str_replace ( '"', '', $vs );
											$pieces = explode ( "=", $vs );
											$new_key = isset ( $pieces [0] ) && ! empty ( $pieces [0] ) ? $pieces [0] : '';
											$new_value = isset ( $pieces [1] ) && ! empty ( $pieces [1] ) ? $pieces [1] : '';
											$new_array [trim ( $new_key )] = $new_value;
										}
										
										$name = isset ( $new_array ['name'] ) ? trim($new_array ['name']) : '';
										$id = isset ( $new_array ['id'] ) ? $new_array ['id'] : '';
										$class = isset ( $new_array ['class'] ) ? $new_array ['class'] : '';
										$date_format = isset ( $new_array ['date'] ) ? $new_array ['date'] : '%Y-%m-%d';
										$processed_body .= '<div class="controls">';
										$processed_body .= JHTML::calendar ( date ( "Y-m-d" ), $name, $id, $date_format, array (
												'size' => '8',
												'maxlength' => '10',
												'class' => 'input-small validate[\'required\']' 
										) );
										$processed_body .= '</div>';
									}
									
									if (preg_match ( '~\bnumber\b~', $ftype )) {
										$processed_body .= '<div class="controls">';
										$processed_body .= '<input ';
										$processed_body .= implode ( ' ', $values );
										$processed_body .= ' />';
										$processed_body .= '</div>';
									}
									
									if (preg_match ( '~\blist\b~', $ftype )) {
										$vnew_array = array ();
										foreach ( $values as $keys => $vs ) {
											$vs = str_replace ( '"', '', $vs );
											$vs = str_replace ( '"', '', $vs );
											$pieces = explode ( "=", $vs );
											$new_key = isset ( $pieces [0] ) && ! empty ( $pieces [0] ) ? trim ( $pieces [0] ) : '';
											$new_value = isset ( $pieces [1] ) && ! empty ( $pieces [1] ) ? $pieces [1] : '';
											$new_key = str_replace ( '', '_', $new_key );
											$vnew_array [trim ( $new_key )] = $new_value;
										}
										
										$options = explode ( ',', $vnew_array ['option'] );
										$coptions = array ();
										foreach ( $options as $key => $opt ) {
											$poptions = explode ( ':', $opt );
											$okey = isset ( $poptions [0] ) ? $poptions [0] : '';
											$ovalue = isset ( $poptions [1] ) ? $poptions [1] : '';
											$coptions [trim ( $okey )] = $ovalue;
										}
										
										$name = isset ( $vnew_array ['name'] ) ? trim($vnew_array ['name']) : '';
										$id = isset ( $vnew_array ['id'] ) ? $vnew_array ['id'] : '';
										$class = isset ( $vnew_array ['class'] ) ? $vnew_array ['class'] : '';
										$processed_body .= JHTML::_ ( 'select.genericlist', $coptions, $name, 'class="input selectmenu"', 'value', 'text', '' );
									}
									
									if (preg_match ( '~\bradio\b~', $ftype )) {
										$vnew_array = array ();
										foreach ( $values as $keys => $vs ) {
											$vs = str_replace ( '"', '', $vs );
											$vs = str_replace ( '"', '', $vs );
											$pieces = explode ( "=", $vs );
											$new_key = isset ( $pieces [0] ) && ! empty ( $pieces [0] ) ? trim ( $pieces [0] ) : '';
											$new_value = isset ( $pieces [1] ) && ! empty ( $pieces [1] ) ? $pieces [1] : '';
											$new_key = str_replace ( '', '_', $new_key );
											$vnew_array [trim ( $new_key )] = $new_value;
										}
										
										$options = explode ( ',', $vnew_array ['option'] );
										$coptions = array ();
										foreach ( $options as $key => $opt ) {
											$poptions = explode ( ':', $opt );
											$okey = isset ( $poptions [0] ) ? $poptions [0] : '';
											$ovalue = isset ( $poptions [1] ) ? $poptions [1] : '';
											$coptions [] = JHTML::_ ( 'select.option', trim ( $okey ), $ovalue );
										}
										
										$name = isset ( $vnew_array ['name'] ) ? trim($vnew_array ['name']) : '';
										$id = isset ( $vnew_array ['id'] ) ? $vnew_array ['id'] : '';
										$class = isset ( $vnew_array ['class'] ) ? $vnew_array ['class'] : '';
										
										$processed_body .= JHTML::_ ( 'select.radiolist', $coptions, $name, 'class="inputbox" ', 'value', 'text', 'left', 'align' );
									}
									
									if (preg_match ( '~\bemail\b~', $ftype )) {
										$processed_body .= '<div class="controls">';
										$processed_body .= '<input ';
										$processed_body .= implode ( ' ', $values );
										$processed_body .= ' />';
										$processed_body .= '</div>';
									}
									
									if (preg_match ( '~\bphone\b~', $ftype )) {
										
										$values = str_replace ( 'type="phone"', 'type="text"', $values );
										$processed_body .= '<div class="controls">';
										$processed_body .= '<input ';
										$processed_body .= implode ( ' ', $values );
										$processed_body .= ' pattern="\d*"  title="Enter valid phone number"';
										$processed_body .= ' />';
										$processed_body .= '</div>';
									}
									
									if (preg_match ( '~\bcheckbox\b~', $ftype )) {
										$vnew_array = array ();
										foreach ( $values as $keys => $vs ) {
											$vs = str_replace ( '"', '', $vs );
											$vs = str_replace ( '"', '', $vs );
											$pieces = explode ( "=", $vs );
											$new_key = isset ( $pieces [0] ) && ! empty ( $pieces [0] ) ? trim ( $pieces [0] ) : '';
											$new_value = isset ( $pieces [1] ) && ! empty ( $pieces [1] ) ? $pieces [1] : '';
											$new_key = str_replace ( '', '_', $new_key );
											$vnew_array [trim ( $new_key )] = $new_value;
										}
										
										$options = explode ( ',', $vnew_array ['option'] );
										$ccoptions = array ();
										foreach ( $options as $key => $opt ) {
											$poptions = explode ( ':', $opt );
											$okey = isset ( $poptions [0] ) ? $poptions [0] : '';
											$ovalue = isset ( $poptions [1] ) ? $poptions [1] : '';
											$ccoptions [trim ( $okey )] = $ovalue;
										}
										
										$name = isset ( $vnew_array ['name'] ) ? trim($vnew_array ['name']) : '';
										$id = isset ( $vnew_array ['id'] ) ? $vnew_array ['id'] : '';
										$class = isset ( $vnew_array ['class'] ) ? $vnew_array ['class'] : '';
										$processed_body .= '<div class="controls">';
										$processed_body .= self::checkboxlist ( $ccoptions, $name, array (
												'class' => $class . 'inline',
												'id' => $id 
										) );
										$processed_body .= '</div>';
									}
									
									if (preg_match ( '~\bsubmit\b~', $ftype )) {
										if ($item->show_captcha) {
											
										if ($item->show_captcha) {
											if (! $rez = self::captcha ( 'onDisplay' )) {
												$processed_body .= JText::_ ( 'JB_EASYFORMMAKER_CAPTCHA_ERROR_SETUP' );
											} else {
												$params = new JRegistry(JPluginHelper::getPlugin('captcha', 'recaptcha')->params); 
												$processed_body .='<div class="g-recaptcha" data-sitekey="'.$params->get('public_key', '').'"></div>';
												$processed_body .= $rez [0];
											}
										}
									 
										}
									 
										$processed_body .= '<div class="controls">';
										$processed_body .= '<input ';
										$processed_body .= implode ( ' ', $values );
										$processed_body .= '  />';
										$processed_body .= '</div>';
									}
								 
								}
							}
						}
					}
				}
				$html = '';
				$html .= $label . $processed_body;
				$item->form_body = str_replace ( $newmatch [0], $html, $item->form_body );
			}
		}
	}
	
	/**
	 * Display captcha
	 *
	 * @param string $event        	
	 * @param string $value        	
	 */
	public static function captcha($event, $value = '') {
		// import the captcha plugin
		JPluginHelper::importPlugin ( 'captcha', JFactory::getConfig ()->get ( 'captcha' ) );
		$dispatcher = JEventDispatcher::getInstance ();
		 	
		if ($event == 'onDisplay') {
			$dispatcher->trigger ( 'onInit', 'capt' );
			//print_r($dispatcher);
			return $dispatcher->trigger ( 'onDisplay', array (
					'capt',
					'capt',
					'' 
			) );
		}
		if ($event == 'onCheckAnswer') {
			return $dispatcher->trigger ( 'onCheckAnswer', $value );
		}
	}
	
	/* custom checkbox */
	public static function checkboxlist($arr, $name, $attribs = null, $key = 'value', $text = 'text', $selected = null, $idtag = false, $translate = false) {
		reset ( $arr );
		$html = '';
		
		if (is_array ( $attribs )) {
			$attribs = JArrayHelper::toString ( $attribs );
		}
		
		$id_text = $name;
		if ($idtag) {
			$id_text = $idtag;
		}
		
		foreach ( $arr as $key => $t ) {
			$extra = '';
			$id = $key;
			$html .= "\n\t<label for=\"$id_text$key\">";
			$html .= "\n\t<input type=\"checkbox\" name=\"$name\" id=\"$id_text$key\" value=\"" . $key . "\"$extra $attribs />";
			$html .= $t . "</label>";
		}
		
		$html .= "\n";
		return $html;
	}
}
