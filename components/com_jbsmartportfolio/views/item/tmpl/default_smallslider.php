<?php 

?> 
<div class="row">
	<div class="col-md-4"> 
		 <?php if($this->item->item_type =='slideshow'):?>
			 <?php $images = JbsmartportfolioHelper::getItemImages($this->item);?>
			<span class="owl-carousel-img owl-carousel owl-theme nav-inside m-none" data-plugin-options='{"items": 1, "margin": 10, "animateOut": "fadeOut", "autoplay": true, "autoplayTimeout": 3000}'>
			<?php foreach($images as $img):?>
				<span>
				<img class="jb-smartportfolio-img img-responsive" src="<?php echo $img;?>" alt="<?php echo $item->title; ?>" />
				</span>
			<?php endforeach;?>
			</span>
		 <?php elseif($this->item->video): ?>
			 <div class="jb-smartportfolio-embed">
				<iframe src="<?php echo $this->video_src; ?>" frameborder="0" 	webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>
			</div>	
		<?php else:?>
			<span class="img-thumbnail">
				<img class="jb-smartportfolio-img img-responsive" src="<?php echo $this->item->image; ?>" alt="<?php echo $this->item->title; ?>" />
			</span>			
		<?php endif;?> 
	</div>
	<div class="col-md-8">
 		<?php echo $this->loadTemplate('portfolioinfo');?> 		
	 </div>
	 
</div>
<?php echo $this->loadTemplate('relateditems');?>		