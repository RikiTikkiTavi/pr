<?php
/**
 * @version    2.7.x
 * @package    K2
 * @author     JoomlaWorks http://www.joomlaworks.net
 * @copyright  Copyright (c) 2006 - 2016 JoomlaWorks Ltd. All rights reserved.
 * @license    GNU/GPL license: http://www.gnu.org/copyleft/gpl.html
 */

// no direct access
defined('_JEXEC') or die;

// Define default image size (do not change)
K2HelperUtilities::setDefaultImage($this->item, 'itemlist', $this->params);

?>

<!-- Start K2 Item Layout -->
<div class="catItemView group<?php echo ucfirst($this->item->itemGroup); ?><?php echo ($this->item->featured) ? ' catItemIsFeatured' : ''; ?><?php if($this->item->params->get('pageclass_sfx')) echo ' '.$this->item->params->get('pageclass_sfx'); ?>">

	<!-- Plugins: BeforeDisplay -->
	<?php echo $this->item->event->BeforeDisplay; ?>

	<!-- K2 Plugins: K2BeforeDisplay -->
	<?php echo $this->item->event->K2BeforeDisplay; ?>
	
	<!-- Blog starts -->
	<div class="blog-posts recent-posts">
		<div class="post post-large">
			<!-- Post image starts -->
			<div class="post-image">
				<div class="img-thumbnail">
					  <!-- Item Image -->
						<a href="<?php echo $this->item->link; ?>" title="<?php if(!empty($this->item->image_caption)) echo K2HelperUtilities::cleanHtml($this->item->image_caption); else echo K2HelperUtilities::cleanHtml($this->item->title); ?>">
							<img class="img-responsive" src="<?php echo $this->item->image; ?>" alt="<?php if(!empty($this->item->image_caption)) echo K2HelperUtilities::cleanHtml($this->item->image_caption); else echo K2HelperUtilities::cleanHtml($this->item->title); ?>" style="width:<?php echo $this->item->imageWidth; ?>px; height:auto;" />
						</a>
				</div>
			</div>
			<!-- Post image Ends -->
			<!-- Post date starts -->
				 <?php if($this->item->params->get('catItemDateCreated')): ?>
					<div class="date">
					  <span class="day"> <?php echo JHTML::_('date', $this->item->created, JText::_('d')); ?><br></span>
					  <span class="month"> <?php echo JHTML::_('date', $this->item->created, JText::_('M')); ?>   </span>
					</div>
				 <?php endif; ?>
			<!-- Post date Ends -->
			<!-- Post Content starts -->
			<div class="post-content">
			  <?php if($this->item->params->get('catItemTitle')): ?>
			  <!-- Item title -->
			  <h2 class="catItemTitle">

				<?php if ($this->item->params->get('catItemTitleLinked')): ?>
					<a href="<?php echo $this->item->link; ?>">
					<?php echo $this->item->title; ?>
				</a>
				<?php else: ?>
				<?php echo $this->item->title; ?>
				<?php endif; ?>

				<?php if($this->item->params->get('catItemFeaturedNotice') && $this->item->featured): ?>
				<!-- Featured flag -->
				<span>
					<sup>
						<?php echo JText::_('K2_FEATURED'); ?>
					</sup>
				</span>
				<?php endif; ?>
			  </h2>
			  <?php endif; ?>
			  
			   <?php if($this->item->params->get('catItemIntroText')): ?>
				  <!-- Item introtext -->
				  <div class="catItemIntroText">
					<?php echo $this->item->introtext; ?>
				  </div>
				<?php endif; ?>
			  
				<div class="post-meta">
					
						<?php if($this->item->params->get('catItemAuthor')): ?>
						<!-- Item Author -->
						<span class="catItemAuthor">
							<i class="fa fa-user"></i>
							<?php if(isset($this->item->author->link) && $this->item->author->link): ?>
							<a rel="author" href="<?php echo $this->item->author->link; ?>"><?php echo $this->item->author->name; ?></a>
							<?php else: ?>
							<i class="fa fa-user"></i>
							<?php echo $this->item->author->name; ?>
							<?php endif; ?>
						</span>
						<?php endif; ?>
					
					
					
						<?php if($this->item->params->get('catItemCategory')): ?>
							<!-- Item category name -->
							<span class="catItemCategory">
								<i class="fa fa-tag"></i>
								<a href="<?php echo $this->item->category->link; ?>"><?php echo $this->item->category->name; ?></a>
							</span>
						<?php endif; ?>
					
					
					
					
						<?php if($this->item->params->get('catItemCommentsAnchor') && ( ($this->item->params->get('comments') == '2' && !$this->user->guest) || ($this->item->params->get('comments') == '1')) ): ?>
						<!-- Anchor link to comments below -->
						<span class="catItemCommentsLink">
							<i class="fa fa-comments"></i>
							<?php if(!empty($this->item->event->K2CommentsCounter)): ?>
								<!-- K2 Plugins: K2CommentsCounter -->
								<?php echo $this->item->event->K2CommentsCounter; ?>
							<?php else: ?>
								<?php if($this->item->numOfComments > 0): ?>
								<a href="<?php echo $this->item->link; ?>#itemCommentsAnchor">
									<?php echo $this->item->numOfComments; ?> <?php echo ($this->item->numOfComments>1) ? JText::_('K2_COMMENTS') : JText::_('K2_COMMENT'); ?>
								</a>
								<?php else: ?>
								<a href="<?php echo $this->item->link; ?>#itemCommentsAnchor">
									<?php echo JText::_('K2_BE_THE_FIRST_TO_COMMENT'); ?>
								</a>
								<?php endif; ?>
							<?php endif; ?>
						</span>
						<?php endif; ?>
					
						<?php if ($this->item->params->get('catItemReadMore')): ?>
						<!-- Item "read more..." link -->
						<span class="catItemReadMore">
							<a class="btn btn-xs btn-primary pull-right" href="<?php echo $this->item->link; ?>">
								<?php echo JText::_('K2_READ_MORE'); ?>
							</a>
						</span>
						<?php endif; ?>

					
				</div>
			</div>
			<!-- Post Content Ends -->
		</div>
	</div>
	<!-- Blog starts -->
								
	
	

	
 
  <!-- Plugins: AfterDisplay -->
  <?php echo $this->item->event->AfterDisplay; ?>

  <!-- K2 Plugins: K2AfterDisplay -->
  <?php echo $this->item->event->K2AfterDisplay; ?>

	<div class="clr"></div>
</div>
<!-- End K2 Item Layout -->
