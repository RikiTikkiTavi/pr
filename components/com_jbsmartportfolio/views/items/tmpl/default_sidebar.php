<?php
?>
<div class="container-removed">
	<div class="row">
		<div class="col-sm-4 col-md-3">
			<aside class="t3-sidebar jb-smartportfolio-filter">
				<h4 class="heading-primary"><strong>Filter</strong> By</h4>
					<ul class="nav nav-list mb-xlg sort-source" data-sort-id="portfolio" data-option-key="filter" data-plugin-options='{"layoutMode": "fitRows", "filter": "*"}'>
						<li data-option-value="*" class="active">
							<a href="#">
								<?php echo JText::_('COM_JBSMARTPORTFOLIO_SHOW_ALL'); ?>
							</a>
						</li>
						<?php $filters = JbsmartportfolioHelper::getTagList( $this->items ); ?>				
						<?php foreach ($filters as $filter): ?>
						<li data-option-value=".<?php echo $filter->alias; ?>" data-group="<?php echo $filter->alias; ?>">
							<a href="#">
								<?php echo $filter->title; ?>
							</a>
						</li>
						<?php endforeach;?>						
					</ul>
			</aside>
		</div>
		<div class="col-sm-8 col-md-9">
			<div class="row">
				<div class="sort-destination-loader1 sort-destination-loader-showing">
					<ul class="portfolio-list sort-destination jb-smartportfolio-items" data-sort-id="portfolio">
						<?php  foreach($this->items as $item):?>
						<?php
							$item->category_title = JbsmartportfolioHelper::getCategoryTitle($item->category_id); 
							$tags = JbsmartportfolioHelper::getTags ( $item->jbsmartportfolio_tag_id );
							$newtags = array ();
							$filter = '';
							$groups = array ();
							foreach ( $tags as $tag ) {
								$newtags [] = $tag->title;
								$filter .= ' ' . $tag->alias;
								$groups [] .= '"' . $tag->alias . '"';
							}
							$groups = implode ( ',', $groups );
							$item->url = JRoute::_ ( 'index.php?option=com_jbsmartportfolio&view=item&id=' . $item->jbsmartportfolio_item_id . ':' . $item->alias . $this->itemId );
							?>
									
						<li class="col-md-4 jb-smartportfolio-item isotope-item" data-groups='[<?php echo $groups; ?>]'>
							<div class="portfolio-item">
								<a href="<?php echo $item->url;?>">
									<span class="thumb-info thumb-info-lighten">
										<span class="thumb-info-wrapper">											
											<img class="jb-smartportfolio-img img-responsive" src="<?php echo JURI::root() . $item->image;?>" alt="<?php echo $item->title; ?>" />
												<span class="thumb-info-title">
													<span class="thumb-info-inner"><?php echo $item->title;?></span>
													<span class="thumb-info-type"><?php echo $item->category_title;?></span>
												</span>
												<span class="thumb-info-action">
													<span class="thumb-info-action-icon"><i class="fa fa-link"></i></span>
												</span>
											</span>
										</span>
									</a>
								</div>
							</li>
						<?php endforeach;?>
						</ul>
					</div>
				</div>	
			</div>
	</div>
</div>

 
