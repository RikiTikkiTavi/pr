<?php
/**
 * @package     JB Smart Portfolio
 *
 * @copyright   Copyright (C) 2016 Joomlabuff. All rights reserved.
 * @license     GNU General Public License version 2 or later.
 */

defined('_JEXEC') or die();

JHtml::_('formbehavior.chosen', 'select');

//echo $this->getRenderedForm();

?>
 
<script type="text/javascript">
	/**
	  * when product title of the
	  * Method
	  */
	function jSelectItem(item_id ,item_title ,field_id){

		field_id ='jform_jbspitem_list_table';
		var form = jQuery("#adminForm");
		var html ='';
		if(form.find('#'+field_id+ '  #item-row-'+item_id).length == 0){
			html +='<tr id="item-row-'+item_id +'"><td><input type="hidden" name="related_items['+item_id+']" value='+item_id+' />'+item_title +'</td><td><button class="btn btn-danger" onclick="jQuery(this).closest(\'tr\').remove();"><i class="icon icon-trash"></button></td></tr>';
			
	form.find("#"+field_id).append(html);
			alert('<?php echo JText::_('ITEM_ADDED');?>');
		}else{
			html ='';
			alert('<?php echo JText::_('ITEM_ADDED_ALREADY_EXISTS');?>');
		}
		
		//form.find("#"+field_id).append(html);
	}
</script>
<style type="text/css">
.control-label > #item_type_ss{
	display:none;
}
</style>
<div class="smartportfolio_items">

<form id="adminForm" class="form-horizontal form-validate" name="adminForm" method="post" action="index.php">
	<input type="hidden" value="com_jbsmartportfolio" name="option">
	<input type="hidden" value="item" name="view">
	<input type="hidden" id="task" value="" name="task">
	<input type="hidden" value="<?php echo $this->item->jbsmartportfolio_item_id;?>" name="jbsmartportfolio_item_id">
	<?php echo JHtml::_('form.token'); ?>
	
	
	<?php
        $fieldsets = $this->form->getFieldsets();
        $shortcode = $this->form->getValue('text');
        $tab_count = 0;
        foreach ($fieldsets as $key => $attr):
        
      /*   	 if($key =='relateditems'):
        	 		echo $this->loadTemplate('relateditems'); 
            	elseif($key =='skills'):
        			 echo $this->loadTemplate('skills');
        	else:
        	  */
            if ( $tab_count == 0 )
	            {
	                echo JHtml::_('bootstrap.startTabSet', 'configuration', array('active' => $attr->name));
	            }
	            echo JHtml::_('bootstrap.addTab', 'configuration', $attr->name, JText::_($attr->label, true));
	            ?>
	         
	            <div class="row-fluid">
	                <div class="span12">
	                    <?php
	                    $layout = '';
	                    $style = '';
	                    $fields = $this->form->getFieldset($attr->name);
	                    
	                    foreach ($fields as $key => $field):	                                        	
	                    ?>
	                        <div class="control-group <?php echo $layout; ?>" <?php echo $style; ?>>
	                            <div class="control-label"><?php echo $field->label; ?></div>
	                            
	                             	<div class="controls"><?php echo $field->input; ?>
	                            	<br />
	                            	<small class="muted"><?php echo JText::_($field->description); ?></small>
	                            	
	                           <?php if($key == 'related_items'):?>
	                            <br/>
	                            <div class="row-fluid">
	                            	<div class="span6">
	                                	<div class="table-responsive">	                                		
		                            		<table class="table table-striped table-condensed" id="jform_jbspitem_list_table">
			          		                	<tbody>
			            		                	<?php if(!empty($this->item->related_items)):?>
			            		                	<tr>
			            		                		<td colspan="3">
			            		                			<a class="btn btn-danger" href="javascript:void(0);"
			            		                				onclick="jQuery('.jbsp-item-list-tr').remove();">
			            		                				<?php echo JText::_('DELETE_ALL');?>

			            		                			<i class="icon icon-cancel"></i></a>

			            		                		</td>

			            		                	</tr>
			    		                        	<?php // $product_ids = explode(',',$this->item->related_items);
			    		                        		$i =1;
			    		                        	?>
													<?php foreach($this->item->related_items as  $pid):?>
													<?php $product = F0FModel::getTmpInstance('Items','JbsmartportfolioModel')->getItem($pid);?>
													<tr class="jbsp-item-list-tr" id="item-row-<?php echo $pid?>">
														<td><input type="hidden" name="related_items[<?php echo $pid;?>]" value='<?php echo $pid;?>' /><?php echo $product->title;?></td>
														<td><a class="btn btn-small" href="javascript:void(0);" onclick="jQuery(this).closest('tr').remove();"><i class="icon icon-cancel"></i></a></td>
													</tr>
													<?php
													$i++;
													endforeach;?>
													</tbody>
													<?php endif;?>
												</table>
											</div>
	                            		</div>
		                            	<div class="span6">
		                            <!--                   		<div class="alert alert-success">
		                            			<?php // echo JText::_('JBSMARTPORTFOLIO');?>
		                            		</div>
		                            	</div> -->
	            	                </div>
							<?php endif;?>
							
	                            	
	                          </div>
	                        </div>
	                    <?php
	                    endforeach
	                    ?>
	                </div>
	            </div>
	             <?php
	            echo JHtml::_('bootstrap.endTab');
	            $tab_count++;
	            
       endforeach;
       
      
        ?>
        
        
        
         
	
</form>
</div>
