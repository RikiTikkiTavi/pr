<?php
/**
 * @package     JB Smart Portfolio
 *
 * @copyright   Copyright (C) 2016 Joomlabuff. All rights reserved.
 * @license     GNU General Public License version 2 or later.
 */

defined('_JEXEC') or die();

class JbsmartportfolioToolbar extends F0FToolbar{

	function onBrowse(){
		JToolBarHelper::preferences('com_jbsmartportfolio');

		$categories_view = 'index.php?option=com_categories&view=categories&extension=com_jbsmartportfolio';
		JHtmlSidebar::addEntry(JText::_('COM_JBSMARTPORTFOLIO_CATEGORIES'), $categories_view, 'categories');

		parent::onBrowse();
	}
}